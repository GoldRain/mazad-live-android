package com.mazadlive.service

import android.app.IntentService
import android.content.Intent
import android.os.Environment
import android.util.Log
import com.downloader.Constants
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.downloader.Status
import com.mazadlive.Interface.ProgressListener
import com.mazadlive.helper.Constant
import com.mazadlive.models.StoryModel

class ImageDownloadService : IntentService("ImageDownloadService") {

    //controlled with local id
    companion object {
        var progressListeners = HashMap<String, ProgressListener>()
        var downloadQueue = ArrayList<StoryModel>()
        var downloadingTasks = ArrayList<StoryModel>()

        fun containsTask(messageid: String): Boolean{
            for (i in 0 until downloadQueue.size){

                if (downloadQueue[i].id==messageid)
                    return true
            }
            for (i in 0 until downloadingTasks.size){

                if (downloadingTasks[i].id==messageid)
                    return true
            }
            return false
        }

        fun addProgressListener(messageid: String, progressListener: ProgressListener){
            progressListeners[messageid] = progressListener
        }

        fun cancelTask(messageid: String){
            for (i in 0 until downloadQueue.size){
                if (downloadQueue[i].id==messageid) {
                    downloadQueue.removeAt(i)
                    if (progressListeners.containsKey(messageid)){
                        progressListeners[messageid]?.onCancel()
                    }
                    break
                }
            }

            for (i in 0 until downloadingTasks.size){
                if (downloadingTasks[i].id==messageid) {
                    val downloadId = downloadingTasks[i].downloadId
                    downloadingTasks.removeAt(i)
                    if (progressListeners.containsKey(messageid)){
                        progressListeners[messageid]?.onCancel()
                    }
                    progressListeners.remove(messageid)
                    PRDownloader.pause(downloadId)
                    break
                }
            }

        }
    }

    override fun onHandleIntent(intent: Intent?) {
        if (intent != null) {
         //   val messageid = intent.getStringExtra("message_id")
            val story = intent.getParcelableExtra<StoryModel>("story")

            Log.e("DOWNLOAD","** ${story.id}  ${story.url}")
       /*     val message = TableChatMessage.getMessageFromid(applicationContext,MySQLiteHelper(applicationContext).readableDatabase,messageid)
                    ?: return*/

            for (i in 0 until downloadQueue.size){
                if (downloadQueue[i].id==story.id)
                    return
            }

            for (i in 0 until downloadingTasks.size){
                if (downloadingTasks[i].id==story.id)
                    return
            }

            if (downloadingTasks.size>1){

                downloadQueue.add(story)
            }else{

                downloadingTasks.add(story)
                downloadImage(story)
            }
        }
    }

    private fun downloadImage(message: StoryModel?) {
        //TODO nullpointer exception
        if (Status.RUNNING == PRDownloader.getStatus(message!!.downloadId)) {
            PRDownloader.pause(message.downloadId)
            return
        }

        if (Status.PAUSED == PRDownloader.getStatus(message.downloadId)) {
            PRDownloader.resume(message.downloadId)
            return;
        }


        val filePath =  "${Constant.STORY_PATH}IMG_${message.id}.jpg"
        val fileDir = Constant.STORY_PATH

        Log.e("DOWNLOAD","** ${message.id}  ${message.url}")
        message.downloadId = PRDownloader.download(message.url, fileDir, "IMG_${message.id}.jpg")
                .build()
                .setOnStartOrResumeListener {

                }
                .setOnPauseListener {
                    if (progressListeners.containsKey(message.id)){
                        progressListeners[message.id]?.onCancel()
                    }
                    for (i in 0 until downloadQueue.size){
                        if (downloadQueue[i].id==message.id){
                            downloadQueue.removeAt(i)
                            break
                        }
                    }
                    for (i in 0 until downloadingTasks.size){
                        if (downloadingTasks[i].id==message.id) {
                            downloadingTasks.removeAt(i)
                            break
                        }
                    }
                    progressListeners.remove(message.id)
                    startNextDownload()
                }
                .setOnCancelListener {
                    if (progressListeners.containsKey(message.id)){
                        progressListeners[message.id]?.onCancel()
                    }
                    for (i in 0 until downloadQueue.size){
                        if (downloadQueue[i].id==message.id){
                            downloadQueue.removeAt(i)
                            break
                        }
                    }
                    for (i in 0 until downloadingTasks.size){
                        if (downloadingTasks[i].id==message.id) {
                            downloadingTasks.removeAt(i)
                            break
                        }
                    }
                    progressListeners.remove(message.id)
                    startNextDownload()
                }
                .setOnProgressListener {
                    val progressPercent = it.currentBytes * 100 / it.totalBytes
                    val progress = progressPercent.toInt()
                    if (progressListeners.containsKey(message.id)){
                        Log.e("ImageDownloadService","$progress **")
                        progressListeners[message.id]?.onProgressUpdate(progress)
                    }
                }
                .start(object : OnDownloadListener {
                    override fun onError(error: com.downloader.Error?) {
                        Log.e("ImageDownloadService"," onError ${error!!.isServerError}  ${error!!.isConnectionError}**")
                        if (progressListeners.containsKey(message.id)){
                            progressListeners[message.id]?.onCancel()
                        }
                        for (i in 0 until downloadQueue.size){
                            if (downloadQueue[i].id==message.id){
                                downloadQueue.removeAt(i)
                                break
                            }
                        }
                        for (i in 0 until downloadingTasks.size){
                            if (downloadingTasks[i].id==message.id) {
                                downloadingTasks.removeAt(i)
                                break
                            }
                        }
                        progressListeners.remove(message.id)
                        startNextDownload()
                    }

                    override fun onDownloadComplete() {
                        if (progressListeners.containsKey(message.id)){
                            progressListeners[message.id]?.onSuccess(filePath)
                        }
                        for (i in 0 until downloadQueue.size){
                            if (downloadQueue[i].id==message.id){
                                downloadQueue.removeAt(i)
                                break
                            }
                        }
                        for (i in 0 until downloadingTasks.size){
                            if (downloadingTasks[i].id==message.id) {
                                downloadingTasks.removeAt(i)
                                break
                            }
                        }
                        progressListeners.remove(message.id)
                        startNextDownload()
                    }

                })

    }

    private fun startNextDownload() {
        if (downloadQueue.size>0){
            val message = downloadQueue[0]
            downloadQueue.removeAt(0)
            downloadingTasks.add(message)
            downloadImage(message)
        }
    }

}
