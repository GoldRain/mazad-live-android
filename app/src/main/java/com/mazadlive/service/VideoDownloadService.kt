package com.mazadlive.service

import android.app.IntentService
import android.content.Intent
import android.os.Environment
import android.util.Log
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.downloader.Status
import com.mazadlive.Interface.ProgressListener
import com.mazadlive.models.StoryModel
import org.jetbrains.anko.doAsync


class VideoDownloadService : IntentService("VideoDownloadService") {

    //controlled with local id
    companion object {
        var progressListeners = HashMap<String, ProgressListener>()
        var downloadQueue = ArrayList<StoryModel>()
        var downloadingTasks = ArrayList<StoryModel>()
        fun containsTask(messageLocalId: String): Boolean {
            for (i in 0 until downloadQueue.size) {
                if (downloadQueue[i].id == messageLocalId)
                    return true
            }
            for (i in 0 until downloadingTasks.size) {
                if (downloadingTasks[i].id == messageLocalId)
                    return true
            }
            return false
        }

        fun addProgressListener(messageLocalId: String, progressListener: ProgressListener) {
            progressListeners[messageLocalId] = progressListener
        }

        fun cancelTask(messageLocalId: String) {
            for (i in 0 until downloadQueue.size) {
                if (downloadQueue[i].id == messageLocalId) {
                    downloadQueue.removeAt(i)
                    if (progressListeners.containsKey(messageLocalId)) {
                        progressListeners[messageLocalId]?.onCancel()
                    }
                    break
                }

            }

            for (i in 0 until downloadingTasks.size) {
                if (downloadingTasks[i].id == messageLocalId) {
                    val downloadId = downloadingTasks[i].downloadId
                    downloadingTasks.removeAt(i)
                    if (progressListeners.containsKey(messageLocalId)) {
                        progressListeners[messageLocalId]?.onCancel()
                    }
                    progressListeners.remove(messageLocalId)
                    PRDownloader.pause(downloadId)
                }
                break
            }

        }

    }

    override fun onHandleIntent(intent: Intent?) {
        if (intent != null) {
            val story = intent.getParcelableExtra<StoryModel>("story")

            Log.e("Downloading", "CheckPoint 2 ")
            for (i in 0 until downloadQueue.size) {
                if (downloadQueue[i].id == story.id)
                    return
            }
            Log.e("Downloading", "CheckPoint 3 ")
            for (i in 0 until downloadingTasks.size) {
                if (downloadingTasks[i].id == story.id)
                    return
            }
            Log.e("Downloading", "CheckPoint 4 ")
            if (downloadingTasks.size > 1) {
                Log.e("Downloading", "CheckPoint 5 ")
                downloadQueue.add(story)
            } else {
                Log.e("Downloading", "CheckPoint 6 ")
                downloadingTasks.add(story)
                downloadVideo(story)
            }


        }
    }
    fun downloadVideo(storyModel: StoryModel?) {
        //TODO nullpointer exception
        if (Status.RUNNING == PRDownloader.getStatus(storyModel!!.downloadId)) {
            PRDownloader.pause(storyModel.downloadId)
            return
        }

        if (Status.PAUSED == PRDownloader.getStatus(storyModel.downloadId)) {
            PRDownloader.resume(storyModel.downloadId)
            return;
        }
        var exe = storyModel.url
        val j = exe.lastIndexOf(".")
        if (j >= 0) {
            exe = exe.substring(j)
        } else {
            exe = ".mp4"
        }
        val filePath = Environment.getExternalStorageDirectory().toString() + "/TecConnect/Media/videos/" + "VID_" + storyModel.createdAt + exe
        val fileDir = Environment.getExternalStorageDirectory().toString() + "/TecConnect/Media/videos/"

        storyModel.downloadId = PRDownloader.download(storyModel.url.replace(" ", "%20"), fileDir, "VID_" + storyModel.createdAt + exe)
                .build()
                .setOnStartOrResumeListener {

                }
                .setOnPauseListener {
                    if (progressListeners.containsKey(storyModel.id)) {
                        progressListeners[storyModel.id]?.onCancel()
                    }
                    for (i in 0 until downloadQueue.size) {
                        if (downloadQueue[i].id == storyModel.id) {
                            downloadQueue.removeAt(i)
                            break
                        }
                    }
                    for (i in 0 until downloadingTasks.size) {
                        if (downloadingTasks[i].id == storyModel.id) {
                            downloadingTasks.removeAt(i)
                            break
                        }
                    }
                    progressListeners.remove(storyModel.id)
                    startNextDownload()
                }
                .setOnCancelListener {
                    if (progressListeners.containsKey(storyModel.id)) {
                        progressListeners[storyModel.id]?.onCancel()
                    }
                    for (i in 0 until downloadQueue.size) {
                        if (downloadQueue[i].id == storyModel.id) {
                            downloadQueue.removeAt(i)
                            break
                        }
                    }
                    for (i in 0 until downloadingTasks.size) {
                        if (downloadingTasks[i].id == storyModel.id) {
                            downloadingTasks.removeAt(i)
                            break
                        }
                    }
                    progressListeners.remove(storyModel.id)
                    startNextDownload()
                }
                .setOnProgressListener {
                    val progressPercent = it.currentBytes * 100 / it.totalBytes
                    val progress = progressPercent.toInt()
                    if (progressListeners.containsKey(storyModel.id)) {
                        progressListeners[storyModel.id]?.onProgressUpdate(progress)
                    }
                }
                .start(object : OnDownloadListener {
                    override fun onDownloadComplete() {
                        //create thumbnail
                        doAsync {

                            Log.e("Downloading", "Path: $filePath")
                           /* try {
                                val bitmap = ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Video.Thumbnails.MINI_KIND)
                            //    val file = ConstantFunctions.getVideoThumbFileFromCreatedAt(message.createdAt)
                                val fOut = FileOutputStream(file)
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
                                fOut.flush()
                                fOut.close()
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }*/
                         //   TableChatMessage.savePath(message.messageId, filePath, MySQLiteHelper(applicationContext).writableDatabase)
                            if (progressListeners.containsKey(storyModel.id)) {
                                progressListeners[storyModel.id]?.onSuccess(filePath)
                            }
                            for (i in 0 until downloadQueue.size) {
                                if (downloadQueue[i].id == storyModel.id) {
                                    downloadQueue.removeAt(i)
                                    break
                                }
                            }
                            for (i in 0 until downloadingTasks.size) {
                                if (downloadingTasks[i].id == storyModel.id) {
                                    downloadingTasks.removeAt(i)
                                    break
                                }
                            }
                            progressListeners.remove(storyModel.id)
                            startNextDownload()
                        }
                    }

                    override fun onError(error: Error?) {
                        if (progressListeners.containsKey(storyModel.id)) {
                            progressListeners[storyModel.id]?.onCancel()
                        }
                        for (i in 0 until downloadQueue.size) {
                            if (downloadQueue[i].id == storyModel.id) {
                                downloadQueue.removeAt(i)
                                break
                            }
                        }
                        for (i in 0 until downloadingTasks.size) {
                            if (downloadingTasks[i].id == storyModel.id) {
                                downloadingTasks.removeAt(i)
                                break
                            }
                        }
                        progressListeners.remove(storyModel.id)
                        startNextDownload()
                    }
                })

    }

    private fun startNextDownload() {
        if (downloadQueue.size > 0) {
            val message = downloadQueue[0]
            downloadQueue.removeAt(0)
            downloadingTasks.add(message)
            downloadVideo(message)
        }
    }
}