package com.mazadlive.service

import android.app.IntentService
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.models.UserModel
import com.mazadlive.parser.UserParser
import com.mazadlive.utils.MyApplication
import org.json.JSONObject

/**
 * Created by bodacious on 25/6/19.
 */
class FetchAllUsersByNumber: IntentService("fetch_all_users") {

    var hashMapUsers = MyApplication.instance.usersHasMap

    override fun onHandleIntent(intent: Intent?) {

        Log.e("phoneNumbers","service is called")

        fetchAllUsers()
    }

    private fun fetchAllUsers() {

        val param = JSONObject()
        param.put("version","1.1")
        param.put("text","")
        param.put("user_id", MyApplication.instance.getUserPreferences().id)
        hashMapUsers.clear()

        ServiceRequest(object : ApiResponseListener {

            override fun onCompleted(`object`: Any) {
                // fragmentView.progress_bar.visibility = View.GONE

                val jsonObject = JSONObject(`object`.toString())
                if(jsonObject.has("description")) {

                    val array = jsonObject.getJSONArray("description")

                    val parser = UserParser()

                    for (i in 0 until array.length()) {

                        val obj = array.getJSONObject(i)

                        val userModel = parser.parse(obj)

                        Log.e("phoneNumbers","phone numbers are: ${userModel.phone}")

                        hashMapUsers.put(userModel.phone, userModel)
                    }

                }
            }

            override fun onError(errorMessage: String) {
                //   fragmentView.progress_bar.visibility = View.GONE
              //  Toast.makeText(this@FetchAllUsersByNumber!!, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).searchUser(param)
    }


}