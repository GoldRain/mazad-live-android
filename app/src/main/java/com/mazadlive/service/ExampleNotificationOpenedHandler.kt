package com.mazadlive.service

import android.content.Intent
import android.util.Log
import com.mazadlive.activities.HomeActivity
import com.mazadlive.utils.MyApplication
import com.onesignal.OSNotificationAction
import com.onesignal.OSNotificationOpenResult
import com.onesignal.OneSignal

/**
 * Created by bodacious on 28/12/18.
 */
class ExampleNotificationOpenedHandler : OneSignal.NotificationOpenedHandler {
    // This fires when a notification is opened by tapping on it.
    override fun notificationOpened(result: OSNotificationOpenResult) {
        val actionType = result.action.type
        val data = result.notification.payload.additionalData
        val customKey: String?
        var openURL: String? = null
        var activityToLaunch: Any = HomeActivity::class.java
        if (data != null) {
            customKey = data.optString("customkey", null)
            openURL = data.optString("openURL", null)
            if (customKey != null)
                Log.e("OneSignalExample", "customkey set with value: " + customKey)
            if (openURL != null)
                Log.e("OneSignalExample", "openURL to webview with URL value: " + openURL)
        }

        if (actionType == OSNotificationAction.ActionType.ActionTaken) {
            Log.e("OneSignalExample", "Button pressed with id: " + result.action.actionID)
            if (result.action.actionID == "id1") {
                Log.e("OneSignalExample", "button id called: " + result.action.actionID)
                activityToLaunch = HomeActivity::class.java
            } else {
                Log.e("OneSignalExample", "button id called: " + result.action.actionID)
            }

        }
        val intent = Intent(MyApplication.instance, activityToLaunch as Class<*>)
        intent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK
        intent.putExtra("openURL", openURL)
        MyApplication.instance.startActivity(intent)

    }
}