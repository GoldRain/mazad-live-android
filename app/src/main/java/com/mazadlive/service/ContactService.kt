package com.mazadlive.service

import android.app.IntentService
import android.content.Intent
import android.provider.ContactsContract
import android.util.Log
import com.mazadlive.database.AppDatabase
import com.mazadlive.database.ContactData
import org.jetbrains.anko.doAsync
import android.support.v4.app.ActivityCompat.startActivityForResult
import android.support.v4.content.ContextCompat
import android.text.TextUtils


/**
 * Created by bodacious on 11/12/18.
 */
class ContactService : IntentService("conatct_service") {

    override fun onHandleIntent(intent: Intent?) {
        Log.e("CONATCT_", "con:::: ************")

        readAllContacts()

    }

    private fun readAllContacts() {

        val cMap = HashMap<String, ContactData>()
        doAsync {
            val cr = contentResolver
            val cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC")

            if (cur?.count ?: 0 > 0) {
                while (cur != null && cur.moveToNext()) {

                    val id = cur.getString(
                            cur.getColumnIndex(ContactsContract.Contacts._ID))

                    val name = cur.getString(cur.getColumnIndex(
                            ContactsContract.Contacts.DISPLAY_NAME))

                    var uri = cur.getString(
                            cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI))

                    Log.e("photo","photo uri is: ${uri}")

                    if (cur.getInt(cur.getColumnIndex(
                                    ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                        val pCur = cr.query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                arrayOf(id), null)
                        while (pCur!!.moveToNext()) {
                            var phoneNo = pCur.getString(pCur.getColumnIndex(
                                    ContactsContract.CommonDataKinds.Phone.NUMBER))
                            val model = ContactData()
                            phoneNo = phoneNo.replace(" ".toRegex(), "")
                            phoneNo = phoneNo.replace("(", "")
                            phoneNo = phoneNo.replace(")", "")
                            phoneNo = phoneNo.replace("-".toRegex(), "")
                            if (phoneNo.length < 10)
                                continue
                            if (phoneNo.length > 10)
                                phoneNo = phoneNo.substring(phoneNo.length - 10)
                            model.phone = phoneNo
                            model.username = name

                            if(uri != null) {
                                model.photo = uri
                            }else{

                                uri = ""
                                model.photo = uri
                            }

                            if (!cMap.containsKey(phoneNo)) {
                                model.id = phoneNo
                                try {
                                    AppDatabase.getAppDatabase().contactDao().insert(model)
                                }catch (e:Exception){
                                    e.printStackTrace()
                                }
                            }
                            cMap.put(phoneNo, model)

                        }
                        pCur.close()
                    }
                }
            }
            cur?.close()
        }
    }

}


//1  {"error":false,"stream":{"user_id":"5c0619c93bf6c11c4f1b60e6","stream_name":"nk_1544524961090","status":"running","viewerCount":0,"country":[],"_id":"5c0f94a217bda64a76d71852","comment":[],"__v":0},"userDetails":{"followers":[{"user_id":"5c06183c71bbd11bde6d6fc1","created_at":"Wed Dec 05 2018 10:26:57 GMT+0530 (IST)"}],"following":[{"user_id":"5c06183c71bbd11bde6d6fc1","created_at":"Tue Dec 04 2018 18:11:52 GMT+0530 (IST)"}],"username":"nk","country_code":"+973","phone":"7891201111","email":"nk3@gmail.com","password":"password","type":"buyer","profilePhoto":"https:\/\/s3-ap-southeast-1.amazonaws.com\/datapost\/image\/IMG_1543903688643.jpg","description":"","_id":"5c0619c93bf6c11c4f1b60e6","__v":0,"is_online":false,"socket_id":"","is_live":"running","favPost":[{"_id":"5c0a67742a8d2a3c620b9b2c","post_id":"5c0a5cfe07be2938de836e29"}]}}
// 2 join  {"error":false,"stream":{"user_id":"5c0619c93bf6c11c4f1b60e6","stream_name":"nk_1544524961090","status":"running","viewerCount":1,"thumbUrl":"","country":[],"_id":"5c0f94a217bda64a76d71852","comment":[],"__v":0},"joined_user_id":{"followers":[{"user_id":"5c06183c71bbd11bde6d6fc1","created_at":"Wed Dec 05 2018 10:26:57 GMT+0530 (IST)"}],"following":[{"user_id":"5c06183c71bbd11bde6d6fc1","created_at":"Tue Dec 04 2018 18:11:52 GMT+0530 (IST)"}],"username":"nk","country_code":"+973","phone":"7891201111","email":"nk3@gmail.com","password":"password","type":"buyer","profilePhoto":"https:\/\/s3-ap-southeast-1.amazonaws.com\/datapost\/image\/IMG_1543903688643.jpg","description":"","_id":"5c0619c93bf6c11c4f1b60e6","__v":0,"is_online":true,"socket_id":"rSuzUYgXOMbA5UZfAAAD","is_live":"running","favPost":[{"_id":"5c0a67742a8d2a3c620b9b2c","post_id":"5c0a5cfe07be2938de836e29"}]},"userDetails":{"followers":[{"user_id":"5c06183c71bbd11bde6d6fc1","created_at":"Wed Dec 05 2018 10:26:57 GMT+0530 (IST)"}],"following":[{"user_id":"5c06183c71bbd11bde6d6fc1","created_at":"Tue Dec 04 2018 18:11:52 GMT+0530 (IST)"}],"username":"nk","country_code":"+973","phone":"7891201111","email":"nk3@gmail.com","password":"password","type":"buyer","profilePhoto":"https:\/\/s3-ap-southeast-1.amazonaws.com\/datapost\/image\/IMG_1543903688643.jpg","description":"","_id":"5c0619c93bf6c11c4f1b60e6","__v":0,"is_online":true,"socket_id":"rSuzUYgXOMbA5UZfAAAD","is_live":"running","favPost":[{"_id":"5c0a67742a8d2a3c620b9b2c","post_id":"5c0a5cfe07be2938de836e29"}]}}
// thumb  {"error":false,"thumbUrl":"https:\/\/s3-ap-southeast-1.amazonaws.com\/datapost\/image\/IMG_1544524966617.jpg","stream":{"user_id":"5c0619c93bf6c11c4f1b60e6","stream_name":"nk_1544524961090","status":"running","viewerCount":1,"thumbUrl":"","country":[],"_id":"5c0f94a217bda64a76d71852","comment":[],"__v":0},"userDetails":{"followers":[{"user_id":"5c06183c71bbd11bde6d6fc1","created_at":"Wed Dec 05 2018 10:26:57 GMT+0530 (IST)"}],"following":[{"user_id":"5c06183c71bbd11bde6d6fc1","created_at":"Tue Dec 04 2018 18:11:52 GMT+0530 (IST)"}],"username":"nk","country_code":"+973","phone":"7891201111","email":"nk3@gmail.com","password":"password","type":"buyer","profilePhoto":"https:\/\/s3-ap-southeast-1.amazonaws.com\/datapost\/image\/IMG_1543903688643.jpg","description":"","_id":"5c0619c93bf6c11c4f1b60e6","__v":0,"is_online":true,"socket_id":"rSuzUYgXOMbA5UZfAAAD","is_live":"running","favPost":[{"_id":"5c0a67742a8d2a3c620b9b2c","post_id":"5c0a5cfe07be2938de836e29"}]},"message":"Done"}
// leave {"error":false,"stream":{"user_id":"5c0619c93bf6c11c4f1b60e6","stream_name":"nk_1544524961090","status":"running","viewerCount":0,"thumbUrl":"https:\/\/s3-ap-southeast-1.amazonaws.com\/datapost\/image\/IMG_1544524966617.jpg","country":[],"_id":"5c0f94a217bda64a76d71852","comment":[],"__v":0},"joined_user_id":{"followers":[{"user_id":"5c06183c71bbd11bde6d6fc1","created_at":"Wed Dec 05 2018 10:26:57 GMT+0530 (IST)"}],"following":[{"user_id":"5c06183c71bbd11bde6d6fc1","created_at":"Tue Dec 04 2018 18:11:52 GMT+0530 (IST)"}],"username":"nk","country_code":"+973","phone":"7891201111","email":"nk3@gmail.com","password":"password","type":"buyer","profilePhoto":"https:\/\/s3-ap-southeast-1.amazonaws.com\/datapost\/image\/IMG_1543903688643.jpg","description":"","_id":"5c0619c93bf6c11c4f1b60e6","__v":0,"is_online":true,"socket_id":"rSuzUYgXOMbA5UZfAAAD","is_live":"running","favPost":[{"_id":"5c0a67742a8d2a3c620b9b2c","post_id":"5c0a5cfe07be2938de836e29"}]},"userDetails":{"followers":[{"user_id":"5c06183c71bbd11bde6d6fc1","created_at":"Wed Dec 05 2018 10:26:57 GMT+0530 (IST)"}],"following":[{"user_id":"5c06183c71bbd11bde6d6fc1","created_at":"Tue Dec 04 2018 18:11:52 GMT+0530 (IST)"}],"username":"nk","country_code":"+973","phone":"7891201111","email":"nk3@gmail.com","password":"password","type":"buyer","profilePhoto":"https:\/\/s3-ap-southeast-1.amazonaws.com\/datapost\/image\/IMG_1543903688643.jpg","description":"","_id":"5c0619c93bf6c11c4f1b60e6","__v":0,"is_online":true,"socket_id":"rSuzUYgXOMbA5UZfAAAD","is_live":"running","favPost":[{"_id":"5c0a67742a8d2a3c620b9b2c","post_id":"5c0a5cfe07be2938de836e29"}]}}


//com {"error":false,"message":"Done","stream":{"_id":"5c0f94fc17bda64a76d71853","stream_name":"nk_1544525051531","user_id":[{"_id":"5c0619c93bf6c11c4f1b60e6","username":"nk","country_code":"+973","phone":"7891201111","email":"nk3@gmail.com","password":"password","type":"buyer","profilePhoto":"https:\/\/s3-ap-southeast-1.amazonaws.com\/datapost\/image\/IMG_1543903688643.jpg","__v":0,"followers":[{"user_id":"5c06183c71bbd11bde6d6fc1","created_at":"Wed Dec 05 2018 10:26:57 GMT+0530 (IST)"}],"following":[{"user_id":"5c06183c71bbd11bde6d6fc1","created_at":"Tue Dec 04 2018 18:11:52 GMT+0530 (IST)"}],"is_online":true,"socket_id":"rSuzUYgXOMbA5UZfAAAD","is_live":"running","favPost":[{"_id":"5c0a67742a8d2a3c620b9b2c","post_id":"5c0a5cfe07be2938de836e29"}]}],"status":"running","viewerCount":1,"country":[],"commenet":{"_id":"5c0f950f17bda64a76d71855","user_id":[{"_id":"5c06183c71bbd11bde6d6fc1","username":"shee_jinping","country_code":"+973","phone":"7891201189","email":"nk@gmail.com","password":"password","type":"buyer","profilePhoto":"https:\/\/s3-ap-southeast-1.amazonaws.com\/datapost\/image\/IMG_1544518010183.jpg","__v":0,"followers":[{"user_id":"5c0619c93bf6c11c4f1b60e6","created_at":"Tue Dec 04 2018 18:11:52 GMT+0530 (IST)"}],"following":[{"user_id":"5c0619c93bf6c11c4f1b60e6","created_at":"Wed Dec 05 2018 10:26:57 GMT+0530 (IST)"}],"is_online":true,"socket_id":"GrvSVz-1UqoF9t-4AAAJ","is_live":"running","favPost":[{"_id":"5c0a6cd15d20994426d6d237","post_id":"5c0a427d8381cd26504f585c"}],"description":"hhh"}],"comment":"hhhhhh"},"thumbUrl":"https:\/\/s3-ap-southeast-1.amazonaws.com\/datapost\/image\/IMG_1544525056951.jpg"},"userDetails":{"_id":"5c0619c93bf6c11c4f1b60e6","username":"nk","country_code":"+973","phone":"7891201111","email":"nk3@gmail.com","password":"password","type":"buyer","profilePhoto":"https:\/\/s3-ap-southeast-1.amazonaws.com\/datapost\/image\/IMG_1543903688643.jpg","__v":0,"followers":[{"user_id":"5c06183c71bbd11bde6d6fc1","created_at":"Wed Dec 05 2018 10:26:57 GMT+0530 (IST)"}],"following":[{"user_id":"5c06183c71bbd11bde6d6fc1","created_at":"Tue Dec 04 2018 18:11:52 GMT+0530 (IST)"}],"is_online":true,"socket_id":"rSuzUYgXOMbA5UZfAAAD","is_live":"running","favPost":[{"_id":"5c0a67742a8d2a3c620b9b2c","post_id":"5c0a5cfe07be2938de836e29"}]}}
