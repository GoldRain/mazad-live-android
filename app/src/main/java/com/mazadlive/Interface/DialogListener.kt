package com.mazadlive.Interface

abstract class DialogListener {

    open fun noClick(){}
    open fun yesClick(){}
    open fun okClick(){}
    open fun okClick(any: Any){}
}