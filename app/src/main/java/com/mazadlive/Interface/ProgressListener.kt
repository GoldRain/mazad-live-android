package com.mazadlive.Interface

/**
 * Created by bodacious on 29/12/17.
 */

interface ProgressListener {
    fun onProgressUpdate(progress: Int)
    fun onSuccess(path: String)
    fun onFileNotFound()
    fun onCancel()
}
