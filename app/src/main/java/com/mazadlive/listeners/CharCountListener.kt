package com.mazadlive.listeners

import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.widget.EditText
import android.widget.TextView

/**
 * Created by bodacious on 10/1/19.
 */
class CharCountListener(val editText:EditText,val textView: TextView,val totalChar:Int):TextWatcher {
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

    }

    override fun afterTextChanged(s: Editable?) {
        if(TextUtils.isEmpty(editText.text.trim())){
            textView.text = ("${totalChar-(editText.text.trim().length)}/$totalChar")
        }else{
            textView.text = ("$totalChar/$totalChar")
        }
    }
}