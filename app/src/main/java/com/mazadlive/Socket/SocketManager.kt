package com.mazadlive.Socket

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.util.Log
import com.mazadlive.Notification.NotificationUtils
import com.mazadlive.activities.SignInActivity
import com.mazadlive.database.AppDatabase
import com.mazadlive.helper.Constant
import com.mazadlive.models.SoldModel
import com.mazadlive.parser.MessageParser
import com.mazadlive.parser.StremDataParse
import com.mazadlive.utils.MyApplication
import io.socket.client.Socket
import io.socket.emitter.Emitter
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.doAsync
import org.json.JSONObject

/**
 * Created by bodacious on 11/12/17.
 */

class SocketManager(private val context: Context, var socket: Socket?) {

    private val userId: String? = null
    private val playerId: String? = null
    private var continueFlag = true
    private var fromService = false

    val START_SREAM = "liveNow"
    val STOP_STREAM = "updateStream"
    val UPDATE_STORY = "updateStory"
    val UPDATE_VOTE = "updateVote"
    val MESSAGE_RECEIVED = "messageReceived"
    val UPDATE_PRODUCT = "updateProduct"
    val LOGOUT = "logout"
    val PRODUCT_SOLD = "sold"


    var socketEventsListener: SocketEventsListener? = null
        private set


    private val onLogout = Emitter.Listener { args ->
        Log.e("Socket", "onLogout ${args[0].toString()}")

        if (MyApplication.instance.getUserPreferences().loginStatus) {
            logoutFromApp()
            if (socketEventsListener != null) {
                socketEventsListener!!.onLogout(args[0].toString())
            }
        }
    }

    private fun logoutFromApp() {

        MyApplication.instance.getUserPreferences().loginStatus = false
        val intent = Intent(MyApplication.instance, SignInActivity::class.java)
        intent.putExtra("showDialog", true)
        MyApplication.instance.startActivity(intent)
    }

    private val onConnected = Emitter.Listener { args ->


        val curTime = System.currentTimeMillis()

        Log.e("Socket", "Connected " + curTime + " " + connectedTime + " " + (curTime - connectedTime))
        if (curTime - connectedTime < 2000) {
            connectedTime = curTime
            return@Listener
        }
        connectedTime = curTime
        // login(MyApplication.instance.prefManager.id, MyApplication.instance.prefManager.playerId)
        if (socketEventsListener != null)
            socketEventsListener!!.onConnected(*args)

    }

    private val onDisconnected = Emitter.Listener { args ->
        Log.e("Socket", "DisConnected")

        socket!!.close()
        if (socketEventsListener != null)
            socketEventsListener!!.onDisconnected(*args)

        if (MyApplication.instance.getUserPreferences().loginStatus) {
            if (!MyApplication.instance.isPause) {
                // socket!!.open()
                // connect()
            }
        }

    }


    private val onStartStream = Emitter.Listener { args ->
        Log.e("Socket", "$socketEventsListener onStartStream ${args[0]}")
        if (socketEventsListener != null) {
            socketEventsListener!!.onStartStream(args[0].toString())
        }
    }

    private val onUpdateStream = Emitter.Listener { args ->
        Log.e("Socket", "$socketEventsListener  onUpdateStream ${args[0]} ")

        val jsonObject = JSONObject(args[0].toString())
        if (!jsonObject.getBoolean("error")) {
            if (jsonObject.has("stream")) {
                val liveStoryModel = StremDataParse().parseStream2(jsonObject.getJSONObject("stream"))
                Log.e("Socket", "CHECK_FOR_ID_ ${liveStoryModel.id} ** ${Constant.CURRENT_STREAM_ID} **  ${liveStoryModel.id.equals(Constant.CURRENT_STREAM_ID)}  onUpdateStream")

                if (TextUtils.isEmpty(Constant.CURRENT_STREAM_ID)) {
                    if (socketEventsListener != null) {
                        socketEventsListener!!.onUpdateStream(args[0].toString())
                    }
                } else if (liveStoryModel.id.equals(Constant.CURRENT_STREAM_ID)) {
                    if (socketEventsListener != null) {
                        socketEventsListener!!.onUpdateStream(args[0].toString())
                    }
                }
            } else {
                if (socketEventsListener != null) {
                    socketEventsListener!!.onUpdateStream(args[0].toString())
                }
            }
        }
    }

    val updateProduct = Emitter.Listener { args ->
        Log.e("Socket", "$socketEventsListener  updateProduct ${args[0]} ")
        val jsonObject = JSONObject(args[0].toString())
        if (!jsonObject.getBoolean("error")) {
            if (jsonObject.has("stream")) {
                val liveStoryModel = StremDataParse().parseStream2(jsonObject.getJSONObject("stream"))
                Log.e("Socket", "CHECK_FOR_ID_ ${liveStoryModel.id} ** ${Constant.CURRENT_STREAM_ID} **  ${liveStoryModel.id.equals(Constant.CURRENT_STREAM_ID)}  updateProduct")

                if (TextUtils.isEmpty(Constant.CURRENT_STREAM_ID)) {
                    if (socketEventsListener != null) {
                        socketEventsListener!!.onUpdateProduct(args[0].toString())
                    }
                } else if (liveStoryModel.id.equals(Constant.CURRENT_STREAM_ID)) {
                    if (socketEventsListener != null) {
                        socketEventsListener!!.onUpdateProduct(args[0].toString())
                    }
                }
            } else {
                if (socketEventsListener != null) {
                    socketEventsListener!!.onUpdateProduct(args[0].toString())
                }
            }
        }
    }

    private val updateStory = Emitter.Listener { args ->
        Log.e("Socket", "$socketEventsListener updateStory ${args[0]} ")

        if (socketEventsListener != null) {
            socketEventsListener!!.onUpdateStory(args[0].toString())
        }
    }

    val updateVote = Emitter.Listener { args ->
        if (socketEventsListener != null) {
            socketEventsListener!!.onUpdateVote(args[0].toString())
        }
    }


    val messageReceived = Emitter.Listener { args ->
        Log.e("Socket", "$socketEventsListener messageReceived ${args[0]} ")

        val jsonObject = JSONObject(args[0].toString())
        val msg = MessageParser().parse(jsonObject)

        msg.message_send = 1
        doAsync {
            AppDatabase.getAppDatabase().messageDao().insert(msg)
            val room = AppDatabase.getAppDatabase().roomDao().getRoom(msg.room_id)
            var title = "MazadLive"
            if (room != null) {
                room.messageData = msg
                AppDatabase.getAppDatabase().roomDao().update(room)
                title = "${room.userList!![0].username} sent you message"
            }
            Log.e("Socket", "${Constant.IS_CHAT_ACTIVE} messageReceived ${Constant.CURRENT_ROOM_ID} ")
            if (!TextUtils.isEmpty(Constant.CURRENT_ROOM_ID)) {
                if (!Constant.CURRENT_ROOM_ID.equals(msg.room_id)) {

                    NotificationUtils.showNotification(context, title, msg.content, msg.room_id, 1, null)
                }
            } else {
                if (!Constant.IS_CHAT_ACTIVE) {
                    NotificationUtils.showNotification(context, title, msg.content, msg.room_id, 1, null)
                }
            }

            if (socketEventsListener != null) {
                socketEventsListener!!.onMessageReceived(args[0].toString())
            }
        }
    }

    val productSold = Emitter.Listener {
        val soldObj = JSONObject(it[0].toString())
        Log.e("Socket", "Product SOLD : $soldObj")
        if (soldObj.has("type") && soldObj.has("type_id")) {
            val soldModel = SoldModel()
            soldModel.type = soldObj.getString("type")
            soldModel.type_id = soldObj.getString("type_id")
            EventBus.getDefault().post(soldModel)
        }
    }

    val isConnected: Boolean
        get() = socket!!.connected()


    fun connect() {
        Log.e("Socket", "Connecting")
        socket!!.on(Socket.EVENT_CONNECT, onConnected)
        socket!!.on(Socket.EVENT_DISCONNECT, onDisconnected)
        socket!!.on(START_SREAM, onStartStream)
        socket!!.on(STOP_STREAM, onUpdateStream)
        socket!!.on(UPDATE_STORY, updateStory)
        socket!!.on(UPDATE_VOTE, updateVote)
        socket!!.on(MESSAGE_RECEIVED, messageReceived)
        socket!!.on(LOGOUT, onLogout)
        socket!!.on(UPDATE_PRODUCT, updateProduct)
        socket!!.on(PRODUCT_SOLD, productSold)

        socket!!.connect()
    }

    fun disConnect() {
        socket!!.off(Socket.EVENT_CONNECT)
        socket!!.off(Socket.EVENT_DISCONNECT)
        socket!!.off(START_SREAM, onStartStream)
        socket!!.off(STOP_STREAM, onUpdateStream)
        socket!!.off(UPDATE_STORY, updateStory)
        socket!!.off(UPDATE_VOTE, updateVote)
        socket!!.off(MESSAGE_RECEIVED, messageReceived)
        socket!!.off(LOGOUT, onLogout)
        socket!!.off(UPDATE_PRODUCT, updateProduct)
        socket!!.off(PRODUCT_SOLD, productSold)

        socket!!.disconnect()
    }

    fun setSocketEventsListener(fromService: Boolean, socketEventsListener: SocketEventsListener?) {
        //Log.e("Socket", "setSocketEventsListener")
        this.fromService = fromService
        this.socketEventsListener = socketEventsListener
    }

    companion object {
        val EMIT_CREATE_STREAM = "createStream"
        var EMIT_STOP_STREAM = "stopStream"
        var EMIT_UPDATE_ONLINE = "updateOnlineStatus"//updateOnlineStatus
        var EMIT_JOIN_LEAVE_STREAM = "joinLeaveStream"
        var EMIT_BOOKMARK_COMMENT = "bookmarkComment"
        var EMIT_STORY_POST = "storyPost"
        val EMIT_ADD_COMMENT = "addComment"

        private var connectedTime: Long = 0
        private var userInfoTime: Long = 0
        private var roomDataTime: Long = 0
        private var oldMessageTime: Long = 0
    }

}
