package com.mazadlive.Socket;


/**
 * Created by bodacious on 14/12/17.
 */

public abstract class SocketEventsListener {
    public  void onConnected(final Object... args){}
    public  void onDisconnected(final Object... args){}
    public  void onStartStream(final String args){}
    public  void onUpdateStream(final String args){}
    public  void onUpdateStory(final String args){}
    public  void onUpdateVote(final String args){}
    public  void onMessageReceived(final String args){}
    public  void onLogout(final String args){}
    public  void onUpdateProduct(final String args){}

}
