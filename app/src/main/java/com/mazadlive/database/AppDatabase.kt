package com.mazadlive.database
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import com.mazadlive.utils.MyApplication

/**
 * {Created by bodacious on 28/9/18.}
 */
@Database(entities = [(FavPost::class),(ContactData::class),(RoomData::class),(MessageData::class),(RoomUserData::class)],
        version = 2, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun favPostDao(): FavPostDao
    abstract fun contactDao(): ContactDao
    abstract fun roomDao(): RoomDao
    abstract fun messageDao(): MessageDao
    abstract fun roomUserDao(): RoomUserDao

    companion object {
        @Volatile private var INSTANCE: AppDatabase? = null

        fun getAppDatabase(): AppDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(MyApplication.instance, AppDatabase::class.java, "mazad_live_database")
                        .fallbackToDestructiveMigration()
                        .build()
            }
            return INSTANCE as AppDatabase
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

}