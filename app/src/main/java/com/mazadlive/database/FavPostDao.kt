package com.mazadlive.database

import android.arch.persistence.room.*

@Dao
interface FavPostDao {
    /*@Query("SELECT * FROM Users WHERE userId LIKE :userId")
    fun getFriend(userId:String): List<Users>*/ //packagesInfo

    @Query("SELECT * FROM favPost ")
    fun getAllPackage(): List<FavPost>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(favPost: FavPost)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(users: List<FavPost>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(users: FavPost)

    @Query("SELECT * FROM favPost WHERE post_id LIKE :post_id")
    fun getFavPost(post_id: String): FavPost

    @Query("DELETE from favPost")
    fun deleteAllFavPost()

    @Query("DELETE from favPost WHERE post_id LIKE :post_id")
    fun deleteFavPost(post_id: String)

    @Query("SELECT COUNT(*) from favPost")
    fun getTotalFavPostCount(): Int
}