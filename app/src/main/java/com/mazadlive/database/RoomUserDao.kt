package com.mazadlive.database

import android.arch.persistence.room.*

@Dao
interface RoomUserDao {
    /*@Query("SELECT * FROM Users WHERE userId LIKE :userId")
    fun getFriend(userId:String): List<Users>*/ //packagesInfo

    @Query("SELECT * FROM roomUser WHERE room_id LIKE :room_id")
    fun getAllRoomUser(room_id:String): List<RoomUserData>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(roomUserData: RoomUserData)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(roomUserData:List<RoomUserData>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(roomUserData: RoomUserData)

    @Query("SELECT * FROM roomUser WHERE user_id LIKE :user_id")
    fun getRoomUser(user_id: String): RoomUserData

    @Query("SELECT * FROM roomUser WHERE room_id LIKE :room_id")
    fun getUserFromRoom(room_id: String): RoomUserData

    @Query("SELECT COUNT(*) FROM roomUser")
    fun getTotalRoomUsers(): Int

    @Query("DELETE FROM roomUser")
    fun deleteAllRoomUser()
}