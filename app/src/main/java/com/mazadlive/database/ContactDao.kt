package com.mazadlive.database

import android.arch.persistence.room.*

@Dao
interface ContactDao {
    /*@Query("SELECT * FROM Users WHERE userId LIKE :userId")
    fun getFriend(userId:String): List<Users>*/ //packagesInfo

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(contactData: ContactData)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(users: List<ContactData>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(users: ContactData)

    @Query("SELECT * FROM contact")
    fun getAllContact(): List<ContactData>

    @Query("DELETE from contact WHERE phone LIKE :phone")
    fun deleteContact(phone: String)

    @Query("SELECT COUNT(*) from contact")
    fun getTotalCount(): Int
}