package com.mazadlive.database
import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "favPost")
class StoryData {
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    var id:String = ""

    @ColumnInfo(name = "path")
    var url:String = ""

    @ColumnInfo(name = "user_id")
    var path:String = ""

    @ColumnInfo(name = "status")
    var status:String = ""

    @ColumnInfo(name = "viewerCounts")
    var viewerCounts:Int = 0

    @ColumnInfo(name = "user_id")
    var createdAt:String = ""

    @ColumnInfo(name = "buyNowPrice")
    var buyNowPrice:String = ""

    @ColumnInfo(name = "is_buyNow")
    var is_buyNow:Boolean = false

    @ColumnInfo(name = "downloadId")
    var downloadId:String = ""

}


/*
    var id  = ""
    var url = ""
    var path = ""
    var user_id = ""
    var streamName = ""
    var status = ""
    var viewerCounts = 0
     var downloadId = -1
    var thumbUrl = ""
    var is_buyNow = false
    var buyNowPrice = ""
     var createdAt = ""
    var userData:UserModel?= null
    var commentsList = ArrayList<CommentModel>()
    var imagesList = ArrayList<String>()*/