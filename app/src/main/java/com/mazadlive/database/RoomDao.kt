package com.mazadlive.database

import android.arch.persistence.room.*

@Dao
interface RoomDao {
    /*@Query("SELECT * FROM Users WHERE userId LIKE :userId")
    fun getFriend(userId:String): List<Users>*/ //packagesInfo

    @Query("SELECT * FROM rooms ")
    fun getAllRooms(): List<RoomData>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(roomData: RoomData)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(roomData:List<RoomData>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(roomData: RoomData)

    @Query("SELECT * FROM rooms WHERE id LIKE :id")
    fun getRoom(id: String): RoomData

    @Query("SELECT COUNT(*) from rooms")
    fun getTotalRooms(): Int

    @Query("DELETE FROM rooms")
    fun deleteAllRooms()
}