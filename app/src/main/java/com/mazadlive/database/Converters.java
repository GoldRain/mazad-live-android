package com.mazadlive.database;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

/**
 * Created by bodacious on 26/12/18.
 */

public class Converters {
    static Gson gson = new Gson();

    @TypeConverter
    public static List<RoomUserData> stringToUserList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<RoomUserData>>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String userListToString(List<RoomUserData> ingredients) {
        return gson.toJson(ingredients);
    }

    @TypeConverter
    public static MessageData stringToMessageDataList(String data) {
        if (data == null) {
            return new MessageData();
        }

        Type listType = new TypeToken<MessageData>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String messageDataToString(MessageData steps) {
        return gson.toJson(steps);
    }
}
