package com.mazadlive.database
import android.arch.persistence.room.*

@Entity(tableName = "rooms")
class RoomData {
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    var id:String = ""

    @ColumnInfo(name = "local_id")
    var local_id:String = ""

    @ColumnInfo(name = "users")
    var users:String = ""

    @ColumnInfo(name = "created_at")
    var created_at:String = ""

    @TypeConverters(Converters::class)
    var userList:List<RoomUserData>?= ArrayList<RoomUserData>()

    @TypeConverters(Converters::class)
    var messageData:MessageData?= null

    /*@ColumnInfo(name = "created_at")
    var created_at:String = ""

    @ColumnInfo(name = "created_at")
    var updated_at:String = ""

    @ColumnInfo(name = "created_at")
    var created_at:String = ""

    @ColumnInfo(name = "created_at")
    var created_at:String = ""*/


}


/* var id :String?= null
    var name :String?= null
    var packagePrice :String?= null
    var packageType :String?= null
    var description :String?= null
    var status :String?= null
    var createdAt :String?= null
    var updatedAt :String?= null
    var amountPayable :String?= null
    var modelId :String?= null
    var brandId :String?= null
    var isExpand:Boolean = false*/