package com.mazadlive.database
import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "messages")
class MessageData {
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    var id:String = ""

    @ColumnInfo(name = "local_id")
    var local_id:String = ""

    @ColumnInfo(name = "room_id")
    var room_id:String = ""

    @ColumnInfo(name = "content_type")
    var content_type:String = ""

    @ColumnInfo(name = "sender_id")
    var sender_id:String = ""

    @ColumnInfo(name = "content")
    var content:String = ""

    @ColumnInfo(name = "created_at")
    var created_at:Long = 0

    @ColumnInfo(name = "message_send")
    var message_send:Long = 0

    @ColumnInfo(name = "message_read")
    var message_read:Int = 0

}


/* var id :String?= null
    var name :String?= null
    var packagePrice :String?= null
    var packageType :String?= null
    var description :String?= null
    var status :String?= null
    var createdAt :String?= null
    var updatedAt :String?= null
    var amountPayable :String?= null
    var modelId :String?= null
    var brandId :String?= null
    var isExpand:Boolean = false*/