package com.mazadlive.database
import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "contact")
class ContactData {
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    var id:String = ""

    @ColumnInfo(name = "username")
    var username:String = ""

    @ColumnInfo(name = "phone")
    var phone:String = ""

    @ColumnInfo(name = "email")
    var email:String = ""

    @ColumnInfo(name = "photo")
    var photo:String = ""
}


/* var id :String?= null
    var name :String?= null
    var packagePrice :String?= null
    var packageType :String?= null
    var description :String?= null
    var status :String?= null
    var createdAt :String?= null
    var updatedAt :String?= null
    var amountPayable :String?= null
    var modelId :String?= null
    var brandId :String?= null
    var isExpand:Boolean = false*/