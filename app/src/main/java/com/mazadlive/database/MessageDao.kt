package com.mazadlive.database

import android.arch.persistence.room.*

@Dao
interface MessageDao {
    /*@Query("SELECT * FROM Users WHERE userId LIKE :userId")
    fun getFriend(userId:String): List<Users>*/ //packagesInfo

    @Query("SELECT * FROM messages WHERE room_id LIKE :room_id")
    fun getAllMessage(room_id: String): List<MessageData>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(messageData: MessageData)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(messageData:List<MessageData>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(messageData: MessageData)

    @Query("SELECT * FROM messages WHERE id LIKE :id")
    fun getMessage(id: String): MessageData

    @Query("SELECT * FROM messages WHERE id LIKE :id")
    fun getRecentMessage(id: String): MessageData
//content_type ='text' order by created_at ASC limit 1
    @Query("SELECT * FROM messages WHERE room_id LIKE :room_id AND content_type LIKE \"text\" AND message_send = 0 order by created_at ASC LIMIT 1")
    fun getFailedMessage(room_id:String):MessageData?

    @Query("SELECT COUNT(*) FROM messages")
    fun getTotalMessage(): Int

    @Query("SELECT COUNT(*) FROM messages WHERE message_read = 0")
    fun getUnreadCount(): Int

    @Query("SELECT COUNT(*) FROM messages WHERE message_read = 0 AND room_id LIKE :room_id")
    fun getUnreadCountForGroup(room_id:String): Int

    @Query("UPDATE messages SET message_read = 1 WHERE room_id LIKE :room_id")
    fun updateMessageReadStatus(room_id:String)

    @Query("UPDATE messages SET message_send = 1 WHERE local_id LIKE :local_id")
    fun updateMessageSend(local_id:String)

    @Query("DELETE FROM messages WHERE id LIKE :local_id")
    fun deleteMessage(local_id: String)

    @Query("DELETE FROM messages")
    fun deleteAllMessages()
}