package com.mazadlive.customdialogs

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.ViewGroup
import android.view.Window

import com.mazadlive.R

class CustomProgressDialog(private val context: Context) {
    private lateinit var dialog: Dialog


    fun displayDialog(dimBackground: Boolean = true) {
        dialog = Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.setContentView(R.layout.custom_progress_dialog_layout)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)
        dialog.show()
        //Glide.with(context).asGif().load(R.drawable.loader_icon).into(dialog.findViewById(R.id.image))
    }

    fun hideDialog() {
        dialog?.cancel()
    }

}
