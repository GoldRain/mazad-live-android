package com.mazadlive.customdialogs

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.text.Html
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.animation.AnimationUtils
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.activities.SignInActivity
import com.mazadlive.adapters.CountryAdapter
import com.mazadlive.adapters.LiveBidAwardAdapter
import com.mazadlive.adapters.TagsAdapter
import com.mazadlive.helper.Constant
import com.mazadlive.models.*
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.custom_dialog_ok.*
import kotlinx.android.synthetic.main.customdialog_alert.*
import kotlinx.android.synthetic.main.customdialog_award_warning.*
import kotlinx.android.synthetic.main.customdialog_bid_approve.*
import kotlinx.android.synthetic.main.customdialog_bid_awared.*
import kotlinx.android.synthetic.main.customdialog_bid_awared_list.*
import kotlinx.android.synthetic.main.customdialog_bid_awared_win.*
import kotlinx.android.synthetic.main.customdialog_country.*
import kotlinx.android.synthetic.main.customdialog_follow.*
import kotlinx.android.synthetic.main.customdialog_image_viewer.*
import kotlinx.android.synthetic.main.customdialog_imagepicker.*
import kotlinx.android.synthetic.main.customdialog_live_end.*
import kotlinx.android.synthetic.main.customdialog_makeabid.*
import kotlinx.android.synthetic.main.customdialog_order_confirmation.*
import kotlinx.android.synthetic.main.customdialog_otp.*
import kotlinx.android.synthetic.main.customdialog_product_details_two.*
import kotlinx.android.synthetic.main.customdialog_tag.*
import kotlinx.android.synthetic.main.view_dialog_reportpost.*
import java.text.DecimalFormat


class CustomDialog(context: Context) : Dialog(context) {

    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
    }

    fun createDialogBox(){

        var dialog = AlertDialog.Builder(context)
                .setMessage("Login is required")
                .setTitle("Login required")
                .setPositiveButton("Login", object: DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {

                        val intent = Intent(context, SignInActivity:: class.java)
                        intent.putExtra("fromDialog",true)
                        context.startActivity(intent)
                    }
                })

                .setNegativeButton("Cancel", object : DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {

                    }

                })

        dialog.create()
        dialog.show()
    }

    fun showErrorDialog(titleText: String, listener: DialogListener? = null) {

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setContentView(R.layout.custom_dialog_ok)
        setCanceledOnTouchOutside(false)
        setCancelable(false)
        show()

        titleok.text = titleText
        ok.setOnClickListener {
            listener?.okClick()
            dismiss()
        }
    }

    fun showLocationSendDialog(titleText: String, listener: DialogListener? = null) {

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setContentView(R.layout.custom_dialog_ok)
        setCanceledOnTouchOutside(false)
        show()

        ok.setText(context.getString(R.string.send_location))
        ok.setTextColor(ContextCompat.getColor(context,R.color.status_red_color))
        titleok.text = titleText
        ok.setOnClickListener {
            listener?.okClick()
            dismiss()
        }
    }

    fun showLiveStreamEndDialog(titleText: String, listener: DialogListener? = null) {

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        setContentView(R.layout.customdialog_live_end)
        setCanceledOnTouchOutside(false)
        show()

        mAltOk.setOnClickListener {
            dismiss()
            listener!!.okClick()
        }

    }

    fun showAwardAlertDialog(msg:String, listener: DialogListener? = null) {

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setContentView(R.layout.customdialog_award_warning)
        setCanceledOnTouchOutside(false)
        show()
        mPrice.setText(msg)
        mConfirm.setOnClickListener {
            dismiss()
            listener!!.okClick()
        }

        mNotNow.setOnClickListener {
            dismiss()
        }

    }

    fun showAwardBidList(list:ArrayList<CommentModel>,listener: DialogListener? = null):Dialog {

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setContentView(R.layout.customdialog_bid_awared_list)
        setCanceledOnTouchOutside(false)
        setCancelable(false)

        awdrd_list_recyclerView.layoutManager = LinearLayoutManager(context)

        awdrd_list_recyclerView.adapter = LiveBidAwardAdapter(context,list)
        (awdrd_list_recyclerView.adapter as LiveBidAwardAdapter).listener = object :LiveBidAwardAdapter.OnClick{
            override fun onSelect(position: Int) {

            }
        }

        awdard_list_mConfirm.setOnClickListener {
            listener!!.okClick()
            dismiss()
        }
        awdard_list_mNotNow.setOnClickListener {
            listener!!.noClick()
            dismiss()
        }
        show()

        return this
    }


    fun showBidAwardedDialog(commentModel: CommentModel, listener: DialogListener? = null,flag: Boolean= false,nextText:String = "") {

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setContentView(R.layout.customdialog_bid_awared)
        setCanceledOnTouchOutside(false)
        show()

        if(!TextUtils.isEmpty(nextText)){
            bid_awarded_next.text = "CONTINUE"
            //""
            bid_awarded_des.text = ("You have won this product\nfor ${commentModel!!.message}")
        }else{
            bid_awarded_des.text = ("You sold this product")
        }

       // bid_awarded_price.text = commentModel!!.message

        val anim = AnimationUtils.loadAnimation(context, R.anim.scale)
        val anim2 = AnimationUtils.loadAnimation(context, R.anim.scale_done)


        cir_image.startAnimation(anim)
        bid_awarded_price.startAnimation(anim2)

        bid_awarded_next.setBackgroundResource(if(flag)R.drawable.feedview_btn_background_gray else R.drawable.feedview_btn_background)
        bid_awarded_next.isEnabled = !flag
        bid_awarded_close.setOnClickListener {
            dismiss()
            listener!!.okClick("stop")
        }

        bid_awarded_next.setOnClickListener {
            dismiss()
            listener!!.okClick("next")
        }

    }

    fun showBidAwardedWinDialog(nextText:String = "",listener: DialogListener? = null,flag: Boolean = false):Dialog {
        /*val dialog  = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.setContentView(R.layout.customdialog_bid_awared_win)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)
        dialog.show()

        dialog.bid_awarded_win_des.text = nextText

        val anim = AnimationUtils.loadAnimation(context, R.anim.scale)
        val anim2 = AnimationUtils.loadAnimation(context, R.anim.scale_done)


        dialog.bid_awarded_win_cir_image.startAnimation(anim)
        dialog.bid_awarded_win_tic.startAnimation(anim2)

        dialog.bid_awarded_win_close.setBackgroundResource(if(flag)R.drawable.feedview_btn_background_gray else R.drawable.feedview_btn_background)
        dialog.bid_awarded_win_close.isEnabled = !flag
        dialog.bid_awarded_win_close.setOnClickListener {
            dialog.dismiss()
            listener!!.okClick("close")
        }
*/

       // val dialog  = Dialog(context)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setContentView(R.layout.customdialog_bid_awared_win)
        setCanceledOnTouchOutside(false)
        setCancelable(false)
       show()

        bid_awarded_win_des.text = nextText

        val anim = AnimationUtils.loadAnimation(context, R.anim.scale)
        val anim2 = AnimationUtils.loadAnimation(context, R.anim.scale_done)


        bid_awarded_win_cir_image.startAnimation(anim)
        bid_awarded_win_tic.startAnimation(anim2)

        bid_awarded_win_close.setBackgroundResource(if(flag)R.drawable.feedview_btn_background_gray else R.drawable.feedview_btn_background)
       bid_awarded_win_close.isEnabled = !flag
       bid_awarded_win_close.setOnClickListener {
            dismiss()
            listener!!.okClick("close")
        }

        return this
    }

    fun showItemInfoDialog(list :ArrayList<LiveProductModel>,position:Int,listener: DialogListener? = null) {

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setContentView(R.layout.customdialog_product_details_two)
        setCanceledOnTouchOutside(false)
        show()

        val model = list[position]

        Log.e("IMAGE_URL","*** ${model.url} ")
        if(!TextUtils.isEmpty(model.path)){
            Glide.with(context).load(model.path).apply(RequestOptions().error(R.drawable.dummy)).into(product_info_image)
        }else{
            Glide.with(context).load(model.url).apply(RequestOptions().error(R.drawable.dummy)).into(product_info_image)
        }


        product_info_itemNo.text = ("Product ${position+1}/${list.size}")
        product_info_des.text = model.description
        product_info_serial.text = model.serial
        product_info_price.text = model.price
        product_info_pay_mode.text = model.paymentMode

        product_info_close.setOnClickListener {
            dismiss()
            listener!!.okClick()
        }

    }

    fun showReportDialog (str:String,listener: DialogListener? = null) {

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setContentView(R.layout.view_dialog_reportpost)
        setCanceledOnTouchOutside(false)

        if(!TextUtils.isEmpty(str)){
            mTitle.setText(str)
        }
        val list = ArrayList<TextView>()
        list.add(Nudity)
        list.add(Violence)
        list.add(Harassment)
        list.add(Suicide)
        list.add(Terrorism)
        list.add(Spam)
        list.add(FalseNews)
        list.add(Incorrect)
        list.add(HateSpeech)
        list.add(UnauthorizedSales)
        list.add(Other)
        //list.add(UnauthorizedSales)

        var hT:TextView?= null
        hT = UnauthorizedSales

        list.forEach { text->

            text.tag = text.text.equals("Unauthorized Sales")

            text.setOnClickListener {

                if((text.tag as  Boolean)){
                    text.setTextColor(ContextCompat.getColor(context,R.color.colorBlack))
                    text.setBackgroundResource(R.drawable.rounded_corner_white)

                    text.tag = false
                    if(text.text.equals("Other")){
                        mDescription.visibility = View.VISIBLE
                    }

                    hT = null
                }else{
                    text.setTextColor(ContextCompat.getColor(context,R.color.colorWhite))
                    text.setBackgroundResource(R.drawable.rounded_corner_pri)
                    text.tag = true
                    if(hT != null){
                        hT!!.setTextColor(ContextCompat.getColor(context,R.color.colorBlack))
                        hT!!.setBackgroundResource(R.drawable.rounded_corner_white)
                        hT!!.tag = false

                        if(hT!!.text.equals("Other")){
                            mDescription.visibility = View.GONE
                        }
                    }

                    hT = text

                    if(text.text.equals("Other")){
                        mDescription.visibility = View.VISIBLE
                    }
                }
            }
        }

        mSend.setOnClickListener {
           if(hT != null && !TextUtils.isEmpty(hT!!.text.toString().trim())){
               if(hT!!.text.equals("Other")){
                   if(TextUtils.isEmpty(mDescription.text.toString().toString())){
                       Toast.makeText(context,"Enter a reason to report",Toast.LENGTH_SHORT).show()
                   }else{
                       dismiss()
                       listener!!.okClick(mDescription.text.toString().trim())
                   }
               }else{
                   dismiss()
                   listener!!.okClick(hT!!.text.toString().trim())
               }
           }else{
               Toast.makeText(context,"Select a reason to report",Toast.LENGTH_SHORT).show()
           }
        }

        show()

    }

    fun showAlertDialog(textTitle: String? = null, textDetail: String? = null, listener: DialogListener? = null) {

        requestWindowFeature(Window.FEATURE_NO_TITLE)

        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        setContentView(R.layout.customdialog_alert)

        setCanceledOnTouchOutside(false)
        setCancelable(false)

        aletOk.setOnClickListener {
            listener?.okClick()
            dismiss()
        }

        if (textTitle != null) {
            alertTitle.text = textTitle
        }

        if (textDetail != null) {
            alertDetail.text = textDetail
            alertDetail.visibility = View.VISIBLE
        }

        show()
    }

    fun showOrderConfirmDialog(listener: DialogListener? = null) {

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setContentView(R.layout.customdialog_order_confirmation)
        setCanceledOnTouchOutside(false)
        setCancelable(true)

        confirmDetail.setText(Html.fromHtml(context.getString(R.string.dialog2_text)), TextView.BufferType.SPANNABLE)

        confirmOk.setOnClickListener {
            listener?.okClick()
            dismiss()
        }

        confirmTitle.setOnClickListener { dismiss() }

        show()
    }

    fun showOtpDialog(listener: DialogListener? = null) {

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setContentView(R.layout.customdialog_otp)
        setCanceledOnTouchOutside(false)
        setCancelable(true)

        verify_otp.setOnClickListener {

            if(!TextUtils.isEmpty(mOtp.text.toString().trim())){
               if(mOtp.text.toString().trim().length == 6){
                   listener!!.okClick(mOtp.text.toString())
                   dismiss()
               }else{
                   Toast.makeText(context,"Enter valid Otp",Toast.LENGTH_SHORT).show()
               }
            }else{
                Toast.makeText(context,"Enter Otp",Toast.LENGTH_SHORT).show()
            }

        }

        show()
    }

    fun showCountryDialog(listener: DialogListener,isNumber : Boolean = false,isAll:Boolean= false) {

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setContentView(R.layout.customdialog_country)
        setCanceledOnTouchOutside(false)
        setCancelable(true)

        val adapter = CountryAdapter(context, object : DialogListener() {
            override fun okClick() {
                super.okClick()
                dismiss()
            }

            override fun okClick(any: Any) {
                super.okClick(any)
                listener.okClick(any)
            }
        })

        if(isAll){
            val cn = CountryModel()
            cn.id = "NA"
            cn.name = "All"
            adapter.addCountry(cn)
        }

        if(Constant.countryList.size <=0 ){
            country_empty.visibility = View.VISIBLE
        }else{
            country_empty.visibility = View.GONE
        }
        adapter.isNumberSelection = isNumber
        countryRecyclerView.layoutManager = LinearLayoutManager(context)
        countryRecyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
        show()
    }

    fun showPickerDialog(listener: DialogListener) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setContentView(R.layout.customdialog_imagepicker)
        setCanceledOnTouchOutside(false)
        setCancelable(true)


        mCamera.setOnClickListener {
            listener.okClick("Camera")
            dismiss()
        }
        mGallary.setOnClickListener {
            listener.okClick("Gallary")
            dismiss()
        }

        show()
    }

    fun showBidConfirmDialog(listener: DialogListener,flag : Boolean = false) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setContentView(R.layout.customdialog_bid_approve)
        setCanceledOnTouchOutside(false)
        setCancelable(true)
        if(flag){
            mApprove.text = "Accept"
            mReject.text = "Cancel"
        }
        mApprove.setOnClickListener {
            if(flag){
                listener.okClick("Accept")
            }else{
                listener.okClick("approved")
            }
            dismiss()
        }
        mReject.setOnClickListener {
            listener.okClick("rejected")
            dismiss()
        }

        show()
    }

    fun showLanguagePickerDialog(listener: DialogListener) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setContentView(R.layout.customdialog_image_viewer)
        setCanceledOnTouchOutside(false)
        setCancelable(true)


        mViewImage.setText("ENGLISH")
        mViewImage.setOnClickListener {
            listener.okClick("ENGLISH")
            dismiss()
        }

        mDelete.setText("ARABIC")
        mDelete.setOnClickListener {
            listener.okClick("ARABIC")
            dismiss()
        }

        mCancel.setOnClickListener {
            listener.okClick("ENGLISH")
            dismiss()
        }

        show()
    }

    fun showDialogForViewImage(tx1:String, tx2:String, listener: DialogListener) {

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setContentView(R.layout.customdialog_image_viewer)
        setCanceledOnTouchOutside(false)
        setCancelable(true)
        mViewImage.setText(tx1)
        mDelete.setText(tx2)

        mViewImage.setOnClickListener {
            listener.okClick("view")
            dismiss()
        }
        mDelete.setOnClickListener {
            listener.okClick("delete")
            dismiss()
        }

        mCancel.setOnClickListener {
            listener.okClick("cancel")
            dismiss()
        }

        show()
    }

    fun showDialogForTag(list:ArrayList<TagModel>,listener: DialogListener) {

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setContentView(R.layout.customdialog_tag)
        setCanceledOnTouchOutside(false)
        setCancelable(true)
        recycleView.layoutManager = LinearLayoutManager(context)
        recycleView.adapter = TagsAdapter(context,list,object :DialogListener(){
            override fun okClick(any: Any) {
               dismiss()
            }
        })

        setTags.setOnClickListener {
            listener.okClick()
            dismiss()
        }

        mCancelTag.setOnClickListener {
            dismiss()
        }


        show()
    }

    fun showDialogForFollow(model : UserModel?,listener: DialogListener) {

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setContentView(R.layout.customdialog_follow)
        setCanceledOnTouchOutside(false)
        setCancelable(true)
        Glide.with(context).load(model!!.profile).apply(RequestOptions().override(200).error(R.drawable.dummy)).into(mProfile)
        mFollowText.setText("${context.getString(R.string.hint_unfollow)} ${model!!.username}")
        mFollow.setOnClickListener {
            listener.okClick()
            dismiss()
        }

        mCancelFollow.setOnClickListener {
            dismiss()
        }


        show()
    }

    fun showDialogBidComment(str : String?,listener: DialogListener) {

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setContentView(R.layout.customdialog_follow)
        setCanceledOnTouchOutside(false)
        setCancelable(true)
        mProfile.visibility = View.GONE
        mFollowText.setText(str)
        mFollow.setText(context.getString(R.string.hint_view_bids))
        mCancelFollow.setText(context.getString(R.string.hint_not_interest))

        mFollow.setOnClickListener {
            listener.okClick(1)
            dismiss()
        }

        mCancelFollow.setOnClickListener {
            listener.okClick(0)
            dismiss()
        }


        show()
    }


    fun showBidialog(amount:String,listener: DialogListener? = null) {

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setContentView(R.layout.customdialog_makeabid)
        setCanceledOnTouchOutside(true)
        setCancelable(true)

        mBidMinAmount.text = String.format("Last Bid Amount (${context.getString(R.string.currency)} ${DecimalFormat("##.##").format(amount.toDouble())})")
        mFlagMakeaBid.setImageResource(context.resources.getIdentifier("drawable/flag_" + MyApplication.instance.getUserPreferences().CountryFlag, null, context.packageName))
        mBidCountry.text = MyApplication.instance.getUserPreferences().CountryName

        mMakeABid.setOnClickListener {

            try {
                val tempamount = mBidAmout.text.toString().trim().toDouble()
                if(tempamount <= 0){
                    mBidAmout.error = "Please Enter Correct Bid Amount"
                    return@setOnClickListener
                }
                if (tempamount > (amount.toDouble())) {
                    listener?.okClick(DecimalFormat("##.##").format(tempamount))
                    dismiss()
                }else{
                    mBidAmout.error = "Please Enter more Bid Amount"
                    return@setOnClickListener
                }

            }catch (e:Exception){
                e.printStackTrace()
                mBidAmout.error = "Enter valid amount"
            }

        }

        setOnDismissListener {
            listener!!.okClick()
        }
        show()
    }

    fun permissionDialog(message: String, listener: DialogListener) {
        AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("Yes") { dialogInterface, _ ->
                    listener.yesClick()
                    dialogInterface.dismiss()
                }.setNegativeButton("No") { dialogInterface, _ ->
            dialogInterface.dismiss()
            listener.noClick()
        }.show()
    }

}