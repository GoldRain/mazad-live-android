package com.mazadlive.customdialogs

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.ViewGroup
import android.view.Window

import com.mazadlive.R
import kotlinx.android.synthetic.main.custom_progress_layout.*

class CustomProgress(private val context: Context) {
    private var dialog: Dialog

    init {
        dialog = Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.setContentView(R.layout.custom_progress_layout)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)

    }

    fun show() {
        dialog.show()
        //Glide.with(context).asGif().load(R.drawable.loader_icon).into(dialog.findViewById(R.id.image))
    }

    fun dismiss() {
        dialog.cancel()
    }

    fun setMessage(str:String){
        (context as Activity).runOnUiThread {
            dialog.message.setText(str)
        }
    }

}
