package com.mazadlive.customdialogs

interface DialogDismissListener {
    fun onDialogDisplay()
    fun onDialogDismiss(blurFlag: Boolean)
}