package com.mazadlive.customdialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.util.Log;
import android.view.View;

/**
 * Created by bodacious on 28/4/18.
 */

public class BottomSheetDialogExpanded extends BottomSheetDialog {

    private BottomSheetBehavior<View> mBehaviour;

    public BottomSheetDialogExpanded(@NonNull Context context) {
        super(context);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        View bottomSheet = getWindow().getDecorView().findViewById(android.support.design.R.id.design_bottom_sheet);
        mBehaviour = BottomSheetBehavior.from(bottomSheet);
        mBehaviour.setState(BottomSheetBehavior.STATE_EXPANDED);
        mBehaviour.setSkipCollapsed(true);
        mBehaviour.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                Log.d("TAG", newState + "");
                if (newState == 5)
                    dismiss();
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBehaviour.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

}
