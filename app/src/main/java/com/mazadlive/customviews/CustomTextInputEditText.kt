package com.mazadlive.customviews

import android.content.Context
import android.graphics.Typeface
import android.support.design.widget.TextInputEditText
import android.util.AttributeSet
import com.mazadlive.R

/**
 * Created by bodacious on 29/9/18.
 */
class CustomTextInputEditText : TextInputEditText {

    constructor(context: Context,attrs : AttributeSet, defStyle : Int) : super(context, attrs, defStyle){
        init(context,attrs)
    }

    constructor(context: Context,attrs: AttributeSet) : super(context,attrs){
        init(context,attrs)
    }

    constructor(context: Context) : super(context)

    private fun init(context: Context, attrs: AttributeSet){
        val typeArray = context.obtainStyledAttributes(attrs,R.styleable.CustomTextView)
        val customFont = typeArray.getString(R.styleable.CustomTextView_customFont)
        try {
            setCustomFont(context,customFont)
        }catch (e : Exception){

        }
        typeArray.recycle()
    }

    private fun setCustomFont(context: Context, ass :String) : Boolean {
        val tf = Typeface.createFromAsset(context.assets,"fonts/.$ass.tff")
        typeface = tf
        return true
    }
}