package com.mazadlive.customviews

import android.content.Context
import android.widget.TextView
import android.graphics.Typeface
import android.util.AttributeSet
import com.mazadlive.R

class CustomTextView : TextView {

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context) : super(context)

    private fun init(context: Context, attrs: AttributeSet) {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomTextView)
        val customFont = typedArray.getString(R.styleable.CustomTextView_customFont)
        try {
            setCustomFont(context, customFont)
        } catch (e: Exception) {
            //Log.e("TextEx", e.message)
        }
        typedArray.recycle()
    }

    private fun setCustomFont(ctx: Context, asset: String): Boolean {
        val tf = Typeface.createFromAsset(ctx.assets, "fonts/$asset.ttf")
        typeface = tf
        return true
    }

}