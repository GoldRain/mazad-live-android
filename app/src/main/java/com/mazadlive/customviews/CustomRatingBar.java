package com.mazadlive.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mazadlive.R;

public class CustomRatingBar extends LinearLayout {

    private Context context;
    private int maxStar = 5;
    private int ratingStar = 0;
    private float starSize = 0;
    private boolean isIndicator = true;

    public CustomRatingBar(Context context) {
        super(context);
    }

    public CustomRatingBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CustomRatingBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CustomRatingBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, @Nullable AttributeSet attrs) {
        this.context = context;
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CustomRatingBar, 0, 0);
        try {
            maxStar = a.getInt(R.styleable.CustomRatingBar_maxStar, 5);
            ratingStar = a.getInt(R.styleable.CustomRatingBar_ratingStar, 0);
            starSize = a.getDimension(R.styleable.CustomRatingBar_starSize, getResources().getDimension(R.dimen.default_star_size));
            isIndicator = a.getBoolean(R.styleable.CustomRatingBar_isIndicator, true);
        } catch (Exception e) {
            Log.e("testing", "EX : " + e.getMessage());
        } finally {
            a.recycle();
        }
        setUp();
    }

    public void setMaxStar(int maxStar) {
        this.maxStar = maxStar;
        setUp();
    }

    public void setRatingStar(int ratingStar) {
        this.ratingStar = ratingStar;
        setUp();
    }

    public int getMaxStar() {
        return this.maxStar;
    }

    public int getRatingStar() {
        return this.ratingStar;
    }

    private void setUp() {
        removeAllViews();
        int count = ratingStar;
        for (int i = 0; i < maxStar; i++) {
            ImageView starView = new ImageView(context);
            LayoutParams params = new LayoutParams((int) starSize, (int) starSize);
            params.setMargins(1, 0, 1, 0);
            if (count > 0)
                starView.setImageResource(R.drawable.ic_rate_star_on);
            else
                starView.setImageResource(R.drawable.ic_rate_star_off);
            starView.setOnClickListener(new ClickListener(i + 1));
            addView(starView, params);
            count--;
        }
        invalidate();
    }

    private class ClickListener implements OnClickListener {
        private int count;

        public ClickListener(int count) {
            this.count = count;
        }

        @Override
        public void onClick(View view) {
            if (!isIndicator)
                setRatingStar(count);
        }
    }

}
