package com.mazadlive.customviews

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.KeyEvent
import android.widget.EditText
import com.mazadlive.R
import com.mazadlive.customviews.CustomEditText.KeyImeChange



class CustomEditText : EditText {

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context) : super(context)

    private fun init(context: Context, attrs: AttributeSet) {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomTextView)
        val customFont = typedArray.getString(R.styleable.CustomTextView_customFont)
        try {
            setCustomFont(context, customFont)
        } catch (e: Exception) {
            //Log.e("EditEx", e.message)
        }
        typedArray.recycle()
    }

    private var keyImeChangeListener: KeyImeChange? = null

    fun setKeyImeChangeListener(listener: KeyImeChange) {
        keyImeChangeListener = listener
    }

    private fun setCustomFont(ctx: Context, asset: String): Boolean {
        val tf = Typeface.createFromAsset(ctx.assets, "fonts/$asset.ttf")
        typeface = tf
        return true
    }


    override fun  onKeyPreIme(keyCode:Int , event: KeyEvent):Boolean {
        if (keyImeChangeListener != null)
        {
            keyImeChangeListener!!.onKeyIme(keyCode, event);
        }
        return false;
    }


    interface KeyImeChange {
        fun onKeyIme(keyCode: Int, event: KeyEvent)
    }
}
