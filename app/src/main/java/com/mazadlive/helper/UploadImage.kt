package com.mazadlive.helper

import android.content.Context
import android.provider.ContactsContract.CommonDataKinds.StructuredPostal.REGION
import android.util.Log
import android.widget.Toast
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client
import com.mazadlive.Interface.ProgressListener
import com.mazadlive.utils.MyApplication
import org.jetbrains.anko.doAsync
import java.io.File
import java.lang.Exception

/**
 * Created by bodacious on 18/1/19.
 */

class UploadImage (val context: Context,val file:File, val type:String){
    var s3: AmazonS3Client? = null
    var transferUtility: TransferUtility? = null

    init {
        credentialsProvider()
        setTransferUtility()
    }

    private fun credentialsProvider(){

        // Initialize the Amazon Cognito credentials provider
        val credentialsProvider = CognitoCachingCredentialsProvider(
                context,
                MyApplication.instance.getUserPreferences().POOL_ID, //indentity pool id
                Constant.REGION)

        setAmazonS3Client(credentialsProvider);
    }

    private fun setAmazonS3Client(credentialsProvider: CognitoCachingCredentialsProvider) {

        // Create an S3 client
        s3 = AmazonS3Client(credentialsProvider)

    }

    private fun setTransferUtility() {
        transferUtility = TransferUtility(s3, context)
    }


    fun uploadToAws( listener: ProgressListener){
        doAsync {
            try {
                when(type){
                    "doc" ->{
                        val transferObserver = transferUtility!!.upload(Constant.BUCKET_NAME,"${Constant.DOC_IMAGE_KEY}/${file!!.name}",file)
                        transferObserverListener(transferObserver,file,listener)
                    }

                    "user" ->{
                        val transferObserver = transferUtility!!.upload(Constant.BUCKET_NAME,"${Constant.USER_IMAGE_KEY}/${file!!.name}",file)

                        transferObserverListener(transferObserver,file,listener)
                    }

                    "post" ->{
                        val transferObserver = transferUtility!!.upload(Constant.BUCKET_NAME,"${Constant.POST_IMAGE_KEY}/${file!!.name}",file)

                        transferObserverListener(transferObserver,file,listener)
                    }

                    "story" ->{
                        val transferObserver = transferUtility!!.upload(Constant.BUCKET_NAME,"${Constant.STORY_IMAGE_KEY}/${file!!.name}",file)

                        transferObserverListener(transferObserver,file,listener)
                    }

                    "stream" ->{
                        val transferObserver = transferUtility!!.upload(Constant.BUCKET_NAME,"${Constant.STREAM_IMAGE_KEY}/${file!!.name}",file)

                        transferObserverListener(transferObserver,file,listener)
                    }
                }

            }catch (e: Exception){
                e.printStackTrace()
            }
        }
    }

    private fun transferObserverListener(transferObserver: TransferObserver,  file: File,listener: ProgressListener) {
        // Log.e("transferObserverList","onState $position")
        transferObserver.setTransferListener(object : TransferListener {
            override fun onStateChanged(id: Int, state: TransferState?) {
                if(state!!.name.equals("COMPLETED",true)){

                    when(type){

                        "doc" ->{
                            val url =  s3!!.getResourceUrl(Constant.BUCKET_NAME,"${Constant.DOC_IMAGE_KEY}/${file.name}")
                            listener.onSuccess(url)
                        }

                        "user" ->{
                            val url =  s3!!.getResourceUrl(Constant.BUCKET_NAME,"${Constant.USER_IMAGE_KEY}/${file.name}")
                            listener.onSuccess(url)
                        }

                        "post" ->{
                            val url =  s3!!.getResourceUrl(Constant.BUCKET_NAME,"${Constant.POST_IMAGE_KEY}/${file.name}")
                            listener.onSuccess(url)
                        }

                        "story" ->{
                            val url =  s3!!.getResourceUrl(Constant.BUCKET_NAME,"${Constant.STORY_IMAGE_KEY}/${file.name}")
                            listener.onSuccess(url)
                        }

                        "stream" ->{
                            val url =  s3!!.getResourceUrl(Constant.BUCKET_NAME,"${Constant.STREAM_IMAGE_KEY}/${file.name}")
                            listener.onSuccess(url)
                        }


                    }

                }else{
                    if(state.name.equals("FAILED",true)){
                        listener.onCancel()
                    }
                }

            }

            override fun onError(id: Int, ex: Exception?) {
                ex!!.printStackTrace()
                listener.onCancel()
            }

            override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                val per = (bytesCurrent/bytesTotal)*100
                listener.onProgressUpdate(per.toInt())
            }
        });
    }
}