package com.mazadlive.helper;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;

public class GpsTracker implements LocationListener {
    private boolean canGetLocation = false;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 5;
    private static final long MIN_TIME_BW_UPDATES = 10000;
    private double latitude;
    private double longitude;

    private LocationManager locationManager;
    private Location location;
    private Context context;


    public GpsTracker(Context context) {
        this.context = context;
        location = getLocation();
    }


    public Location getCurrentLocation(){
        if(location == null){
            getLocation();
        }

        return location;
    }

    public Location getLocation() {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean isGPSOn = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkOn = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (isGPSOn && isNetworkOn) {
            canGetLocation = true;
        }

        if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            try {
                if (isNetworkOn) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();


                    }

                }

                if (isGPSOn) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                        String provider = locationManager.getBestProvider(new Criteria(), true);
                        location = locationManager.getLastKnownLocation(provider);
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();


                    }
                }

            } catch (Exception ignored) {

            }
            return location;
        }

        return null;

    }

    public void stopUsingGPS() {
        if (locationManager != null) {
            locationManager.removeUpdates(GpsTracker.this);
        }
    }


    public double getLatitude() {
        if (location != null)
            latitude = location.getLatitude();

        return latitude;

    }

    public double getLongitude() {
        if (location != null) {
            longitude = location.getLongitude();
        }

        return longitude;
    }

    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    public void settingDialog() {
        final AlertDialog.Builder alt = new AlertDialog.Builder(context);
        alt.setTitle("GPS Setting")
                .setMessage("GPS is Not On DO you Want to Change Setting ")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        context.startActivity(intent);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                }).show();
    }


    @Override
    public void onProviderEnabled(String s) {
    }

    @Override
    public void onProviderDisabled(String s) {
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onLocationChanged(Location location) {
          if(this.location != null){
              if(this.location.distanceTo(location) >10){
                  //updateUserLocation(location);
              }
          }
        this.location = location;
    }


   /* private void updateUserLocation(final Location location) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.UPDATE_USER_LOCATION_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (!jsonObject.getBoolean("error")) {
                        if(jsonObject.has("data")){
                            JSONObject object = jsonObject.getJSONObject("data");
                            if(object.has("latitude")){
                                MyApplication.getInstance().getPrefManager().holdLatitude(object.getString("latitude"));
                            }
                            if(object.has("longitude")){
                                MyApplication.getInstance().getPrefManager().holdLongitude(object.getString("longitude"));
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("longitude", location.getLongitude() + "");
                param.put("latitude", location.getLatitude() + "");
                param.put("userId", MyApplication.getInstance().getPrefManager().getId());
                return param;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }*/

}
