package com.mazadlive.helper

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import com.mazadlive.activities.SignInActivity

/**
 * Created by bodacious on 7/6/19.
 */
class AfterSkipDialogBox(context: Context) {

    var mContext = context

    var dialog = AlertDialog.Builder(context)

      fun createDialogBox(){

                    dialog.setMessage("Login is required")
                    .setTitle("Login required")
                    .setPositiveButton("Login", object: DialogInterface.OnClickListener{
                        override fun onClick(dialog: DialogInterface?, which: Int) {

                            mContext.startActivity(Intent(mContext, SignInActivity:: class.java))
                        }
                    })

                    .setNegativeButton("Cancel", object : DialogInterface.OnClickListener{
                        override fun onClick(dialog: DialogInterface?, which: Int) {

                        }

                    })

          dialog.create()
          dialog.show()
        }


}