package com.mazadlive.helper

import android.app.Activity
import android.support.v4.app.Fragment
import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.os.Build
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.util.Log
import com.mazadlive.BuildConfig
import com.mazadlive.Interface.DialogListener
import com.mazadlive.customdialogs.CustomDialog
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class PickImageHelper(val context: Context, private val requestContext: Fragment?, private val forActivity: Boolean, val onImagePickListener: OnImagePickerListener) {

    var currentImagePath: String? = null

    fun pickImage(mode: Int) {
        when (mode) {
            1 -> {
                cameraAction()
            }
            2 -> {
                galleryAction()
            }
            3 -> {
                videoAction()
            }
            4 -> {
                showOptionDialog()
            }
        }
    }


    private fun showOptionDialog() {

        CustomDialog(context).showPickerDialog(object : DialogListener(){
            override fun okClick(any: Any) {
                super.okClick(any)
                if (any == "Camera"){
                    cameraAction()
                }
                if(any == "Gallary"){
                    galleryAction()
                }
            }
        })
    }

    private fun cameraAction() {
        var imageName = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        imageName = "${imageName}_JPEG"
        val filePath = Constant.getTempLocation()
        val file = File(filePath)
        file.mkdirs()
        val imageFile = File.createTempFile(imageName, ".jpg", file.absoluteFile)
        currentImagePath = imageFile.absolutePath
        val uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID, imageFile)
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
            cameraIntent.clipData = ClipData.newRawUri("", uri)
            cameraIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION)
        }

        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
        if (forActivity)
            (context as Activity).startActivityForResult(cameraIntent, Constant.CAMERA_IMAGE_REQUEST)
        else
            requestContext!!.startActivityForResult(cameraIntent, Constant.CAMERA_IMAGE_REQUEST)
    }

    private fun galleryAction() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        if (forActivity)
            (context as Activity).startActivityForResult(Intent.createChooser(intent, "Select Picture"), Constant.GALLARY_IMAGE_REQUEST)
        else
            (context as Activity).startActivityForResult(Intent.createChooser(intent, "Select Picture"), Constant.GALLARY_IMAGE_REQUEST)
    }

    private fun videoAction() {
        val intent = Intent()
        intent.type = "video/*"
        intent.action = Intent.ACTION_GET_CONTENT
        if (forActivity)
            (context as Activity).startActivityForResult(Intent.createChooser(intent, "Select Video"), Constant.GALLARY_VIDEO_REQUEST)
        else
            requestContext!!.startActivityForResult(Intent.createChooser(intent, "Video Picture"), Constant.GALLARY_VIDEO_REQUEST)
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.e("DOC_IMAGES", "*** Pick Image  onActivityResult")
        if (requestCode == Constant.CAMERA_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            currentImagePath.let {
                onImagePickListener.onImagePicked(currentImagePath!!, requestCode)
            }
        } else if (requestCode == Constant.GALLARY_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            val imageUri = data?.data
            imageUri.let {
                currentImagePath = GetFilePath.getFilePath(context, imageUri)
                onImagePickListener.onImagePicked(currentImagePath!!, requestCode)
            }
        }
        if( requestCode == Constant.GALLARY_VIDEO_REQUEST){
            if(resultCode == Activity.RESULT_OK){
                val imageUri = data?.data
                imageUri.let {
                    currentImagePath = GetFilePath.getFilePath(context, imageUri)
                    onImagePickListener.onImagePicked(currentImagePath!!, requestCode)
                }
            }else{
                if(File(currentImagePath).exists()){
                    File(currentImagePath).delete()
                }
            }

        }
    }


    interface OnImagePickerListener {
        fun onImagePicked(imagePath: String, requestCode: Int)
    }
}


