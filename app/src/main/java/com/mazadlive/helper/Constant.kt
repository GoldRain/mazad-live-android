package com.mazadlive.helper

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import android.text.TextUtils
import android.util.Base64
import com.amazonaws.regions.Regions
import com.mazadlive.Socket.SocketManager
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.models.CountryModel
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.UserPreferences
import org.json.JSONArray
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

class Constant {
    companion object {
        val PREFIX = "https://mazadlive.page.link"
        var LINK_URL = ""
        val AD_TIME = 60000.toLong()
        var SHOW_ADD = true
        var IS_CHAT_ACTIVE = false
        var CURRENT_ROOM_ID = ""
        var CURRENT_STREAM_ID = ""
        val path = Environment.getExternalStorageDirectory().toString()
        val STORY_PATH = "$path/MazadLive/Media/.Story/"
        val LOGGED_IN_TEXT = "You have been logged in some other device also"


        val PLAYSTORE_LINK = "https://play.google.com/store/apps/details?id=com.mazadlive&hl=en"

        /******************************************************************************red 5 ************************************************/
        //   var LICENSE_KEY = "VZCJ-BDRP-WA4H-7NR5" -"YMSU-UDLS-C3AR-XLPC
        var LICENSE_KEY = "EU2B-2BTM-K455-ZPHK"
        var HOST = "" +
                "" +
                "" +
                ""
     //   var HOST = "192.168.0.129"
        var PORT = 8554
        var SERVER_PORT = "5080"

        var VAL = 0.5f
        var BUFFER_TIME = 0.5f


        /*********************************************************************************************aws..*****************************************/
        val BUCKET_NAME = "mazadpostimages"
        // val POOL_ID = "ap-southeast-1:56468b27-2ed4-4051-8e53-8b188132c6c3"
     //   val POOL_ID = "us-east-2:48f0f72b-9b02-4d68-84ef-4163ce0144d9"
        //val REGION = Regions.AP_SOUTHEAST_1
        val REGION = Regions.US_EAST_2

        //************************************************************************* TAP PAYMENT **************************************************
       // val AUTH_TOKEN = "sk_live_xqfRC2K1i9AcmvOFYke68dUn"
      //  val ENCRYPTION_KEY = "pk_live_fA9qwjrTsbZG6ENo82hlW0IC" ;
       /* val ENCRYPTION_KEY = "MIIBIDANBgkqhkiG9w0BAQEFAAOCAQ0AMIIBCAKCAQEAndkqmyMcuBdK81jsddnv\n" +
                "MxdDMxl2qccY+15CGJGiFub1uYl5Z+Lm6Bb50EFtaU10D8QziwdWWACtVu3llHvu\n" +
                "lcq+1Vzo7fr8SxAWlKI4Xlndmuat/1qG29y+zXOyAcezHe6X793YGYU6k3pC3LVb\n" +
                "AOR4gDFOKZBTQAwdmsChcVh2ibOA5Bt7l1lxVKfR2GZun72HNYYs+2Jvwz7SeQZP\n" +
                "TkbDMDfrWRJTurCk66WSU/Zh0dlF/5Nkzfnv/xXd8xIOo9J3zddpIbpEDMGa8wU7\n" +
                "Vy54szyBbbPH5+OMGj1G3Okb2UNy3JuM9CVfHHO2J/dKjud4MsdhtJ6NWLB/Cq49\n" +
                "8QIBEQ=="
*/
        val TEST_CARD_NUMBER = "4111111111111111"
        //********************************************************************************************************************************************
        val POST_IMAGE_KEY = "postimages"
        val STORY_IMAGE_KEY = "storyimages"
        val USER_IMAGE_KEY = "userimages"
        val STREAM_IMAGE_KEY = "streamimages"
        val DOC_IMAGE_KEY = "documentimages"

        val CAMERA_IMAGE_REQUEST = 101
        val GALLARY_IMAGE_REQUEST = 102
        val GALLARY_VIDEO_REQUEST = 103

        val ADDRESS_CODE = 121
        val BUY_NOW_CODE = 131
        val FEEDBACK_CODE = 132
        val NOTOFICATION_KEY = "is_notification"
        val ROOM_KEY = "room_id"
        val LOG_OUT_KEY = "LOG_OUT_KEY"
        val ORDER_KEY = "order"
        val MAX_TRANSACTION_AMOUNT = 1000.toDouble()

        val API_KEY = "18b20a8a9d574695abcb1b70cd4371ce"
        val LOCAL_STORAGE = Environment.getExternalStorageDirectory().toString() + "/MazadLive/"

        var countryList = ArrayList<CountryModel>()
        fun getTempLocation(): String {
            return LOCAL_STORAGE + "TempFolder"
        }

        fun setOnLine(flag: Boolean) {
            val socketManager = MyApplication.instance.getSocketManager()
            if (!socketManager!!.isConnected) {
                socketManager!!.socket!!.open()
                socketManager!!.connect()
            }
            val map = JSONObject()
            map.put("user_id", MyApplication.instance.getUserPreferences().id)
            map.put("is_online", flag)
            map.put("user_type", "android")
            map.put("last_login", MyApplication.instance.getUserPreferences().lastLoginTime)
            map.put("playerId", MyApplication.instance.getUserPreferences().playerId)

            socketManager!!.socket!!.emit(SocketManager.EMIT_UPDATE_ONLINE, map)

        }

        fun getResizedBitmap(image: Bitmap, maxSize: Int): Bitmap {
            var width = image.width
            var height = image.height

            val bitmapRatio = width.toFloat() / height.toFloat()
            if (bitmapRatio > 1) {
                width = maxSize
                height = (width / bitmapRatio).toInt()
            } else {
                height = maxSize
                width = (height * bitmapRatio).toInt()
            }
            return Bitmap.createScaledBitmap(image, width, height, true)
        }

        fun getBitmapFromPath(path: String): Bitmap? {
            var bitmap: Bitmap? = null
            try {

                val f = File(path)
                val options = BitmapFactory.Options()
                options.inPreferredConfig = Bitmap.Config.ARGB_8888

                bitmap = BitmapFactory.decodeStream(FileInputStream(f), null, options)

            } catch (e: Exception) {
                e.printStackTrace()

            } finally {
                return bitmap
            }
        }

        fun getStringFromBitmap(bitmap: Bitmap): String {
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val imageBytes = baos.toByteArray()
            return Base64.encodeToString(imageBytes, Base64.DEFAULT)

        }

        fun getBitmapFromString(encodedString: String): Bitmap? {
            try {
                val encodeByte = Base64.decode(encodedString, Base64.DEFAULT)
                return BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size)
            } catch (e: Exception) {
                e.message
                return null
            }

        }


        fun saveStreamThumbImage(bitmap: Bitmap): File? {
            val filename: File
            val imageName = System.currentTimeMillis().toString() + ""
            try {
                val path = Environment.getExternalStorageDirectory().toString()
                val file = File(path + "/MazadLive/Media/.Stream/Th_Images")
                file.mkdirs()
                filename = File("$path/MazadLive/Media/.Stream/Th_Images/IMG_$imageName.jpg")
                val out = FileOutputStream(filename)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out)
                out.flush()
                out.close()
                return filename
            } catch (e: Exception) {
                e.printStackTrace()
                return null
            }


        }

        fun savePostImage(bitmap: Bitmap, name: String): File? {
            val filename: File
            var imageName = name
            if (TextUtils.isEmpty(imageName)) {
                imageName = "IMG_" + System.currentTimeMillis().toString()
            }
            try {
                val path = Environment.getExternalStorageDirectory().toString()
                val file = File(path + "/MazadLive/Media/.Post/images")
                file.mkdirs()
                filename = File("$path/MazadLive/Media/.Post/images/$imageName.jpg")
                val out = FileOutputStream(filename)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out)
                out.flush()
                out.close()
                return filename
            } catch (e: Exception) {
                e.printStackTrace()
                return null
            }


        }

        fun saveStoryImage(bitmap: Bitmap): File? {
            val filename: File
            val imageName = System.currentTimeMillis().toString() + ""
            try {
                val path = Environment.getExternalStorageDirectory().toString()
                val file = File(path + "/MazadLive/Media/.Story/images")
                file.mkdirs()
                filename = File("$path/MazadLive/Media/.Story/images/IMG_$imageName.jpg")
                val out = FileOutputStream(filename)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
                out.flush()
                out.close()
                return filename
            } catch (e: Exception) {
                e.printStackTrace()
                return null
            }


        }

        fun saveUserImage(bitmap: Bitmap): File? {
            val filename: File
            val imageName = System.currentTimeMillis().toString() + ""
            try {
                val path = Environment.getExternalStorageDirectory().toString()
                val file = File(path + "/MazadLive/Media/.User/images")
                file.mkdirs()
                filename = File("$path/MazadLive/Media/.User/images/IMG_$imageName.jpg")
                val out = FileOutputStream(filename)
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out)
                out.flush()
                out.close()
                return filename
            } catch (e: Exception) {
                e.printStackTrace()
                return null
            }


        }

        fun deleteUserImage() {

            try {
                val path = Environment.getExternalStorageDirectory().toString()
                val file = File(path + "/MazadLive/Media/.User/images")
                if (file.isDirectory) {
                    file.listFiles().forEach {
                        if (it.exists()) {
                            it.delete()
                        }
                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }


        }

        fun deletePostImage() {
            try {
                val path = Environment.getExternalStorageDirectory().toString()
                val file = File(path + "/MazadLive/Media/.Post/images")
                if (file.isDirectory) {
                    file.listFiles().forEach {
                        if (it.exists()) {
                            it.delete()
                        }
                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }


        }

        fun deleteStoryImage() {

            try {
                val path = Environment.getExternalStorageDirectory().toString()
                val file = File(path + "/MazadLive/Media/.Story/images")
                if (file.isDirectory) {
                    file.listFiles().forEach {
                        if (it.exists()) {
                            it.delete()
                        }
                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        fun deleteStreameThumbImage() {

            try {
                val path = Environment.getExternalStorageDirectory().toString()
                val file = File(path + "/MazadLive/Media/.Stream/Th_Images")
                if (file.isDirectory) {
                    file.listFiles().forEach {
                        if (it.exists()) {
                            it.delete()
                        }
                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }


        }

        fun getDateAndTime(milliSeconds: Long): String {
            var dateSend = ""
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = milliSeconds

            val dateFormat2 = SimpleDateFormat("EEE, dd-MMM-yyyy, hh:mm a")

            dateSend = dateFormat2.format(calendar.time)
            return dateSend
        }

        fun getDate(milliSeconds: Long): String {
            var dateSend = ""
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = milliSeconds

            val dateFormat2 = SimpleDateFormat("dd-MMM-yyyy")

            dateSend = dateFormat2.format(calendar.time)
            return dateSend
        }

        fun getTime(milliSeconds: Long): String {
            var dateSend = ""
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = milliSeconds

            val dateFormat2 = SimpleDateFormat("hh:mm a")

            dateSend = dateFormat2.format(calendar.time)
            return dateSend
        }

        fun getAllCountries() {

            ServiceRequest(object : ApiResponseListener {
                override fun onCompleted(`object`: Any) {

                }

                override fun onError(errorMessage: String) {

                }

            }).getAllCountry(Constant.countryList)
        }

        fun getDynamicLink(id:String):String{
            var link = ""
            val str = MyApplication.instance.getUserPreferences().getString(UserPreferences.KEY_DYNEMIC_LINK)
            var jsArray = JSONArray()

            if(!TextUtils.isEmpty(str)){
                jsArray = JSONArray(str)

                for (i in 0 until jsArray.length()){
                    val json = jsArray.getJSONObject(i)
                    if(json.has(id)){
                        link = json.getString(id)
                        break
                    }
                }
            }

            return link
        }

        fun saveDynamicLink(id:String, link:String){
            try {

                val str = MyApplication.instance.getUserPreferences().getString(UserPreferences.KEY_DYNEMIC_LINK)
                var jsArray = JSONArray()
                if(!TextUtils.isEmpty(str)){
                    jsArray = JSONArray(str)
                }

                val json = JSONObject()
                json.put(id, link)
                jsArray.put(json)

                MyApplication.instance.getUserPreferences().setString(UserPreferences.KEY_DYNEMIC_LINK,jsArray.toString())

            }catch (e:java.lang.Exception){
                e.printStackTrace()
            }
        }

        val SUBSCRIPTION_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAj4YjyNHMIMZLAJ0k7jlZg5g5rz4+cIru7ef0sB/1lAqLUjo2spD8LbiVdXoQSV+5LmTZp6Tx3dYjUFmypmavErKa0GIqcvXgZlu71myOxBZc9boO3A8BPM5IyA49pKroS" +
                "+BJs1fBTWGN/E7Hn3zUOslydIK/Ne9/dY39anR+qMc65IcF/j3Gq/d5h2huy5kR5MqczhqECH7xmd5KHPAkGhAVm" +
                "+ZQ94a0LLfF9QI6TfUGDuNtcBIn999BsqmsfisNvKB9ta6U8y2Fdj7Ghe1x2kvZzHxyKfOz/b23wM24mVvXVGZjxz29ouNrXkvrkO1YOw4Lumdly/ymzghQkVUKCQIDAQAB"

    }

}
