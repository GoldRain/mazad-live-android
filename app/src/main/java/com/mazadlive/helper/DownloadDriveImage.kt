package com.mazadlive.helper

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.AsyncTask
import android.os.Environment
import android.support.v4.provider.DocumentFile
import android.util.Log
import com.mazadlive.customdialogs.CustomProgressDialog
import java.io.File
import java.io.FileOutputStream

class DownloadDriveImage(val context: Context, val onDownloadCompletedListener: OnDownloadCompletedListener) {

    private var progressDialog: CustomProgressDialog? = null

    @SuppressLint("StaticFieldLeak")
    fun handleDocumentFromDrive(documentFile: DocumentFile) {
        object : AsyncTask<String, String, String>() {

            override fun onPreExecute() {
                progressDialog = CustomProgressDialog(context)
                progressDialog?.displayDialog(true)
                super.onPreExecute()
            }

            override fun doInBackground(vararg args: String): String? {
                try {
                    val stream = context.contentResolver.openInputStream(documentFile.uri)
                    try {
                        val path = Environment.getExternalStorageDirectory().toString()

                        val parent = File(path + "/Images")
                        parent.mkdirs()
                        val file = File(path + "/Images/" + System.currentTimeMillis() + "_" + documentFile.name)
                        val output = FileOutputStream(file)
                        try {
                            val buffer = ByteArray(4 * 1024) // or other buffer size
                            var read: Int
                            read = stream.read(buffer)
                            while (read != -1) {
                                output.write(buffer, 0, read)
                                read = stream.read(buffer)
                            }
                            output.flush()
                            (context as Activity).runOnUiThread(Runnable {
                                progressDialog?.hideDialog()
                                onDownloadCompletedListener?.onCompleted(file.path)
                            })

                        } finally {
                            output.close()
                            progressDialog?.hideDialog()
                        }
                    } finally {
                        stream.close()
                        (context as Activity).runOnUiThread(Runnable { progressDialog?.hideDialog() })
                    }
                } catch (e: Exception) {
                    Log.e("D_DriveImage", e.message)
                    (context as Activity).runOnUiThread(Runnable {
                        progressDialog?.hideDialog()
                        onDownloadCompletedListener?.onError()
                    })

                }

                return ""
            }

            override fun onPostExecute(result: String) {
                super.onPostExecute(result)
                progressDialog?.hideDialog()
            }
        }.execute()
    }

    interface OnDownloadCompletedListener {
        fun onCompleted(imagePath: String)
        fun onError()
    }

}