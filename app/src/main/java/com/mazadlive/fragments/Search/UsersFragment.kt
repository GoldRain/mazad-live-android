package com.mazadlive.fragments.Search


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.mazadlive.R
import com.mazadlive.activities.UserProfileActivity
import com.mazadlive.adapters.ProfilePostAdapter
import com.mazadlive.adapters.SearchUserAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.fragments.Main.SearchFragment
import com.mazadlive.helper.GridSpacingItemDecoration
import com.mazadlive.models.StoryModel
import com.mazadlive.models.UserModel
import com.mazadlive.parser.UserParser
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.fragment_users.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONObject

class UsersFragment : Fragment() {
    private val TAG = "UsersFragment"
    var fragPositon = 0
    var searchFragment:SearchFragment?= null
    private lateinit var fragmentView : View
    private lateinit var progress : CustomProgress

    private var userList = ArrayList<UserModel>()

    var serachFrag:SearchFragment?= null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_users, container, false)
        progress = CustomProgress(activity!!)


        fragmentView.recycleview.adapter = SearchUserAdapter(activity!!,userList,"",OnitemClick())

        fragmentView.recycleview.layoutManager = GridLayoutManager(activity,3)
        fragmentView.recycleview.addItemDecoration(GridSpacingItemDecoration(3, 6, false))

        if(fragPositon == searchFragment!!.currentPosition){
            searchData("")
        }
        return fragmentView
    }

    override fun onResume() {
        EventBus.getDefault().register(this)
        super.onResume()
    }

    override fun onPause() {
        EventBus.getDefault().unregister(this)
        super.onPause()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(fragment: Fragment){
        if(fragment is UsersFragment){
            searchData("")
        }

    }


    fun searchData(text:String){
        fragmentView.progress_bar.visibility = View.VISIBLE

        val param = JSONObject()
        param.put("version","1.1")
        param.put("text",text.trim())
        param.put("user_id",MyApplication.instance.getUserPreferences().id)
        userList.clear()
        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                fragmentView.progress_bar.visibility = View.GONE

                val jsonObject = JSONObject(`object`.toString())
                if(jsonObject.has("description")){

                    val array = jsonObject.getJSONArray("description")
                    Log.e(TAG,"SIZE :${array.length()}")
                    val parser = UserParser()
                    for (i in 0 until array.length()){
                        val obj  = array.getJSONObject(i)
                        val userModel = parser.parse(obj)
                        if (userModel.id != MyApplication.instance.getUserPreferences().id){
                            userList.add(userModel)
                        }
                    }

                    updateList()
                }
            }

            override fun onError(errorMessage: String) {
                fragmentView.progress_bar.visibility = View.GONE
                Toast.makeText(activity!!, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).searchUser(param)

    }

    private fun updateList() {
        (activity!!).runOnUiThread {
            if(userList.size > 0){
                fragmentView.tx_empty.visibility = View.GONE
            }else{
                fragmentView.tx_empty.visibility = View.VISIBLE
            }
            fragmentView.recycleview!!.adapter.notifyDataSetChanged()
        }
    }


    inner class OnitemClick:SearchUserAdapter.OnClick{
        override fun onClick(position: Int) {
            //serachFrag!!.setProfileFragment(userList[position])

            val intent = Intent(activity,UserProfileActivity::class.java)
            intent.putExtra("user",userList[position])
            intent.putExtra("isMe",false)
            startActivity(intent)
        }
    }
}
