package com.mazadlive.fragments.Search


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.mazadlive.R
import com.mazadlive.adapters.PlaceListAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ApiUrl
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.fragments.Main.SearchFragment
import com.mazadlive.models.CategoryModel
import kotlinx.android.synthetic.main.fragment_places.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONObject

class PlacesFragment : Fragment() {

    var fragPositon = 0
    var searchFragment:SearchFragment?= null

    private val TAG = "CategoriesFragment"
    private lateinit var fragmentView : View
    lateinit var  progress: CustomProgress
    private var catList = ArrayList<CategoryModel>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_places, container, false)
        progress = CustomProgress(activity!!)

        fragmentView.recycleview.layoutManager = LinearLayoutManager(activity)
        fragmentView.recycleview.adapter = PlaceListAdapter(activity!!,catList)

        if(fragPositon == searchFragment!!.currentPosition){
            getCategoryListData()
        }

        return fragmentView
    }

    override fun onResume() {
        EventBus.getDefault().register(this)
        super.onResume()
    }

    override fun onPause() {
        EventBus.getDefault().unregister(this)
        super.onPause()
    }

    private fun getCategoryListData(){

        fragmentView.mProgressBar.visibility = View.VISIBLE

        ServiceRequest(object : ApiResponseListener{

            override fun onCompleted(`object`: Any) {
                fragmentView.mProgressBar.visibility = View.GONE
                catList.clear()

                val jsonObject = JSONObject(`object`.toString())
                if(jsonObject.has("description")){
                    val array = jsonObject.getJSONArray("description")
                    for ( i in 0 until array.length()){
                        val obj = array.getJSONObject(i)
                        val categoryModel = CategoryModel()
                        if (obj.has("name")){
                            categoryModel.name = obj.getString("name")
                            categoryModel.id = obj.getString("name")
                        }
                        if (obj.has("number")){
                            categoryModel.number = obj.getString("number")
                        }
                        categoryModel.isTag = false
                        categoryModel.URL = ApiUrl.searchByPlace
                        if (categoryModel.number!!.toInt() > 0){
                            catList.add(categoryModel)
                        }
                    }

                    catList.sortWith(kotlin.Comparator { o1, o2 -> o1.name!!.compareTo(o2.name!!)})
                    (fragmentView.recycleview.adapter as PlaceListAdapter).notifyDataSetChanged()
                }
            }

            override fun onError(errorMessage: String) {
                fragmentView.mProgressBar.visibility = View.GONE
                Toast.makeText(activity!!, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
            }


        }).getplaceAndCount()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(fragment: Fragment){
        if(fragment is PlacesFragment){
            getCategoryListData()
        }
    }

}
