package com.mazadlive.fragments.Search

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.mazadlive.R
import com.mazadlive.adapters.SearchTopAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.fragments.Main.SearchFragment
import com.mazadlive.helper.GridSpacingItemDecoration
import com.mazadlive.models.PostModel
import com.mazadlive.parser.PostDataParser
import kotlinx.android.synthetic.main.fragment_top.*
import kotlinx.android.synthetic.main.fragment_top.view.*
import org.json.JSONObject
import java.lang.Exception

class TopFragment : Fragment() {
    var fragPositon = 0
    var searchFragment: SearchFragment? = null

    private lateinit var fragmentView: View
    private lateinit var progress: CustomProgress
   
    var linkPostId = ""
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_top, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progress = CustomProgress(activity!!)

        images_list.layoutManager = GridLayoutManager(activity!!, 2)
        images_list.addItemDecoration(GridSpacingItemDecoration(2, 16, true))

        Log.e("TAG", "position ::")

        searchData()
    }

    var call = false
   private fun searchData() {

       progress_bar.visibility = View.VISIBLE
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                try {
                    val postList = ArrayList<PostModel>()

                    Log.e("TOP_FRAGENNET", (`object` as JSONObject).toString())

                    if (`object`.has("sortByOrders")) {
                        val sortByOrders = `object`.getJSONArray("sortByOrders")
                        for (i in 0 until sortByOrders.length()) {
                            val postObj = sortByOrders.getJSONObject(i)
                            val postModel = PostDataParser().parse(postObj)
                            postModel.isPopular = true
                            postList.add(postModel)
                        }
                    }
                    if (`object`.has("sortByLikes")) {
                        val sortByLikes = `object`.getJSONArray("sortByLikes")
                        for (i in 0 until sortByLikes.length()) {
                            val postObj = sortByLikes.getJSONObject(i)
                            postList.add(PostDataParser().parse(postObj))
                        }
                    }

                    images_list.adapter = SearchTopAdapter(activity!!, postList)
                    (images_list.adapter as SearchTopAdapter).notifyDataSetChanged()

                    Log.e("SADASDASDASDAd"," linkPostId $linkPostId "+ postList.size.toString())

                    if(!TextUtils.isEmpty(linkPostId)){
                        for (i in 0 until postList.size){
                            if(postList[i].id.equals(linkPostId)){
                                (images_list.adapter as SearchTopAdapter).showProfileDialog(i)
                                break
                            }
                        }
                    }
                    progress_bar.visibility = View.GONE
                    linkPostId = ""
                }catch (e:Exception){
                    Log.e("SADASDASDASDAd"," Exception  "+ e.message)
                }
            }

            override fun onError(errorMessage: String) {

                if(call){
                    progress_bar.visibility = View.GONE
                    call = false
                    Toast.makeText(activity!!, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
                            }else{
                                call = false
                                searchData()
                            }

            }

        }).getSearchPopularPost()

    }
}
