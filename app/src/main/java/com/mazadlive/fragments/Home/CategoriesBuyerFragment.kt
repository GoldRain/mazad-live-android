package com.mazadlive.fragments.Home


import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.facebook.drawee.backends.pipeline.Fresco
import com.mazadlive.R
import com.mazadlive.activities.StoryCommentActivity
import com.mazadlive.adapters.FeedBuyerAdapter
import com.mazadlive.adapters.LiveStoryAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ApiUrl
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.customviews.ImageOverLayView
import com.mazadlive.models.*
import com.mazadlive.parser.StremDataParse
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.UserPreferences
import com.stfalcon.frescoimageviewer.ImageViewer
import kotlinx.android.synthetic.main.fragment_categories_buyer.view.*
import kotlinx.android.synthetic.main.view_image_overlay.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONObject
import java.util.*

class CategoriesBuyerFragment : Fragment(), FeedBuyerAdapter.OnClikPost {

    lateinit var userRef: UserPreferences
    var progress: CustomProgress? = null
    var postList = ArrayList<PostModel>()
    var storyList = ArrayList<LiveStoryModel>()
    var dupPostList = ArrayList<PostModel>()
    var fragmentView: View? = null
    val mapStory = HashMap<String, LiveStoryModel>()
    var parent: CategoriesFragment? = null

    var isRecall = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_categories_buyer, container, false)
        userRef = MyApplication.instance.getUserPreferences()
        progress = CustomProgress(activity!!)

        val linearLayoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        fragmentView!!.recycleviewstory.layoutManager = linearLayoutManager
        fragmentView!!.recycleviewstory.adapter = LiveStoryAdapter(activity!!, storyList)
        fragmentView!!.recycleviewstory.adapter.notifyDataSetChanged()


        val linearLayoutManager1 = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        fragmentView!!.recycleviewfeeds.layoutManager = linearLayoutManager1
        fragmentView!!.recycleviewfeeds.adapter = FeedBuyerAdapter(activity!!, postList, this)

        getPostFromCountry()
        getRunningStatus()

        fragmentView!!.swipe_to_refresh.setOnRefreshListener {
            getPostFromCountry()
            getRunningStatus()
        }

        return fragmentView
    }

    // If product sold delete from list
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(soldModel: SoldModel) {
        if (soldModel.type == "post"){
            for (i in 0 until postList.size) {
                if (soldModel.type_id.equals(postList[i].id)) {
                    // postList[i].isVote = voteModel.vote
                    postList[i].is_sold = true
                    (activity!!).runOnUiThread {
                        fragmentView!!.recycleviewfeeds.adapter.notifyItemChanged(i)
                    }
                    break
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(voteModel: VoteModel) {
        for (i in 0 until postList.size) {
            if (voteModel.postId.equals(postList[i].id)) {
                // postList[i].isVote = voteModel.vote
                postList[i].percent = voteModel.percent
                (activity!!).runOnUiThread {
                    fragmentView!!.recycleviewfeeds.adapter.notifyItemChanged(i)
                }
                break
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(liveStoryModel: LiveStoryModel) {

        Log.e("CHECK_USER", "** ${liveStoryModel.id}")

        if (liveStoryModel.userData == null || !userRef!!.id.equals(liveStoryModel.userData!!.id)) {

            if (!mapStory.containsKey(liveStoryModel.streamName)) {
                storyList.add(liveStoryModel)
                mapStory.put(liveStoryModel.streamName, liveStoryModel)
            } else {

                for (i in 0 until storyList.size) {
                    if (liveStoryModel.status.equals("stopped")) {
                        if (storyList[i].streamName.equals(liveStoryModel.streamName, true)) {
                            mapStory.remove(liveStoryModel.streamName)
                            storyList.removeAt(i)
                            break
                        }

                    } else {
                        if (storyList[i].streamName.equals(liveStoryModel.streamName, true)) {
                          //  mapStory.put(liveStoryModel.streamName, liveStoryModel)
                           // storyList[i] = liveStoryModel
                        }

                    }
                }

            }
        }

        (activity!!).runOnUiThread {
            if (storyList.size <= 0) {
                fragmentView!!.recycleviewstory.visibility = View.GONE
            } else {

                fragmentView!!.recycleviewstory.visibility = View.VISIBLE
            }
            fragmentView!!.recycleviewstory.adapter.notifyDataSetChanged()
        }

    }


    override fun onResume() {
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this)
        }
        super.onResume()
    }

    override fun onDestroy() {
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this)
        }
        super.onDestroy()
    }



    private fun showAllImage(position: Int) {

        val imageList2 = postList[position].postImages

        var currentImagePositon = position
        Fresco.initialize(activity!!);
        val overView = ImageOverLayView(activity!!)

        val mImageViewerBuilder = ImageViewer.Builder<PostImages>(activity!!, imageList2)
        val mImageViewer = mImageViewerBuilder.setStartPosition(position)
                .setOverlayView(overView)
                .setStartPosition(0)
                .setBackgroundColor(ContextCompat.getColor(activity!!, R.color.colorBlack))
                .setFormatter(object : ImageViewer.Formatter<PostImages> {
                    override fun format(t: PostImages?): String {
                        return t!!.url
                    }

                })

                .show()


        mImageViewerBuilder.setImageChangeListener(object : ImageViewer.OnImageChangeListener {
            override fun onImageChange(positionImage: Int) {
                currentImagePositon = positionImage
            }

        })

        overView.back.setOnClickListener {
            mImageViewer.onDismiss()
        }
    }

    override fun onClickPost(position: Int) {
        val post = postList[position]
        if (post.postImages.size > 0) {

            if (post.postImages[0].isVideo) {
                var path = post.postImages[0].url
                val ind = path.lastIndexOf(".")
                if (ind > 0) {
                    path = path.substring(0, ind) + ".mp4"
                }
                Log.e("PATH__", "$path  ***")
                val intent = Intent(activity!!, StoryCommentActivity::class.java)
                intent.putExtra("isVideo", true)
                intent.putExtra("path", path)
                startActivity(intent)
            } else {
                showAllImage(position)
            }
        }
    }

    //type: all/one , country_code: id .
    private fun getPostFromCountry() {
        if (parent == null ){
            return
        }
        val map = JSONObject()

        map.put("version", "1.1")

        if(MyApplication.instance.getUserPreferences().loginStatus) {
            if (parent!!.isAll) {
                map.put("type", "all")
            } else {
                map.put("type", "one")
                map.put("country_id", userRef!!.countryId)
            }
            map.put("user_id", MyApplication.instance.getUserPreferences().id)
        }else{

            map.put("type", "all")
        }
        Log.e("MAP_DATA", "*** ${map}")

        fragmentView!!.progress_bar.visibility = View.VISIBLE
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                //   progress!!.dismiss()
                fragmentView!!.progress_bar.visibility = View.GONE
                if (postList.size > 0) {
                    Collections.reverse(postList)
                    fragmentView!!.tx_empty.visibility = View.GONE
                } else {
                    fragmentView!!.tx_empty.visibility = View.VISIBLE
                }

                if (fragmentView!!.recycleviewfeeds.adapter != null) {
                    fragmentView!!.recycleviewfeeds.adapter.notifyDataSetChanged()
                }

                if (fragmentView!!.swipe_to_refresh.isRefreshing) {
                    fragmentView!!.swipe_to_refresh.isRefreshing = false
                }

            }

            override fun onError(errorMessage: String) {

                if(!MyApplication.instance.getUserPreferences().loginStatus && MyApplication.instance.isNetworkAvailable()){

                    if(! isRecall) {
                        Log.e("gfdg","in error second")
                        isRecall = true
                        activity!!.runOnUiThread {
                            Handler().postDelayed(object: Runnable{
                                override fun run() {
                                    getPostFromCountry()
                                }
                            },1000)
                        }
                        return
                    }
                }
                //progress!!.dismiss()
                Log.e("gfdg","in error first")
                fragmentView!!.progress_bar.visibility = View.GONE
                Toast.makeText(activity!!, "${getString(R.string.failed)} $errorMessage", Toast.LENGTH_SHORT).show()
                fragmentView!!.tx_empty.visibility = View.VISIBLE
            }

        }).getPost(postList, map, ApiUrl.getPost, false)
    }

    private fun getRunningStatus() {

        if(!MyApplication.instance.getUserPreferences().loginStatus) {

            return
        }

        val map = JSONObject()
        map.put("user_id", userRef!!.id)
        map.put("version", "1.1")
        Log.e("API_CALLING", "******* $map")
        //progress!!.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()

                val json = JSONObject(`object`.toString())
                if (json.has("description")) {
                    val array = json.getJSONArray("description")
                    storyList.clear()
                    mapStory.clear()

                    try {
                        (activity!!).runOnUiThread {
                            fragmentView!!.recycleviewstory.adapter.notifyDataSetChanged()
                        }
                    }catch (e: Exception){

                        Log.e("fdghfd", e.message)
                    }


                    val parser = StremDataParse()
                    for (i in 0 until array.length()) {

                        val streamObj = array.getJSONObject(i)
                        val storyModel =  parser.parseStream2(streamObj)

                        Log.e("ID_CHECK","${storyModel.id} ${storyModel.streamName}")
                        if (!mapStory.containsKey(storyModel.streamName)) {
                            if (!storyModel.userData!!.id.equals(userRef.id)) {
                                storyList.add(storyModel)
                                mapStory.put(storyModel.streamName, storyModel)
                            }
                        }

                    }
                }

                if (json.has("message")) {
                    if (json.getString("message").equals("No one is live")) {
                        mapStory.clear()
                        storyList.clear()

                        (activity!!).runOnUiThread {
                            if (storyList.size <= 0) {
                               fragmentView!!.recycleviewstory.visibility = View.GONE
                            } else {
                                fragmentView!!.recycleviewstory.visibility = View.VISIBLE
                            }
                            fragmentView!!.recycleviewstory.adapter.notifyDataSetChanged()
                        }
                    }
                }

                (activity!!).runOnUiThread {
                    if (storyList.size <= 0) {
                        fragmentView!!.recycleviewstory.visibility = View.GONE
                    } else {
                        fragmentView!!.recycleviewstory.visibility = View.VISIBLE
                    }

                    fragmentView!!.recycleviewstory.adapter.notifyDataSetChanged()
                }

            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                // Toast.makeText(activity!!,"Failed to get post $errorMessage",Toast.LENGTH_SHORT).show()
                // CustomDialog(activity!!).showErrorDialog("Failed to get post $errorMessage")
            }

        }).streamDetails(map)

    }


    private var amount = 0

}