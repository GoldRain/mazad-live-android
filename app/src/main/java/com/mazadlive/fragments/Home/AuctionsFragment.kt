package com.mazadlive.fragments.Home


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.facebook.drawee.backends.pipeline.Fresco

import com.mazadlive.R
import com.mazadlive.activities.StoryCommentActivity
import com.mazadlive.adapters.FeedBuyerAdapter
import com.mazadlive.adapters.LiveStoryAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ApiUrl
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.customviews.ImageOverLayView
import com.mazadlive.fragments.Main.HomeFragment
import com.mazadlive.helper.MessageEvent
import com.mazadlive.models.*
import com.mazadlive.parser.PostDataParser
import com.mazadlive.parser.StremDataParse
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.UserPreferences
import com.stfalcon.frescoimageviewer.ImageViewer
import kotlinx.android.synthetic.main.fragment_auctions.view.*
import kotlinx.android.synthetic.main.view_image_overlay.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.util.*

class AuctionsFragment : Fragment() {
   lateinit var fragmentView: View

    var fragPositon = 3
    var parent: HomeFragment?= null
    lateinit var userRef: UserPreferences
    var progress: CustomProgress? = null
    var postList = ArrayList<PostModel>()
    var storyList = ArrayList<LiveStoryModel>()
    var dupPostList = ArrayList<PostModel>()

    val mapStory = HashMap<String, LiveStoryModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_auctions, container, false)
        userRef = MyApplication.instance.getUserPreferences()
        progress = CustomProgress(activity!!)

        if (!userRef.appModeBuyer){
            fragmentView.recycleviewstory.visibility = View.GONE
        }else{
            fragmentView.recycleviewstory.visibility = View.VISIBLE
        }

        val linearLayoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        fragmentView.recycleviewstory.layoutManager = linearLayoutManager
        fragmentView.recycleviewstory.adapter = LiveStoryAdapter(activity!!, storyList)
        fragmentView.recycleviewstory.adapter.notifyDataSetChanged()

        val linearLayoutManager1 = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        fragmentView.recycleviewfeeds.layoutManager = linearLayoutManager1
        fragmentView.recycleviewfeeds.adapter = FeedBuyerAdapter(activity!!, postList, OnClickPost())



        if(parent!= null && fragPositon == parent!!.currentPosition){
            getRunningStatus()
            getPostFromCountry()
        }



        fragmentView!!.swipe_to_refresh.setOnRefreshListener {
            getRunningStatus()
            getPostFromCountry()
        }

        return fragmentView
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(fragment: Fragment) {
        if(fragment is AuctionsFragment){
            getRunningStatus()
            getPostFromCountry()
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(voteModel: VoteModel) {
        for (i in 0 until postList.size){
            if(voteModel.postId.equals(postList[i].id)){
                // postList[i].isVote = voteModel.vote
                postList[i].percent = voteModel.percent
                (activity!!).runOnUiThread {
                    fragmentView.recycleviewfeeds.adapter.notifyItemChanged(i)
                }
                break
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(liveStoryModel:LiveStoryModel) {

        Log.e("CHECK_USER", "** ${liveStoryModel.userData}")

        if(liveStoryModel.userData == null || !userRef!!.id.equals(liveStoryModel.userData!!.id)){

            if (!mapStory.containsKey(liveStoryModel.streamName)) {
                storyList.add(liveStoryModel)
                mapStory.put(liveStoryModel.streamName, liveStoryModel)
            }else{

                for (i in 0 until storyList.size){
                    if(liveStoryModel.status.equals("stopped")){
                        if(storyList[i].streamName.equals(liveStoryModel.streamName,true)){
                            mapStory.remove(liveStoryModel.streamName)
                            storyList.removeAt(i)
                            break
                        }

                    }else{
                        if(storyList[i].streamName.equals(liveStoryModel.streamName,true)){
                            mapStory.put(liveStoryModel.streamName, liveStoryModel)
                            storyList[i] = liveStoryModel
                        }

                    }
                }

            }
        }

        (activity!!).runOnUiThread {
            if (storyList.size <= 0) {
                fragmentView!!.recycleviewstory.visibility = View.GONE
            } else {

                fragmentView!!.recycleviewstory.visibility = View.VISIBLE
            }
            fragmentView!!.recycleviewstory.adapter.notifyDataSetChanged()

            if (!userRef.appModeBuyer){
                fragmentView.recycleviewstory.visibility = View.GONE
            }else{
                fragmentView.recycleviewstory.visibility = View.VISIBLE
            }
        }




    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {

        getPostFromCountry()
    }

    override fun onResume() {
        getRunningStatus()
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this)
        }
        super.onResume()
    }

    override fun onDestroy() {
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this)
        }
        super.onDestroy()
    }


    private fun showAllImage(position: Int) {

        val imageList2 = postList[position].postImages

        var currentImagePositon = position
        Fresco.initialize(activity!!);
        val overView = ImageOverLayView(activity!!)

        val mImageViewerBuilder = ImageViewer.Builder<PostImages>(activity!!, imageList2)
        val mImageViewer = mImageViewerBuilder.setStartPosition(position)
                .setOverlayView(overView)
                .setStartPosition(0)
                .setBackgroundColor(ContextCompat.getColor(activity!!, R.color.colorBlack))
                .setFormatter(object : ImageViewer.Formatter<PostImages> {
                    override fun format(t: PostImages?): String {
                        return t!!.url
                    }

                })

                .show()


        mImageViewerBuilder.setImageChangeListener(object : ImageViewer.OnImageChangeListener {
            override fun onImageChange(positionImage: Int) {
                currentImagePositon = positionImage
            }

        })

        overView.back.setOnClickListener {
            mImageViewer.onDismiss()
        }


    }


    //type: all/one , country_code: id .
    private fun getPostFromCountry() {

        val map = JSONObject()
        map.put("type", "one")
        map.put("country_id", userRef!!.countryId)
        map.put("version", "1.1")

        Log.e("MAP_DATA", "*** ${map}")

        var url = ApiUrl.getPost
        if(!userRef.appModeBuyer){
            url = ApiUrl.getMyPosts
        }

        postList.clear()

        fragmentView.progress_bar.visibility = View.VISIBLE
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                //   progress!!.dismiss()
                val response = JSONObject(`object`.toString())
                if (!response.getBoolean("error")) {
                    if(response.has("description")){
                       val parser =  PostDataParser()
                        val postObjArray = response.getJSONArray("description")
                        for (i in 0 until postObjArray.length()){
                            val postModel = parser.parse(postObjArray.getJSONObject(i))
                            if(postModel.is_auction){
                                postList.add(postModel)
                            }
                        }
                    }

                }

                fragmentView.progress_bar.visibility = View.GONE
                if(postList.size >0){
                    Collections.reverse(postList)
                    fragmentView.tx_empty.visibility = View.GONE
                }else{
                    fragmentView.tx_empty.visibility = View.VISIBLE
                }

                if (fragmentView.recycleviewfeeds.adapter != null) {
                    fragmentView.recycleviewfeeds.adapter.notifyDataSetChanged()
                }

                if(fragmentView.swipe_to_refresh.isRefreshing){
                    fragmentView.swipe_to_refresh.isRefreshing = false
                }

            }

            override fun onError(errorMessage: String) {
                //progress!!.dismiss()
                fragmentView.progress_bar.visibility = View.GONE
               // Toast.makeText(activity!!, "Failed  $errorMessage", Toast.LENGTH_SHORT).show()
                fragmentView.tx_empty.visibility = View.VISIBLE
            }

        }).getAuctionPost( map, url,false)
    }

    private fun getRunningStatus() {
        val map = JSONObject()
        map.put("user_id", userRef!!.id)
        map.put("version", "1.1")

        Log.e("API_CALLING", "******* $map")
        //progress!!.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()

                val json = JSONObject(`object`.toString())
                if (json.has("description")) {
                    val array = json.getJSONArray("description")
                    storyList.clear()
                    mapStory.clear()
                    (activity!!).runOnUiThread {
                        fragmentView!!.recycleviewstory.adapter.notifyDataSetChanged()
                    }
                    val parser = StremDataParse()
                    for (i in 0 until array.length()) {

                        val streamObj = array.getJSONObject(i)
                        val storyModel =  parser.parseStream2(streamObj)
                        if (!mapStory.containsKey(storyModel.streamName)) {
                            if (!storyModel!!.userData!!.id.equals(userRef.id)) {
                                storyList.add(storyModel)
                                mapStory.put(storyModel.streamName, storyModel)
                            }
                        }

                    }
                }

                if (json.has("message")) {
                    if (json.getString("message").equals("No one is live")) {
                        mapStory.clear()
                        storyList.clear()

                        (activity!!).runOnUiThread {
                            if (storyList.size <= 0) {
                                fragmentView!!.recycleviewstory.visibility = View.GONE
                            } else {
                                fragmentView!!.recycleviewstory.visibility = View.VISIBLE
                            }
                            fragmentView!!.recycleviewstory.adapter.notifyDataSetChanged()
                        }
                    }
                }

                (activity!!).runOnUiThread {
                    if (storyList.size <= 0) {
                        fragmentView!!.recycleviewstory.visibility = View.GONE
                    } else {
                        fragmentView!!.recycleviewstory.visibility = View.VISIBLE
                    }

                    fragmentView!!.recycleviewstory.adapter.notifyDataSetChanged()

                    if (!userRef.appModeBuyer){
                        fragmentView.recycleviewstory.visibility = View.GONE
                    }else{
                        fragmentView.recycleviewstory.visibility = View.VISIBLE
                    }
                }

            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                // Toast.makeText(activity!!,"Failed to get post $errorMessage",Toast.LENGTH_SHORT).show()
                // CustomDialog(activity!!).showErrorDialog("Failed to get post $errorMessage")
            }

        }).streamDetails(map)


    }



    inner class OnClickPost:FeedBuyerAdapter.OnClikPost{
        override fun onClickPost(position: Int) {
            val post = postList[position]
            if(post.postImages.size >0){

                if(post.postImages[0].isVideo){
                    var path = post.postImages[0].url
                    val ind = path.lastIndexOf(".")
                    if(ind >0){
                        path = path.substring(0,ind)+".mp4"
                    }
                    Log.e("PATH__","$path  ***")
                    val intent = Intent(activity!!, StoryCommentActivity::class.java)
                    intent.putExtra("isVideo",true)
                    intent.putExtra("path",path)
                    startActivity(intent)
                }else{
                    showAllImage(position)
                }
            }
        }

    }

    fun setRefeshData(){
        getPostFromCountry()
    }


}
