package com.mazadlive.fragments.Home

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.facebook.drawee.backends.pipeline.Fresco
import com.mazadlive.R
import com.mazadlive.activities.StoryCommentActivity
import com.mazadlive.adapters.FeedSellerAdapter
import com.mazadlive.adapters.LiveStoryAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ApiUrl
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.customviews.ImageOverLayView
import com.mazadlive.database.AppDatabase
import com.mazadlive.database.FavPost
import com.mazadlive.helper.Constant
import com.mazadlive.models.*
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.UserPreferences
import com.stfalcon.frescoimageviewer.ImageViewer
import kotlinx.android.synthetic.main.fragment_categories_seller.view.*
import kotlinx.android.synthetic.main.view_image_overlay.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.util.*

class CategoriesSellerFragment : Fragment(),FeedSellerAdapter.OnClikPost {

    lateinit var fragmentView : View
    lateinit var userRef: UserPreferences
    var progress : CustomProgress?= null
    var postList = ArrayList<PostModel>()
    var dupPostList = ArrayList<PostModel>()
    var parent: CategoriesFragment?= null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_categories_seller, container, false)
        userRef = MyApplication.instance.getUserPreferences()
        progress = CustomProgress(activity!!)


        val linearLayoutManager1 = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        fragmentView.recycleviewfeeds.layoutManager = linearLayoutManager1
        fragmentView.recycleviewfeeds.adapter = FeedSellerAdapter(activity!!, postList,this)
        fragmentView.recycleviewfeeds.adapter.notifyDataSetChanged()

        getPostFromCountry()


        fragmentView!!.swipe_to_refresh.setOnRefreshListener {
            getPostFromCountry()
        }
        return fragmentView
    }

    override fun onResume() {
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this)
        }
        super.onResume()
    }

    override fun onDestroy() {
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this)
        }
        super.onDestroy()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(voteModel: VoteModel) {
        for (i in 0 until postList.size){
            if(voteModel.postId.equals(postList[i].id)){
                // postList[i].isVote = voteModel.vote
                postList[i].percent = voteModel.percent
                (activity!!).runOnUiThread {
                    fragmentView!!.recycleviewfeeds.adapter.notifyItemChanged(i)
                }
                break
            }
        }
    }
    private fun showAllImage(position: Int) {

        val imageList2 = postList[position].postImages

        var currentImagePositon = position
        Fresco.initialize(activity!!);
        val overView = ImageOverLayView(activity!!)

        val mImageViewerBuilder = ImageViewer.Builder<PostImages>(activity!!, imageList2)
        val mImageViewer = mImageViewerBuilder.setStartPosition(position)
                .setOverlayView(overView)
                .setStartPosition(0)
                .setBackgroundColor(ContextCompat.getColor(activity!!, R.color.colorBlack))
                .setFormatter(object : ImageViewer.Formatter<PostImages> {
                    override fun format(t: PostImages?): String {
                        return t!!.url
                    }

                })

                .show()


        mImageViewerBuilder.setImageChangeListener(object : ImageViewer.OnImageChangeListener {
            override fun onImageChange(positionImage: Int) {
                currentImagePositon = positionImage
            }

        })

        overView.back.setOnClickListener {
            mImageViewer.onDismiss()
        }


    }

    override fun onClickPost(position: Int) {
        val post = postList[position]
        if(post.postImages.size >0){
            if(post.postImages[0].isVideo){
                var path = post.postImages[0].url
                val ind = path.lastIndexOf(".")
                if(ind >0){
                    path = path.substring(0,ind)+".mp4"
                }
                Log.e("PATH__","$path  ***")
                val intent = Intent(activity!!, StoryCommentActivity::class.java)
                intent.putExtra("isVideo",true)
                intent.putExtra("path",path)
                startActivity(intent)
            }else{
                showAllImage(position)
            }
        }
    }


    //type: all/one , country_code: id .
    private fun getPostFromCountry() {
        val map = JSONObject()
        map.put("user_id",userRef.id)
        //map.put("country_id",userRef!!.countryId)
        map.put("version","1.1")

        fragmentView!!.progress_bar.visibility = View.VISIBLE
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                fragmentView!!.progress_bar.visibility = View.GONE

                if(postList.size >0){
                    Collections.reverse(postList)
                    fragmentView!!.tx_empty.visibility = View.GONE
                }else{
                    fragmentView!!.tx_empty.visibility = View.VISIBLE
                }
                fragmentView.recycleviewfeeds.adapter.notifyDataSetChanged()

                if(fragmentView!!.swipe_to_refresh.isRefreshing){
                    fragmentView!!.swipe_to_refresh.isRefreshing = false
                }

            }

            override fun onError(errorMessage: String) {
                fragmentView!!.progress_bar.visibility = View.GONE
                fragmentView!!.tx_empty.visibility = View.VISIBLE
                Toast.makeText(activity!!,"${getString(R.string.failed)} $errorMessage", Toast.LENGTH_SHORT).show()

            }

        }).getPost(postList,map,ApiUrl.getMyPosts,false)
    }
}
