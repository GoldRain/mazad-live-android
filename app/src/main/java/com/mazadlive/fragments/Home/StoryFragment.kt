package com.mazadlive.fragments.Home


import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.opengl.Visibility
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.github.florent37.runtimepermission.kotlin.askPermission
import com.lite.imagepickerlib.GalleryActivity
import com.mazadlive.Interface.DialogListener

import com.mazadlive.R
import com.mazadlive.activities.HomeActivity
import com.mazadlive.activities.StoryActivity
import com.mazadlive.activities.StoryPreviewActivity
import com.mazadlive.adapters.StoryAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.fragments.Main.HomeFragment
import com.mazadlive.helper.MessageEvent
import com.mazadlive.models.PostModel
import com.mazadlive.models.StoryModel
import com.mazadlive.models.SubscribeModel
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.UserPreferences
import kotlinx.android.synthetic.main.fragment_story.*
import kotlinx.android.synthetic.main.fragment_story.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap


/**
 * A simple [Fragment] subclass.
 */
class StoryFragment : Fragment(),StoryAdapter.OnClikPost {

    private val TAG = "StoryFragment"
    lateinit var userRef: UserPreferences
    var progress: CustomProgress? = null
    var storyList = ArrayList<StoryModel>()
    var fragmentView: View? = null

    val mapStory = HashMap<String,StoryModel>()

    private val GALLARY_CODE = 1000
    private val STATUS_PREVIEW_CODE = 1001

    var parent:HomeFragment?= null
    var fragPositon = 0

    var linkPostId = ""
    
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(liveStoryModel: StoryModel) {

        if(!mapStory.containsKey(liveStoryModel.id)){
            storyList.add(0,liveStoryModel)
            mapStory.put(liveStoryModel.id,liveStoryModel)
            (activity!!).runOnUiThread {
                recycleviewfeeds.adapter.notifyDataSetChanged()
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(fragment: Fragment) {
        if(fragment is StoryFragment){
            getAllStoryList()
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        getAllStoryList()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_story, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userRef = MyApplication.instance.getUserPreferences()
        progress = CustomProgress(activity!!)
        storyList = ArrayList()

        val linearLayoutManager1 = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        recycleviewfeeds.layoutManager = linearLayoutManager1
        recycleviewfeeds.adapter = StoryAdapter(activity!!,storyList,this)

        Glide.with(activity!!).load(userRef.profile).apply(RequestOptions().override(80).error(R.drawable.dummy)).into(mProfile)

        add_btn.setOnClickListener {
            getAllCounts()
        }

        swipe_to_refresh.setOnRefreshListener {
            getAllStoryList()
        }

        if(parent != null && fragPositon == parent!!.currentPosition){
            getAllStoryList()
        }

    }

    private fun getAllCounts() {
        progress!!.show()
        val param = JSONObject()
        param.put("version","1.1")
        param.put("user_id",userRef.id)

        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                val jsonObject = JSONObject(`object`.toString())
                if(jsonObject.has("description")){
                    val obj = jsonObject.getJSONObject("description")
                    if(obj.has("available_story_count")){
                        val count = obj.getInt("available_story_count")
                        if(count == 0){
                            CustomDialog(activity!!).showAlertDialog("Alert",getString(R.string.warning_story_limit))
                        }else{
                            openGallary()
                        }
                    }

                    //unlimited_story_count
                    if(obj.has("unlimited_story_count")){
                        val subObj  =obj.getJSONObject("unlimited_story_count")
                        val subModel = SubscribeModel()
                        if(subObj.has("status")){
                            subModel.status = subObj.getBoolean("status")
                            //  ad1.status = subObj.getBoolean("status")
                            //   ad4.status = subObj.getBoolean("status")
                        }

                        if(subObj.has("activation")){
                            subModel.activation = subObj.getString("activation")
                            //ad1.activation = subObj.getString("activation")
                            //     .//    ad4.activation = subObj.getString("activation")
                        }

                        if(subObj.has("expiry")){
                            subModel.expiry = subObj.getString("expiry")
                            /* ad1.expiry = subObj.getString("expiry")
                             ad4.expiry = subObj.getString("expiry")*/
                        }
                        if(subObj.has("count")){
                            subModel.previousCount = subObj.getInt("count")

                            /* ad4.available = subObj.getInt("count").toString()
                             ad1.available = subObj.getInt("count").toString()*/
                        }

                        val count = subModel.previousCount
                        if (!subModel.status && count == 0) {
                            CustomDialog(activity!!).showAlertDialog("Alert", getString(R.string.warning_story_limit))
                        }else{
                            openGallary()
                        }
                        //   ad4.subModel = subModel
                    }
                }

            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(activity!!,"${getString(R.string.failed)}  $errorMessage",Toast.LENGTH_SHORT).show()
            }

        }).showCounts(param)
    }

    private fun getAllStoryList() {

        val param = JSONObject()
        param.put("version","1.1")
        param.put("user_id",userRef.id)

        if(userRef.appModeBuyer){
            fragmentView?.add_btn?.visibility = View.GONE
            param.put("type","buyer")
        }else{
            fragmentView?.add_btn?.visibility = View.VISIBLE
            param.put("type","seller")
        }

       // progress!!.show()
        storyList.clear()
        progress_bar.visibility = View.VISIBLE
        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                progress_bar.visibility = View.GONE

                if(storyList.size >0){
                    Collections.sort(storyList,SortStory())
                    tx_empty.visibility = View.GONE
                    recycleviewfeeds.visibility = View.VISIBLE
                }else{
                    tx_empty.visibility = View.VISIBLE
                    recycleviewfeeds.visibility = View.GONE
                }
                recycleviewfeeds.adapter.notifyDataSetChanged()

                if(!TextUtils.isEmpty(linkPostId)){
                    for (i in 0 until storyList.size){
                        if(linkPostId.equals(storyList[i].id)){
                            (recycleviewfeeds.adapter as  StoryAdapter).openAllStory(i)
                            break
                        }
                    }
                }
                linkPostId = ""

                //    Log.e(TAG,"${swipe_to_refresh.isRefreshing}")
                if(swipe_to_refresh.isRefreshing){
                    swipe_to_refresh.isRefreshing = false
                }



            }

            override fun onError(errorMessage: String) {
                progress_bar.visibility = View.GONE
                Toast.makeText(activity!!,"${getString(R.string.failed)} $errorMessage",Toast.LENGTH_SHORT).show()
                tx_empty.visibility = View.VISIBLE
            }

        }).getMyStoryList(storyList,param)
    }


    private fun openGallary() {
        askPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA) {

            val intent = Intent(activity!!, GalleryActivity::class.java)
            intent.putExtra(GalleryActivity.KEY_MAX_SELECTION,1)
            intent.putExtra(GalleryActivity.KEY_ALLOW_MULTIPLE,true)
            startActivityForResult(intent,GALLARY_CODE)


        }.onDeclined { result ->
            if (result.hasDenied()) {
                CustomDialog(activity!!).showErrorDialog(getString(R.string.permission_required))
            } else if (result.hasForeverDenied()) {
                result.foreverDenied.forEach {
                    CustomDialog(activity!!).permissionDialog(getString(R.string.accept_permission), object : DialogListener() {
                        override fun yesClick() {
                            super.yesClick()
                            result.goToSettings()
                        }
                    })
                }
            }
        }

    }

    override fun onResume() {
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this)
        }
        super.onResume()
    }

    override fun onDestroy() {
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this)
        }
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.e(TAG,"onActivityResult $requestCode")
        if( resultCode== Activity.RESULT_OK && requestCode == GALLARY_CODE ){

            //  val arrayList = data!!.getStringArrayListExtra("path_list")
            val arrayList = data!!.getStringArrayListExtra("images")
            Log.e(TAG,"onActivityResult ${arrayList.size}")
            val isVideo = data!!.getBooleanExtra("isVideo",false)
            val intent = Intent(activity!!,StoryPreviewActivity::class.java)
            intent.putStringArrayListExtra("images",arrayList)
            intent.putExtra("isVideo",isVideo)
            (activity!!).startActivityForResult(intent,STATUS_PREVIEW_CODE)
        }

        if( resultCode== Activity.RESULT_OK && requestCode == STATUS_PREVIEW_CODE ){
            Log.e(TAG,"onActivityResult")
            getAllStoryList()
        }
    }

    override fun onClickPost(position: Int) {

        Log.e(TAG,"${storyList[position].is_video}")


    }

    inner class SortStory:Comparator<StoryModel>{
        override fun compare(o1: StoryModel?, o2: StoryModel?): Int {

            if(o1!!.createdAt.toLong() < o2!!.createdAt.toLong()){
                return 1
            }
            return -1
        }

    }

}