package com.mazadlive.fragments.Home

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.mazadlive.R
import com.mazadlive.helper.MessageEvent
import com.mazadlive.utils.MyApplication
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class LyricsFragment : Fragment() {

    lateinit var fragmentView: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_lyrics, container, false)
       /* if (MyApplication.instance.getUserPreferences().appModeBuyer){
            childFragmentManager.beginTransaction()
                    .replace(R.id.container_root, CategoriesBuyerFragment())
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .addToBackStack(null)
                    .commit()
        }else{
            childFragmentManager.beginTransaction()
                    .replace(R.id.container_root, CategoriesSellerFragment())
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .addToBackStack(null)
                    .commit()
        }*/


        return fragmentView
    }

    override fun onResume() {
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this)
        }
        super.onResume()
    }

    override fun onDestroy() {
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this)
        }
        super.onDestroy()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        /*if (event.flag  ){
            childFragmentManager.beginTransaction()
                    .replace(R.id.container_root, CategoriesBuyerFragment())
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .addToBackStack(null)
                    .commit()
        }else{
            childFragmentManager.beginTransaction()
                    .replace(R.id.container_root, CategoriesSellerFragment())
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .addToBackStack(null)
                    .commit()
        }*/
    }
}