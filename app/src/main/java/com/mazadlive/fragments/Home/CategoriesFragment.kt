package com.mazadlive.fragments.Home

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mazadlive.R
import com.mazadlive.fragments.Main.HomeFragment
import com.mazadlive.helper.MessageEvent
import com.mazadlive.utils.MyApplication
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class CategoriesFragment : Fragment() {

    lateinit var fragmentView: View
    var fragPositon = 1
    var parent: HomeFragment?= null
     var isAll = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_root, container, false)


       if(parent != null){
           if(fragPositon == parent!!.currentPosition){
               setFragment()
           }
       }
        return fragmentView
    }

    override fun onResume() {
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this)
        }
        super.onResume()
    }

    override fun onDestroy() {
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this)
        }
        super.onDestroy()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(fragment: Fragment) {
        if(fragment is CategoriesFragment){
           setFragment()
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        Log.e("onMessageEvent","EVENT ${event.flag}")
        setFragment()
    }

    fun setFragment(){
        isAll = parent!!.isAll

        if (MyApplication.instance.getUserPreferences().appModeBuyer){
            val categoriesFrag = CategoriesBuyerFragment()
            categoriesFrag.parent = this
            childFragmentManager.beginTransaction()
                    .replace(R.id.container_root, categoriesFrag)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .addToBackStack(null)
                    .commit()
        }else{
            val categoriesFrag = CategoriesSellerFragment()
            categoriesFrag.parent = this
            childFragmentManager.beginTransaction()
                    .replace(R.id.container_root,categoriesFrag)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .addToBackStack(null)
                    .commit()
        }
    }

    fun setRefeshData(){
        setFragment()
    }
}