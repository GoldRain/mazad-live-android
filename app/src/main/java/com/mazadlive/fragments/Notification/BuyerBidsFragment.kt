package com.mazadlive.fragments.Notification


import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.*
import android.widget.Toast
import com.mazadlive.R
import com.mazadlive.adapters.*
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ApiUrl
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.database.AppDatabase
import com.mazadlive.helper.DividerItemDecorator
import com.mazadlive.helper.GridSpacingItemDecoration
import com.mazadlive.models.*
import com.mazadlive.parser.PostDataParser
import com.mazadlive.parser.StoryDataParse
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.SortBid
import kotlinx.android.synthetic.main.customdialog_post_view_recycle.*
import kotlinx.android.synthetic.main.fragment_bids.view.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

class BuyerBidsFragment : Fragment() {

    lateinit var progress : CustomProgress
    lateinit var fragmentView:View
    var bidsList = ArrayList<FeedSellerModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_bids, container, false)


        fragmentView.bids_list.layoutManager = LinearLayoutManager(activity!!)
        fragmentView.bids_list.addItemDecoration(DividerItemDecorator(ContextCompat.getDrawable(activity!!, R.drawable.divider_line)))
        //fragmentView.bids_list.addItemDecoration(GridSpacingItemDecoration(1,20,false))
        fragmentView.bids_list.adapter = PostBidsListBuyerAdapter(activity!!, bidsList)
        (fragmentView.bids_list.adapter as PostBidsListBuyerAdapter).listener = OnItemClick()

        fragmentView.swipe_to_refresh.setOnRefreshListener {
            getBidList()
        }

        getBidList()

        return fragmentView
    }

    private fun getBidList() {
        val map = JSONObject()
        map.put("user_id", MyApplication.instance.getUserPreferences().id)
        map.put("version","1.1")
        map.put("type","buyer")
        Log.e("PARAM","$map")

        fragmentView.progress_bar.visibility = View.VISIBLE
        bidsList.clear()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                fragmentView.progress_bar.visibility = View.GONE

                val response = JSONObject(`object`.toString())
                if(response.has("post")){
                    val postObjArray = response.getJSONArray("post")

                    val parser = PostDataParser()
                    for (i in 0 until postObjArray.length()) {

                        val postObj = postObjArray.getJSONObject(i)
                        val postModel = parser.parseForBid(postObj)

                        doAsync {
                            val favPost = AppDatabase.getAppDatabase().favPostDao().getFavPost(postModel.id)
                            postModel.isFav = favPost.post_id.equals(postModel.id, true)
                        }
                        if(postModel.userBid != null){
                            val feedSellerModel = FeedSellerModel()
                            feedSellerModel.type = "post"
                            feedSellerModel.typeId = postModel.id
                            feedSellerModel.post = postModel
                            feedSellerModel.created_at = postModel.userBid!!.createdAt
                            bidsList.add(feedSellerModel)
                        }
                    }
                }

                if (response.has("story")) {
                    val postObjArray = response.getJSONArray("story")

                    val parser = StoryDataParse()
                    for (i in 0 until postObjArray.length()) {

                        val postObj = postObjArray.getJSONObject(i)
                        val storyModel = parser.parse(postObj)
                        val feedSellerModel = FeedSellerModel()
                        feedSellerModel.type = "story"
                        feedSellerModel.typeId = storyModel.id
                        feedSellerModel.story = storyModel
                        feedSellerModel.created_at = storyModel.userBid!!.createdAt
                        bidsList.add(feedSellerModel)
                    }
                }

                Collections.sort(bidsList,SortBid())
                fragmentView.bids_list.adapter.notifyDataSetChanged()

                if(fragmentView.swipe_to_refresh.isRefreshing){
                    fragmentView.swipe_to_refresh.isRefreshing = false
                }

                if(bidsList.size <= 0){
                    fragmentView.tx_empty.visibility = View.VISIBLE
                }else{
                    fragmentView.tx_empty.visibility = View.GONE
                }

            }

            override fun onError(errorMessage: String) {
                fragmentView!!.progress_bar.visibility = View.GONE
                if(fragmentView.swipe_to_refresh.isRefreshing){
                    fragmentView.swipe_to_refresh.isRefreshing = false
                }
                if(bidsList.size <= 0){
                    fragmentView.tx_empty.visibility = View.VISIBLE
                }else{
                    fragmentView.tx_empty.visibility = View.GONE
                }

              //  Toast.makeText(activity!!,"Failed$errorMessage", Toast.LENGTH_SHORT).show()

            }

        }).getBidsForBuyer(map)
    }

    inner class OnItemClick:PostBidsListBuyerAdapter.OnItemClick{
        override fun onItemClick(position: Int) {

           // Log.e("POST_TYPE","${bidsList[position].type}")
            when(bidsList[position].type){
                "post" ->{

                    showPostDialog(position)
                }

                "story" ->{
                    showStoryDialog(position)
                }


            }
        }
    }

    private fun showPostDialog(position: Int) {

        val dialog = Dialog(activity!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.customdialog_post_view_recycle)
        val wlp = dialog.window.attributes
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog.window.attributes = wlp

        val tempPostList = ArrayList<PostModel>()

        tempPostList.add(bidsList[position].post!!)
        dialog.recyclerview.layoutManager = LinearLayoutManager(activity!!,LinearLayoutManager.HORIZONTAL,false)
        dialog.recyclerview.adapter = FeedBuyerAdapter(activity!!,tempPostList,object : FeedBuyerAdapter.OnClikPost{
            override fun onClickPost(position: Int) {

            }
        })

        (dialog.recyclerview.adapter as FeedBuyerAdapter).reportListener = object : FeedBuyerAdapter.ReportListener(){
            override fun onReport(position: Int) {
                dialog.dismiss()
                getBidList()
            }

        }
        dialog.recyclerview.adapter.notifyDataSetChanged()
        dialog.recyclerview.scrollToPosition(position)
        dialog.show()
    }

    private fun showStoryDialog(position: Int) {

        val dialog = Dialog(activity!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.customdialog_post_view_recycle)
        val wlp = dialog.window.attributes
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog.window.attributes = wlp

        val tempPostList = ArrayList<StoryModel>()

        tempPostList.add(bidsList[position].story!!)
        dialog.recyclerview.layoutManager = LinearLayoutManager(activity!!,LinearLayoutManager.HORIZONTAL,false)
        dialog.recyclerview.adapter = StoryAdapter(activity!!,tempPostList,object : StoryAdapter.OnClikPost{
            override fun onClickPost(position: Int) {

            }
        })

        dialog.recyclerview.adapter.notifyDataSetChanged()
        dialog.recyclerview.scrollToPosition(position)
        dialog.show()
    }
}
