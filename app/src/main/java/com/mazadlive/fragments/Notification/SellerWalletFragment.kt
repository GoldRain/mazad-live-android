package com.mazadlive.fragments.Notification


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.mazadlive.R
import com.mazadlive.adapters.SellerWalletListAdapter
import com.mazadlive.adapters.WalletListAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.helper.DividerItemDecorator
import com.mazadlive.models.WalletModel
import com.mazadlive.parser.WalletParser
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.SortWallet
import kotlinx.android.synthetic.main.fragment_wallet.view.*
import org.json.JSONObject
import java.util.*

class SellerWalletFragment : Fragment() {

    private lateinit var fragmentView : View
    private val walletList = ArrayList<WalletModel>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_wallet, container, false)

        fragmentView.wallet_list.layoutManager = LinearLayoutManager(activity!!)
        fragmentView.wallet_list.addItemDecoration(DividerItemDecorator(ContextCompat.getDrawable(activity!!, R.drawable.divider_line)))
        fragmentView.wallet_list.adapter = SellerWalletListAdapter(activity!!, walletList)

        fragmentView.swipe_to_refresh.setOnRefreshListener {
            getWalletList()
        }
        getWalletList()
        return fragmentView
    }


    fun  getWalletList(){
        val param = JSONObject()
        param.put("version", "1.1")
        param.put("user_id", MyApplication.instance.getUserPreferences().id)
        param.put("type_id", MyApplication.instance.getUserPreferences().id)
        param.put("type", "seller")
        if(MyApplication.instance.getUserPreferences().appModeBuyer){
            param.put("type", "buyer")
        }


        walletList.clear()
        fragmentView!!.progress_bar.visibility = View.VISIBLE

        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                fragmentView!!.progress_bar.visibility = View.GONE

                val jsonObject = JSONObject(`object`.toString())
                if(jsonObject.has("description")){

                    val array = jsonObject.getJSONArray("description")

                    val parser = WalletParser()
                    for (i in 0 until array.length()){
                        val walletModel = parser.parse(array.getJSONObject(i))
                        walletList.add(walletModel)
                    }

                    if(walletList.size >0){
                        Collections.sort(walletList,SortWallet())
                        fragmentView.tx_empty.visibility = View.GONE
                    }else{
                        fragmentView.tx_empty.visibility = View.VISIBLE
                    }

                    if(fragmentView.swipe_to_refresh.isRefreshing){
                        fragmentView.swipe_to_refresh.isRefreshing =false
                    }

                    fragmentView.wallet_list.adapter.notifyDataSetChanged()
                }

            }

            override fun onError(errorMessage: String) {

                if(walletList.size >0){
                    fragmentView.tx_empty.visibility = View.GONE
                }else{
                    fragmentView.tx_empty.visibility = View.VISIBLE
                }

                if(fragmentView.swipe_to_refresh.isRefreshing){
                    fragmentView.swipe_to_refresh.isRefreshing =false
                }
                fragmentView!!.progress_bar.visibility = View.GONE
            }

        }).walletList(param)
    }
}