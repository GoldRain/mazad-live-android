package com.mazadlive.fragments.Notification


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.androidnetworking.model.Progress

import com.mazadlive.R
import com.mazadlive.adapters.ActivitiesListAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.models.ActivityModel
import com.mazadlive.models.OrderModel
import com.mazadlive.parser.ActivityParser
import com.mazadlive.parser.OrderParser
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.fragment_activities.view.*
import org.json.JSONObject

class ActivitiesFragment : Fragment() {

    private lateinit var fragmentView : View
    private val activitiesList = ArrayList<ActivityModel>()

    private lateinit var progress: CustomProgress

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        fragmentView = inflater.inflate(R.layout.fragment_activities, container, false)
        progress = CustomProgress(activity!!)

        fragmentView.activities_list.layoutManager = LinearLayoutManager(activity!!)
        fragmentView.activities_list.addItemDecoration(DividerItemDecoration(activity!!, DividerItemDecoration.VERTICAL))
        fragmentView.activities_list.adapter = ActivitiesListAdapter(activity!!, activitiesList)

        fragmentView.swipe_to_refresh.setOnRefreshListener {
            getActivitiesList()
        }

        getActivitiesList()
        return fragmentView
    }

    private fun getActivitiesList() {
        val param = JSONObject()
        param.put("version","1.1")
        param.put("user_id", MyApplication.instance.getUserPreferences().id)


        activitiesList.clear()
        fragmentView.progress_bar.visibility = View.VISIBLE

        ServiceRequest(object : ApiResponseListener {

            override fun onCompleted(`object`: Any) {

                fragmentView.progress_bar.visibility = View.GONE
                val activityParser =  ActivityParser()

                val jsonObject = JSONObject(`object`.toString())
                if(jsonObject.has("description")){
                    val array = jsonObject.getJSONArray("description")
                    for (i in 0 until array.length()){
                        val orderModel = activityParser.parse(array.getJSONObject(i))
                        activitiesList.add(orderModel)
                    }

                    (activity)!!.runOnUiThread {
                        fragmentView.activities_list.adapter.notifyDataSetChanged()
                        if(fragmentView.swipe_to_refresh.isRefreshing){
                            fragmentView.swipe_to_refresh.isRefreshing = false
                        }

                        if(activitiesList.size <= 0){
                            fragmentView.tx_empty.visibility = View.VISIBLE
                        }else{
                            fragmentView.tx_empty.visibility = View.GONE
                        }
                    }
                }
            }

            override fun onError(errorMessage: String) {
                fragmentView.progress_bar.visibility = View.GONE
                if(fragmentView.swipe_to_refresh.isRefreshing){
                    fragmentView.swipe_to_refresh.isRefreshing = false
                }

                if(activitiesList.size <= 0){
                    fragmentView.tx_empty.visibility = View.VISIBLE
                }else{
                    fragmentView.tx_empty.visibility = View.GONE
                }

            }

        }).activities(param)
    }

}
