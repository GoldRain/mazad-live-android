package com.mazadlive.fragments.Notification


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.mazadlive.R
import com.mazadlive.activities.BidListActivity
import com.mazadlive.activities.StoryActivity
import com.mazadlive.adapters.BidsListAdapter
import com.mazadlive.adapters.PostBidListSellerAdapter
import com.mazadlive.adapters.PostBidsListAdapter
import com.mazadlive.adapters.PostFeedListSellerAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ApiUrl
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.database.AppDatabase
import com.mazadlive.helper.DividerItemDecorator
import com.mazadlive.helper.GridSpacingItemDecoration
import com.mazadlive.models.BidModel
import com.mazadlive.models.FeedSellerModel
import com.mazadlive.models.PostModel
import com.mazadlive.parser.PostDataParser
import com.mazadlive.parser.StoryDataParse
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.fragment_bids_seller.view.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.lang.Exception
import java.util.*

class SellerBidsFragment : Fragment() {

    lateinit var progress :CustomProgress
    lateinit var fragmentView:View
    var postList = ArrayList<PostModel>()
    var bidsList = ArrayList<FeedSellerModel>()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
         fragmentView = inflater.inflate(R.layout.fragment_bids_seller, container, false)

        fragmentView.bids_list.layoutManager = LinearLayoutManager(activity!!)
        //fragmentView.bids_list.addItemDecoration(GridSpacingItemDecoration(1,20,false))
        fragmentView.bids_list.addItemDecoration(DividerItemDecorator(ContextCompat.getDrawable(activity!!, R.drawable.divider_line)))
        fragmentView.bids_list.adapter = PostBidListSellerAdapter(activity!!, bidsList)

        (fragmentView.bids_list.adapter as PostBidListSellerAdapter).listener = OnItemClick()

        fragmentView.swipe_to_refresh.setOnRefreshListener {
           getBidList()
        }
        return fragmentView
    }

    override fun onResume() {
        getBidList()
        super.onResume()
    }


    private fun getBidList() {
        val map = JSONObject()
        map.put("user_id", MyApplication.instance.getUserPreferences().id)
        map.put("version","1.1")
        map.put("type","seller")
        Log.e("PARAM_BIDS","${map.toString()}")

        fragmentView!!.progress_bar.visibility = View.VISIBLE
        bidsList.clear()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                fragmentView.progress_bar.visibility = View.GONE

                val response = JSONObject(`object`.toString())
                if(response.has("post")){
                    val postObjArray = response.getJSONArray("post")

                    val parser = PostDataParser()
                    for (i in 0 until postObjArray.length()) {

                        val postObj = postObjArray.getJSONObject(i)
                        val postModel = parser.parseForBid(postObj)

                        val feedSellerModel = FeedSellerModel()
                        feedSellerModel.type = "post"
                        feedSellerModel.typeId = postModel.id
                        feedSellerModel.post = postModel
                        bidsList.add(feedSellerModel)
                    }
                }

                if (response.has("story")) {
                    val postObjArray = response.getJSONArray("story")

                    val parser = StoryDataParse()
                    for (i in 0 until postObjArray.length()) {

                        val postObj = postObjArray.getJSONObject(i)
                        val storyModel = parser.parse(postObj)
                        val feedSellerModel = FeedSellerModel()
                        feedSellerModel.type = "story"
                        feedSellerModel.typeId = storyModel.id
                        feedSellerModel.story = storyModel
                        bidsList.add(feedSellerModel)
                    }
                }

                bidsList.reverse()
                fragmentView.bids_list.adapter.notifyDataSetChanged()

                if(fragmentView.swipe_to_refresh.isRefreshing){
                    fragmentView.swipe_to_refresh.isRefreshing = false
                }

                if(bidsList.size <= 0){
                    fragmentView.tx_empty.visibility = View.VISIBLE
                }else{
                    fragmentView.tx_empty.visibility = View.GONE
                }

            }

            override fun onError(errorMessage: String) {
                fragmentView!!.progress_bar.visibility = View.GONE
                if(fragmentView.swipe_to_refresh.isRefreshing){
                    fragmentView.swipe_to_refresh.isRefreshing = false
                }
                if(bidsList.size <= 0){
                    fragmentView.tx_empty.visibility = View.VISIBLE
                }else{
                    fragmentView.tx_empty.visibility = View.GONE
                }

                //  Toast.makeText(activity!!,"Failed$errorMessage", Toast.LENGTH_SHORT).show()

            }

        }).getBidsForBuyer(map)
    }

    inner class OnItemClick:PostBidListSellerAdapter.OnItemClick{
        override fun onItemClick(position: Int) {
            try {

                val intent = Intent(activity!!,BidListActivity::class.java)
                intent.putExtra("type_id",bidsList[position].typeId)

                if(bidsList[position].type.equals("post",true)){
                    intent.putExtra("type","post")
                    intent.putExtra("post",bidsList[position].post)

                }else{
                    intent.putExtra("type","story")
                    intent.putExtra("story",bidsList[position].story)
                }

                startActivity(intent)
            }catch (e:Exception){
                e.printStackTrace()
            }
        }
    }


}
