package com.mazadlive.fragments.Notification


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mazadlive.R
import com.mazadlive.activities.FeedsActivity
import com.mazadlive.adapters.PostFeedListSellerAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.helper.Constant
import com.mazadlive.helper.DividerItemDecorator
import com.mazadlive.models.FeedSellerModel
import com.mazadlive.parser.PostDataParser
import com.mazadlive.parser.StoryDataParse
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.fragment_seller_feedback.view.*
import org.json.JSONObject

class SellerFeedbackFragment : Fragment() {

    val TAG = "SellerFeedbackFragment"
    lateinit var fragmentView: View
    lateinit var progress: CustomProgress

    var feedList = ArrayList<FeedSellerModel>()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_seller_feedback, container, false)

        fragmentView.feedback_list.layoutManager = LinearLayoutManager(activity!!)
        fragmentView.feedback_list.addItemDecoration(DividerItemDecorator(ContextCompat.getDrawable(activity!!, R.drawable.divider_line)))
        fragmentView.feedback_list.adapter = PostFeedListSellerAdapter(activity!!, feedList)

        (fragmentView.feedback_list.adapter as PostFeedListSellerAdapter).listener = OnItemClick()
        fragmentView.swipe_to_refresh.setOnRefreshListener {
            getFeedList()
        }
        getFeedList()
        return fragmentView
    }


    private fun getFeedList() {
        val map = JSONObject()
        map.put("user_id", MyApplication.instance.getUserPreferences().id)
        map.put("version", "1.1")
        Log.e("PARAM", "$map")



        fragmentView.progress_bar.visibility = View.VISIBLE
        feedList.clear()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                fragmentView.progress_bar.visibility = View.GONE

                val response = JSONObject(`object`.toString())
                if (response.has("posts")) {
                    val postObjArray = response.getJSONArray("posts")

                    val parser = PostDataParser()
                    for (i in 0 until postObjArray.length()) {

                        val postObj = postObjArray.getJSONObject(i)
                        val postModel = parser.parse(postObj)
                        Log.e(TAG,"FEED_SIZE ${postModel.feedbackList.size}")
                        val feedSellerModel =FeedSellerModel()
                        feedSellerModel.type = "post"
                        feedSellerModel.typeId = postModel.id
                        feedSellerModel.post = postModel
                        feedList.add(feedSellerModel)
                        //   bidsList.add(postModel.userBid!!)
                    }
                }

                if (response.has("stories")) {
                    val postObjArray = response.getJSONArray("stories")

                    val parser = StoryDataParse()
                    for (i in 0 until postObjArray.length()) {

                        val postObj = postObjArray.getJSONObject(i)
                        val storyModel = parser.parse(postObj)
                        val feedSellerModel =FeedSellerModel()
                        feedSellerModel.type = "story"
                        feedSellerModel.typeId = storyModel.id
                        feedSellerModel.story = storyModel
                        feedList.add(feedSellerModel)
                        Log.e(TAG,"FEED_SIZE_Story ${storyModel.feedbackList.size}")
                        //feedList.add(postModel)
                        // feedList.add(orderModel)
                        //   bidsList.add(postModel.userBid!!)
                    }
                }

                if (fragmentView.swipe_to_refresh.isRefreshing) {
                    fragmentView.swipe_to_refresh.isRefreshing = false
                }

                if(feedList.size <= 0){
                    fragmentView.tx_empty.visibility = View.VISIBLE
                }else{
                    fragmentView.tx_empty.visibility = View.GONE
                }

                fragmentView.feedback_list.adapter.notifyDataSetChanged()


            }

            override fun onError(errorMessage: String) {
                fragmentView!!.progress_bar.visibility = View.GONE
                if(feedList.size <= 0){
                    fragmentView.tx_empty.visibility = View.VISIBLE
                }else{
                    fragmentView.tx_empty.visibility = View.GONE
                }

                if (fragmentView.swipe_to_refresh.isRefreshing) {
                    fragmentView.swipe_to_refresh.isRefreshing = false
                }
                fragmentView.feedback_list.adapter.notifyDataSetChanged()
//                Toast.makeText(activity!!, "Failed $errorMessage", Toast.LENGTH_SHORT).show()

            }

        }).getFeedbackForSeller(map)
    }



    inner class OnItemClick: PostFeedListSellerAdapter.OnItemClick{
        override fun onItemClick(position: Int) {
            val intent = Intent(context, FeedsActivity::class.java)
            if(feedList[position].type == "post"){
                intent.putParcelableArrayListExtra("feeds",feedList[position].post!!.feedbackList)
            }else{
                intent.putParcelableArrayListExtra("feeds",feedList[position].story!!.feedbackList)
            }
            intent.putExtra("isYourFeedback",true)
            startActivityForResult(intent, Constant.FEEDBACK_CODE)
        }
    }

}
