package com.mazadlive.fragments.Notification

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.mazadlive.R
import com.mazadlive.activities.OrderDetailsActivity
import com.mazadlive.adapters.PaymentListAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.helper.Constant
import com.mazadlive.helper.DividerItemDecorator
import com.mazadlive.models.PaymentModel
import com.mazadlive.parser.PaymentParser
import com.mazadlive.parser.WalletParser
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.SortPayment
import kotlinx.android.synthetic.main.fragment_payments.view.*
import org.json.JSONObject
import java.util.*

class BuyerPaymentsFragment : Fragment() {

    private lateinit var fragmentView : View
    private val paymentsList = ArrayList<PaymentModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_payments, container, false)

        fragmentView.paymemts_list.layoutManager = LinearLayoutManager(activity)
        fragmentView.paymemts_list.addItemDecoration(DividerItemDecorator(ContextCompat.getDrawable(activity!!, R.drawable.divider_line)))
        fragmentView.paymemts_list.adapter = PaymentListAdapter(activity!!, paymentsList)
        (fragmentView.paymemts_list.adapter as PaymentListAdapter).listener = OnItemClick()
        fragmentView.swipe_to_refresh.setOnRefreshListener {
            getWalletList()
        }

        getWalletList()
        return fragmentView
    }

    fun  getWalletList(){
        val param = JSONObject()
        param.put("version", "1.1")
        param.put("user_id", MyApplication.instance.getUserPreferences().id)
        param.put("type_id", MyApplication.instance.getUserPreferences().id)
        param.put("type", "seller")
        if(MyApplication.instance.getUserPreferences().appModeBuyer){
            param.put("type", "buyer")
        }


        paymentsList.clear()
        fragmentView!!.progress_bar.visibility = View.VISIBLE

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                fragmentView!!.progress_bar.visibility = View.GONE

                val jsonObject = JSONObject(`object`.toString())
                if(jsonObject.has("description")){

                    val array = jsonObject.getJSONArray("description")

                    val parser = PaymentParser()
                    for (i in 0 until array.length()){
                        val paymentModel = parser.parse(array.getJSONObject(i))
                        paymentsList.add(paymentModel)
                    }

                    if(paymentsList.size >0){
                        Collections.sort(paymentsList,SortPayment())
                        fragmentView.tx_empty.visibility = View.GONE
                    }else{
                        fragmentView.tx_empty.visibility = View.VISIBLE
                    }

                    if(fragmentView.swipe_to_refresh.isRefreshing){
                        fragmentView.swipe_to_refresh.isRefreshing =false
                    }

                    fragmentView.paymemts_list.adapter.notifyDataSetChanged()
                }

            }

            override fun onError(errorMessage: String) {

                if(paymentsList.size >0){
                    fragmentView.tx_empty.visibility = View.GONE
                }else{
                    fragmentView.tx_empty.visibility = View.VISIBLE
                }

                if(fragmentView.swipe_to_refresh.isRefreshing){
                    fragmentView.swipe_to_refresh.isRefreshing =false
                }
                fragmentView!!.progress_bar.visibility = View.GONE
            }

        }).walletList(param)
    }

    inner class OnItemClick:PaymentListAdapter.OnItemClick{
        override fun onItemClick(position: Int) {
            val intent = Intent(activity!!, OrderDetailsActivity::class.java)
            intent.putExtra(Constant.ORDER_KEY,paymentsList[position].orderModel)
            startActivity(intent)
        }
    }
}
