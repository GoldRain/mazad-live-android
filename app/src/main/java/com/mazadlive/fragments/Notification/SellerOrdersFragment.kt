package com.mazadlive.fragments.Notification

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mazadlive.R
import com.mazadlive.activities.OrderDetailsActivity
import com.mazadlive.adapters.SellerOrdersListAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.helper.Constant
import com.mazadlive.helper.DividerItemDecorator
import com.mazadlive.models.OrderModel
import com.mazadlive.parser.OrderParser
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.SortOrder
import kotlinx.android.synthetic.main.fragment_seller_orders.view.*
import org.json.JSONObject
import java.util.*

class SellerOrdersFragment : Fragment() {

    private lateinit var fragmentView: View
    private val ordersList = ArrayList<OrderModel>()
    private lateinit var progress: CustomProgress
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_seller_orders, container, false)
        progress = CustomProgress(activity!!)

        fragmentView.orders_list.layoutManager = LinearLayoutManager(activity!!)
        fragmentView.orders_list.addItemDecoration(DividerItemDecorator(ContextCompat.getDrawable(activity!!, R.drawable.divider_line)))
        fragmentView.orders_list.adapter = SellerOrdersListAdapter(activity!!, ordersList)

        fragmentView.swipe_to_refresh.setOnRefreshListener {
            getMyOrders()
        }

        (fragmentView.orders_list.adapter as SellerOrdersListAdapter).listener = object : SellerOrdersListAdapter.OnOrderClick{
            override fun onClick(position: Int) {
                val intent = Intent(activity!!, OrderDetailsActivity::class.java)
                intent.putExtra(Constant.ORDER_KEY,ordersList[position])
                startActivity(intent)
            }

        }

        getMyOrders()
        return fragmentView
    }


    private fun getMyOrders() {

        val param = JSONObject()
        param.put("version", "1.1")
        param.put("user_id", MyApplication.instance.getUserPreferences().id)

        Log.e("SELLER_ORDER","${param.toString()}")
        //progress.show()
        ordersList.clear()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
              //  progress.dismiss()
                val orderParser = OrderParser()

                val jsonObject = JSONObject(`object`.toString())
                if (jsonObject.has("description")) {
                    val array = jsonObject.getJSONArray("description")
                    for (i in 0 until array.length()) {
                        val orderModel = orderParser.parse(array.getJSONObject(i))
                        ordersList.add(orderModel)
                    }

                    Collections.sort(ordersList,SortOrder())
                    (activity)!!.runOnUiThread {
                        if(ordersList.size <= 0){
                            fragmentView.tx_empty.visibility = View.VISIBLE
                        }else{
                            fragmentView.tx_empty.visibility = View.GONE
                        }
                        
                        if (fragmentView.swipe_to_refresh.isRefreshing) {
                            fragmentView.swipe_to_refresh.isRefreshing = false
                        }
                        fragmentView.orders_list.adapter.notifyDataSetChanged()
                    }
                }
            }

            override fun onError(errorMessage: String) {
              //  progress.dismiss()
                if (fragmentView.swipe_to_refresh.isRefreshing) {
                    fragmentView.swipe_to_refresh.isRefreshing = false
                }
                if(ordersList.size <= 0){
                    fragmentView.tx_empty.visibility = View.VISIBLE
                }else{
                    fragmentView.tx_empty.visibility = View.GONE
                }
                //Toast.makeText(activity!!, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).sellerOrder(param)
    }



}
