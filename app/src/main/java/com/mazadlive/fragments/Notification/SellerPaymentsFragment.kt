package com.mazadlive.fragments.Notification


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.mazadlive.R
import com.mazadlive.adapters.PaymentListAdapter
import com.mazadlive.helper.DividerItemDecorator
import com.mazadlive.models.PaymentModel
import kotlinx.android.synthetic.main.fragment_seller_payments.view.*

class SellerPaymentsFragment : Fragment() {

    private lateinit var fragmentView : View
    private val paymentsList = ArrayList<PaymentModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_seller_payments, container, false)


        paymentsList.add(PaymentModel("Not yet"))
        paymentsList.add(PaymentModel("Delivery Approved"))
        paymentsList.add(PaymentModel("In the wallet\nAsk for Deposit"))
        paymentsList.add(PaymentModel("Paid"))

        fragmentView.paymemts_list.layoutManager = LinearLayoutManager(activity)
        fragmentView.paymemts_list.addItemDecoration(DividerItemDecorator(ContextCompat.getDrawable(activity!!, R.drawable.divider_line)))
        fragmentView.paymemts_list.adapter = PaymentListAdapter(activity!!, paymentsList)

        return fragmentView
    }

}
