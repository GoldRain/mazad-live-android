package com.mazadlive.fragments.Notification


import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.*
import android.widget.Toast

import com.mazadlive.R
import com.mazadlive.activities.FeedBackActivity
import com.mazadlive.adapters.BuyerFeedbackListAdapter
import com.mazadlive.adapters.FeedBuyerAdapter
import com.mazadlive.adapters.SellerFeedbackListAdapter
import com.mazadlive.adapters.StoryAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.helper.Constant
import com.mazadlive.helper.DividerItemDecorator
import com.mazadlive.models.*
import com.mazadlive.parser.*
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.SortOrder
import kotlinx.android.synthetic.main.customdialog_post_view_recycle.*
import kotlinx.android.synthetic.main.fragment_feedback.view.*
import org.json.JSONObject
import java.util.*

class BuyerFeedbackFragment : Fragment() {

    lateinit var fragmentView: View
    lateinit var progress: CustomProgress

    var feedList = ArrayList<OrderModel>()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_feedback, container, false)

        fragmentView.feedback_list.layoutManager = LinearLayoutManager(activity!!)
        fragmentView.feedback_list.addItemDecoration(DividerItemDecorator(ContextCompat.getDrawable(activity!!, R.drawable.divider_line)))
        fragmentView.feedback_list.adapter = BuyerFeedbackListAdapter(activity!!, feedList)

        (fragmentView.feedback_list.adapter as BuyerFeedbackListAdapter).listener = OnItemClick()


        fragmentView.swipe_to_refresh.setOnRefreshListener {
            getFeedList()
        }

        getFeedList()

        return fragmentView
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == Constant.FEEDBACK_CODE && resultCode == Activity.RESULT_OK){
            getFeedList()
        }
    }

    private fun showPostDialog(position: Int) {

        val dialog = Dialog(activity!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.customdialog_post_view_recycle)
        val wlp = dialog.window.attributes
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog.window.attributes = wlp

        val tempPostList = ArrayList<PostModel>()

        tempPostList.add(feedList[position].post!!)
        dialog.recyclerview.layoutManager = LinearLayoutManager(activity!!,LinearLayoutManager.HORIZONTAL,false)
        dialog.recyclerview.adapter = FeedBuyerAdapter(activity!!,tempPostList,object : FeedBuyerAdapter.OnClikPost{
            override fun onClickPost(position: Int) {

            }
        })

        (dialog.recyclerview.adapter as FeedBuyerAdapter).reportListener = object : FeedBuyerAdapter.ReportListener(){
            override fun onReport(position: Int) {
                dialog.dismiss()
                getFeedList()
            }

        }
        dialog.recyclerview.adapter.notifyDataSetChanged()
        dialog.recyclerview.scrollToPosition(position)
        dialog.show()
    }

    private fun showStoryDialog(position: Int) {

        val dialog = Dialog(activity!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.customdialog_post_view_recycle)
        val wlp = dialog.window.attributes
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog.window.attributes = wlp

        val tempPostList = ArrayList<StoryModel>()

        tempPostList.add(feedList[position].story!!)
        dialog.recyclerview.layoutManager = LinearLayoutManager(activity!!,LinearLayoutManager.HORIZONTAL,false)
        dialog.recyclerview.adapter = StoryAdapter(activity!!,tempPostList,object : StoryAdapter.OnClikPost{
            override fun onClickPost(position: Int) {

            }
        })

        dialog.recyclerview.adapter.notifyDataSetChanged()
        dialog.recyclerview.scrollToPosition(position)
        dialog.show()
    }

    private fun getFeedList() {
        val map = JSONObject()
        map.put("user_id", MyApplication.instance.getUserPreferences().id)
        map.put("version", "1.1")
        Log.e("PARAM", "${map.toString()}")


        fragmentView.progress_bar.visibility = View.VISIBLE
        feedList.clear()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                fragmentView.progress_bar.visibility = View.GONE

                val response = JSONObject(`object`.toString())
                if (response.has("description")) {
                    val postObjArray = response.getJSONArray("description")

                    val parser = OrderParser()
                    for (i in 0 until postObjArray.length()) {

                        val postObj = postObjArray.getJSONObject(i)
                        val orderModel = parser.parse(postObj)

                        if (postObj.has("type")){
                            if (postObj.getString("type") == "post"){
                                Log.e("SADASDASDASDASDASD","POST")
                                orderModel.post = PostDataParser().parse(postObj.getJSONArray("type_data").getJSONObject(0))
                            }else{
                                Log.e("SADASDASDASDASDASD","Story")
                                orderModel.story = StoryDataParse().parse(postObj.getJSONArray("type_data").getJSONObject(0))
                            }
                        }

                        // Seller User Details
                        if (postObj.has("sellerDetails")){
                            val objData = UserParser().parse(postObj.getJSONArray("sellerDetails").getJSONObject(0))
                            if (postObj.getString("type") == "post"){
                                orderModel.post!!.userData = objData
                            }else{
                                orderModel.story!!.userData = objData
                            }

                        }

                        feedList.add(orderModel)
                        //   bidsList.add(postModel.userBid!!)
                    }
                }

                Collections.sort(feedList,SortOrder())
                if (fragmentView.swipe_to_refresh.isRefreshing) {
                    fragmentView.swipe_to_refresh.isRefreshing = false
                }

                if(feedList.size <= 0){
                    fragmentView.tx_empty.visibility = View.VISIBLE
                }else{
                    fragmentView.tx_empty.visibility = View.GONE
                }

                fragmentView.feedback_list.adapter.notifyDataSetChanged()


            }

            override fun onError(errorMessage: String) {
                fragmentView!!.progress_bar.visibility = View.GONE
                if(feedList.size <= 0){
                    fragmentView.tx_empty.visibility = View.VISIBLE
                }else{
                    fragmentView.tx_empty.visibility = View.GONE
                }

                if (fragmentView.swipe_to_refresh.isRefreshing) {
                    fragmentView.swipe_to_refresh.isRefreshing = false
                }
                fragmentView.feedback_list.adapter.notifyDataSetChanged()
//                Toast.makeText(activity!!, "Failed $errorMessage", Toast.LENGTH_SHORT).show()

            }

        }).getFeedbackForBuyer(map)
    }



    inner class OnItemClick:BuyerFeedbackListAdapter.OnItemCLick{
        override fun onFeedback(position: Int) {
            val intent = Intent(context, FeedBackActivity::class.java)
            intent.putExtra("order",feedList[position])
            startActivityForResult(intent,Constant.FEEDBACK_CODE)
        }

        override fun onPostClick(position: Int) {
            when(feedList[position].orderType){
                "post" ->{
                    showPostDialog(position)
                }

                "story" ->{
                    showStoryDialog(position)
                }


            }

        }
    }

}