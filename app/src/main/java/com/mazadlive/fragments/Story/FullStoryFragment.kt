package com.mazadlive.fragments.Story


import android.app.Dialog
import android.content.ComponentName
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.activities.StoryActivity
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.models.StoryModel
import com.mazadlive.utils.UserPreferences
import kotlinx.android.synthetic.main.fragment_full_story.view.*
import kotlinx.android.synthetic.main.view_story_reply.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import android.content.Intent
import android.net.Uri
import android.telephony.PhoneNumberUtils
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.upstream.BandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.TransferListener
import com.google.android.exoplayer2.util.Util
import com.mazadlive.Interface.ProgressListener
import com.mazadlive.activities.PaymentActivity
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customviews.CustomEditText
import com.mazadlive.helper.Constant
import com.mazadlive.service.ImageDownloadService
import com.mazadlive.utils.MyApplication
import org.json.JSONObject
import java.io.File


/**
 * A simple [Fragment] subclass.
 */
class FullStoryFragment : Fragment() {


    private val TAG = "FullStoryFragment"
    private var dialog: Dialog? = null


    private var timer: Int = 0
    lateinit var fragmentView: View
    private lateinit var userRef: UserPreferences
    private var progress: CustomProgress? = null

    private var amount = 1.toDouble()
    private var isDialogShow = false

    private val runnable = object : Runnable {
        override fun run() {
            if (!isDialogShow) {

               if(storyModel!!.is_video && player != null){
                   timer = player!!.currentPosition.toInt()

                   if (timer >= player!!.duration) {

                       timer = 0
                       if (storyActivity != null) {
                           storyActivity!!.moveNext()
                       }
                   } else {

                       fragmentView.mSeekbar.progress = timer
                    //   Log.e(TAG,"TIMER $timer ${fragmentView.mSeekbar.max} ${fragmentView.mSeekbar.progress}")
                       handler.postDelayed(this, 50)
                   }


               }else{
                   timer += 1
                   if (timer >= 100) {

                       timer = 0
                       if (storyActivity != null) {
                           storyActivity!!.moveNext()
                       }
                   } else {
                       fragmentView.mSeekbar.progress = timer

                       handler.postDelayed(this, 50)
                   }
               }
            }

        }
    }

    private val handler = Handler()

    private var countDown: CountDownTimer? = null
    private var storyModel: StoryModel? = null

    var storyActivity: StoryActivity? = null
  //  var softKeyboard: SoftKeyboard? = null

    var isdownload = false
    var fragPosition = -1

    private var player: SimpleExoPlayer? = null
    private lateinit var mediaDataSourceFactory: com.google.android.exoplayer2.upstream.DataSource.Factory
    private val bandwidthMeter: BandwidthMeter = DefaultBandwidthMeter()
    private var trackSelector: DefaultTrackSelector? = null
    private var lastSeenTrackGroupArray: TrackGroupArray? = null

    private var currentWindow: Int = 0
    private var playbackPosition: Long = 0
    var SeekTo: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        fragmentView = inflater.inflate(R.layout.fragment_full_story, container, false)


        progress = CustomProgress(activity!!)
        fragmentView.close.setOnClickListener { (activity!! as StoryActivity).onBackPressed() }

        storyModel = arguments!!.getParcelable("story") as StoryModel

        if (storyModel!!.userData != null) {

            if(storyModel!!.userData!!.id.equals(MyApplication.instance.getUserPreferences().id)){
                fragmentView.buy_now.visibility = View.GONE
            }
            Glide.with(activity!!).load(storyModel!!.userData!!.profile).apply(RequestOptions().override(80).error(R.drawable.dummy)).into(fragmentView.image)
            fragmentView.user_name.setText(storyModel!!.userData!!.username)

        }


        if(!TextUtils.isEmpty(storyModel!!.buyNowPrice)){
            fragmentView.price.text = String.format("Price %s%s",getString(R.string.currency),storyModel!!.buyNowPrice)
             //amount = (storyModel!!.buyNowPrice).toDouble()
        }


        if(storyModel!!.whatsapp_and_call){
            fragmentView.im_whats_app_call.visibility = View.VISIBLE
        }else{
            fragmentView.im_whats_app_call.visibility = View.GONE
        }

        if(storyModel!!.whatsapp_only){
            fragmentView.im_whats_app_msg.visibility = View.VISIBLE
        }else{
            fragmentView.im_whats_app_msg.visibility = View.GONE
        }

        if(storyModel!!.is_buyNow){
            fragmentView.buy_now.text = getString(R.string.buy_now)
        }else{
            fragmentView.buy_now.text = getString(R.string.add_bid)
        }


        fragmentView.im_whats_app_call.setOnClickListener {
            Toast.makeText(activity!!,getString(R.string.feature_not_work),Toast.LENGTH_SHORT).show()
        }

        fragmentView.im_whats_app_msg.setOnClickListener {
            sendWhatsAppMsg()
        }

        fragmentView.buy_now.setOnClickListener {
            handler.removeCallbacks(runnable)
            if(storyModel!!.is_buyNow){
                val intent  = Intent(context, PaymentActivity::class.java)
                intent.putExtra("story",storyModel)
                intent.putExtra("type","story")
                startActivity(intent)
            }else{
                getHeighestBid()
            }
        }

        fragmentView.touch_view.setOnTouchListener(MyTouch())

        fragmentView.player_view.setOnTouchListener(MyTouch())

        if(storyModel!!.is_video){
            mediaDataSourceFactory = DefaultDataSourceFactory(activity!!, Util.getUserAgent(activity!!, "mediaPlayerSample"),
                    bandwidthMeter as TransferListener<in com.google.android.exoplayer2.upstream.DataSource>)
        }

        if(storyActivity!!.currentPosition == fragPosition){
            fragmentView.mSeekbar.progress = 0
            if(storyModel!!.is_video){
                playVideo()
            }else{
                setImage()
            }
        }

        Log.e(TAG,"DESCRIPTION :: ${storyModel!!.product_description}")

        fragmentView.product_description.setText(storyModel!!.product_description)

        return fragmentView
    }

    private fun sendWhatsAppMsg() {

        var smsNumber = "0123456789"
        if(storyModel!!.userData!= null){
            smsNumber = storyModel!!.userData!!.phone
        }
        val sendIntent = Intent("android.intent.action.MAIN")


        try {
            sendIntent.component = ComponentName("com.whatsapp", "com.whatsapp.Conversation")
            sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(smsNumber) + "@s.whatsapp.net")//phone number without "+" prefix
            startActivity(sendIntent)
        } catch (e: Exception) {
            Toast.makeText(context, "${getString(R.string.no_watsapp)}", Toast.LENGTH_SHORT).show()
        }

    }

    fun bidDialog(){
        CustomDialog(activity!!).showBidialog(amount.toString(),object : DialogListener() {
            override fun okClick(any: Any) {
                if(any.toString().toDouble() > amount){
                    sendReply(any.toString(), "amount")
                }else{
                    Toast.makeText(activity!!,getString(R.string.enter_more_bid),Toast.LENGTH_SHORT).show()
                }
            }

            override fun okClick() {
                handler.post(runnable)
            }
        })
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(liveStoryModel: StoryModel) {

        if (liveStoryModel.id.equals(storyModel!!.id)) {
            fragmentView.mSeekbar.progress = 0
            if(storyModel!!.is_video){
                playVideo()
            }else{
                setImage()
            }
        } else {
            timer = 0

            handler.removeCallbacks(runnable)
        }
    }


    override fun onResume() {
        timer = 0

        EventBus.getDefault().register(this)

        super.onResume()
    }

    override fun onPause() {
        EventBus.getDefault().unregister(this)

        timer = 0
        handler.removeCallbacks(runnable)

        if(player != null){

            player!!.stop()
        }
        if(storyModel!!.is_video){
            releasePlayer()
        }
        super.onPause()
    }

    override fun onStart() {

        super.onStart()
    }

    override fun onStop() {

        super.onStop()
    }

    private fun addListener(){

        fragmentView.mMessageLay.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if(TextUtils.isEmpty(fragmentView.mMessageLay.text)){
                    fragmentView.mBid.visibility = View.VISIBLE
                    fragmentView.mSend.visibility = View.INVISIBLE
                }else{
                    fragmentView!!.mBid.visibility = View.INVISIBLE
                    fragmentView.mSend.visibility = View.VISIBLE
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
    }


    private fun showStoryReplyDialog() {

        if (dialog == null) {
            dialog = Dialog(activity)
            dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog!!.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog!!.setContentView(R.layout.view_story_reply)

            val wlp = dialog!!.window.attributes

            wlp.width = WindowManager.LayoutParams.MATCH_PARENT
            wlp.horizontalMargin = 0F
            wlp.gravity = Gravity.BOTTOM
            dialog!!.window.attributes = wlp
            dialog!!.setCanceledOnTouchOutside(false)
        }


        dialog!!.mMessageRep.setKeyImeChangeListener(OnKeyChange())

        dialog!!.mMessageRep.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val msg = dialog!!.mMessageRep.text.toString().trim()
                if (TextUtils.isEmpty(msg)) {
                    dialog!!.mSend.visibility = View.INVISIBLE
                    dialog!!.mReplyBid.visibility = View.VISIBLE
                } else {
                    dialog!!.mSend.visibility = View.VISIBLE
                    dialog!!.mReplyBid.visibility = View.INVISIBLE
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        dialog!!.mReplyBid.setOnClickListener {

        }

        dialog!!.mSend.setOnClickListener {
            dialog!!.dismiss()
            dialog!!.mMessageRep.setText("")
            fragmentView.mMessageLay.setText("")
            sendReply(dialog!!.mMessageRep.text.toString().trim(), "comment")
        }

        dialog!!.setOnDismissListener {

            isDialogShow = false
            handler.post(runnable)
            fragmentView.mMessageLay.text = dialog!!.mMessageRep.text

            fragmentView.cl_bottom.visibility = View.VISIBLE
        }

        dialog!!.setOnShowListener {
            handler.removeCallbacks(runnable)
            isDialogShow = true
            dialog!!.mMessageRep.text = fragmentView.mMessageLay.text
            dialog!!.mMessageRep.requestFocus()

            val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(dialog!!.mMessageRep, InputMethodManager.SHOW_IMPLICIT)

            fragmentView.cl_bottom.visibility = View.GONE
        }
        dialog!!.show()
    }

    fun start(){
        handler.post(runnable)
    }


    private fun setImage() {
        fragmentView.player_view.visibility = View.GONE
        fragmentView.progress_bar.visibility = View.GONE
        timer = 0

        fragmentView.progress_bar1.visibility = View.VISIBLE

        Log.e(TAG,"URL ${storyModel!!.url}")
        val file = File("${Constant.STORY_PATH}IMG_${storyModel!!.id}.jpg")
        Log.e(TAG,"URL ${file.path}  ${file.exists()}")
        if(file.exists()){
            isdownload = true
            fragmentView.progress_bar1.visibility = View.GONE
            Glide.with(activity!!).load(file.path).apply(RequestOptions().override(1000)).into(fragmentView.post_image)
            start()

        }else{
            Glide.with(activity!!).load(storyModel!!.url).apply(RequestOptions().override(30)).into(fragmentView.post_image)

            ImageDownloadService.addProgressListener(storyModel!!.id,object :ProgressListener{
                override fun onProgressUpdate(progress: Int) {
                    Log.e(TAG,"onProgressUpdate $progress")
                    if(progress >10){

                        if(!storyActivity!!.isFinishing){
                            (activity)!!.runOnUiThread {
                                if(fragmentView.progress_bar1.visibility == View.VISIBLE){
                                    fragmentView.progress_bar1.visibility = View.GONE
                                    fragmentView.progress_bar2.visibility = View.VISIBLE
                                }
                                fragmentView.progress_bar2.progress = progress
                            }
                        }
                    }
                }

                override fun onSuccess(path: String) {
                    Log.e(TAG,"onSuccess $path")
                    if(!storyActivity!!.isFinishing){
                        (activity)!!.runOnUiThread {
                            isdownload = true
                            fragmentView.progress_bar1.visibility = View.GONE
                            fragmentView.progress_bar2.visibility = View.GONE
                            setImage()
                        }
                    }

                }

                override fun onFileNotFound() {

                    if(!storyActivity!!.isFinishing){
                        (activity)!!.runOnUiThread {

                            fragmentView.progress_bar1.visibility = View.GONE
                            fragmentView.progress_bar2.visibility = View.GONE

                        }
                        Toast.makeText(activity!!,"File not download",Toast.LENGTH_SHORT).show()
                        Log.e(TAG,"onFileNotFound ")
                    }

                }

                override fun onCancel() {

                    if(!storyActivity!!.isFinishing){
                        (activity)!!.runOnUiThread {

                            fragmentView.progress_bar1.visibility = View.GONE
                            fragmentView.progress_bar2.visibility = View.GONE

                        }
                        Toast.makeText(activity!!,getString(R.string.file_not_download),Toast.LENGTH_SHORT).show()
                        Log.e(TAG,"onCancel ")
                    }


                }

            })
            if(!ImageDownloadService.containsTask(storyModel!!.id)){
                val intent = Intent(activity!!,ImageDownloadService::class.java)
                intent.putExtra("story",storyModel!!)
                (activity!!).startService(intent)
            }
        }

    }


    private fun playVideo(){

        fragmentView.progress_bar1.visibility = View.GONE
        fragmentView.player_view.visibility = View.VISIBLE
        fragmentView.player_view.controllerAutoShow = false
     //   fragmentView.player_view.con = false

        val file = File("${Constant.STORY_PATH}VID_${storyModel!!.id}.mp4")
        Log.e(TAG,"URL ${file.path}  ${file.exists()}")

        if(file.exists()){
            isdownload = true
            fragmentView.progress_bar1.visibility = View.GONE
            Glide.with(activity!!).load(storyModel!!.url).apply(RequestOptions().override(80)).into(fragmentView.post_image)
            start()
            playVideoWithPath()
        }else{

            var videoFile = storyModel!!.url
            val index = videoFile.lastIndexOf(".")
            if(index >0){
                videoFile = ("${videoFile.substring(0,index)}.mp4")
            }

            playVideoWithUrl(videoFile)
           // downloadVideo()
        }

        if(player != null){
            fragmentView.mSeekbar.max = player!!.duration.toInt()
        }


    }

    private fun playVideoWithUrl(videoFile: String) {
        Log.e("VIDEO_FILE","${videoFile}")
        fragmentView.player_view.requestFocus()

        val videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)

        trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)
        //lastSeenTrackGroupArray = null

        player = ExoPlayerFactory.newSimpleInstance(activity!!, trackSelector)

        fragmentView.player_view.player = player

        with(player!!) {
            addListener(PlayerEventListener())
            playWhenReady = true
        }

        // Use Hls, Dash or other smooth streaming media source if you want to test the track quality selection.
        /*val mediaSource: MediaSource = HlsMediaSource(Uri.parse("https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"),
                mediaDataSourceFactory, mainHandler, null)*/

        val mediaSource = ExtractorMediaSource.Factory(mediaDataSourceFactory)
                .createMediaSource(Uri.parse(videoFile))

        var haveStartPosition = currentWindow != C.INDEX_UNSET
        if (haveStartPosition) {
            player!!.seekTo(currentWindow, playbackPosition)
        }
        haveStartPosition = currentWindow != SeekTo
        if (haveStartPosition) {
            player!!.seekTo(currentWindow, SeekTo.toLong())
        }
        player!!.prepare(mediaSource, !haveStartPosition, false)
    }

    private fun playVideoWithPath() {

    }

    private inner class PlayerEventListener : Player.DefaultEventListener(),Player.EventListener {

        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            when (playbackState) {
                Player.STATE_IDLE       // The player does not have any media to play yet.
                -> {
                    handler.removeCallbacks(runnable)
                    fragmentView.progress_bar.visibility = View.VISIBLE}

                Player.STATE_BUFFERING  // The player is buffering (loading the content)
                -> {
                    handler.removeCallbacks(runnable)
                    fragmentView.progress_bar.visibility = View.VISIBLE
                }
                Player.STATE_READY      // The player is able to immediately play
                -> {
                    start()
                    fragmentView.mSeekbar.max = player!!.duration.toInt()
                    fragmentView.progress_bar.visibility = View.GONE
                }
                Player.STATE_ENDED      // The player has finished playing the media
                -> {
                    start()
                    fragmentView.progress_bar.visibility = View.GONE
                    if (storyActivity != null) {
                        storyActivity!!.moveNext()
                    }
                }

            }
            // updateButtonVisibilities()
        }

        override fun onTracksChanged(trackGroups: TrackGroupArray?, trackSelections: TrackSelectionArray?) {
            // updateButtonVisibilities()
            // The video tracks are no supported in this device.
            /*if (trackGroups !== lastSeenTrackGroupArray) {
                val mappedTrackInfo = trackSelector!!.currentMappedTrackInfo
                if (mappedTrackInfo != null) {
                    if (mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_VIDEO) == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                        Toast.makeText(this@StoryCommentActivity, "Error unsupported track", Toast.LENGTH_SHORT).show()
                    }
                }
                //lastSeenTrackGroupArray = trackGroups
            }*/
        }

        override fun onSeekProcessed() {
            Log.e(TAG,"onSeekProcessed")
            super.onSeekProcessed()
        }

        override fun onTimelineChanged(timeline: Timeline?, manifest: Any?, reason: Int) {
            Log.e(TAG,"onTimelineChanged")
            super.onTimelineChanged(timeline, manifest, reason)
        }

        override fun onPositionDiscontinuity(reason: Int) {
            Log.e(TAG,"onPositionDiscontinuity")
            super.onPositionDiscontinuity(reason)
        }

        override fun onLoadingChanged(isLoading: Boolean) {
            Log.e(TAG,"onPositionDiscontinuity")
            super.onLoadingChanged(isLoading)
        }

        override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {
            Log.e(TAG,"onPlaybackParametersChanged")
            super.onPlaybackParametersChanged(playbackParameters)
        }



    }


    private fun releasePlayer() {
        if (player != null) {
            //   updateStartPosition()
            // shouldAutoPlay = player!!.playWhenReady
            player!!.release()
            player = null
            trackSelector = null
        }
    }


    private fun downloadVideo() {
        Glide.with(activity!!).load(storyModel!!.url).apply(RequestOptions().override(30)).into(fragmentView!!.post_image)

        fragmentView.progress_bar1.visibility = View.GONE
        fragmentView.progress_bar2.visibility = View.GONE

        ImageDownloadService.addProgressListener(storyModel!!.id,object :ProgressListener{
            override fun onProgressUpdate(progress: Int) {
                Log.e(TAG,"onProgressUpdate $progress")
                if(progress >10){

                }
            }

            override fun onSuccess(path: String) {
                Log.e(TAG,"onSuccess $path")
                (activity)!!.runOnUiThread {
                    isdownload = true
                }
            }

            override fun onFileNotFound() {

                Toast.makeText(activity!!,"${getString(R.string.file_not_download)}",Toast.LENGTH_SHORT).show()
                Log.e(TAG,"onFileNotFound ")
            }

            override fun onCancel() {

                Toast.makeText(activity!!,"${getString(R.string.file_not_download)}",Toast.LENGTH_SHORT).show()
                Log.e(TAG,"onCancel ")
            }

        })
        if(!ImageDownloadService.containsTask(storyModel!!.id)){
            val intent = Intent(activity!!,ImageDownloadService::class.java)
            intent.putExtra("story",storyModel!!)
            (activity!!).startService(intent)
        }
    }

    private fun sendReply(msg: String, type: String) {

        fragmentView.mMessageLay.setText("")
        if(dialog != null){
            dialog!!.mMessageRep.setText("")
        }

        val param = JSONObject() // : bid_user_id, bid_amount: bid_amount.
        param.put("version", "1.1")
        param.put("user_id", MyApplication.instance.getUserPreferences().id)
        param.put("story_id", storyModel!!.id)
        param.put("type", type)
        param.put("text", msg)

        progress!!.show()
        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()

                if(dialog != null){
                    dialog!!.mMessageRep.setText("")
                }
                fragmentView.mMessageLay.setText("")
                Toast.makeText(activity!!,getString(R.string.sent_reply),Toast.LENGTH_SHORT).show()
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(activity!!,"${getString(R.string.failed)} $errorMessage ",Toast.LENGTH_SHORT).show()
            }

        }).bidStories(param)
    }

    private fun getHeighestBid() { ////type: stream / post / story, stream_id: streamId, story_id: storyId, post_id: postId .
        val param = JSONObject() // : bid_user_id, bid_amount: bid_amount.
        param.put("version", "1.1")

        param.put("story_id", storyModel!!.id)
        param.put("type", "story")


        progress!!.show()
        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                val js = JSONObject(`object`.toString())//amount
                if(js.has("description")){
                    val json = js.getJSONObject("description")
                    if(json.has("amount")){
                        try {
                            amount = json.get("amount").toString().toDouble()

                        }catch (e:Exception){

                        }finally {
                            bidDialog()
                        }
                    }else{
                        bidDialog()
                    }
                }else{
                    bidDialog()
                }
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                bidDialog()

            }

        }).getHighestBid(param)

    }


    inner class OnKeyChange: CustomEditText.KeyImeChange{
        override fun onKeyIme(keyCode: Int, event: KeyEvent) {
            if(keyCode == KeyEvent.KEYCODE_BACK){
                if(dialog != null){
                    dialog!!.dismiss()
                }
            }
        }

    }

    inner class MyTouch:View.OnTouchListener{
        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            if(isdownload){

                if(event!!.action == MotionEvent.ACTION_DOWN){
                    handler.removeCallbacks(runnable)
                }

                if(event.action == MotionEvent.ACTION_UP){
                    handler.post(runnable)
                }
            }
            return true
        }
    }


}