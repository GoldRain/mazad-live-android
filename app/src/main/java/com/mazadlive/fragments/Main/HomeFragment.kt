package com.mazadlive.fragments.Main

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.activities.HomeActivity
import com.mazadlive.activities.UpgradeActivity
import com.mazadlive.activities.settings.SettingActivity
import com.mazadlive.adapters.TabsAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.fragments.Home.AuctionsFragment
import com.mazadlive.fragments.Home.CategoriesFragment
import com.mazadlive.fragments.Home.PickFragment
import com.mazadlive.fragments.Home.StoryFragment
import com.mazadlive.helper.Constant
import com.mazadlive.helper.MessageEvent
import com.mazadlive.models.CommentModel
import com.mazadlive.models.CountryModel
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.UserPreferences
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONObject

class HomeFragment : Fragment() {

    private lateinit var fragmentView: View

    private var storyFragment: StoryFragment? = null
    private var categoriesFragment: CategoriesFragment? = null
    private var pickFragment: PickFragment? = null
    private var auctionsFragment: AuctionsFragment? = null
    var currentPosition = 1
    var isAll = false

    var ref: UserPreferences? = null
    var progress: CustomProgress? = null

    private lateinit var adapter: TabsAdapter

    var linkPostId = ""
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        progress = CustomProgress(activity!!)
        ref = MyApplication.instance.getUserPreferences()

        setFlag()
        if (Constant.countryList.size <= 0) {
            Constant.getAllCountries()
        }
        //flag.setImageResource(activity!!.resources.getIdentifier("drawable/flag_" + MyApplication.instance.getUserPreferences().CountryFlag, null, activity!!.packageName))

        if (MyApplication.instance.getUserPreferences().appModeBuyer) {
            switch_account.setImageResource(R.drawable.switch_account_buyer)
        } else {
            switch_account.setImageResource(R.drawable.switch_account_seller)
        }

        setting.setOnClickListener {

            val intent = Intent(activity, SettingActivity::class.java)
            startActivity(intent)
        }

        upgrade.setOnClickListener {
            if(MyApplication.instance.getUserPreferences().loginStatus) {
                val intent = Intent(activity, UpgradeActivity::class.java)
                startActivity(intent)
            }else{

                CustomDialog(activity!!).createDialogBox()
            }

        }

        switch_account.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                changeUserType()
            }else{

                CustomDialog(activity!!).createDialogBox()
            }
        }

        flag.setOnClickListener {
            CustomDialog(activity!!).showCountryDialog(object : DialogListener() {
                override fun okClick(any: Any) {
                    super.okClick(any)
                    val countryModel = any as CountryModel

                    if (!countryModel.id.equals("NA")) {
                        ref!!.CountryFlag = countryModel.flag.toString().trim()
                        ref!!.CountryName = countryModel.name.toString().trim()
                        ref!!.countryId = countryModel.id.toString().trim()
                        setFlag()
                        isAll = false
                    } else {
                        isAll = true
                        flag.setImageResource(R.drawable.ic_internet)
                    }

                    EventBus.getDefault().post(MessageEvent(ref!!.appModeBuyer))
                }
            }, false, true)
        }

        viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                currentPosition = position
                EventBus.getDefault().post(adapter.mFragmentList[position])
            }

            override fun onPageScrollStateChanged(state: Int) {

            }

        })

        adapter = TabsAdapter(childFragmentManager)
        setFragments()


    }
    override fun onPause() {
        super.onPause()
        // MyApplication.instance.getSocketManager().setSocketEventsListener(false, null)
    }

    override fun onResume() {

        (activity!! as HomeActivity).updateBottomTab(0)
        (activity!! as HomeActivity).currentFragment = this


        super.onResume()
    }


    private fun setFlag() {
        Glide.with(this@HomeFragment).load(ref!!.CountryFlag).apply(RequestOptions().override(80).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true)).into(flag)

    }

    private fun setFragments() {
        storyFragment = StoryFragment()
        storyFragment!!.linkPostId= linkPostId

        categoriesFragment = CategoriesFragment()
        pickFragment = PickFragment()
        auctionsFragment = AuctionsFragment()

        storyFragment!!.parent = this
        categoriesFragment!!.parent = this
        pickFragment!!.parent = this
        auctionsFragment!!.parent = this

        adapter.mFragmentList.add(storyFragment!!)
        adapter.mFragmentName.add(getString(R.string.hint_story))
        adapter.mFragmentList.add(categoriesFragment!!)
        adapter.mFragmentName.add(getString(R.string.hint_categories))
        adapter.mFragmentList.add(pickFragment!!)
        adapter.mFragmentName.add(getString(R.string.hint_pick))
        adapter.mFragmentList.add(auctionsFragment!!)
        adapter.mFragmentName.add(getString(R.string.hint_auction))


        viewpager.adapter = adapter
        tablayout.setupWithViewPager(viewpager)
        viewpager.offscreenPageLimit = adapter.count

        Log.e("linkPostId"," ** "+linkPostId)
        if(TextUtils.isEmpty(linkPostId)){
            viewpager.currentItem = 1
            currentPosition = 1
        }else{
            viewpager.currentItem = 0
            currentPosition = 0
        }

        adapter.notifyDataSetChanged()

        viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {


            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                if (position == 0) {
                    //(activity as HomeActivity).hideTopLayout(true)
                } else {
                    //(activity as HomeActivity).hideTopLayout(false)
                }
            }

        })
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        if (MyApplication.instance.getUserPreferences().appModeBuyer) {
            switch_account.setImageResource(R.drawable.switch_account_buyer)
        } else {
            switch_account.setImageResource(R.drawable.switch_account_seller)
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(str: String) {
        Log.e("ON_LOGOUT", "$str")
    }


    private fun changeUserType() {

        val map = HashMap<String, String>()
        map.put("version", "1.1")

        map.put("id", ref!!.id)

        if (ref!!.appModeBuyer) {
            map.put("type", "seller")
        } else {
            map.put("type", "buyer")
        }

        //Log.e("USER_TYPE","type ${ref!!.appModeBuyer}")

        progress!!.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                //val json = JSONObject(`object`.toString())
                val json = JSONObject(`object`.toString())

                if (json.has("data")) {
                    val obj = json.getJSONObject("data")
                    if (obj.has("username")) {
                        ref!!.username = obj.getString("username")
                    }

                    if (obj.has("country_code")) {
                        ref!!.countryCode = obj.getString("country_code")
                    }

                    if (obj.has("phone")) {
                        ref!!.phone = obj.getString("phone")
                    }

                    if (obj.has("email")) {
                        ref!!.email = obj.getString("email")
                    }

                    if (obj.has("password")) {
                        ref!!.password = obj.getString("password")
                    }

                    if (obj.has("type")) {
                        ref!!.type = obj.getString("type")
                    }

                    if (obj.has("profilePhoto")) {
                        ref!!.profile = obj.getString("profilePhoto")
                    }

                    if (obj.has("_id")) {
                        ref!!.id = obj.getString("_id")
                    }

                    ref!!.appModeBuyer = ref!!.type.equals("buyer", true)

                }


                if (ref!!.appModeBuyer) {
                    // MyApplication.instance.getUserPreferences().appModeBuyer = false
                    switch_account.setImageResource(R.drawable.switch_account_buyer)
                    EventBus.getDefault().post(MessageEvent(true))

                } else {
                    // MyApplication.instance.getUserPreferences().appModeBuyer = true
                    switch_account.setImageResource(R.drawable.switch_account_seller)
                    EventBus.getDefault().post(MessageEvent(false))
                }
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                CustomDialog(activity!!).showErrorDialog("${getString(R.string.failed)} $errorMessage")
            }

        }).changeUserType(map)

    }

    fun setRefreshData(){

        Log.e("SADASDASDASDASD",viewpager.currentItem.toString()+"")

        when(viewpager.currentItem){
             1 -> {
                 categoriesFragment!!.setRefeshData()
             }
            2 -> {
                pickFragment!!.setRefeshData()
            }
            3 -> {
                auctionsFragment!!.setRefeshData()
            }
        }
    }

}

