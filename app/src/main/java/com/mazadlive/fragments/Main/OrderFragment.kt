package com.mazadlive.fragments.Main


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.mazadlive.R
import com.mazadlive.activities.HomeActivity
import com.mazadlive.activities.OrderDetailsActivity
import com.mazadlive.adapters.OrdersListAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.helper.Constant
import com.mazadlive.helper.DividerItemDecorator
import com.mazadlive.models.OrderModel
import com.mazadlive.parser.OrderParser
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.SortOrder
import kotlinx.android.synthetic.main.fragment_order.view.*
import org.json.JSONObject
import java.util.*

class OrderFragment : Fragment() {

    private lateinit var fragmentView : View
    private val ordersList = ArrayList<OrderModel>()
    private lateinit var progress: CustomProgress
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        fragmentView = inflater.inflate(R.layout.fragment_order, container, false)

        progress = CustomProgress(activity!!)

        fragmentView.orders_list.layoutManager = LinearLayoutManager(activity!!)
        fragmentView.orders_list.addItemDecoration(DividerItemDecorator(ContextCompat.getDrawable(activity!!, R.drawable.divider_line)))
        fragmentView.orders_list.adapter = OrdersListAdapter(activity!!, ordersList)

        (fragmentView.orders_list.adapter as OrdersListAdapter).listener = OnOrderClick()

        fragmentView.back.setOnClickListener { (activity as HomeActivity).setHomeFragment() }

        fragmentView.swipe_to_refresh.setOnRefreshListener {
            getMyOrders()
        }
        getMyOrders()

        return fragmentView
    }


    private fun getMyOrders() {

        val param = JSONObject()
        param.put("version","1.1")
        param.put("user_id",MyApplication.instance.getUserPreferences().id)


        fragmentView.progress_bar.visibility = View.VISIBLE
        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {

                fragmentView.progress_bar.visibility = View.GONE
                val orderParser =  OrderParser()

                val jsonObject = JSONObject(`object`.toString())
                if(jsonObject.has("description")){
                    val array = jsonObject.getJSONArray("description")
                    for (i in 0 until array.length()){
                        val orderModel = orderParser.parse(array.getJSONObject(i))
                        ordersList.add(orderModel)
                    }

                    if(ordersList.size > 0){
                        fragmentView!!.tx_empty.visibility = View.GONE
                        Collections.sort(ordersList,SortOrder())
                    }else{
                        fragmentView!!.tx_empty.visibility = View.VISIBLE
                    }

                    if(fragmentView.swipe_to_refresh.isRefreshing){
                        fragmentView.swipe_to_refresh.isRefreshing = false
                    }
                    (activity)!!.runOnUiThread {
                        fragmentView.orders_list.adapter.notifyDataSetChanged()
                    }
                }
            }

            override fun onError(errorMessage: String) {
                fragmentView!!.progress_bar.visibility = View.GONE
                fragmentView!!.tx_empty.visibility = View.VISIBLE
                if(fragmentView.swipe_to_refresh.isRefreshing){
                    fragmentView.swipe_to_refresh.isRefreshing = false
                }
                Toast.makeText(activity!!,"${getString(R.string.failed)} $errorMessage",Toast.LENGTH_SHORT).show()
            }

        }).orderList(param)
    }

    override fun onResume() {
        (activity!! as HomeActivity).updateBottomTab(2)
        (activity!! as HomeActivity).currentFragment = this
        super.onResume()
    }


    inner class OnOrderClick:OrdersListAdapter.OnOrderClick{
        override fun onClick(position: Int) {

            Log.e("TIME_ACTIVTTY", "${ordersList[position]!!.created_at}")
            val intent = Intent(activity!!,OrderDetailsActivity::class.java)
            intent.putExtra(Constant.ORDER_KEY,ordersList[position])
            startActivity(intent)
        }
    }

}
