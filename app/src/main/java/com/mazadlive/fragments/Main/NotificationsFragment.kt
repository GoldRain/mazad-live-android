package com.mazadlive.fragments.Main


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.mazadlive.R
import com.mazadlive.activities.HomeActivity
import com.mazadlive.adapters.TabsAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.fragments.Notification.*
import com.mazadlive.helper.MessageEvent
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.fragment_notifications.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONObject


class NotificationsFragment : Fragment() {

    lateinit var fragmentView: View
    lateinit var adapter: TabsAdapter
    lateinit var progress: CustomProgress

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val fragmentView = inflater.inflate(R.layout.fragment_notifications, container, false)

        progress = CustomProgress(activity!!)
        adapter = TabsAdapter(childFragmentManager)
        fragmentView.viewpager.adapter = adapter
        fragmentView.tablayout.setupWithViewPager(fragmentView.viewpager)
        fragmentView.back.setOnClickListener { (activity as HomeActivity).setHomeFragment() }

        setFragments()
      //  getNotification()

        return fragmentView
    }

    private fun getNotification() { //type = read / delete, number = one / all, user_id = user_id, notification_id = notification_id
        val param = JSONObject()
        param.put("version","1.1")
        param.put("type","read")
        param.put("number","all")
        param.put("user_id",MyApplication.instance.getUserPreferences().id)

        Log.e("PARAM","*** ${param.toString()}")
    //    progress.show()
        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
               // progress.dismiss()

            }

            override fun onError(errorMessage: String) {
             //   progress.dismiss()
                Toast.makeText(activity!!,"${getString(R.string.failed)} $errorMessage",Toast.LENGTH_SHORT).show()
            }
        }).notification(param)


    }


    private fun setFragments() {


        if (MyApplication.instance.getUserPreferences().appModeBuyer) {

            adapter.mFragmentList.add(ActivitiesFragment())
            adapter.mFragmentName.add(getString(R.string.hint_activity))

            adapter.mFragmentList.add(BuyerOrdersFragment())
            adapter.mFragmentName.add(getString(R.string.hint_orders))

            adapter.mFragmentList.add(BuyerBidsFragment())
            adapter.mFragmentName.add(getString(R.string.hint_bids))

            adapter.mFragmentList.add(BuyerFeedbackFragment())
            adapter.mFragmentName.add(getString(R.string.hint_feedback))

            adapter.mFragmentList.add(BuyerPaymentsFragment())
            adapter.mFragmentName.add(getString(R.string.hint_payments))

        } else {

            adapter.mFragmentList.add(ActivitiesFragment())
            adapter.mFragmentName.add(getString(R.string.hint_activity))

            adapter.mFragmentList.add(SellerOrdersFragment())
            adapter.mFragmentName.add(getString(R.string.hint_orders))

            adapter.mFragmentList.add(SellerBidsFragment())
            adapter.mFragmentName.add(getString(R.string.hint_bids))

            adapter.mFragmentList.add(SellerFeedbackFragment())
            adapter.mFragmentName.add(getString(R.string.hint_feedback))

            adapter.mFragmentList.add(SellerWalletFragment())
            adapter.mFragmentName.add(getString(R.string.hint_wallet))
        }

        adapter.notifyDataSetChanged()

    }

    override fun onResume() {
        (activity!! as HomeActivity).updateBottomTab(3)
        (activity!! as HomeActivity).currentFragment = this
        super.onResume()
    }
    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(eventBus : MessageEvent) {
        setFragments()
    }
}
