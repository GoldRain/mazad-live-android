package com.mazadlive.fragments.Main

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.TranslateAnimation
import android.widget.PopupMenu
import android.widget.Toast
import com.mazadlive.helper.PickImageHelper
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.github.florent37.runtimepermission.kotlin.askPermission
import com.mazadlive.Interface.DialogListener
import com.mazadlive.Interface.ProgressListener
import com.mazadlive.R
import com.mazadlive.activities.HomeActivity
import com.mazadlive.activities.LiveOnActivity
import com.mazadlive.activities.StreamingActivity
import com.mazadlive.activities.WebViewActivity
import com.mazadlive.activities.settings.AccountActivity
import com.mazadlive.activities.settings.FollowActivity
import com.mazadlive.activities.settings.SettingActivity
import com.mazadlive.adapters.FeedBuyerAdapter
import com.mazadlive.adapters.FeedSellerAdapter
import com.mazadlive.adapters.UserPostAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ApiUrl
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.database.MessageData
import com.mazadlive.helper.Constant
import com.mazadlive.helper.GridSpacingItemDecoration
import com.mazadlive.helper.MessageEvent
import com.mazadlive.helper.UploadImage
import com.mazadlive.models.*
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.customdialog_badge_alert.*
import kotlinx.android.synthetic.main.customdialog_badge_form.*
import kotlinx.android.synthetic.main.customdialog_post_view_recycle.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import kotlinx.android.synthetic.main.view_id_list.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONObject


class ProfileFragment : Fragment() {

    private lateinit var fragmentView: View
    private var progress: CustomProgress? = null
    var postList = ArrayList<PostModel>()
    var isMe = true
    private var isFollowing = false
    private var isUserFollow = false

    private var userRef = MyApplication.instance.getUserPreferences()
    private var userModel: UserModel? = null
    private var totalFollower = 0
    private var totalFollowing = 0
    private var imageFilePath = ""
    private var docBitmap: Bitmap? = null
    private var dialogBadgeForm: Dialog? = null
    lateinit var pickImageHelper: PickImageHelper
    private var REQUEST_BUDGE_PAYMENT = 5000

    override fun onResume() {
        (activity!! as HomeActivity).updateBottomTab(4)
        (activity!! as HomeActivity).currentFragment = this
        if (!isMe) {
            (activity!! as HomeActivity).hideTopLayout(true)
        }
        setMessageRead()
        super.onResume()
    }

    var isUserLive = "stopped"

    override fun onPause() {
        if (!isMe) {
            (activity!! as HomeActivity).hideTopLayout(false)
        }
        super.onPause()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(msg: MessageData) {
        setMessageRead()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
       // Log.e("PROFILE_UPDA","profile shjjshf  ${userRef.appModeBuyer}  ${MyApplication.instance.getUserPreferences().appModeBuyer}")
        if (MyApplication.instance.getUserPreferences().appModeBuyer) {
            fragmentView.mGoLive.visibility = View.GONE
            fragmentView.mBadgeBottomBar.visibility = View.GONE
        } else {
            fragmentView.mGoLive.visibility = View.VISIBLE
            fragmentView.mBadgeBottomBar.visibility = View.VISIBLE
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_profile, container, false)

        progress = CustomProgress(activity!!)
        pickImageHelper = PickImageHelper(activity!!, this, false, PickerHelper())

        userModel = arguments!!.getParcelable("user")
        isMe = arguments!!.getBoolean("isMe")

        if (!isMe) {

            fragmentView.mGoLive.visibility = View.GONE
            fragmentView.menu.visibility = View.VISIBLE

            fragmentView.mMenu.setImageResource(R.drawable.ic_clear_white)
            if (isUserLive.equals("running", true)) {
                fragmentView.ripple_lay.startRippleAnimation()
                val anim2 = AnimationUtils.loadAnimation(context, R.anim.zoom_in_out)
                fragmentView.frame_profile.startAnimation(anim2)
            }

            // fragmentView.live.visibility = View.VISIBLE
        } else {

            fragmentView.mMenu.setImageResource(R.drawable.menu)

            if (userRef.appModeBuyer) {
                fragmentView.mGoLive.visibility = View.GONE
            } else {
                fragmentView.mGoLive.visibility = View.VISIBLE
                setBudgeData()
            }
            fragmentView.menu.visibility = View.GONE
        }

        getUserData()
        saveUserData()

        val vto = fragmentView.viewTreeObserver
        vto.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            @SuppressLint("ObsoleteSdkInt")
            override fun onGlobalLayout() {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    fragmentView.viewTreeObserver.removeGlobalOnLayoutListener(this)
                } else {
                    fragmentView.viewTreeObserver.removeOnGlobalLayoutListener(this)
                }

                val height = fragmentView.measuredHeight
                fragmentView.relativeLayout2.requestLayout()
                fragmentView.relativeLayout2.layoutParams.height = (height * 56) / 100
            }
        })

        getUserPost()

        fragmentView.recycleview.adapter = UserPostAdapter(activity!!, postList)
        fragmentView.recycleview.layoutManager = GridLayoutManager(activity!!, 3)
        fragmentView.recycleview.addItemDecoration(GridSpacingItemDecoration(3, 6, false))

        (fragmentView.recycleview.adapter as UserPostAdapter).listener = OnItemClick()

        fragmentView.mGoLive.setOnClickListener {

            getStreamCount()
        }

        fragmentView.mMenu.setOnClickListener {
            if (!isMe) {
                (activity!! as HomeActivity).onBackPressed()
            } else {
                (activity!! as HomeActivity).openDrawer()
            }
        }

        fragmentView.mSetting.setOnClickListener { startActivity(Intent(activity!!, SettingActivity::class.java)) }

        fragmentView.mEdit.setOnClickListener {
            val intent = Intent(activity, AccountActivity::class.java)
            startActivity(intent)
        }

        fragmentView.menu.setOnClickListener {
            showPopupMenu(it)
        }

        fragmentView.lay_following.setOnClickListener {
            val intent = Intent(activity!!, FollowActivity::class.java)
            intent.putExtra("type", "following")
            intent.putExtra("isMe", isMe)

            if (!isMe) {
                intent.putExtra("userModel", userModel)
            }
            startActivity(intent)
        }


        fragmentView.lay_follow.setOnClickListener {
            val intent = Intent(activity!!, FollowActivity::class.java)
            intent.putExtra("type", "follower")
            intent.putExtra("isMe", isMe)

            if (!isMe) {
                intent.putExtra("userModel", userModel)
            }

            startActivity(intent)
        }

        fragmentView.mBadgeBottomBar.setOnClickListener {

            if (TextUtils.isEmpty(userRef.verificationStatus)) {
                showBadgeAlertDialog(false)
            } else if (userRef.verificationStatus == "approved") {
                showBadgeAlertDialog(true)
            }
        }


        return fragmentView
    }


    private fun setBudgeData(vararg arg:Int) {

        if (!userRef.verified){
            if (TextUtils.isEmpty(userRef.verificationStatus)) {
                // Show Badge Alert Dialog
                if (!userRef.badgeDailogShow) {
                    userRef.badgeDailogShow = true
                    showBadgeAlertDialog(false)
                } else {
                    fragmentView.mBadgeBottomBar.visibility = View.VISIBLE
                    fragmentView.mVerificationStatus.text = resources.getString(R.string.verification_upload)
                }
            } else {


                if (userRef.verificationStatus == "pending") {

                    fragmentView.mBadgeBottomBar.visibility = View.VISIBLE
                    fragmentView.mVerificationStatus.text = resources.getString(R.string.verification_padding)
                } else if (userRef.verificationStatus == "approved") {
                    fragmentView.mBadgeBottomBar.visibility = View.VISIBLE
                    fragmentView.mVerificationStatus.text = resources.getString(R.string.verification_approved)


                }
                fragmentView.mBadgeBottomBar.visibility = View.GONE
            }
        }else{
            fragmentView.mBadgeBottomBar.visibility = View.GONE
        }



    }

    fun setPic(requestCode: Int, resultCode: Int, data: Intent?){
        Log.e("DOC_IMAGES", "$requestCode *** onActivityResult")
        pickImageHelper.onActivityResult(requestCode, resultCode, data)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.e("DOC_IMAGES", "*** onActivityResult")
        if ( resultCode == Activity.RESULT_OK && requestCode == REQUEST_BUDGE_PAYMENT){
            val tapPaymentModel = data!!.getParcelableExtra<TapPaymentModel>("model")

            ServiceRequest(object : ApiResponseListener{
                override fun onCompleted(`object`: Any) {
                    userRef.verified = true
                    userRef.verificationStatus == "verified"
                    setBudgeData()
                }

                override fun onError(errorMessage: String) {
                    CustomDialog(activity!!).showErrorDialog(getString(R.string.error_payment))
                }

            }).verificationBudgePayment(tapPaymentModel)
        }else{
            pickImageHelper.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun getStreamCount() {
        val listener = object : DialogListener() {
            override fun okClick() {

            }
        }

        progress!!.show()
        val param = JSONObject()
        param.put("version", "1.1")
        param.put("user_id", userRef!!.id)

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                val jsonObject = JSONObject(`object`.toString())
                if (jsonObject.has("description")) {
                    val obj = jsonObject.getJSONObject("description")
                    if (obj.has("available_live_count")) {
                        val count = obj.getInt("available_live_count")
                        if (count == 0) {
                            CustomDialog(activity!!).showAlertDialog(getString(R.string.alert), getString(R.string.warning_stream), listener)
                        }else{
                            val intent = Intent(activity!!, LiveOnActivity::class.java)
                            startActivity(intent)
                        }
                    }else{
                        val intent = Intent(activity!!, LiveOnActivity::class.java)
                        startActivity(intent)
                    }

                    if(obj.has("unlimited_live_count")){
                        val subObj  =obj.getJSONObject("unlimited_live_count")
                        val subModel = SubscribeModel()
                        if(subObj.has("status")){
                            subModel.status = subObj.getBoolean("status")
                            //  ad1.status = subObj.getBoolean("status")
                            //   ad4.status = subObj.getBoolean("status")
                        }

                        if(subObj.has("activation")){
                            subModel.activation = subObj.getString("activation")
                            //ad1.activation = subObj.getString("activation")
                            //     .//    ad4.activation = subObj.getString("activation")
                        }

                        if(subObj.has("expiry")){
                            subModel.expiry = subObj.getString("expiry")
                            /* ad1.expiry = subObj.getString("expiry")
                             ad4.expiry = subObj.getString("expiry")*/
                        }
                        if(subObj.has("count")){
                            subModel.previousCount = subObj.getInt("count")

                            /* ad4.available = subObj.getInt("count").toString()
                             ad1.available = subObj.getInt("count").toString()*/
                        }

                        val count = subModel.previousCount

                        if (!subModel.status && count == 0) {
                            CustomDialog(activity!!).showAlertDialog(getString(R.string.alert), getString(R.string.warning_stream), listener)
                        }else{
                            val intent = Intent(activity!!, LiveOnActivity::class.java)
                            startActivity(intent)
                        }
                        //   ad4.subModel = subModel
                    }
                }



            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(activity!!, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).showCounts(param)
    }

    private fun moveNext() {

        val intent = Intent(activity!!, StreamingActivity::class.java)
        startActivity(intent)
        // finish()
    }

    fun setMessageRead() {
        /* doAsync {
            *//* val count = AppDatabase.getAppDatabase().messageDao().getUnreadCount()
            Log.e("Count_Count", "Count : $count")
            (activity!!).runOnUiThread {
                if (count > 0) {
                    fragmentView.mMenu.setImageResource(R.drawable.ic_menu_alert)
                    // msg_count.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.border_round_red,0)
                } else {
                    // msg_count.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                    fragmentView.mMenu.setImageResource(R.drawable.menu)
                }
            }*//*
            // Log.e(TAG, "$count ** ")
        }*/
    }

    private fun showPopupMenu(view: View) {

        val popupMenu = PopupMenu(activity!!, view)
        popupMenu.inflate(R.menu.profile_menu)

        if (isFollowing) {
            popupMenu.menu.findItem(R.id.follow).title = getString(R.string.unfollow)
        } else {
            if (isUserFollow) {
                popupMenu.menu.findItem(R.id.follow).title = getString(R.string.follow_back)
            } else {
                popupMenu.menu.findItem(R.id.follow).title = getString(R.string.follow)
            }
        }
        popupMenu.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.follow -> {
                    followPeople()
                }

                R.id.mReport -> {
                    CustomDialog(activity!!).showReportDialog(getString(R.string.give_feedbak_for_user), object : DialogListener() {
                        override fun okClick(any: Any) {
                            reportUser(any.toString())
                        }
                    })
                }

                R.id.mBlock -> {
                    blockUser()
                }
            }

            true
        }

        popupMenu.show()
    }

    private fun showProfileDialog(position: Int) {

        val dialog = Dialog(activity!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.customdialog_post_view_recycle)
        val wlp = dialog.window.attributes
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog.window.attributes = wlp

        Log.e("GET_LIST_POST", "** ${postList.size} $isMe")

        val tempPostList = ArrayList<PostModel>()
        tempPostList.add(postList.get(position))
        dialog.recyclerview.layoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.HORIZONTAL, false)
        if (isMe) {
            dialog.recyclerview.adapter = FeedSellerAdapter(activity!!, tempPostList, object : FeedSellerAdapter.OnClikPost {
                override fun onClickPost(position: Int) {

                }
            })

            (dialog.recyclerview.adapter as FeedSellerAdapter).reportListener = object : FeedSellerAdapter.ReportListener() {
                override fun onReport(position: Int) {
                    dialog.dismiss()
                    getUserPost()
                }

            }

        } else {
            dialog.recyclerview.adapter = FeedBuyerAdapter(activity!!, tempPostList, object : FeedBuyerAdapter.OnClikPost {
                override fun onClickPost(position: Int) {

                }
            })

            (dialog.recyclerview.adapter as FeedBuyerAdapter).reportListener = object : FeedBuyerAdapter.ReportListener() {
                override fun onReport(position: Int) {
                    dialog.dismiss()
                    getUserPost()
                }

            }
        }
        dialog.recyclerview.adapter.notifyDataSetChanged()
        dialog.recyclerview.scrollToPosition(position)
        dialog.show()
    }

    private fun showBadgeAlertDialog(isForPayment : Boolean) {

        val dialog = Dialog(activity!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.customdialog_badge_alert)
        val wlp = dialog.window.attributes
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog.window.attributes = wlp
        dialog.window.attributes.windowAnimations = R.style.dialog_anim


        Glide.with(activity!!).load(userRef.profile).apply(RequestOptions().override(500).error(R.drawable.dummy)).into(dialog.badge_alert_profileImage)

        val mAnimation = TranslateAnimation(0F, 0F, 0F, 50F);
        mAnimation.setDuration(500);
        mAnimation.setFillAfter(true);
        mAnimation.setRepeatCount(-1);
        mAnimation.setRepeatMode(Animation.REVERSE);

        dialog.badge_alert_im_thumb.setAnimation(mAnimation)
        val anim2 = AnimationUtils.loadAnimation(context, R.anim.zoom_in_out)
        dialog.badge_alert_im_verify.startAnimation(anim2)

        if (isForPayment){
            dialog.badge_alert_des.text = resources.getString(R.string.verification_payment_dialog)
        }else{
            dialog.badge_alert_des.text = resources.getString(R.string.verification_id_dialog)
        }

        dialog.badge_alert_continue_btn.setOnClickListener {
            dialog.dismiss()
            if (isForPayment){

                val tapPaymentModel = TapPaymentModel()
                tapPaymentModel.amount = "1100" // Payment on profile verification

                val intent = Intent(activity, WebViewActivity::class.java)
                intent.putExtra("tapModel",tapPaymentModel)
                intent.putExtra("url",ApiUrl.getPaymetLoader)
                startActivityForResult(intent,REQUEST_BUDGE_PAYMENT)
            }else{
                showBadgeFormDialog()
            }
        }

        dialog.badge_alert_later_btn.setOnClickListener {
            dialog.dismiss()
            userRef.showVerifyDialog = false
        }

        dialog.setOnDismissListener {
            fragmentView.mBadgeBottomBar.visibility = View.VISIBLE
            fragmentView.mVerificationStatus.text = resources.getString(R.string.verification_upload)
        }
        dialog.show()
    }

    private fun showBadgeFormDialog() {

        docBitmap = null
        imageFilePath = ""
        if (dialogBadgeForm == null) {
            dialogBadgeForm = Dialog(activity!!)
            dialogBadgeForm!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialogBadgeForm!!.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            dialogBadgeForm!!.setCanceledOnTouchOutside(false)
            dialogBadgeForm!!.setContentView(R.layout.customdialog_badge_form)
            val wlp = dialogBadgeForm!!.window.attributes
            wlp.width = WindowManager.LayoutParams.MATCH_PARENT
            wlp.height = WindowManager.LayoutParams.MATCH_PARENT
            dialogBadgeForm!!.window.attributes = wlp
            dialogBadgeForm!!.window.attributes.windowAnimations = R.style.dialog_anim


            dialogBadgeForm!!.badge_form_frame_doc_type.setOnClickListener {
                dialogBadgeForm!!.badge_form_card_list_layout.visibility = View.VISIBLE
            }

            val cardsList = java.util.ArrayList<String>()
            //cardsList.add("Id Type")
            cardsList.add("Driving License")
            cardsList.add("Passport")
            cardsList.add("Id")

            for ((index, card) in cardsList.withIndex()) {
                val itemView = layoutInflater.inflate(R.layout.view_id_list, null)
                itemView.id_name.text = card
                itemView.setOnClickListener {

                    dialogBadgeForm!!.badge_form_doc_type.text = card
                    dialogBadgeForm!!.badge_form_card_list_layout.visibility = View.GONE
                }
                dialogBadgeForm!!.badge_form_type_list.addView(itemView)
            }

            dialogBadgeForm!!.badge_form_add.setOnClickListener {
                askPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) {
                    pickImageHelper.pickImage(4)
                }.onDeclined { result ->
                    if (result.hasDenied()) {
                        CustomDialog(activity!!).showErrorDialog(getString(R.string.permission_required))
                    } else if (result.hasForeverDenied()) {
                        result.foreverDenied.forEach {
                            CustomDialog(activity!!).permissionDialog(getString(R.string.accept_permission), object : DialogListener() {
                                override fun yesClick() {
                                    super.yesClick()
                                    result.goToSettings()
                                }
                            })
                        }
                    }
                }
            }

            dialogBadgeForm!!.badge_form_select_btn.setOnClickListener {
                if (docValidation()) {

                    val name = "DOC_${System.currentTimeMillis()}"
                    val file = Constant.savePostImage(docBitmap!!, name)
                    progress!!.show()
                    UploadImage(activity!!, file!!, "doc").uploadToAws(object : ProgressListener {
                        override fun onProgressUpdate(progress: Int) {

                        }

                        override fun onSuccess(path: String) {
                            val map = JSONObject()
                            map.put("user_id", userRef!!.id)
                            map.put("id_type", dialogBadgeForm!!.badge_form_doc_type.text.toString().trim())
                            map.put("id_number", dialogBadgeForm!!.badge_form_doc_number.text.toString().trim())
                            map.put("image_url", path)

                            map.put("version", "1.1")
                            uploadDocumentToServer(map)
                        }

                        override fun onFileNotFound() {
                            Toast.makeText(activity!!, "${getString(R.string.failed)}", Toast.LENGTH_SHORT).show()
                        }

                        override fun onCancel() {
                            Toast.makeText(activity!!, "${getString(R.string.failed)}", Toast.LENGTH_SHORT).show()
                        }

                    })
                }
            }

        }
        dialogBadgeForm!!.show()
    }

    private fun docValidation(): Boolean {

        if (docBitmap == null) {
            Toast.makeText(activity!!, getString(R.string.select_doc_image), Toast.LENGTH_SHORT).show()
            return false
        }

        if (TextUtils.isEmpty(dialogBadgeForm!!.badge_form_doc_type.toString().trim())) {
            Toast.makeText(activity!!, getString(R.string.select_doc_type), Toast.LENGTH_SHORT).show()
            return false
        }

        if (TextUtils.isEmpty(dialogBadgeForm!!.badge_form_doc_number.toString().trim())) {
            Toast.makeText(activity!!, getString(R.string.select_doc_num), Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }


    private fun updateValue() {
        fragmentView.post_count.setText("${postList.size}")
        fragmentView.follow.setText("$totalFollower")
        fragmentView.following.setText("$totalFollowing")
    }

    private fun saveUserData() {
        var profileUrl = userRef.profile
        var username = userRef.username
        var countryName = userRef.CountryName
        var des = userRef.description

        if (!isMe) {
            profileUrl = userModel!!.profile
            username = userModel!!.username
            countryName = userModel!!.countryName
            des = ""
            fragmentView.mEdit.visibility = View.GONE
            fragmentView.mSetting.visibility = View.GONE
            fragmentView.menu.visibility = View.VISIBLE

            if (userModel!!.verified) {
                fragmentView.im_verify.setImageResource(R.drawable.ic_verify)
            } else {
                fragmentView.im_verify.setImageResource(R.drawable.ic_unverified)
            }
        } else {
            if (userRef.verified) {
                fragmentView.im_verify.setImageResource(R.drawable.ic_verify)
            } else {
                fragmentView.im_verify.setImageResource(R.drawable.ic_unverified)
            }

        }
        Glide.with(activity!!).load(profileUrl).apply(RequestOptions().override(500).error(R.drawable.dummy)).into(fragmentView.mProfileImage)
        fragmentView.user_name.text = username
        fragmentView.loaction.text = countryName
        fragmentView.details.text = des
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }


     fun getUserPost() {
        val map = JSONObject()
        map.put("user_id", userRef!!.id)
        map.put("version", "1.1")

        postList.clear()
        if (!isMe) {
            map.put("user_id", userModel!!.id)
        }

        Log.e("PROFILE_PARAM", "${map.toString()}")
        progress!!.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                fragmentView.recycleview.adapter.notifyDataSetChanged()
                updateValue()
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(activity!!, "${getString(R.string.failed)} $errorMessage", Toast.LENGTH_SHORT).show()
                // CustomDialog(activity!!).showErrorDialog("Failed to get post $errorMessage")
            }

        }).getPost(postList, map, ApiUrl.getMyPosts, false)
    }

    private fun followPeople() {

        val map = JSONObject()
        map.put("self_id", userRef!!.id)
        map.put("his_id", userModel!!.id)
        map.put("version", "1.1")

        if (isFollowing) {
            map.put("is_follow", 0)
        } else {
            map.put("is_follow", 1)
        }


        progress!!.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                isFollowing = !isFollowing
                getUserData()

            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(activity!!, "${getString(R.string.failed)} $errorMessage", Toast.LENGTH_SHORT).show()
                // CustomDialog(activity!!).showErrorDialog("Failed to get post $errorMessage")
            }

        }).followPeople(map)
    }

    private fun getUserData() {
        val map = JSONObject()
        if (isMe) {
            map.put("user_id", userRef!!.id)
        } else {
            map.put("user_id", userModel!!.id)
        }
        map.put("version", "1.1")
        progress!!.show()

        Log.e("getUserData","${map.toString()}")
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                val json = JSONObject(`object`.toString())
                if (json.has("description")) {
                    val userArray = json.getJSONArray("description")
                    for (i in 0 until userArray.length()) {
                        val user = UserModel()
                        val obj = userArray.getJSONObject(i)

                        if (obj.has("following")) {

                            val array1 = obj.getJSONArray("following")

                            totalFollowing = array1.length()

                            val followingList = ArrayList<UserModel>()
                            for (i in 0 until array1.length()) {

                                val userModel = UserModel()
                                val following = array1.getJSONObject(i)
                                if (following.has("_id")) {
                                    if (userRef.id.equals(following.getString("_id"))) {
                                        isUserFollow = true
                                        break
                                    } else {
                                        isUserFollow = false
                                    }
                                }

                                if (following.has("username")) {
                                    userModel.username = following.getString("username")
                                }

                                if (following.has("country_code")) {
                                    userModel.countryCode = following.getString("country_code")
                                }

                                if (following.has("country_name")) {
                                    userModel.countryName = following.getString("country_name")
                                }

                                if (following.has("phone")) {
                                    userModel.phone = following.getString("phone")
                                }

                                if (following.has("email")) {
                                    userModel.email = following.getString("email")
                                }
                                if (following.has("type")) {
                                    userModel.type = following.getString("type")
                                }

                                if (following.has("is_live")) {
                                    isUserLive = following.getString("is_live")
                                }

                                if (following.has("profilePhoto")) {
                                    userModel.profile = following.getString("profilePhoto")
                                }

                                if (obj.has("verification")) {
                                    val vObj = obj.getJSONObject("verification")
                                    Log.e("getUserData","${vObj.toString()}")
                                    if (vObj.has("status")) {
                                        if (vObj.getString("status").equals("approved", true)) {
                                          //  userModel.verified = true
                                        }
                                    }
                                }


                                if(obj.has("is_verified")){
                                    userModel.verified = obj.get("is_verified").toString().equals("true",true)
                                }else{
                                    userModel.verified = false
                                }

                                followingList.add(userModel)
                            }
                        }

                        if (obj.has("followers")) {
                            val userModel = UserModel()
                            val array2 = obj.getJSONArray("followers")
                            totalFollower = array2.length()

                            val followerList = ArrayList<UserModel>()
                            for (i in 0 until array2.length()) {
                                val follower = array2.getJSONObject(i)

                                Log.e("FOLLOWER", " ** ${follower.toString()}")
                                if (follower.has("_id")) {

                                    if (userRef.id.equals(follower.getString("_id"))) {
                                        isFollowing = true
                                        break
                                    } else {
                                        isFollowing = false
                                    }
                                }

                                if (follower.has("username")) {
                                    userModel.username = follower.getString("username")
                                }

                                if (follower.has("country_code")) {
                                    userModel.countryCode = follower.getString("country_code")
                                }

                                if (follower.has("country_name")) {
                                    userModel.countryName = follower.getString("country_name")
                                }

                                if (follower.has("phone")) {
                                    userModel.phone = follower.getString("phone")
                                }

                                if (follower.has("email")) {
                                    userModel.email = follower.getString("email")
                                }
                                if (follower.has("type")) {
                                    userModel.type = follower.getString("type")
                                }

                                if (follower.has("profilePhoto")) {
                                    userModel.profile = follower.getString("profilePhoto")
                                }
                                if(obj.has("is_verified")){
                                    userModel.verified = obj.get("is_verified").toString().equals("true",true)
                                }else{
                                    userModel.verified = false
                                }

                                followerList.add(userModel)
                            }
                        }
                        updateValue()
                    }
                }
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(activity!!, "${getString(R.string.failed)} $errorMessage", Toast.LENGTH_SHORT).show()
                // CustomDialog(activity!!).showErrorDialog("Failed to get post $errorMessage")
            }

        }).userDetails(map)
    }

    private fun blockUser() {

        val param = JSONObject()
        param.put("version", "1.1")
        param.put("his_id", userModel!!.id)
        param.put("self_id", userRef.id)
        param.put("is_block", 1)

        Log.e("PARAM:", "${param.toString()} **")
        progress!!.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                (activity!!).onBackPressed()
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(activity!!, "Could not block $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).blockUser(param)
    }

    private fun reportUser(msg: String) {
        val param = JSONObject()
        param.put("version", "1.1")
        param.put("his_id", userModel!!.id)
        param.put("self_id", userRef.id)
        param.put("reason", msg)
        param.put("message", msg)

        progress!!.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                (activity!!).onBackPressed()
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(activity!!, "${getString(R.string.failed)} $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).reportUser(param)
    }

    private fun uploadDocumentToServer(param: JSONObject) {

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()

                userRef.showVerifyDialog = false
                dialogBadgeForm!!.dismiss() //you will be contacted by admin
                CustomDialog(activity!!).showAlertDialog(getString(R.string.alert), getString(R.string.doc_uploaded), object : DialogListener() {
                    override fun okClick() {
                        userRef.verificationStatus = "pending"
                        setBudgeData(3)
                        getUserData()
                    }
                })
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(activity!!, "${getString(R.string.failed)} $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).userVerification(param)
    }

    inner class OnItemClick : UserPostAdapter.OnItemClick {
        override fun onItemClick(position: Int) {
            showProfileDialog(position)
        }
    }


    inner class PickerHelper : PickImageHelper.OnImagePickerListener {

        override fun onImagePicked(imagePath: String, requestCode: Int) {
            Log.e("DOC_IMAGES", "*** $imagePath")

            Glide.with(activity!!).asBitmap().load(imagePath).apply(RequestOptions().override(1500)).listener(object : RequestListener<Bitmap> {
                override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                    return false
                }

                override fun onResourceReady(resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                    Log.e("DOC_IMAGES", "*** $imagePath")

                    dialogBadgeForm!!.badge_form_doc_image.setImageBitmap(resource)
                    docBitmap = resource
                    return true
                }

            }).into(dialogBadgeForm!!.badge_form_doc_image)
        }
    }

}
