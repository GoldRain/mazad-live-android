package com.mazadlive.fragments.Main

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.mazadlive.R
import com.mazadlive.activities.HomeActivity
import com.mazadlive.fragments.Search.CategoriesFragment
import com.mazadlive.fragments.Search.PlacesFragment
import com.mazadlive.fragments.Search.TopFragment
import com.mazadlive.fragments.Search.UsersFragment
import com.mazadlive.models.UserModel
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.fragment_search.view.*
import org.greenrobot.eventbus.EventBus
import com.mazadlive.R.id.view
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView


class SearchFragment : Fragment() {
    var currentPosition = 0
    private lateinit var fragmentView : View

    var topFragment:TopFragment?= null
    var usersFragment:UsersFragment?= null
    var categoriesFragment:CategoriesFragment?= null
    var placesFragment:PlacesFragment?= null

    var linkPostId = ""
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_search, container, false)

        val fragments = ArrayList<Fragment>()

        topFragment = TopFragment()
        topFragment!!.linkPostId = linkPostId

        usersFragment = UsersFragment()

        usersFragment!!.serachFrag = this

        categoriesFragment = CategoriesFragment()
        placesFragment = PlacesFragment()

        fragments.add(topFragment!!)
        fragments.add(usersFragment!!)
        fragments.add(categoriesFragment!!)
        fragments.add(placesFragment!!)

        topFragment!!.searchFragment = this
        usersFragment!!.searchFragment = this
        categoriesFragment!!.searchFragment = this
        placesFragment!!.searchFragment = this


        topFragment!!.fragPositon = 0
        usersFragment!!.fragPositon = 1
        categoriesFragment!!.fragPositon = 2
        placesFragment!!.fragPositon = 3

        fragmentView.viewpager.adapter = ViewPagerAdapter(activity!!,childFragmentManager,fragments)
        fragmentView.tablayout.setupWithViewPager(fragmentView.viewpager)

        fragmentView.back.setOnClickListener { (activity as HomeActivity).setHomeFragment() }
        fragmentView.search_icon.setOnClickListener {

            searchData()
        }



        fragmentView.et_search.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if(TextUtils.isEmpty(fragmentView.et_search.text.toString().trim())){
                    fragmentView.search_icon.visibility = View.GONE
                    searchData()
                }else{
                    fragmentView.search_icon.visibility = View.VISIBLE
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        fragmentView.viewpager.addOnPageChangeListener(object :ViewPager.OnPageChangeListener{
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {

                currentPosition = position
                EventBus.getDefault().post(fragments[position])
            }

            override fun onPageScrollStateChanged(state: Int) {

            }

        })

       fragmentView.et_search.setOnKeyListener(object :View.OnKeyListener{
           override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {

               if ((event!!.action == KeyEvent.KEYCODE_SEARCH) &&
                       (keyCode == KeyEvent.KEYCODE_SEARCH)) {

                   val imm = activity!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                   imm.hideSoftInputFromWindow(fragmentView.et_search.getWindowToken(), 0)
                   searchData()

                   return true;
               }

               return false
           }


       })

        fragmentView.et_search.setOnEditorActionListener(object :TextView.OnEditorActionListener{
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    val imm = activity!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(fragmentView.et_search.getWindowToken(), 0)
                    searchData()

                    return true;
                }
                return false
            }

        })

        return fragmentView
    }



    class ViewPagerAdapter(val context: Context,fm: FragmentManager, var mFragmentList: List<Fragment>) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            return mFragmentList.get(position)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            var title = ""

            when (position) {
                0 -> {
                    title = context.getString(R.string.serach_top)
                }
                1 -> {
                    title = context.getString(R.string.search_users)
                }
                2 -> {
                    title = context.getString(R.string.search_categories)
                }
                3 -> {
                    title = context.getString(R.string.search_palces)
                }
            }
            return title
        }
    }

    override fun onResume() {
        (activity!! as HomeActivity).updateBottomTab(1)
        (activity!! as HomeActivity).currentFragment = this
        super.onResume()
    }


    private fun searchData() {

        when(fragmentView.viewpager.currentItem){
            0 -> {

            }
            1 -> {
                usersFragment!!.searchData(et_search.text.toString())
            }
            2 -> {
                categoriesFragment!!.searchData(et_search.text.toString())
            }
            3 -> {
                //placesFragment!!.searchData(et_search.text.toString())
            }
        }
    }


     fun setProfileFragment(userModel: UserModel){

         val bundle = Bundle()

         if (userModel.id.equals(MyApplication.instance.getUserPreferences().id)) {
             bundle.putBoolean("isMe", true)
         }
         bundle.putParcelable("user", userModel)
         (context as HomeActivity).setProfileFragment(bundle)
    }


}
