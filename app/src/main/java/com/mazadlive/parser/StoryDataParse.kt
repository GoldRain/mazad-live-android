package com.mazadlive.parser

import android.text.TextUtils
import android.util.Log
import com.mazadlive.activities.PlayerActivity
import com.mazadlive.activities.StreamingActivity
import com.mazadlive.database.AppDatabase
import com.mazadlive.models.*
import com.mazadlive.utils.MyApplication
import org.jetbrains.anko.doAsync
import org.json.JSONArray
import org.json.JSONObject
import java.util.ArrayList

/**
 * Created by bodacious on 8/12/18.
 */
class StoryDataParse {

    fun parseStoryList(storyList: ArrayList<StoryModel>, storyObjArray: JSONArray) {
        storyList.clear()
        for (i in 0 until storyObjArray.length()) {
            val postObj = storyObjArray.getJSONObject(i)
            val storyModel = parse(postObj)

            storyList.add(storyModel)
        }

    }

    fun parse(jsonObject: JSONObject): StoryModel {

        Log.e("JSON", "${jsonObject.toString()}")
        val storyModel = StoryModel()

        if (jsonObject.has("_id")) {
            storyModel.id = jsonObject.getString("_id")
        }

        if (jsonObject.has("url")) {
            storyModel.url = jsonObject.getString("url").replace(",", "")
        }

        if (jsonObject.has("product_description")) {
            storyModel.product_description = jsonObject.getString("product_description")
        }

        if (jsonObject.has("created_at")) {
            storyModel.createdAt = jsonObject.getString("created_at")
        } else {
            if (jsonObject.has("created_time")) {
                storyModel.createdAt = jsonObject.getString("created_time")
            }
        }

        if (jsonObject.has("is_buyNow")) {
            storyModel.is_buyNow = jsonObject.getBoolean("is_buyNow")
        }

        if (jsonObject.has("is_sold")) {
            storyModel.is_sold = jsonObject.getBoolean("is_sold")
        }


        if (jsonObject.has("is_video")) {
            Log.e("JSON", "${jsonObject.getBoolean("is_video")}")
            storyModel.is_video = jsonObject.getBoolean("is_video")
        }

        if (jsonObject.has("status")) {
            storyModel.status = jsonObject.getString("status")
        }

        if (jsonObject.has("whatsapp_only")) {
            storyModel.whatsapp_only = jsonObject.getBoolean("whatsapp_only")
        }

        if (jsonObject.has("whatsapp_and_call")) {
            storyModel.whatsapp_and_call = jsonObject.getBoolean("whatsapp_and_call")
        }

        if (jsonObject.has("product_serial")) {
            storyModel.serial = jsonObject.getString("product_serial")
        }

        if (jsonObject.has("buyNowPrice")) {
            storyModel.buyNowPrice = jsonObject.getString("buyNowPrice")
        }

        if (jsonObject.has("user_id")) {
            storyModel.user_id = jsonObject.getString("user_id")
        }

        if (jsonObject.has("userDetails")) {
            val obj = jsonObject.getJSONArray("userDetails")
            for (i in 0 until obj.length()) {
                val userObj = obj.getJSONObject(0)
                val userModel = UserParser().parse(userObj)
                storyModel.userData = userModel
            }
        }

        if (jsonObject.has("bidsAndComments")) {
            val bidList = ArrayList<BidModel>()
            val bidArray = jsonObject.getJSONArray("bidsAndComments")
            for (i in 0 until bidArray.length()) {

                val bidObj = bidArray.getJSONObject(i)
                val bid = BidModel("")
                if (bidObj.has("_id")) {
                    bid.id = bidObj.getString("_id")
                }

                if (bidObj.has("post_id")) {
                    bid.postId = bidObj.getString("post_id")
                }
//user_id
                if (bidObj.has("bid_user_id")) {
                    bid.userId = bidObj.getString("bid_user_id")
                }

                if (bidObj.has("user_id")) {
                    bid.userId = bidObj.getString("user_id")
                }

                if (bidObj.has("bid_comment")) {
                    bid.comment = bidObj.getString("bid_comment")
                }
//amount
                if (bidObj.has("bid_status")) {
                    bid.status = bidObj.getString("bid_status")
                } else {
                    if (bidObj.has("status")) {
                        bid.status = bidObj.getString("status")
                    }
                }

                if (bidObj.has("bid_amount")) {
                    bid.bidAmount = bidObj.getString("bid_amount")
                } else {
                    if (bidObj.has("amount")) {
                        bid.bidAmount = bidObj.getString("amount")
                    }
                }


                if (bidObj.has("bid_created_at")) {
                    bid.createdAt = bidObj.getString("bid_created_at")
                } else {
                    if (bidObj.has("created_at")) {
                        bid.createdAt = bidObj.getString("created_at")
                    }
                }

                if (bidObj.has("is_pinned")) {
                    bid.isPinned = bidObj.getBoolean("is_pinned")
                }

                if (bidObj.has("is_paid")) {
                    bid.isPaid = bidObj.getBoolean("is_paid")
                }

                if (bidObj.has("user_name")) {
                    bid.username = bidObj.getString("user_name")
                }


                if (MyApplication.instance.getUserPreferences().id.equals(bid.userId)) {
                    storyModel.userBid = bid
                }

                bidList.add(bid)
            }

            storyModel.bidList = bidList
        }

        if (jsonObject.has("bids")) {
            val bidList = ArrayList<BidModel>()
            val bidArray = jsonObject.getJSONArray("bids")
            for (i in 0 until bidArray.length()) {

                val bidObj = bidArray.getJSONObject(i)
                val bid = BidModel("")
                if (bidObj.has("_id")) {
                    bid.id = bidObj.getString("_id")
                }

                if (bidObj.has("post_id")) {
                    bid.postId = bidObj.getString("post_id")
                }

                if (bidObj.has("bid_user_id")) {
                    bid.userId = bidObj.getString("bid_user_id")
                }

                if (bidObj.has("user_id")) {
                    bid.userId = bidObj.getString("user_id")
                }

                if (bidObj.has("bid_comment")) {
                    bid.comment = bidObj.getString("bid_comment")
                }

                if (bidObj.has("bid_status")) {
                    bid.status = bidObj.getString("bid_status")
                } else {
                    if (bidObj.has("status")) {
                        bid.status = bidObj.getString("status")
                    }
                }

                if (bidObj.has("bid_amount")) {
                    bid.bidAmount = bidObj.getString("bid_amount")
                }

                if (bidObj.has("bid_created_at")) {
                    bid.createdAt = bidObj.getString("bid_created_at")
                }

                if (bidObj.has("is_pinned")) {
                    bid.isPinned = bidObj.getBoolean("is_pinned")
                }

                if (bidObj.has("is_paid")) {
                    bid.isPaid = bidObj.getBoolean("is_paid")
                }

                if (bidObj.has("user_name")) {
                    bid.username = bidObj.getString("user_name")
                }

                if (MyApplication.instance.getUserPreferences().id.equals(bid.userId)) {
                    storyModel.userBid = bid
                }
                bidList.add(bid)
            }

            storyModel.bidList = bidList
        }

        if (jsonObject.has("feedback")) {
            val fArray = jsonObject.getJSONArray("feedback")
            val parser = PostFeedBackParser()
            val list = ArrayList<PostFeedbackModel>()
            for (i in 0 until fArray.length()) {
                val feedObj = fArray.getJSONObject(i)
                val postFeedbackModel = parser.parse(feedObj)
                list.add(postFeedbackModel)
            }
            storyModel.feedbackList = list
        }

        return storyModel
    }

}