package com.mazadlive.parser

import android.util.Log
import com.mazadlive.api.ApiUrl
import com.mazadlive.database.AppDatabase
import com.mazadlive.models.*
import com.mazadlive.utils.MyApplication
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.doAsync
import org.json.JSONArray
import org.json.JSONObject
import java.util.ArrayList

/**
 * Created by bodacious on 15/12/18.
 */
class PostDataParser {

    val TAG = "PostDataParser"
    fun parsePostList(postList: ArrayList<PostModel>, postObjArray: JSONArray, flag: Boolean) {
        postList.clear()
        for (i in 0 until postObjArray.length()) {
            Log.e(TAG, "POST_ ${postObjArray.getJSONObject(i)}")
            val postObj = postObjArray.getJSONObject(i)
            val postModel = parse(postObj)

            doAsync {
                val favPost = AppDatabase.getAppDatabase().favPostDao().getFavPost(postModel.id)
                if (favPost != null) {
                    postModel.isFav = favPost.post_id.equals(postModel.id, true)
                }
            }

            postList.add(postModel)
        }


    }

    fun parse(postObj1: JSONObject): PostModel {
        var postObj = postObj1
        if (postObj1.has("favPost")) {
            postObj = postObj1.getJSONObject("favPost")
        }
        Log.e("POST_OBJ", "******  ${postObj.toString()}")
        val posModel = PostModel()
        if (postObj.has("_id")) {
            posModel.id = postObj.getString("_id")
        }

        if (postObj.has("latitude")) {
            posModel.latitude = postObj.getString("latitude")
        }

        if (postObj.has("longitude")) {
            posModel.longitude = postObj.getString("longitude")
        }

        if (postObj.has("place_name")) {
            posModel.place_name = postObj.getString("place_name")
        }

        if (postObj.has("post_user_id")) {
            posModel.post_user_id = postObj.getString("post_user_id")
            //Log.e("API_ALl_Post_User", "${posModel.post_user_id} ** ")
        }//description

        if (postObj.has("details")) {
            posModel.description = postObj.getString("details")
            //Log.e("API_ALl_Post_User", "${posModel.post_user_id} ** ")
        }

        if (postObj.has("description")) {
            posModel.description = postObj.getString("description")
        }

        if (postObj.has("payment_type")) {
            posModel.payment_type = postObj.getString("payment_type")
        }

        if (postObj.has("language")) {
            posModel.language = postObj.getString("language")
        }

        if (postObj.has("post_created_at")) {
            posModel.post_created_at = postObj.getString("post_created_at")
        }

        if (postObj.has("status")) {
            posModel.status = postObj.getString("status")
        }

        if (postObj.has("is_instagram")) {
            posModel.is_instagram = postObj.getBoolean("is_instagram")
        }
        if (postObj.has("is_twitter")) {
            posModel.is_twitter = postObj.getBoolean("is_twitter")
        }

        if (postObj.has("is_auction")) {
            posModel.is_auction = postObj.getBoolean("is_auction")
        }
        if (postObj.has("buy_now")) {
            posModel.buy_now = postObj.getBoolean("buy_now")
        }

        if (postObj.has("whatsapp_only")) {
            posModel.whatsapp_only = postObj.getBoolean("whatsapp_only")
        }

        if (postObj.has("is_deleted")) {
            posModel.is_deleted = postObj.getBoolean("is_deleted")
        }

        if (postObj.has("whatsapp_and_call")) {
            posModel.whatsapp_and_call = postObj.getBoolean("whatsapp_and_call")
        }

        if (postObj.has("is_sold")) {
            posModel.is_sold = postObj.getBoolean("is_sold")
        }
//only_user_from_selected_country
        if (postObj.has("is_story")) {
            posModel.is_story = postObj.getBoolean("is_story")
        }

        if (postObj.has("only_user_from_selected_country")) {
            posModel.only_user_from_selected_country = postObj.getBoolean("only_user_from_selected_country")
        }

        if (postObj.has("amount")) {
            posModel.amount = postObj.getString("amount").toString()
        }

        if (postObj.has("product_serial")) {
            posModel.serial = postObj.getString("product_serial")
        }

        if (postObj.has("bids")) {

            val bidList = ArrayList<BidModel>()
            val bidArray = postObj.getJSONArray("bids")

            val bidParse = BidDataParser()
            for (i in 0 until bidArray.length()) {

                val bidObj = bidArray.getJSONObject(i)
                val bid = bidParse.parse(bidObj)
                /*if (bidObj.has("_id")) {
                    bid.id = bidObj.getString("_id")
                }

                if (bidObj.has("status")) {
                    bid.status = bidObj.getString("status")
                }

                if (bidObj.has("post_id")) {
                    bid.postId = bidObj.getString("post_id")
                }


                if (bidObj.has("bid_user_id")) {
                    bid.userId = bidObj.getString("bid_user_id")
                }

                if (bidObj.has("bid_comment")) {
                    bid.comment = bidObj.getString("bid_comment")
                }

                if (bidObj.has("bid_amount")) {
                    bid.bidAmount = bidObj.getString("bid_amount")
                }

                if (bidObj.has("bid_created_at")) {
                    bid.createdAt = bidObj.getString("bid_created_at")
                }

                if (bidObj.has("is_pinned")) {
                    bid.isPinned = bidObj.getBoolean("is_pinned")
                }

                if (bidObj.has("is_paid")) {
                    bid.isPaid = bidObj.getBoolean("is_paid")
                }*/

                bidList.add(bid)
            }


            posModel.bidsList = bidList
        }

        if (postObj.has("tags")) {
            val tagList = ArrayList<TagModel>()
            val tagArray = postObj.getJSONArray("tags")
            for (i in 0 until tagArray.length()) {
                val tagObj = tagArray.getJSONObject(i)
                val tagModel = TagModel()

                if (tagObj.has("_id")) {
                    tagModel.id = tagObj.getString("_id")
                }

                if (tagObj.has("name")) {
                    tagModel.name = tagObj.getString("name")
                }

                if (tagObj.has("createdAt")) {
                    tagModel.createdAt = tagObj.getString("createdAt")
                }

                tagList.add(tagModel!!)
            }
            posModel.selectedTags = tagList
        }

        if (postObj.has("available_country_id")) {
            val countryList = ArrayList<CountryModel>()
            val countryArray = postObj.getJSONArray("available_country_id")
            for (i in 0 until countryArray.length()) {
                val countryObj = countryArray.getJSONObject(i)
                val countryModel = CountryModel()

                if (countryObj.has("_id")) {
                    countryModel.id = countryObj.getString("_id")
                }

                if (countryObj.has("countryName")) {
                    countryModel.name = countryObj.getString("countryName")
                }

                if (countryObj.has("countryCode")) {
                    countryModel.code = countryObj.getString("countryCode")
                }


                if (countryObj.has("flagImageUrl")) {
                    countryModel.flag = "${ApiUrl.server}${countryObj.getString("flagImageUrl")}"
                }

                if (countryObj.has("createdAt")) {
                    //countryModel.cr = countryObj.getString("createdAt")
                }

                if (posModel.only_user_from_selected_country) {

                    if (MyApplication.instance.getUserPreferences().countryId.equals(countryModel.id)) {
                        posModel.isBuyAvailable = true
                    }
                } else {
                    posModel.isBuyAvailable = true
                }

                countryList.add(countryModel!!)
            }
            posModel.selectedCountry = countryList
        }


        if (postObj.has("post_images")) {

            val imgList = ArrayList<PostImages>()
            val imgArray = postObj.getJSONArray("post_images")
            for (i in 0 until imgArray.length()) {
                val postImage = PostImages()
                val imgObj = imgArray.getJSONObject(i)
                if (imgObj.has("_id")) {
                    postImage.id = imgObj.getString("_id")
                }

                if (imgObj.has("image_url")) {
                    postImage.url = imgObj.getString("image_url")
                }

                if (imgObj.has("is_video")) {
                    postImage.isVideo = imgObj.getBoolean("is_video")
                }

                imgList.add(postImage)
            }

            //  Log.e("POATIMAGE_IM","${imgList.size} ***")
            posModel.postImages = imgList
        }

        if (postObj.has("userDetails")) {

            val userArray = postObj.getJSONArray("userDetails")

            for (i in 0 until userArray.length()) {
                val userObj = userArray.getJSONObject(i)
                val userData = UserParser().parse(userObj)
                posModel.userData = userData
            }
        }

        if (postObj.has("post_like")) {
            val likeArray = postObj.getJSONArray("post_like")
            Log.e("LIKE_DETAILS", "*** ${likeArray.toString()}")

            for (j in 0 until likeArray.length()) {
                val userObj = likeArray.getJSONObject(j)
                val userData = UserModel()
                if (userObj.has("username")) {
                    userData.username = userObj.getString("username")
                }

                if (userObj.has("country_code")) {
                    userData.countryCode = userObj.getString("country_code")
                }

                if (userObj.has("phone")) {
                    userData.phone = userObj.getString("phone")
                }

                if (userObj.has("email")) {
                    userData.email = userObj.getString("email")
                }

                if (userObj.has("password")) {
                    //userData.password = userObj.getString("password")
                }

                if (userObj.has("type")) {
                    userData.type = userObj.getString("type")
                }

                if (userObj.has("profilePhoto")) {
                    userData.profile = userObj.getString("profilePhoto")
                }

                if (userObj.has("_id")) {
                    userData.id = userObj.getString("_id")
                }

                if (userData.id.equals(MyApplication.instance.getUserPreferences().id, true)) {
                    posModel.isLike = true
                    break
                }
                // posModel.userData = userData
            }
        }

        if (postObj.has("percent")) {
            posModel.percent = postObj.getString("percent")
            // Log.e("VOTE_DETAILS", "Per *** ${postObj.getString("percent").toString()}")
        }

        if(postObj.has("is_promoted")){
            posModel.isPromoted = postObj.get("is_promoted").toString().equals("true",true)
        }

        if (postObj.has("vote")) {
            val voteArray = postObj.getJSONArray("vote")
            Log.e("VOTE_DETAILS", "*** ${voteArray.toString()}")

            for (j in 0 until voteArray.length()) {
                val voteObj = voteArray.getJSONObject(j)

                if (voteObj.has("user_id")) {
                    if (MyApplication.instance.getUserPreferences().id.equals(voteObj.getString("user_id"))) {
                        if (voteObj.has("response")) {
                            posModel.isVote = voteObj.get("response").toString().toInt()
                        }
                    }
                }

            }
        }


        if (postObj.has("feedback")) {
            val fArray = postObj.getJSONArray("feedback")
            val parser = PostFeedBackParser()
            val list = ArrayList<PostFeedbackModel>()
            for (i in 0 until fArray.length()) {
                val feedObj = fArray.getJSONObject(i)
                val postFeedbackModel = parser.parse(feedObj)
                list.add(postFeedbackModel)
            }
            posModel.feedbackList = list
        }

        return posModel
    }

    fun parseForBid(postObj1: JSONObject): PostModel {
        var postObj = postObj1
        if (postObj1.has("favPost")) {
            postObj = postObj1.getJSONObject("favPost")
        }
        Log.e("POST_OBJ", "******  ${postObj.toString()}")
        val posModel = PostModel()
        if (postObj.has("_id")) {
            posModel.id = postObj.getString("_id")
        }

        if (postObj.has("latitude")) {
            posModel.latitude = postObj.getString("latitude")
        }

        if (postObj.has("longitude")) {
            posModel.longitude = postObj.getString("longitude")
        }

        if (postObj.has("place_name")) {
            posModel.place_name = postObj.getString("place_name")
        }

        if (postObj.has("post_user_id")) {
            posModel.post_user_id = postObj.getString("post_user_id")
            //Log.e("API_ALl_Post_User", "${posModel.post_user_id} ** ")
        }

        if (postObj.has("details")) {
            posModel.description = postObj.getString("details")
            //Log.e("API_ALl_Post_User", "${posModel.post_user_id} ** ")
        }

        if (postObj.has("payment_type")) {
            posModel.payment_type = postObj.getString("payment_type")
        }

        if (postObj.has("language")) {
            posModel.language = postObj.getString("language")
        }

        if (postObj.has("post_created_at")) {
            posModel.post_created_at = postObj.getString("post_created_at")
        }

        if (postObj.has("status")) {
            posModel.status = postObj.getString("status")
        }



        if (postObj.has("is_instagram")) {
            posModel.is_instagram = postObj.getBoolean("is_instagram")
        }
        if (postObj.has("is_twitter")) {
            posModel.is_twitter = postObj.getBoolean("is_twitter")
        }

        if (postObj.has("is_auction")) {
            posModel.is_auction = postObj.getBoolean("is_auction")
        }
        if (postObj.has("buy_now")) {
            posModel.buy_now = postObj.getBoolean("buy_now")
        }

        if (postObj.has("whatsapp_only")) {
            posModel.whatsapp_only = postObj.getBoolean("whatsapp_only")
        }

        if(postObj.has("is_promoted")){
            posModel.isPromoted = postObj.get("is_promoted").toString().equals("true",true)
        }

        if (postObj.has("whatsapp_and_call")) {
            posModel.whatsapp_and_call = postObj.getBoolean("whatsapp_and_call")
        }

        if (postObj.has("is_sold")) {
            posModel.is_sold = postObj.getBoolean("is_sold")
        }
//only_user_from_selected_country
        if (postObj.has("is_story")) {
            posModel.is_story = postObj.getBoolean("is_story")
        }

        if (postObj.has("only_user_from_selected_country")) {
            posModel.only_user_from_selected_country = postObj.getBoolean("only_user_from_selected_country")
        }

        if (postObj.has("amount")) {
            posModel.amount = postObj.get("amount").toString()
        }

        if (postObj.has("product_serial")) {
            posModel.serial = postObj.getString("product_serial")
        }

        if (postObj.has("bids")) {

            val bidList = ArrayList<BidModel>()
            val bidArray = postObj.getJSONArray("bids")
            val bidParse = BidDataParser()
            Log.e(TAG, "BID_LIST ${bidArray.length()} ${bidArray.toString()}")
            for (i in 0 until bidArray.length()) {

                val bidObj = bidArray.getJSONObject(i)
                val bid = bidParse.parse(bidObj)
               /* if (bidObj.has("_id")) {
                    bid.id = bidObj.getString("_id")
                }

                if (bidObj.has("status")) {
                    bid.status = bidObj.getString("status")
                }

                if (bidObj.has("post_id")) {
                    bid.postId = bidObj.getString("post_id")
                }


                if (bidObj.has("bid_user_id")) {
                    bid.userId = bidObj.getString("bid_user_id")
                }

                if (bidObj.has("bid_comment")) {
                    bid.comment = bidObj.getString("bid_comment")
                }

                if (bidObj.has("bid_amount")) {
                    bid.bidAmount = bidObj.getString("bid_amount")
                }

                if (bidObj.has("bid_created_at")) {
                    bid.createdAt = bidObj.getString("bid_created_at")
                }

                if (bidObj.has("is_pinned")) {
                    bid.isPinned = bidObj.getBoolean("is_pinned")
                }*/

                if (MyApplication.instance.getUserPreferences().id.equals(bid.userId)) {

                    posModel.userBid = bid
                    break
                }
                bidList.add(bid)
            }


            posModel.bidsList = bidList
        }

        if (postObj.has("tags")) {
            val tagList = ArrayList<TagModel>()
            val tagArray = postObj.getJSONArray("tags")
            for (i in 0 until tagArray.length()) {
                val tagObj = tagArray.getJSONObject(i)
                val tagModel = TagModel()

                if (tagObj.has("_id")) {
                    tagModel.id = tagObj.getString("_id")
                }

                if (tagObj.has("name")) {
                    tagModel.name = tagObj.getString("name")
                }

                if (tagObj.has("createdAt")) {
                    tagModel.createdAt = tagObj.getString("createdAt")
                }

                tagList.add(tagModel!!)
            }
            posModel.selectedTags = tagList
        }


        if (postObj.has("available_country_id")) {
            val countryList = ArrayList<CountryModel>()
            val countryArray = postObj.getJSONArray("available_country_id")
            for (i in 0 until countryArray.length()) {
                val countryObj = countryArray.getJSONObject(i)
                val countryModel = CountryModel()

                if (countryObj.has("_id")) {
                    countryModel.id = countryObj.getString("_id")
                }

                if (countryObj.has("countryName")) {
                    countryModel.name = countryObj.getString("countryName")
                }

                if (countryObj.has("countryCode")) {
                    countryModel.code = countryObj.getString("countryCode")
                }


                if (countryObj.has("flagImageUrl")) {
                    countryModel.flag = "${ApiUrl.server}${countryObj.getString("flagImageUrl")}"
                }

                if (countryObj.has("createdAt")) {
                    //countryModel.cr = countryObj.getString("createdAt")
                }

                if (posModel.only_user_from_selected_country) {

                    if (MyApplication.instance.getUserPreferences().countryId.equals(countryModel.id)) {
                        posModel.isBuyAvailable = true
                    }
                } else {
                    posModel.isBuyAvailable = true
                }

                countryList.add(countryModel!!)
            }
            posModel.selectedCountry = countryList
        }


        if (postObj.has("post_images")) {

            val imgList = ArrayList<PostImages>()
            val imgArray = postObj.getJSONArray("post_images")
            for (i in 0 until imgArray.length()) {
                val postImage = PostImages()
                val imgObj = imgArray.getJSONObject(i)
                if (imgObj.has("_id")) {
                    postImage.id = imgObj.getString("_id")
                }

                if (imgObj.has("image_url")) {
                    postImage.url = imgObj.getString("image_url")
                }

                if (imgObj.has("is_video")) {
                    postImage.isVideo = imgObj.getBoolean("is_video")
                }

                imgList.add(postImage)
            }

            //  Log.e("POATIMAGE_IM","${imgList.size} ***")
            posModel.postImages = imgList
        }

        if (postObj.has("userDetails")) {

            val userArray = postObj.getJSONArray("userDetails")

            for (i in 0 until userArray.length()) {
                val userObj = userArray.getJSONObject(i)
                val userData =  UserParser().parse(userObj)

                posModel.userData = userData
            }
        }

        if (postObj.has("post_like")) {
            val likeArray = postObj.getJSONArray("post_like")
            Log.e("LIKE_DETAILS", "*** ${likeArray.toString()}")

            for (j in 0 until likeArray.length()) {
                val userObj = likeArray.getJSONObject(j)
                val userData = UserParser().parse(userObj)

               // posModel.userData = userData
            }
        }

        if (postObj.has("percent")) {
            posModel.percent = postObj.getString("percent")
            // Log.e("VOTE_DETAILS", "Per *** ${postObj.getString("percent").toString()}")
        }

        if (postObj.has("vote")) {
            val voteArray = postObj.getJSONArray("vote")
            Log.e("VOTE_DETAILS", "*** ${voteArray.toString()}")

            for (j in 0 until voteArray.length()) {
                val voteObj = voteArray.getJSONObject(j)

                if (voteObj.has("user_id")) {
                    if (MyApplication.instance.getUserPreferences().id.equals(voteObj.getString("user_id"))) {
                        if (voteObj.has("response")) {
                            posModel.isVote = voteObj.get("response").toString().toInt()
                        }
                    }
                }

            }
        }

        /*doAsync {
            val favPost = AppDatabase.getAppDatabase().favPostDao().getFavPost(posModel.id)
            if(favPost != null){
                posModel.isFav = favPost.post_id.equals(posModel.id,true)
                if(flag && posModel.isFav){
                   EventBus.getDefault().post(posModel)
                }
            }

        }*/



        return posModel
    }
}