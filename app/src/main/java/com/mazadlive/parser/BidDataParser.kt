package com.mazadlive.parser

import android.util.Log
import com.mazadlive.api.ApiUrl
import com.mazadlive.database.MessageData
import com.mazadlive.models.AddressModel
import com.mazadlive.models.BidModel
import com.mazadlive.models.CountryModel
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by bodacious on 26/12/18.
 */
class BidDataParser {

    fun parseList(jsonObject: JSONObject, list: ArrayList<BidModel>) {

        if (jsonObject.has("description")) {

        }

    }

    fun parse(bidObj: JSONObject): BidModel {

        val bid = BidModel("")
        if (bidObj.has("_id")) {
            bid.id = bidObj.getString("_id")
        }

        if (bidObj.has("status")) {
            bid.status = bidObj.getString("status")
        }

        if (bidObj.has("post_id")) {
            bid.postId = bidObj.getString("post_id")
        }


        if (bidObj.has("bid_user_id")) {
            bid.userId = bidObj.getString("bid_user_id")
        }

        if (bidObj.has("bid_comment")) {
            bid.comment = bidObj.getString("bid_comment")
        }

        if (bidObj.has("bid_amount")) {
            bid.bidAmount = bidObj.getString("bid_amount")
        }

        if (bidObj.has("bid_created_at")) {
            bid.createdAt = bidObj.getString("bid_created_at")
        }

        if (bidObj.has("is_pinned")) {
            bid.isPinned = bidObj.getBoolean("is_pinned")
        }

        if (bidObj.has("is_paid")) {
            bid.isPaid = bidObj.getBoolean("is_paid")
        }

        if (bidObj.has("user_name")) {
            bid.username = bidObj.getString("user_name")
        }else{
            if (bidObj.has("bid_user_name")) {
                bid.username = bidObj.getString("bid_user_name")
            }
        }


        return bid

    }
}