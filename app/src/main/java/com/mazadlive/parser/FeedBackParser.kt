package com.mazadlive.parser

import android.util.Log
import com.mazadlive.models.FeedbackModel
import com.mazadlive.models.OrderModel
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by bodacious on 28/12/18.
 */
class FeedBackParser {

    val TAG = "OrderParser"
    fun parseList(jsonObject: JSONObject){
        if(jsonObject.has("description")){
            val array = jsonObject.getJSONArray("description")
            for (i in 0 until array.length()){
                val orderModel = parse(array.getJSONObject(i))
            }
        }
    }

    fun parse(jsonObject: JSONObject):FeedbackModel{
        var  adId  = ""

        val feedbackModel = FeedbackModel()

        if(jsonObject.has("_id")){
            feedbackModel.id = jsonObject.getString("_id")
        }

        if(jsonObject.has("created_at")){
            feedbackModel.created_at = jsonObject.getString("created_at").toLong()
        }


        if(jsonObject.has("user_payment_status")){
            feedbackModel.user_payment_status = jsonObject.getString("user_payment_status")
        }

        if(jsonObject.has("user_id")){
            feedbackModel.user_id = jsonObject.getString("user_id")
        }

        if(jsonObject.has("type_id")){
            feedbackModel.type_id = jsonObject.getString("type_id")
        }

        if(jsonObject.has("type")){
            feedbackModel.type = jsonObject.getString("type")
        }

        if(jsonObject.has("type_data")){
            val typeData = jsonObject.get("type_data")
            var typeObj = JSONObject()

            if(typeData is JSONArray){
                typeObj = typeData.getJSONObject(0)
            }else{
                typeObj = typeData as JSONObject
            }
            when(feedbackModel.type){

                "post" ->{
                    val postModel = PostDataParser().parse(typeObj)
                    feedbackModel.postModel =  postModel
                }

            }

        }



        if(jsonObject.has("transaction_id")){
            feedbackModel.transaction_id = jsonObject.getString("transaction_id")
        }
        if(jsonObject.has("status")){
            feedbackModel.status = jsonObject.getString("status")
        }

      /*  if(jsonObject.has("payment_method")){
            orderModel. = jsonObject.getString("payment_method")
        }*/

        if(jsonObject.has("amount")){
            feedbackModel.amount = jsonObject.getString("amount")
        }

        if(jsonObject.has("address_id")){
            adId= jsonObject.getString("address_id")
        }

       /* if(jsonObject.has("address")){
            val adArray = jsonObject.getJSONArray("address")
            val obj =adArray.getJSONObject(0)
            if(obj.has("addresses")){
                val adparser = AddressParser()
                val jsonArray = obj.getJSONArray("addresses")

                for (i in 0 until jsonArray.length()){
                    val model  = adparser.parse(jsonArray.getJSONObject(i))

                    if(model.id.equals(adId,true)){
                        orderModel.addressModel = model
                        break
                    }
                }
            }
        }
*/

        return feedbackModel
    }
}