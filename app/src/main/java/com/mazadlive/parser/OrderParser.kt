package com.mazadlive.parser

import android.text.TextUtils
import android.util.Log
import com.mazadlive.models.OrderModel
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by bodacious on 28/12/18.
 */
class OrderParser {

    val TAG = "OrderParser"
    fun parseList(jsonObject: JSONObject) {
        if (jsonObject.has("description")) {
            val array = jsonObject.getJSONArray("description")
            for (i in 0 until array.length()) {
                val orderModel = parse(array.getJSONObject(i))
            }
        }
    }

    fun parse(jsonObject: JSONObject): OrderModel {

        //  Log.e("ORDER_ADDRESS","${jsonObject.toString()}")
        var adId = ""

        val orderModel = OrderModel()

        try {
            if (jsonObject.has("_id")) {
                orderModel.orderId = jsonObject.getString("_id")
            }

            if (jsonObject.has("display_id")) {
                orderModel.displayId = jsonObject.getString("display_id")
            }

            if (jsonObject.has("feedback")) {
                orderModel.feedback = jsonObject.getString("feedback")
            }

            if (jsonObject.has("created_at")) {
                orderModel.created_at = jsonObject.getString("created_at").toLong()
            }

            if (jsonObject.has("user_country")) {
                val cObj = jsonObject.getJSONObject("user_country")
                val countryModel = CountryParser().parse(cObj)
                orderModel.buyerCountry = countryModel
            }

            if (jsonObject.has("user_payment_status")) {
                orderModel.paymentStatus = jsonObject.getString("user_payment_status")
            }

            if (jsonObject.has("user_id")) {
                orderModel.user_id = jsonObject.getString("user_id")
            }

            if (jsonObject.has("type_id")) {
                orderModel.typeId = jsonObject.getString("type_id")
            }

            if (jsonObject.has("type")) {
                orderModel.orderType = jsonObject.getString("type")
            }

            if (jsonObject.has("type_data")) {
                val typeData = jsonObject.get("type_data")
                var typeObj = JSONObject()

                if (typeData is JSONArray) {
                    typeObj = typeData.getJSONObject(0)
                } else {
                    typeObj = typeData as JSONObject
                }
                when (orderModel.orderType) {

                    "post" -> {
                        val postModel = PostDataParser().parse(typeObj)
                        orderModel.post = postModel
                    }

                    "story" -> {
                        val storyModel = StoryDataParse().parse(typeObj)
                        orderModel.story = storyModel
                    }

                }

            }

            if (jsonObject.has("transaction_id")) {
                if (!TextUtils.isEmpty(jsonObject.getString("transaction_id")))
                    orderModel.txnId = jsonObject.getString("transaction_id")
            }
            if (jsonObject.has("status")) {
                orderModel.orderStatus = jsonObject.getString("status")
            }

            if (jsonObject.has("payment_method")) {
                orderModel.paymenMethod = jsonObject.getString("payment_method")
            }

            if (jsonObject.has("amount")) {
                orderModel.amount = jsonObject.getString("amount")
            }

            if (jsonObject.has("address_id")) {
                adId = jsonObject.getString("address_id")
            }

            if (jsonObject.has("buyerDetails")) {
                val adArray = jsonObject.getJSONArray("buyerDetails")
                val obj = adArray.getJSONObject(0)
                val userModel = UserParser().parse(obj)
                orderModel.userBuyer = userModel
            }

            if (jsonObject.has("seller_data")) {
                val adArray = jsonObject.getJSONArray("seller_data")
                val obj = adArray.getJSONObject(0)
                val userModel = UserParser().parse(obj)
                orderModel.userSeller = userModel
            }

            if (jsonObject.has("address")) {
                Log.e("ORDER_ADDRESS", "${jsonObject.getJSONArray("address")}")
                val adArray = jsonObject.getJSONArray("address")
                val obj = adArray.getJSONObject(0)
                if (obj.has("addresses")) {
                    val adparser = AddressParser()
                    val jsonArray = obj.getJSONArray("addresses")

                    for (i in 0 until jsonArray.length()) {
                        val model = adparser.parse(jsonArray.getJSONObject(i))

                        if (model.id.equals(adId, true)) {
                            orderModel.addressModel = model
                            break
                        }
                    }
                }
            }


        } catch (e: Exception) {
            Log.e("Exception : ", e.toString())
        }


        return orderModel
    }
}