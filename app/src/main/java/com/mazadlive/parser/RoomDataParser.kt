package com.mazadlive.parser

import android.util.Log
import com.mazadlive.database.AppDatabase
import com.mazadlive.database.RoomData
import com.mazadlive.database.RoomUserData
import com.mazadlive.utils.MyApplication
import org.jetbrains.anko.doAsync
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by bodacious on 26/12/18.
 */
class RoomDataParser {
    fun parse(jsonObject: JSONObject){

        if(jsonObject.has("description")){
            val array = jsonObject.getJSONArray("description")
            for (i in 0 until array.length()){
                val roomObj  = array.getJSONObject(i)
                val roomData = RoomData()
                if(roomObj.has("_id")){
                    roomData.id = roomObj.getString("_id")
                }

                if(roomObj.has("created_at")){
                    roomData.created_at= roomObj.getString("created_at")
                }


                if(roomObj.has("usersDetails")){

                    val userArray = roomObj.getJSONArray("usersDetails")

                    roomData.users = userArray.toString()
                    val userParser =  UserParser()

                    val userList = ArrayList<RoomUserData>()
                    for (i in 0 until userArray.length()){
                        val roomUserData = RoomUserData()
                        val userModel = userParser.parse(userArray.getJSONObject(i))
                        if(!userModel.id.equals(MyApplication.instance.getUserPreferences().id)){
                            roomUserData.user_id = userModel.id
                            roomUserData.username = userModel.username
                            roomUserData.id = userModel.id
                            roomUserData.room_id = roomData.id
                            roomUserData.profile = userModel.profile
                            roomUserData.email = userModel.email
                            roomUserData.phone = userModel.phone

                            Log.e("USER_DATA"," ${roomUserData.username}  ${userModel.username}")
                            doAsync {
                                val tUser = AppDatabase.getAppDatabase().roomUserDao().getRoomUser(roomUserData.user_id)
                                if (tUser == null){
                                    AppDatabase.getAppDatabase().roomUserDao().insert(roomUserData)
                                }else{
                                    AppDatabase.getAppDatabase().roomUserDao().update(roomUserData!!)
                                }

                            }

                            userList.add(roomUserData)
                        }
                    }

                    roomData.userList = userList

                }


                doAsync {
                    val temp = AppDatabase.getAppDatabase().roomDao().getRoom(roomData.id)
                    if(temp != null){
                        temp.userList = roomData.userList
                        AppDatabase.getAppDatabase().roomDao().update(temp)
                    }else{
                        AppDatabase.getAppDatabase().roomDao().insert(roomData)
                    }

                }


            }

        }
    }

    fun parse2(jsonObject: JSONObject){

        if(jsonObject.has("description")){
            val roomObj  = jsonObject.getJSONObject("description")
            val roomData = RoomData()
            if(roomObj.has("_id")){
                roomData.id = roomObj.getString("_id")
            }

            if(roomObj.has("created_at")){
                roomData.created_at= roomObj.getString("created_at")
            }



            if(roomObj.has("usersDetails")){

                val userArray = roomObj.getJSONArray("usersDetails")

                roomData.users = userArray.toString()
                val userParser =  UserParser()

                val userList = ArrayList<RoomUserData>()
                for (i in 0 until userArray.length()){
                    val roomUserData = RoomUserData()
                    val userModel = userParser.parse(userArray.getJSONObject(i))
                    if(!userModel.id.equals(MyApplication.instance.getUserPreferences().id)){
                        roomUserData.user_id = userModel.id
                        roomUserData.username = userModel.username
                        roomUserData.id = userModel.id
                        roomUserData.room_id = roomData.id
                        roomUserData.profile = userModel.profile
                        roomUserData.email = userModel.email
                        roomUserData.phone = userModel.phone

                        Log.e("USER_DATA"," ${roomUserData.username}  ${userModel.username}")
                        doAsync {

                            val tUser = AppDatabase.getAppDatabase().roomUserDao().getRoomUser(roomUserData.user_id)
                            if (tUser == null){
                                AppDatabase.getAppDatabase().roomUserDao().insert(roomUserData)
                            }else{
                                AppDatabase.getAppDatabase().roomUserDao().update(roomUserData)
                            }
                        }

                        userList.add(roomUserData)
                    }
                }

                roomData.userList = userList

            }
            doAsync {
                val temp = AppDatabase.getAppDatabase().roomDao().getRoom(roomData.id)
                if(temp != null){
                    temp.userList = roomData.userList
                    AppDatabase.getAppDatabase().roomDao().update(temp)
                }else{
                    AppDatabase.getAppDatabase().roomDao().insert(roomData)
                }

            }
        }
    }


}