package com.mazadlive.parser

import android.util.Log
import com.mazadlive.api.ApiUrl
import com.mazadlive.database.MessageData
import com.mazadlive.models.AddressModel
import com.mazadlive.models.CountryModel
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by bodacious on 26/12/18.
 */
class AddressParser {

    fun parseList(jsonObject: JSONObject,list: ArrayList<AddressModel>){

        if(jsonObject.has("description")){
            val obj = jsonObject.getJSONObject("description")
            if(obj.has("addresses")){
                val jsonArray = obj.getJSONArray("addresses")
                for (i in 0 until jsonArray.length()){
                    val model  = parse(jsonArray.getJSONObject(i))
                    list.add(model)
                }
            }
        }

    }

    fun parse(adObj: JSONObject):AddressModel{
        val addressModel = AddressModel()

        if(adObj.has("_id")){
            addressModel.id = adObj.getString("_id")
        }

        if(adObj.has("addressLineOne")){
            addressModel.addressLine1 = adObj.getString("addressLineOne")
        }

        if(adObj.has("addressLineTwo")){
            addressModel.addressLine2 = adObj.getString("addressLineTwo")
        }
        if(adObj.has("city")){
            addressModel.city = adObj.getString("city")
        }
        if(adObj.has("state")){
            addressModel.state = adObj.getString("state")
        }
        if(adObj.has("pincode")){
            addressModel.pincode = adObj.getString("pincode")
        }

        if(adObj.has("name")){
            addressModel.username = adObj.getString("name")
        }

        if(adObj.has("phone")){
            addressModel.phone = adObj.getString("phone")
        }

        if(adObj.has("countryId")){
            addressModel.countryId = adObj.getString("countryId")
        }
        if(adObj.has("is_active")){
            addressModel.isActive = adObj.getBoolean("is_active")
        }
        if(adObj.has("created_at")){
            addressModel.createdAt = adObj.getString("created_at")
        }

        if(adObj.has("CountryDetails")){
            val cObj = adObj.getJSONObject("CountryDetails")
            val countryModel = CountryModel()
            if(cObj.has("_id")){
                countryModel.id = cObj.getString("_id")
            }

            if(cObj.has("countryName")){
                countryModel.name = cObj.getString("countryName")
            }

            if(cObj.has("countryCode")){
                countryModel.code = cObj.getString("countryCode")
            }

            if(cObj.has("flagImageUrl")){
                countryModel.flag = "${ApiUrl.server}${cObj.getString("flagImageUrl")}"
            }

            if(cObj.has("createdAt")){
                //  countryModel.cr = obj.getString("createdAt")
            }

            addressModel.countryModel = countryModel
        }

        return addressModel

    }
}