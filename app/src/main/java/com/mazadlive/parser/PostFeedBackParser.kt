package com.mazadlive.parser

import android.util.Log
import com.mazadlive.api.ApiUrl
import com.mazadlive.models.CountryModel
import com.mazadlive.models.FeedbackModel
import com.mazadlive.models.OrderModel
import com.mazadlive.models.PostFeedbackModel
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by bodacious on 28/12/18.
 */
class PostFeedBackParser {

    val TAG = "OrderParser"

    fun parse(jsonObject: JSONObject):PostFeedbackModel{
        var  adId  = ""

        val feedbackModel = PostFeedbackModel()

        if(jsonObject.has("_id")){
            feedbackModel.id = jsonObject.getString("_id")
        }

        if(jsonObject.has("created_at")){
            feedbackModel.created_at = jsonObject.getString("created_at").toLong()
        }


        if(jsonObject.has("user_name")){
            feedbackModel.user_name = jsonObject.getString("user_name")
        }

        if(jsonObject.has("user_id")){
            feedbackModel.user_id = jsonObject.getString("user_id")
        }

        if(jsonObject.has("rating")){
            feedbackModel.rating = jsonObject.getString("rating")
        }

        if(jsonObject.has("user_profile")){
            feedbackModel.user_profile = jsonObject.getString("user_profile")
        }

        if(jsonObject.has("order_id")){
            feedbackModel.order_id = jsonObject.getString("order_id")
        }

        if(jsonObject.has("comment")){
            feedbackModel.comment = jsonObject.getString("comment")
        }

        if(jsonObject.has("user_country")){
            val cObj = jsonObject.getJSONObject("user_country")
            val countryModel = CountryModel()
            if(cObj.has("_id")){
                countryModel.id = cObj.getString("_id")
            }

            if(cObj.has("countryName")){
                countryModel.name = cObj.getString("countryName")
            }

            if(cObj.has("countryCode")){
                countryModel.code = cObj.getString("countryCode")
            }

            if(cObj.has("flagImageUrl")){
                countryModel.flag = "${ApiUrl.server}${cObj.getString("flagImageUrl")}"
            }

            if(cObj.has("createdAt")){
                //  countryModel.cr = obj.getString("createdAt")
            }
            feedbackModel.country = countryModel
        }
        return feedbackModel
    }
}