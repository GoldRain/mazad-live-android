package com.mazadlive.parser

import android.util.Log
import com.mazadlive.models.UserModel
import com.mazadlive.utils.MyApplication
import org.json.JSONObject

/**
 * Created by bodacious on 15/12/18.
 */
class UserParser {

    fun parse(userObj: JSONObject):UserModel{
        val userData = UserModel()

        Log.e("UserParser",userObj.toString())
        if(userObj.has("username")){
            userData.username = userObj.getString("username")
        }

        if(userObj.has("country_code")){
            userData.countryCode = userObj.getString("country_code")
        }

        if(userObj.has("phone")){
            userData.phone = userObj.getString("phone")
        }

        if(userObj.has("email")){
            userData.email = userObj.getString("email")
        }

        if(userObj.has("password")){
            //userData.password = userObj.getString("password")
        }

        if(userObj.has("type")){
            userData.type = userObj.getString("type")
        }

        if(userObj.has("profilePhoto")){
            userData.profile = userObj.getString("profilePhoto")
        }

        if (userObj.has("description")) {
            userData.description = userObj.getString("description")
        }

        if (userObj.has("following")) {

            val array1 = userObj.getJSONArray("following")

            val followingList = ArrayList<UserModel>()
            for (i in 0 until array1.length()) {

                val userModel = UserModel()
                val following = array1.getJSONObject(i)
                if (following.has("_id")) {

                    userModel.id = following.getString("_id")
                   /* if (MyApplication.instance.getUserPreferences().id.equals(following.getString("_id"))) {
                        isUserFollow = true
                        break
                    } else {
                        isUserFollow = false
                    }*/
                }else{
                    if (following.has("user_id")) {
                        userModel.id = following.getString("user_id")
                    }
                }

                if(following.has("username")){
                    userModel.username = following.getString("username")
                }

                if(following.has("country_code")){
                    userModel.countryCode = following.getString("country_code")
                }

                if(following.has("country_name")){
                    userModel.countryName = following.getString("country_name")
                }

                if(following.has("phone")){
                    userModel.phone = following.getString("phone")
                }

                if(following.has("email")){
                    userModel.email = following.getString("email")
                }
                if(following.has("type")){
                    userModel.type = following.getString("type")
                }

                if(following.has("profilePhoto")){
                    userModel.profile = following.getString("profilePhoto")
                }

                if (following.has("description")) {
                    userModel.description = following.getString("description")
                }

                followingList.add(userModel)
            }

            userData.following = followingList
        }

        if (userObj.has("followers")) {
            val userModel = UserModel()
            val array2 = userObj.getJSONArray("followers")
        //    totalFollower = array2.length()

            val followerList = ArrayList<UserModel>()
            for (i in 0 until array2.length()) {
                val follower = array2.getJSONObject(i)

                Log.e("FOLLOWER"," ** ${follower.toString()}")
                if (follower.has("_id")) {
                    userModel.id = follower.getString("_id")
                    /*if (userRef.id.equals(follower.getString("_id"))) {
                        isFollowing = true
                        break
                    } else {
                        isFollowing = false
                    }*/
                }else{
                    if (follower.has("user_id")) {
                        userModel.id = follower.getString("user_id")
                    }
                }

                if(follower.has("username")){
                    userModel.username = follower.getString("username")
                }

                if(follower.has("country_code")){
                    userModel.countryCode = follower.getString("country_code")
                }

                if(follower.has("country_name")){
                    userModel.countryName = follower.getString("country_name")
                }

                if(follower.has("phone")){
                    userModel.phone = follower.getString("phone")
                }

                if(follower.has("email")){
                    userModel.email = follower.getString("email")
                }
                if(follower.has("type")){
                    userModel.type = follower.getString("type")
                }

                if(follower.has("profilePhoto")){
                    userModel.profile = follower.getString("profilePhoto")
                }

                if (follower.has("description")) {
                    userModel.description = follower.getString("description")
                }
                followerList.add(userModel)
            }

            userData.follower = followerList
        }



        if (userObj.has("_id")) {
            userData.id = userObj.getString("_id")
        }

        if(userObj.has("is_verified")){
            userData.verified = userObj.get("is_verified").toString().equals("true",true)
        }

        if(userObj.has("verification")){
            val vObj  = userObj.getJSONObject("verification")
            if(vObj.has("status")){
                if(vObj.getString("status").equals("approved",true)){
                   // userData.verified = true
                }
            }
        }

        if(userObj.has("is_verified")){
            userData.verified = userObj.get("is_verified").toString().equals("true",true)
        }else{
            userData.verified = false
        }

        return userData
//payment_status
    }
}