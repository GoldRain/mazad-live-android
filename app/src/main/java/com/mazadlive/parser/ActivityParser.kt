package com.mazadlive.parser

import android.util.Log
import com.mazadlive.models.ActivityModel
import com.mazadlive.models.OrderModel
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by bodacious on 28/12/18.
 */
class ActivityParser {

    val TAG = "OrderParser"
    fun parseList(jsonObject: JSONObject){
        if(jsonObject.has("description")){
            val array = jsonObject.getJSONArray("description")
            for (i in 0 until array.length()){
                val activityModel = parse(array.getJSONObject(i))
            }
        }
    }

    /* "description": [
        {
            "_id": "5c18878b4de8cf1aa41cef77",
            "message": "You created this post on Tue Dec 18 2018 11:07:15 GMT+0530 (IST)",
            "type": "Post created",
            "user_id": "5c0a5ac207be2938de836e24",
            "type_id": "5c18878b4de8cf1aa41cef6f",
            "__v": 0
        },
        {
            "_id": "5c188c100ab5571db8c29eef",
            "message": "You marked this as your favourite post on Tue Dec 18 2018 11:26:32 GMT+0530 (IST)",
            "type": "marked favourite",
            "user_id": "5c0a5ac207be2938de836e24",
            "type_id": "5c18878b4de8cf1aa41cef6f",
            "__v": 0
        }
    ]*/
    fun parse(jsonObject: JSONObject):ActivityModel{
       val activityModel = ActivityModel()

        if(jsonObject.has("_id")){
            activityModel.id = jsonObject.getString("_id")
        }

        if(jsonObject.has("created_at")){
            activityModel.created_at = jsonObject.getString("created_at").toLong()
        }

        if(jsonObject.has("message")){
            activityModel.message = jsonObject.getString("message")
        }

        if(jsonObject.has("type")){
            activityModel.type = jsonObject.getString("type")
        }

        if(jsonObject.has("user_id")){
            activityModel.user_id = jsonObject.getString("user_id")
        }

        if(jsonObject.has("type_id")){
            activityModel.type_id = jsonObject.getString("type_id")
        }
        if(jsonObject.has("type_data")){
            val typeData = jsonObject.get("type_data")
            var typeObj = JSONObject()

            if(typeData is JSONArray){
               for (i in 0 until (typeData as JSONArray).length()){
                   typeObj = typeData.getJSONObject(0)
               }

                when(activityModel.type){
                    "post" ->{
                        val postModel = PostDataParser().parse(typeObj)
                        activityModel.post =  postModel
                    }
                }
            }else{
                typeObj = typeData as JSONObject

                when(activityModel.type){
                    "post" ->{
                        val postModel = PostDataParser().parse(typeObj)
                        activityModel.post =  postModel
                    }
                }
            }



        }

        if(jsonObject.has("user_data")){
            val array = jsonObject.getJSONArray("user_data")

            val user = UserParser().parse(array.getJSONObject(0))
            activityModel.user = user
        }



        return activityModel
    }
}