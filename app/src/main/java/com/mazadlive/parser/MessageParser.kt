package com.mazadlive.parser

import com.mazadlive.database.MessageData
import org.json.JSONObject

/**
 * Created by bodacious on 26/12/18.
 */
class MessageParser {

//{"error":false,"message":"vgg","description":{"user":["5c0a5ac207be2938de836e24","5c0a5b0707be2938de836e25"],"_id":"5c22563ae965305926c2d632","created_at":"1545754170691","__v":0},"sender_id":"5c06183c71bbd11bde6d6fc1","room_id":"5c22563ae965305926c2d632","message_type":"text","created_at":"1545800164431"}

    fun parse(jsonObject: JSONObject):MessageData{
        val  messageData = MessageData()


        /*?"messageDetails": {
            "_id": "5c2315acb4a12e18daa948aa",
            "sender_id": "5c06183c71bbd11bde6d6fc1",
            "room_id": "5c22563ae965305926c2d632",
            "message": "Hey",
            "message_type": "text",
            "created_at": "1545803180992",
            "__v": 0
        }*/


        if(jsonObject.has("messageDetails")){

            val msgObj = jsonObject.getJSONObject("messageDetails")
            if(msgObj.has("_id")){
                messageData.id = msgObj.getString("_id")
            }

            if(msgObj.has("sender_id")){

                messageData.sender_id = msgObj.getString("sender_id")
            }

            if(msgObj.has("room_id")){
                messageData.room_id = msgObj.getString("room_id")
            }

            if(msgObj.has("message")){
                messageData.content = msgObj.getString("message")
            }

            if(msgObj.has("message_type")){
                messageData.content_type = msgObj.getString("message_type")
            }

            if(msgObj.has("created_at")){
                messageData.created_at = msgObj.getString("created_at").toLong()
            }


        }


        if(jsonObject.has("description")){

          /*  val obj = jsonObject.getJSONObject("description")

            if(obj.has("_id")){
                messageData.id = obj.getString("_id")
            }
*/
        }

        return messageData

    }
}