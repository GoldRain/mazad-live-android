package com.mazadlive.parser

import com.mazadlive.api.ApiUrl
import com.mazadlive.models.CountryModel
import com.mazadlive.models.CurrencyModel
import org.json.JSONObject

/**
 * Created by bodacious on 4/1/19.
 */
class CountryParser {

    fun parse(countryObj: JSONObject):CountryModel?{
        val countryModel = CountryModel()

        if (countryObj.has("_id")) {
            countryModel.id = countryObj.getString("_id")
        }

        if (countryObj.has("countryName")) {
            countryModel.name = countryObj.getString("countryName")
        }

        if (countryObj.has("countryCode")) {
            countryModel.code = countryObj.getString("countryCode")
        }


        if (countryObj.has("flagImageUrl")) {
            countryModel.flag = "${ApiUrl.server}${countryObj.getString("flagImageUrl")}"
        }

        if (countryObj.has("createdAt")) {
            //countryModel.cr = countryObj.getString("createdAt")
        }

        return countryModel
    }

}