package com.mazadlive.parser

import android.util.Log
import com.mazadlive.api.ApiUrl
import com.mazadlive.database.MessageData
import com.mazadlive.models.AddressModel
import com.mazadlive.models.BidModel
import com.mazadlive.models.CountryModel
import com.mazadlive.models.WalletModel
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by bodacious on 26/12/18.
 */
class WalletParser {

    fun parseList(jsonObject: JSONObject,list: ArrayList<WalletModel>){

        if(jsonObject.has("description")){

            val array = jsonObject.getJSONArray("description")

            for (i in 0 until array.length()){
                val walletModel = parse(array.getJSONObject(i))
                list.add(walletModel)
            }
        }

    }

    fun parse(jsonObject: JSONObject):WalletModel{

        val walletModel = WalletModel()
        if (jsonObject.has("_id")) {
            walletModel.id = jsonObject.getString("_id")
        }

        if (jsonObject.has("status")) {
            walletModel.status = jsonObject.getString("status")
        }

        if (jsonObject.has("message")) {
            walletModel.message = jsonObject.getString("message")
        }


        if (jsonObject.has("payment_method")) {
            walletModel.paymenMethod = jsonObject.getString("payment_method")
        }

        if (jsonObject.has("created_at")) {
            walletModel.created_at = jsonObject.getString("created_at").toLong()
        }

        if (jsonObject.has("seller_id")) {
            walletModel.seller_id = jsonObject.getString("seller_id")
        }

        if (jsonObject.has("buyer_id")) {
            walletModel.buyer_id = jsonObject.getString("buyer_id")
        }

        if (jsonObject.has("order_id")) {
            walletModel.order_id = jsonObject.getString("order_id")
        }

        if(jsonObject.has("type_id")){
            walletModel.typeId = jsonObject.getString("type_id")
        }

        if(jsonObject.has("type")){
            walletModel.type = jsonObject.getString("type")
        }

        if(jsonObject.has("type_data")){
            val typeData = jsonObject.get("type_data")
            var typeObj = JSONObject()

            if(typeData is JSONArray){
                typeObj = typeData.getJSONObject(0)
            }else{
                typeObj = typeData as JSONObject
            }
            when(walletModel.type){

                "post" ->{
                    val postModel = PostDataParser().parse(typeObj)
                    walletModel.postModel =  postModel
                }

                "story" ->{
                    val storyModel = StoryDataParse().parse(typeObj)
                    walletModel.storyModel =  storyModel
                }

            }

        }
        if (jsonObject.has("orderDetails")) {
            val array = jsonObject.getJSONArray("orderDetails")
            if(array.length() >0){
                val order = array.getJSONObject(0)
                val orderModel = OrderParser().parse(order)
                walletModel.order_id = orderModel.orderId!!
                walletModel.displayId = orderModel.displayId
                walletModel.orderModel =orderModel
            }
        }

        return walletModel

    }
}