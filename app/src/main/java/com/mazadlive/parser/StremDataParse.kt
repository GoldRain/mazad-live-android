package com.mazadlive.parser

import android.content.res.Resources
import android.text.TextUtils
import android.util.Log
import com.bumptech.glide.load.engine.Resource
import com.mazadlive.R
import com.mazadlive.activities.PlayerActivity
import com.mazadlive.activities.StreamingActivity
import com.mazadlive.api.ApiUrl
import com.mazadlive.helper.Constant
import com.mazadlive.models.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.ArrayList

/**
 * Created by bodacious on 8/12/18.
 */
class StremDataParse {
    val TAG = "StremDataParse"
    fun parseStream(liveStoryModel: LiveStoryModel, jsonObject: JSONObject): LiveStoryModel {
        try {
            val holdSize1 = PlayerActivity.mapComment.size
            val holdSize2 = StreamingActivity.mapComment.size
            var rv = ""

            if (jsonObject.has("stream")) {
                val streamObj = jsonObject.getJSONObject("stream")
                Log.e(TAG, " STREAM_OBJECT ***** ${streamObj.toString()}")

                if (streamObj.has("_id")) {
                    liveStoryModel.id = streamObj.getString("_id")
                }

                if (streamObj.has("stream_name")) {
                    liveStoryModel.streamName = streamObj.getString("stream_name")
                }

                if (streamObj.has("start_price")) {
                    liveStoryModel.price = streamObj.getString("start_price")
                }


                if (streamObj.has("user_id")) {
                    liveStoryModel.user_id = streamObj.getString("user_id")
                }


                if (streamObj.has("status")) {
                    liveStoryModel.status = streamObj.getString("status")
                }

                if (streamObj.has("viewerCount")) {
                    liveStoryModel.viewerCounts = streamObj.getInt("viewerCount")
                }

                if (streamObj.has("thumbUrl")) {
                    liveStoryModel.thumbUrl = streamObj.getString("thumbUrl")
                }

                if (streamObj.has("country")) {
                    val array = streamObj.getJSONArray("country")
                    val list = ArrayList<CountryModel>()
                    var coun = ""
                    for (j in 0 until array.length()) {
                        val cObj = array.getJSONObject(j)
                        val countryModel = CountryModel()
                        countryModel.id = cObj.getString("country_id")
                        list.add(countryModel)
                    }


                    liveStoryModel.countryList = list
                }
                  Log.e("comments_LIST_comment","${streamObj.has("comment")}")
                  if (streamObj.has("comment")) {

                      val comArray = streamObj.getJSONArray("comment")
                      val cmLIst = ArrayList<CommentModel>()


                      for (i in 0 until comArray.length()) {
                          val comObj = comArray.getJSONObject(i)
                          Log.e("comments_LIST_comment","$comObj")
                          val commentModel = CommentModel()

                          if (comObj.has("_id")) {
                              commentModel.id = comObj.getString("_id")
                          }
                          if (comObj.has("comment")) {
                              commentModel.message = comObj.getString("comment")
                              commentModel.type = "comment"
                          }

                          if (comObj.has("status")) {
                              commentModel.status = comObj.getString("status")
                          }

                          if (comObj.has("amount")) {
                              Log.e("SADASDASDSADASD","Herer come")

                              commentModel.message = comObj.getString("amount")
                              commentModel.type = "amount"

                              Log.e("SADASDASDSADASD",commentModel.message)
                          }



                          if(StreamingActivity.isStream){
                              if(!StreamingActivity.mapComment.containsKey(commentModel.id)){
                                  cmLIst.add(commentModel)
                              }
                              StreamingActivity.mapComment.put(commentModel.id,commentModel)
                          }else{
                              if(!PlayerActivity.mapComment.containsKey(commentModel.id)){
                                  cmLIst.add(commentModel)
                              }

                              Log.e("StremDataParse", "COMMENTS_STATUS :: ${commentModel.status}")
                              PlayerActivity.mapComment.put(commentModel.id,commentModel)
                          }

                      }
                      Log.e("StremDataParse", "COMMENTS :: ${cmLIst.size}")
                      liveStoryModel.commentsList = cmLIst
                  }

                if (streamObj.has("current_product")) {
                    val productObj = streamObj.getJSONObject("current_product")
                    Log.e(TAG, "CURRENT_PRODUCT_PARSE****  ${productObj.toString()}")

                    val productModel = LiveProductModel()
                    if (productObj.has("stream_id")) {
                        productModel.streamId = productObj.getString("stream_id")
                    }

                    if (productObj.has("image_url")) {
                        productModel.url = productObj.getString("image_url")
                    }

                    if (productObj.has("payment_type")) {
                        productModel.paymentMode = productObj.getString("payment_type")
                    }

                    if (productObj.has("product_serial")) {
                        productModel.serial = productObj.getString("product_serial")
                    }

                    if (productObj.has("price")) {
                        productModel.price = productObj.getString("price")
                    }

                    if (productObj.has("description")) {
                        productModel.description = productObj.getString("description")
                    }

                    liveStoryModel.currentProduct = productModel
                }

            }

            if (jsonObject.has("thumbUrl")) {
                liveStoryModel.thumbUrl = jsonObject.getString("thumbUrl")
            }

            if (jsonObject.has("userDetails")) {

                val obj = jsonObject.get("userDetails")
                if (obj is JSONArray && (obj as JSONArray).length() > 0) {
                    val userObj = obj.getJSONObject(0)
                    val userModel = UserParser().parse(userObj)
                    liveStoryModel.userData = userModel
                } else {
                    val userObj = obj as JSONObject
                    val userModel = UserParser().parse(userObj)
                    liveStoryModel.userData = userModel
                }
            }

            if (jsonObject.has("comments")) {

                val comArray = jsonObject.getJSONArray("comments")
                val cmLIst = ArrayList<CommentModel>()

                Log.e("comments_LIST", "${comArray.toString()}")
                for (i in 0 until comArray.length()) {
                    val comObj = comArray.getJSONObject(i)
                    val commentModel = CommentModel()

                    if (comObj.has("_id")) {
                        commentModel.id = comObj.getString("_id")
                    }
                    if (comObj.has("comment")) {
                        commentModel.message = comObj.getString("comment")
                        commentModel.type = "comment"
                    }

                    if (comObj.has("status")) {
                        commentModel.status = comObj.getString("status")
                    }

                    if (comObj.has("amount")) {
                        commentModel.message = comObj.getString("amount")
                        commentModel.type = "amount"
                    }

                    if (comObj.has("user_id")) {
                        val userArray = comObj.getJSONArray("user_id")
                        val userObj = userArray.getJSONObject(0)
                        val userData = UserModel()
                        if (userObj.has("username")) {
                            userData.username = userObj.getString("username")
                        }

                        if (userObj.has("country_code")) {
                            userData.countryCode = userObj.getString("country_code")
                        }

                        if (userObj.has("phone")) {
                            userData.phone = userObj.getString("phone")
                        }

                        if (userObj.has("email")) {
                            userData.email = userObj.getString("email")
                        }

                        if (userObj.has("password")) {
                            //userData.password = userObj.getString("password")
                        }

                        if (userObj.has("type")) {
                            userData.type = userObj.getString("type")
                        }

                        if (userObj.has("profilePhoto")) {
                            userData.profile = userObj.getString("profilePhoto")
                        }

                        if (userObj.has("_id")) {
                            userData.id = userObj.getString("_id")
                        }
                        commentModel.userData = userData
                    }


                    if (StreamingActivity.isStream) {
                        if (!StreamingActivity.mapComment.containsKey(commentModel.id)) {
                            cmLIst.add(commentModel)
                        }
                        StreamingActivity.mapComment.put(commentModel.id, commentModel)
                    } else {
                        if (!PlayerActivity.mapComment.containsKey(commentModel.id)) {
                            cmLIst.add(commentModel)
                        }

                        //  Log.e("StremDataParse", "COMMENTS_STATUS :: ${commentModel.status}")
                        PlayerActivity.mapComment.put(commentModel.id, commentModel)
                    }

                }
                //      Log.e("StremDataParse", "COMMENTS :: ${PlayerActivity.mapComment.size}  ${StreamingActivity.mapComment.size} ${cmLIst.size}")
                liveStoryModel.commentsList = cmLIst
            }

            return liveStoryModel
        } catch (e: Exception) {
            e.printStackTrace()
            return liveStoryModel
        }

    }

    fun parseStream2(streamObj: JSONObject): LiveStoryModel {

        Log.e(TAG,"PARSE_STREAM2  ${streamObj.toString()}")
        val liveStoryModel = LiveStoryModel()
        try {

            val holdSize1 = PlayerActivity.mapComment.size
            val holdSize2 = StreamingActivity.mapComment.size
            var rv = ""

            if (streamObj.has("_id")) {
                liveStoryModel.id = streamObj.getString("_id")
            }

            if (streamObj.has("start_price")) {
                liveStoryModel.price = streamObj.getString("start_price")
            }


            if (streamObj.has("user_id")) {
                liveStoryModel.user_id = streamObj.getString("user_id")
            }

            if (streamObj.has("stream_name")) {
                liveStoryModel.streamName = streamObj.getString("stream_name")
            }
            if (streamObj.has("status")) {
                liveStoryModel.status = streamObj.getString("status")
            }

            if (streamObj.has("userDetails")) {
                val obj = streamObj.get("userDetails")
                if (obj is JSONArray && (obj as JSONArray).length() > 0) {
                    val userObj = obj.getJSONObject(0)
                    val userModel = UserParser().parse(userObj)
                    liveStoryModel.userData = userModel
                } else {
                    val userObj = obj as JSONObject
                    val userModel = UserParser().parse(userObj)
                    liveStoryModel.userData = userModel
                }

            }

            if (streamObj.has("thumbUrl")) {
                liveStoryModel.thumbUrl = streamObj.getString("thumbUrl")
            }

            if (streamObj.has("country")) {
                val array = streamObj.getJSONArray("country")
                val list = ArrayList<CountryModel>()
                var coun = ""
                for (j in 0 until array.length()) {
                    val cObj = array.getJSONObject(j)
                    val countryModel = CountryModel()
                    countryModel.id = cObj.getString("country_id")
                    list.add(countryModel)
                }


                liveStoryModel.countryList = list
            }


            if (streamObj.has("current_product")) {

                val productObj = streamObj.getJSONObject("current_product")
                Log.e(TAG, "CURRENT_PRODUCT ****  ${productObj.toString()}")
                val productModel = LiveProductModel()
                if (productObj.has("stream_id")) {
                    productModel.streamId = productObj.getString("stream_id")
                }

                if (productObj.has("payment_type")) {
                    productModel.paymentMode = productObj.getString("payment_type")
                }

                if (productObj.has("image_url")) {
                    productModel.url = productObj.getString("image_url")
                }

                if (productObj.has("product_serial")) {
                    productModel.serial = productObj.getString("product_serial")
                }

                if (productObj.has("price")) {
                    productModel.price = productObj.getString("price")
                }

                if (productObj.has("description")) {
                    productModel.description = productObj.getString("description")
                }

                liveStoryModel.currentProduct = productModel

            }

            return liveStoryModel
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e(TAG,"PARSE_STREAM2  ${e.message}")
            return liveStoryModel
        }

    }
}