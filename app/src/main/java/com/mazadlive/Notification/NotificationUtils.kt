package com.mazadlive.Notification

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.DrawableRes
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.mazadlive.R
import com.mazadlive.activities.HomeActivity
import com.mazadlive.helper.Constant


class NotificationUtils {
    private val notificationId: Int = 0

    companion object {
      //  public var message:Message?= null
        private val foregroud: Boolean = false
        private val alt: AlertDialog.Builder? = null

        fun showNotification(context: Context,title: String, message: String, groupId: String, notificationId: Int,bundle: Bundle?) {

            val intent = Intent(context, HomeActivity::class.java)
            intent.putExtra("message", message)
            intent.putExtra(Constant.NOTOFICATION_KEY, true)
            intent.putExtra("room_id",groupId)
            intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP

            bundle?.let {
                intent.putExtras(bundle)
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                /* Create or update. */
                Log.e("Oreo", "yes it is oreo")
                showOreoNotification(context, title, message, groupId, R.mipmap.ic_launcher_round, notificationId,bundle)
                return
            }
            Log.e("Lets", "Display notification ** $groupId **")

            val pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

            val notificationBuilder: NotificationCompat.Builder
            notificationBuilder = NotificationCompat.Builder(context, "notif")
                    .setSmallIcon(R.mipmap.ic_launcher_round)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)


            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            Log.d("notificationId", notificationId.toString() + "")
            notificationManager.notify(notificationId, notificationBuilder.build())

        }

        @RequiresApi(Build.VERSION_CODES.O)
        private fun showOreoNotification(context: Context, title: String, message: String, groupId: String, icon: Int, notificationId: Int,bundle: Bundle?) {

            val intent = Intent(context, HomeActivity::class.java)
            intent.action = "temp"
            intent.putExtra("message", message)
            intent.putExtra(Constant.NOTOFICATION_KEY, true)
            intent.putExtra("room_id", groupId)
            bundle?.let {
                intent.putExtras(bundle)
            }
            //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
            val pendingIntent = PendingIntent.getActivity(context, 0, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT)

            /*  val remoteViews =  RemoteViews(context.packageName,
                      R.layout.oreo_notification_ui);*/


            val inboxStyle = NotificationCompat.InboxStyle()
            inboxStyle.addLine(message)
            val notificationBuilder: NotificationCompat.Builder
            notificationBuilder = NotificationCompat.Builder(context, "my_channel_01")
                    .setSmallIcon(icon)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setChannelId("my_channel_02")
                    .setPriority(NotificationCompat.PRIORITY_MAX)

           /* val replyIntent = Intent(context,NotificationReceiver::class.java)
            replyIntent.setAction(Constant.REPLY_ACTION)

            val pendingIntentReply = PendingIntent.getBroadcast(context, 12345, replyIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            notificationBuilder?.addAction(R.drawable.ic_reply_black, "Reply", pendingIntentReply)*/

            val mChannel = NotificationChannel("my_channel_02", "my_channel_m", NotificationManager.IMPORTANCE_HIGH)

            // Configure the notification channel.
            mChannel.description = ""

            mChannel.enableLights(true)
            // Sets the notification light color for notifications posted to this
            // channel, if the device supports this feature.
            mChannel.lightColor = Color.RED

            mChannel.enableVibration(true)
            mChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)

            // mChannel.setSound(null,null);

            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)
            Log.d("notificationId", notificationId.toString() + "")
            notificationManager.notify(notificationId, notificationBuilder.build())

        }

    }



}
