package com.mazadlive.Notification

import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.mazadlive.R
import com.mazadlive.activities.HomeActivity
import com.mazadlive.database.AppDatabase
import com.mazadlive.helper.Constant
import com.mazadlive.parser.MessageParser
import com.onesignal.NotificationExtenderService
import com.onesignal.OSNotificationReceivedResult
import org.jetbrains.anko.doAsync
import org.json.JSONObject

/**
 * Created by bodacious on 27/12/18.
 */
class SilentNotification : NotificationExtenderService() {
    val TAG = "SilentNotification"
    override fun onNotificationProcessing(notification: OSNotificationReceivedResult?): Boolean {

        Log.e(TAG,"SilentNotification ${notification!!.payload.additionalData}")

        val jsonObject = JSONObject((notification!!.payload.additionalData).toString())

        val type = jsonObject.getString("type")
        when(type){
            "message" ->{
                if(jsonObject.has("message")){
                    val Obj  = jsonObject.getJSONObject("message")
                    var title = "MazadLive"
                    if(Obj.has("message")){
                       title  = Obj.getString("message")
                    }

                    if(Obj.has("typeDetails")){
                        val msgObj = Obj.getJSONObject("typeDetails")
                        val msg  = MessageParser().parse(msgObj)

                        doAsync {
                            AppDatabase.getAppDatabase().messageDao().insert(msg)
                            val room = AppDatabase.getAppDatabase().roomDao().getRoom(msg.room_id)
                            var id  = 11
                            var flag = false
                            if(room != null){
                                room.messageData = msg
                                flag = true
                                AppDatabase.getAppDatabase().roomDao().update(room)
                               // id = (room.created_at).toInt()
                            }

                            val bundle = Bundle()
                            bundle.putString("user_id",msg.sender_id)
                            bundle.putBoolean(Constant.NOTOFICATION_KEY,flag)
                            NotificationUtils.showNotification(applicationContext,title,msg.content,msg.room_id,id,bundle)

                        }

                    }
                }
            }

            else ->{
                if(jsonObject.has("message")){
                    val Obj  = jsonObject.getJSONObject("message")
                    var title = "MazadLive"
                    var content = ""
                    if(Obj.has("message")){
                        content   = Obj.getString("message")
                    }

                    val intent = Intent(applicationContext, HomeActivity::class.java)
                    intent.putExtra("message", content)
                    intent.putExtra(Constant.NOTOFICATION_KEY, true)
                    intent.putExtra("room_id", "")
                    intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP

                    NotificationUtils.showNotification(applicationContext,title,content,"",11,null)
                }

            }

        }
        return true
    }
}