package com.mazadlive.activities

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.view.Window
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgressDialog
import com.mazadlive.helper.Constant
import com.mazadlive.models.CountryModel
import kotlinx.android.synthetic.main.activity_forget_password.*
import org.json.JSONObject

class ForgetPasswordActivity : BaseActivity() {

    private var switchToEmail = true

    private var countryCode = "91"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setTheme(R.style.whiteStatusTheme)
        setContentView(R.layout.activity_forget_password)
        supportActionBar!!.hide()

        if(Constant.countryList.size <= 0){
            Constant.getAllCountries()
        }

        switchToEmail = intent.getBooleanExtra("for_email", true)
        if (!switchToEmail) {
            switchToEmail = true
            switchToPhone()
        }

        email_btn.setOnClickListener { switchToEmail() }
        phone_btn.setOnClickListener { switchToPhone() }

        back.setOnClickListener { onBackPressed() }

        mOpenCountry.setOnClickListener {
            CustomDialog(this).showCountryDialog(object : DialogListener() {
                override fun okClick(any: Any) {
                    super.okClick(any)
                    val countryModel = any as CountryModel
                    codeinput.setText(countryModel.code)
                    countryCode  = countryModel.code.toString()
                    mPhone1.setText("")
                }
            }, true)
        }

        sign_in_btn.setOnClickListener {

            if(switchToEmail){
                if (TextUtils.isEmpty(mEmail1.text.toString())) {
                    CustomDialog(this).showErrorDialog(getString(R.string.email_suggestion1))
                    return@setOnClickListener
                }
                if(!Patterns.EMAIL_ADDRESS.matcher(mEmail1.text.toString()).matches()){
                    CustomDialog(this).showErrorDialog(getString(R.string.email_suggestion))
                   return@setOnClickListener
                }
            }else{

                if (TextUtils.isEmpty(mPhone1.text.toString())) {
                    CustomDialog(this).showErrorDialog(getString(R.string.select_country_code))
                    return@setOnClickListener
                }

                if (TextUtils.isEmpty(mPhone1.text.toString())) {
                    CustomDialog(this).showErrorDialog(getString(R.string.phone_suggestion))
                    return@setOnClickListener
                }
            }

            callApi()

        }

        sign_up_btn.setOnClickListener {
            super.onBackPressed()
        }
    }



    private fun switchToEmail() {

        txt_email.setTextColor(ContextCompat.getColor(this,R.color.color_otpblack))
        txt_phone.setTextColor(ContextCompat.getColor(this,R.color.color_whitelight))

        val sliderEnterAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_from_right)
        val viewEnterAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_from_left)
        val exitAnim = AnimationUtils.loadAnimation(this, R.anim.slide_right_to_left)
        val viewExitAnim = AnimationUtils.loadAnimation(this, R.anim.slide_out_to_right)
        sliderEnterAnim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationStart(animation: Animation?) {
                email_slider.visibility = View.VISIBLE
            }
            override fun onAnimationEnd(animation: Animation?) {
                phone_slider.visibility = View.INVISIBLE
            }
        })
        viewEnterAnim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationStart(animation: Animation?) {
                email_layout.visibility = View.VISIBLE
            }
            override fun onAnimationEnd(animation: Animation?) {
                phone_layout.visibility = View.INVISIBLE
            }
        })

        if (!switchToEmail) {
            email_slider.startAnimation(sliderEnterAnim)
            phone_slider.startAnimation(exitAnim)

            email_layout.startAnimation(viewEnterAnim)
            phone_layout.startAnimation(viewExitAnim)

            switchToEmail = true
        }
    }

    private fun switchToPhone() {

        txt_phone.setTextColor(ContextCompat.getColor(this,R.color.color_otpblack))
        txt_email.setTextColor(ContextCompat.getColor(this,R.color.color_whitelight))

        val sliderEnterAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_from_left)
        val viewEnterAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_from_right)
        val exitAnim = AnimationUtils.loadAnimation(this, R.anim.slide_left_to_right)
        val viewExitAnim = AnimationUtils.loadAnimation(this, R.anim.slide_out_to_left)
        sliderEnterAnim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationStart(animation: Animation?) {
                phone_slider.visibility = View.VISIBLE
            }
            override fun onAnimationEnd(animation: Animation?) {
                email_slider.visibility = View.INVISIBLE
            }
        })
        viewEnterAnim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationStart(animation: Animation?) {
                phone_layout.visibility = View.VISIBLE
            }
            override fun onAnimationEnd(animation: Animation?) {
                email_layout.visibility = View.INVISIBLE
            }
        })


        if (switchToEmail) {
            phone_slider.startAnimation(sliderEnterAnim)
            email_slider.startAnimation(exitAnim)

            phone_layout.startAnimation(viewEnterAnim)
            email_layout.startAnimation(viewExitAnim)

            switchToEmail = false
        }
    }


    private fun callApi() {

        val param = HashMap<String,String>()
        param.put("version","1.1")
        param.put("admin","false")

        if(switchToEmail){
            param.put("type","email")
            param.put("email",mEmail1.text.toString().trim())

        }else{
            param.put("country_code",countryCode.replace("+",""))
            param.put("type","phone")
            param.put("phone",mPhone1.text.toString().trim())
        }

        val progressDialog = CustomProgressDialog(this)
        progressDialog.displayDialog(true)
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progressDialog.hideDialog()
                try {

                    val intent = Intent(this@ForgetPasswordActivity,OtpForgotPasswordActivity::class.java)

                    if(switchToEmail){
                        intent.putExtra("EMAIL",param.get("email"))
                        intent.putExtra("type","email")

                    }else{
                        intent.putExtra("NUMBER",param.get("phone"))

                        intent.putExtra("CODE",countryCode)
                        intent.putExtra("type","phone")

                    }
                    startActivity(intent)
                }catch (e: Exception){
                    e.printStackTrace()
                }
            }

            override fun onError(errorMessage: String) {
                progressDialog.hideDialog()
                CustomDialog(this@ForgetPasswordActivity).showErrorDialog("Failed to reset password. $errorMessage")
            }

        }).forgotPassword(param)
    }
}
