package com.mazadlive.activities

import android.app.Application
import android.content.Intent
import android.graphics.Paint
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.Window
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.mazadlive.R
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.helper.Constant
import com.mazadlive.service.FetchAllUsersByNumber
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.activity_sign_in.*
import kotlinx.android.synthetic.main.splash_activity.*
import org.json.JSONObject
import java.util.*

class SplashActivity : BaseActivity() {

    companion object {
        init {
            System.loadLibrary("keys")
        }
    }

    external fun getNativeApiKey(): String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.splash_activity)
        supportActionBar!!.hide()
        MyApplication.instance.getUserPreferences().apiKey = getNativeApiKey()

        if(MyApplication.instance.getUserPreferences().loginStatus){

            startService(Intent(this,FetchAllUsersByNumber::class.java))
        }

        getAllCountries()

        english_txt.paintFlags = english_txt.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG
        arabic_txt.paintFlags = arabic_txt.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG

        getDynamicLink(intent)
        /*icon.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus){
                HomeActivity.start(this@SplashActivity,null)
               // startActivity(Intent(this@SplashActivity,SignInActivity::class.java))
            }else{
                startActivity(Intent(this@SplashActivity,SignInActivity::class.java))
               // HomeActivity.start(this@SplashActivity,null)
            }
        }*/

        english.setOnClickListener { nextScreen("en") }
        arabic.setOnClickListener { nextScreen("ar") }



        Log.e("TEST_DATA", Arrays.toString(Array(5,{i->i*2})))
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        getDynamicLink(intent!!)
        Log.e("onNewIntent", " onNewIntent ")

    }
    var linkUrl = ""
    private fun getDynamicLink(intent:Intent){
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(intent)
                .addOnSuccessListener(this@SplashActivity) { pendingDynamicLinkData ->

                    var deepLink: Uri? = null
                    if (pendingDynamicLinkData != null) {
                        deepLink = pendingDynamicLinkData.link
                        if (deepLink != null) {
                            MyApplication.instance.getUserPreferences().isSkip = true
                            linkUrl = "$deepLink"
                            Constant.LINK_URL = linkUrl
                        } else {
                            Constant.LINK_URL = ""
                        }
                    }

                    moveNext(false)
                    Log.e("LINK_GET", ": getDynamicLink: ${Constant.LINK_URL}")
                }
                .addOnFailureListener(this@SplashActivity) { e ->
                    Log.e("Analytic", "getDynamicLink:onFailure", e)
                    moveNext(false)
                }

    }
    private fun moveNext(flag:Boolean= true) {

        if (MyApplication.instance.getUserPreferences().loginStatus || MyApplication.instance.getUserPreferences().isSkip){
          if(flag){
              Handler().postDelayed(object :Runnable{
                  override fun run() {
                     openNext()
                  }
              },500)
          }else{
              openNext()
          }
        }else{
            startActivity(Intent(this@SplashActivity,SignInActivity::class.java))
        }

    }

    private fun openNext() {

        setLocale( MyApplication.instance.getUserPreferences().appLanguage)
        Log.e("TAG_MyApplication","${MyApplication.instance.getUserPreferences().loginStatus}")

        HomeActivity.start(this@SplashActivity,null)
        finish()
    }

    private fun nextScreen(language: String) {
        MyApplication.instance.getUserPreferences().appLanguage = language
        setLocale(language)
        if(MyApplication.instance.getUserPreferences().loginStatus || MyApplication.instance.getUserPreferences().isSkip){
            HomeActivity.start(this@SplashActivity,null)
            finish()
            // startActivity(Intent(this@SplashActivity,SignInActivity::class.java))
        }else{
            startActivity(Intent(this@SplashActivity,SignInActivity::class.java))
            finish()
            // HomeActivity.start(this@SplashActivity,null)
        }

    }

    fun setLocale(lang: String) {

        try {

            Log.e("LANGUAGE","$lang   *** ")
            val myLocale = Locale(lang)
            Locale.setDefault(myLocale)
            val res = resources
            val dm = res.displayMetrics
            val conf = res.configuration
            conf.locale = myLocale
            applicationContext.resources.updateConfiguration(conf, dm)

            if(MyApplication.instance.getUserPreferences().loginStatus){
                HomeActivity.start(this@SplashActivity,null)
                finish()
                // startActivity(Intent(this@SplashActivity,SignInActivity::class.java))
            }else{
                startActivity(Intent(this@SplashActivity,SignInActivity::class.java))
                finish()
                // HomeActivity.start(this@SplashActivity,null)
            }

        }catch (e: Exception){

            e.printStackTrace()
            Log.e("sdgfd","exception in language is: ${e.message}")
        }

    }

    fun setlanguage(){
        /* var languageToLoad = mSharedPreferenceUtils!!.getStringValue(PreferenceKey.LANGUAGE_KEY,"en")
         val locale = Locale(languageToLoad)
         Locale.setDefault(locale)
         val config = Configuration()
         config.locale = locale*/
        val i = packageManager
                .getLaunchIntentForPackage(packageName)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(i)

    }

    private fun getAllCountries() {


        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {

            }

            override fun onError(errorMessage: String) {

            }

        }).getCredentials()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {

            }

            override fun onError(errorMessage: String) {

            }

        }).getAllCountry(Constant.countryList)
    }

}
