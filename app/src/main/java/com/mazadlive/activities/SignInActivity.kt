package com.mazadlive.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.Window
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ApiUrl
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgressDialog
import com.mazadlive.database.AppDatabase
import com.mazadlive.database.FavPost
import com.mazadlive.helper.Constant
import com.mazadlive.models.CountryModel
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.activity_sign_in.*
import org.jetbrains.anko.doAsync
import org.json.JSONArray
import org.json.JSONObject

class SignInActivity : BaseActivity() {

    companion object {
        fun startLogin(intent: Intent) {

        }

    }

    private var switchToEmail = true
    private var emailBuyerSwitch = true
    private var phoneBuyerSwitch = true
    private var signInByEmail = true

    private var countryCode = "91"

    val TAG = "SignInActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_sign_in)
        supportActionBar!!.hide()

        // Comment this if Go into Live
        //mEmail1.setText("bb@gmail.com")
        //mPassword1.setText("password")

        if (Constant.countryList.size <= 0) {
            Constant.getAllCountries()
        }
        gmail_forgot_password.setOnClickListener { }
        phone_forgot_password.setOnClickListener { }

        email_btn.setOnClickListener { switchToEmail() }
        phone_btn.setOnClickListener { switchToPhone() }

        phone_buyer_switch.tag = false
        phone_buyer_switch.setOnClickListener { performPhoneSwitch(phone_buyer_switch, phone_seller_switch) }
        phone_seller_switch.tag = true
        phone_seller_switch.setOnClickListener { performPhoneSwitch(phone_seller_switch, phone_buyer_switch) }

        email_buyer_switch.tag = false
        email_buyer_switch.setOnClickListener { performEmailSwitch(email_buyer_switch, email_seller_switch) }
        email_seller_switch.tag = true
        email_seller_switch.setOnClickListener { performEmailSwitch(email_seller_switch, email_buyer_switch) }

        performEmailSwitch(email_buyer_switch, email_seller_switch)
        performPhoneSwitch(phone_buyer_switch, phone_seller_switch)

        gmail_forgot_password.setOnClickListener {
            val intent = Intent(this, ForgetPasswordActivity::class.java)
            intent.putExtra("for_email", true)
            startActivity(intent)
        }
        phone_forgot_password.setOnClickListener {
            val intent = Intent(this, ForgetPasswordActivity::class.java)
            intent.putExtra("for_email", false)
            startActivity(intent)
        }

        sign_in_btn.setOnClickListener {
            if (validation()) {
                loginUser()
            }

        }

        skip_btn.setOnClickListener {
            MyApplication.instance.getUserPreferences().isSkip = true
            HomeActivity.start(this, null)

            ActivityCompat.finishAffinity(this)
        }

        sign_up_btn.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }

        back.setOnClickListener {
            onBackPressed()
        }

        mOpenCountry.setOnClickListener {
            CustomDialog(this).showCountryDialog(object : DialogListener() {
                override fun okClick(any: Any) {
                    super.okClick(any)
                    val countryModel = any as CountryModel
                    codeinput.setText(String.format("+%s", countryModel.code))
                    countryCode = countryModel.code.toString()
                    mPhoneNmuber.setText("")
                }
            }, true)
        }

        Log.e(TAG, "${intent.getBooleanExtra("showDialog", false)}")

        /*if(intent.getBooleanExtra("showDialog",false)){
            CustomDialog(this).showAlertDialog("Alert",Constant.LOGGED_IN_TEXT,object :DialogListener(){
                override fun okClick() {

                }
            })
        }*/
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {

            }

            override fun onError(errorMessage: String) {

            }

        }).getCredentials()


        val flag  = intent.getBooleanExtra("fromDialog",false)

        if(!flag){
            MyApplication.instance.getUserPreferences().isSkip = false
        }

        if(MyApplication.instance.getUserPreferences().isSkip){
            skip_btn.visibility = View.GONE
        }else{
            skip_btn.visibility = View.VISIBLE
        }


    }

    override fun onBackPressed() {

        val flag  = intent.getBooleanExtra("fromDialog",false)
        Log.e(TAG,"  ** "+flag)
        if(!flag){
            finishAffinity()
            finish()
        }else{
            super.onBackPressed()
        }

    }

    private fun validation(): Boolean {
        if (signInByEmail) {
            return validationEmail()

        } else {
            return validationPhone()
        }

    }

    private fun loginUser() {
        /*version:1.1
type:email
//email:paruhack@gmail.com
password:password
//type:phone
country_code:91
phone:9314669334*/

        val param = HashMap<String, String>()
        param.put("version", "1.1")

        if (signInByEmail) {
            param.put("type", "email")
            param.put("email", mEmail1.text.toString())
            param.put("password", mPassword1.text.toString())
        } else {
            param.put("country_code", countryCode)
            param.put("type", "phone")
            param.put("phone", mPhoneNmuber.text.toString().trim())
            param.put("password", mPassword2.text.toString())
        }

        Log.e("PARAMS: ", " PA: $param")
        val progressDialog = CustomProgressDialog(this)
        progressDialog.displayDialog(true)

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progressDialog.hideDialog()
                try {
                    //  val jsonArray = JSONArray(`object`.toString())
                    val json = JSONObject(`object`.toString())
                    val intent = Intent(this@SignInActivity, HomeActivity::class.java)
                    val ref = MyApplication.instance.getUserPreferences()

                    ref.badgeDailogShow = false // First Time Show Dialog to User for verified Budge

                    if (json.has("data")) {

                        val jsonArray = json.getJSONArray("data")
                        val obj = jsonArray.getJSONObject(0)
                        if (obj.has("username")) {
                            ref.username = obj.getString("username")
                        }

                        if (obj.has("country_code")) {
                            ref.countryCode = obj.getString("country_code")
                        }

                        if (obj.has("country_id")) {
                            ref.countryId = obj.getString("country_id")
                        }

                        if (obj.has("country_name")) {
                            ref.CountryName = obj.getString("country_name")
                        }

                        if (obj.has("phone")) {
                            ref.phone = obj.getString("phone")
                        }

                        if (obj.has("email")) {
                            ref.email = obj.getString("email")
                        }

                        if (obj.has("password")) {
                            ref.password = obj.getString("password")
                        }

                        if (obj.has("type")) {
                            ref.type = obj.getString("type")
                        }

                        if (obj.has("profilePhoto")) {
                            ref.profile = obj.getString("profilePhoto")
                        }

                        if (obj.has("tap_customer_id")) {
                            ref.tapCustomerId = obj.getString("tap_customer_id")
                        }

                        if (obj.has("last_login")) {
                            ref.lastLoginTime = obj.getString("last_login")
                        }
///is_activity
                        if (obj.has("_id")) {
                            ref.id = obj.getString("_id")
                        }

                        if (obj.has("is_activity")) {
                            ref.activityStatus = obj.get("is_activity").toString().equals("true", true)
                        }

                        if (obj.has("show_ads")) {
                            ref.showAds = obj.get("show_ads").toString().equals("true", true)
                        }

                        if (obj.has("is_promoted")) {
                            ref.promotted = obj.get("is_promoted").toString().equals("true", true)
                        }

                        if (obj.has("is_verified")) {
                            ref.verified = obj.get("is_verified").toString().equals("true", true)
                        }

                        if (obj.has("is_buyNow")) {
                            ref.isBuyNow     = obj.get("is_buyNow").toString().equals("true", true)
                        }


                        if (obj.has("is_auction")) {
                            ref.isAuction     = obj.get("is_auction").toString().equals("true", true)
                        }

                        if (obj.has("countryDetails")) {
                            val cArray = obj.getJSONArray("countryDetails")
                            for (i in 0 until cArray.length()) {

                                val countryObj = cArray.getJSONObject(i)
                                ref.countryId = countryObj.getString("_id")
                                ref.CountryName = countryObj.getString("countryName")
                                ref.CountryFlag = "${ApiUrl.server}${countryObj.getString("flagImageUrl")}"
                            }
                        }

                        if (obj.has("favPost")) {

                            val favArray = obj.getJSONArray("favPost")
                            //var favIds = ""
                            for (i in 0 until favArray.length()) {
                                val favObj = favArray.getJSONObject(i)
                                Log.e("FAV_OBJ", "${favObj.toString()} *** ")
                                val favPost = FavPost()

                                if (favObj.has("_id")) {
                                    favPost.id = favObj.getString("_id")
                                }

                                if (favObj.has("post_id")) {
                                    favPost.post_id = favObj.getString("post_id")
                                }
                                doAsync {
                                    Log.e("FAV_OBJ", "${favObj.toString()} *** ")
                                    AppDatabase.getAppDatabase().favPostDao().insert(favPost)

                                    /* if (signInByEmail) {
                                         MyApplication.instance.getUserPreferences().appModeBuyer = email_buyer_switch.tag as Boolean
                                     } else {
                                         MyApplication.instance.getUserPreferences().appModeBuyer = phone_buyer_switch.tag as Boolean
                                     }*/

                                }

                            }

                            // ref.favIds = favIds
                        }

                        if (obj.has("push_notification")) {
                            ref.pushNotification = obj.get("push_notification").toString().equals("true", true)
                        }

                        if (obj.has("email_and_notification")) {
                            ref.emailAndNotification = obj.get("email_and_notification").toString().equals("true", true)
                        }
                        /*  verification*/
                        if (obj.has("IBAN")) {
                            val ibanObj = obj.getJSONObject("IBAN")

                            if (ibanObj.has("banNumber")) {
                                ref.ibanBanNumber = ibanObj.getString("banNumber")
                            }
                            if (ibanObj.has("bankName")) {
                                ref.ibanBankName = ibanObj.getString("banNumber")
                            }

                            if (ibanObj.has("phoneNumber")) {
                                ref.ibanPhoneNumber = ibanObj.getString("phoneNumber")
                            }

                            if (ibanObj.has("country_code")) {
                                ref.ibanCountryCode = ibanObj.getString("country_code")
                            }
                        }

                        if (obj.has("verification")) {
                            val vObj = obj.getJSONObject("verification")
                            if (vObj.has("status")) {
                                ref.verificationStatus = vObj.getString("status")
                            } else {
                                ref.verificationStatus = ""
                            }
                        }

                        ref.loginStatus = true

                        val flag  = intent.getBooleanExtra("fromDialog",false)
                        if(!flag){
                            MyApplication.instance.getUserPreferences().appModeBuyer = ref.type.equals("buyer", true)
                            HomeActivity.start(this@SignInActivity, null)
                            ActivityCompat.finishAffinity(this@SignInActivity)
                        }else{
                            finish()
                        }
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onError(errorMessage: String) {
                progressDialog.hideDialog()
                CustomDialog(this@SignInActivity).showErrorDialog("Failed to login try again. $errorMessage")
            }

        }).loginRequest(param)


    }

    private fun performEmailSwitch(checkedImage: ImageView, unCheckedImage: ImageView) {
        if (checkedImage.tag as Boolean) {
            checkedImage.tag = false
            unCheckedImage.tag = true
            emailBuyerSwitch = false
        } else {
            checkedImage.tag = true
            unCheckedImage.tag = false
            emailBuyerSwitch = true
        }

        if (checkedImage.tag as Boolean) {
            checkedImage.setImageResource(R.drawable.ic_switch_on)
            unCheckedImage.setImageResource(R.drawable.ic_switch_off)
        } else {
            checkedImage.setImageResource(R.drawable.ic_switch_off)
            unCheckedImage.setImageResource(R.drawable.ic_switch_on)
        }
    }

    private fun performPhoneSwitch(checkedImage: ImageView, unCheckedImage: ImageView) {
        if (checkedImage.tag as Boolean) {
            checkedImage.tag = false
            unCheckedImage.tag = true
            phoneBuyerSwitch = false
        } else {
            checkedImage.tag = true
            unCheckedImage.tag = false
            phoneBuyerSwitch = true
        }

        if (checkedImage.tag as Boolean) {
            checkedImage.setImageResource(R.drawable.ic_switch_on)
            unCheckedImage.setImageResource(R.drawable.ic_switch_off)
        } else {
            checkedImage.setImageResource(R.drawable.ic_switch_off)
            unCheckedImage.setImageResource(R.drawable.ic_switch_on)
        }
    }

    private fun switchToEmail() {

        signInByEmail = true
        email_txt.setTextColor(ContextCompat.getColor(this, R.color.color_otpblack))
        phone_txt.setTextColor(ContextCompat.getColor(this, R.color.color_whitelight))

        val sliderEnterAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_from_right)
        val viewEnterAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_from_left)
        val exitAnim = AnimationUtils.loadAnimation(this, R.anim.slide_right_to_left)
        val viewExitAnim = AnimationUtils.loadAnimation(this, R.anim.slide_out_to_right)
        sliderEnterAnim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationStart(animation: Animation?) {
                email_slider.visibility = View.VISIBLE
            }

            override fun onAnimationEnd(animation: Animation?) {
                phone_slider.visibility = View.INVISIBLE
            }
        })
        viewEnterAnim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationStart(animation: Animation?) {
                gmail_view.visibility = View.VISIBLE
            }

            override fun onAnimationEnd(animation: Animation?) {
                phone_view.visibility = View.INVISIBLE
            }
        })

        if (!switchToEmail) {
            email_slider.startAnimation(sliderEnterAnim)
            phone_slider.startAnimation(exitAnim)

            gmail_view.startAnimation(viewEnterAnim)
            phone_view.startAnimation(viewExitAnim)

            switchToEmail = true
        }
    }

    private fun switchToPhone() {

        signInByEmail = false
        phone_txt.setTextColor(ContextCompat.getColor(this, R.color.color_otpblack))
        email_txt.setTextColor(ContextCompat.getColor(this, R.color.color_whitelight))

        val sliderEnterAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_from_left)
        val viewEnterAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_from_right)
        val exitAnim = AnimationUtils.loadAnimation(this, R.anim.slide_left_to_right)
        val viewExitAnim = AnimationUtils.loadAnimation(this, R.anim.slide_out_to_left)
        sliderEnterAnim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationStart(animation: Animation?) {
                phone_slider.visibility = View.VISIBLE
            }

            override fun onAnimationEnd(animation: Animation?) {
                email_slider.visibility = View.INVISIBLE
            }
        })
        viewEnterAnim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationStart(animation: Animation?) {
                phone_view.visibility = View.VISIBLE
            }

            override fun onAnimationEnd(animation: Animation?) {
                gmail_view.visibility = View.INVISIBLE
            }
        })


        if (switchToEmail) {
            phone_slider.startAnimation(sliderEnterAnim)
            email_slider.startAnimation(exitAnim)

            phone_view.startAnimation(viewEnterAnim)
            gmail_view.startAnimation(viewExitAnim)

            switchToEmail = false
        }
    }

    private fun validationEmail(): Boolean {
        if (TextUtils.isEmpty(mEmail1.text.toString())) {
            CustomDialog(this).showErrorDialog(getString(R.string.email_suggestion1))
            return false
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(mEmail1.text.toString()).matches()) {
            CustomDialog(this).showErrorDialog(getString(R.string.email_suggestion))
            return false
        }
        if (TextUtils.isEmpty(mPassword1.text.toString()) || mPassword1.text.toString().trim().length < 8) {
            CustomDialog(this).showErrorDialog(getString(R.string.password_suggestion))
            return false
        }
        return true
    }

    private fun validationPhone(): Boolean {
        if (TextUtils.isEmpty(countryCode)) {
            CustomDialog(this).showErrorDialog(getString(R.string.select_country_code))
            return false
        }

        if (TextUtils.isEmpty(mPhoneNmuber.text.toString())) {
            CustomDialog(this).showErrorDialog(getString(R.string.phone_suggestion))
            return false
        }
        if (TextUtils.isEmpty(mPassword2.text.toString()) || mPassword2.text.toString().trim().length < 8) {
            CustomDialog(this).showErrorDialog(getString(R.string.password_suggestion))
            return false
        }
        return true
    }
}
