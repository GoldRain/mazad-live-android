package com.mazadlive.activities

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.PorterDuff
import android.location.Geocoder
import android.location.Location
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment
import com.google.android.gms.location.places.ui.PlaceSelectionListener
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.helper.GpsTracker
import kotlinx.android.synthetic.main.activity_location_select.*
import kotlinx.android.synthetic.main.view_map_marker.view.*
import java.io.IOException

class LocationSelectActivity : AppCompatActivity(), OnMapReadyCallback {


    companion object {
        fun startForResult(context:Context,code: Int){
            val intent = Intent(context, LocationSelectActivity::class.java)
            (context as Activity).startActivityForResult(intent,code)
        }
    }
    private var geocoder: Geocoder? = null
    private var mapFragment: SupportMapFragment? = null
    private var latLng: LatLng? = null
    private var mMap: GoogleMap? = null
    private var address: String? = null
    private var mCountry: String? = null

    private var currentLocation:Location? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_select)

        currentLocation = GpsTracker(this).currentLocation
        geocoder = Geocoder(this)
        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment!!.getMapAsync(this)

        val placeAutocompleteFragment = fragmentManager.findFragmentById(R.id.search) as PlaceAutocompleteFragment
        placeAutocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {

                mMap!!.clear()
                latLng = place.latLng
                address = place.address!!.toString()
                pointToPosition(latLng!!)
                mMap!!.addMarker(MarkerOptions()
                        .position(latLng!!)
                        .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(address!!))))
            }

            override fun onError(status: Status) {

            }
        })

        cancel.setOnClickListener {
            onBackPressed()
        }
        if(currentLocation != null){

        }
        sendLocation.setOnClickListener {

            showSendDialog("$address\n")
        }

    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }

        mMap!!.setMyLocationEnabled(true)

        currentLocation = GpsTracker(this).currentLocation
        latLng = LatLng(currentLocation!!.getLatitude(), currentLocation!!.getLongitude())


        pointToPosition(latLng!!)

        getAddress(latLng!!, false)

        mMap!!.setOnMarkerClickListener(GoogleMap.OnMarkerClickListener {
            showSendDialog("$address\n")
            false
        })

        mMap!!.setOnMapClickListener(GoogleMap.OnMapClickListener { latLng ->
            mMap!!.clear()

            getAddress(latLng, true)
        })


    }

    private fun pointToPosition(position: LatLng) {
        // camera position
        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 16f))
        // Zoom in, animating the camera.
        mMap!!.animateCamera(CameraUpdateFactory.zoomIn())
        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
        mMap!!.animateCamera(CameraUpdateFactory.zoomTo(16f), 2000, null)
    }

    private fun getMarkerBitmapFromView(addressText: String): Bitmap {

        val customMarkerView = (getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.view_map_marker, null)

        customMarkerView.address.setText(addressText)
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight())
        customMarkerView.buildDrawingCache()
        val returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888)
        val canvas = Canvas(returnedBitmap)
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
        val drawable = customMarkerView.getBackground()
        if (drawable != null)
            drawable!!.draw(canvas)
        customMarkerView.draw(canvas)
        return returnedBitmap
    }

    private fun getAddress(latLng: LatLng, flag: Boolean) {

        Handler().post {
            try {
                val arrayList = geocoder!!.getFromLocation(latLng.latitude, latLng.longitude, 1)
                if (arrayList.size > 0) {
                    address = arrayList[0].getAddressLine(0)
                    mCountry = arrayList[0].countryName
                }

                if (flag) {
                    mMap!!.addMarker(MarkerOptions()
                            .position(latLng)
                            .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(address!!))))
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun showSendDialog(text: String) {
        /*val intent = Intent()

                        val bundle = Bundle()
                        bundle.putDouble("latitude", latLng.latitude)
                        bundle.putDouble("longitude", latLng.longitude)
                        bundle.putString("address", address)
                        bundle.putString("country", mCountry)
                        intent.putExtra("data", bundle)

                        setResult(RESULT_OK, intent)
                        finish()*/

        CustomDialog(this).showLocationSendDialog(text,object: DialogListener(){
            override fun okClick() {
                val intent = Intent()

                Log.e("ADDRESS","$address **")
                val bundle = Bundle()
                bundle.putDouble("latitude", latLng!!.latitude)
                bundle.putDouble("longitude", latLng!!.longitude)
                bundle.putString("address", address)
                bundle.putString("country", mCountry)
                intent.putExtra("data", bundle)

                setResult(RESULT_OK, intent)
                finish()
            }
        } )

    }

}