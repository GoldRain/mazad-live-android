package com.mazadlive.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.ImageView
import android.widget.Toast
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.services.s3.AmazonS3Client
import com.mazadlive.helper.PickImageHelper
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.github.florent37.runtimepermission.kotlin.askPermission
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.helper.Constant
import com.mazadlive.models.LiveProductModel
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.activity_add_live_product.*
import kotlinx.android.synthetic.main.header_view.*
import java.lang.Exception
import java.text.DecimalFormat

class AddLiveProductActivity : BaseActivity() {

    private val userPref = MyApplication.instance.getUserPreferences()
    private val TAG = "AddLiveProductActivity"
    private var imageFilePath = ""
    private var imageFileUrl = ""

    private lateinit var pickImageHelper: PickImageHelper
    private var s3: AmazonS3Client? = null
    private var transferUtility: TransferUtility? = null
    private val imageCounter = 0
    lateinit var progress: CustomProgress


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_live_product)
        supportActionBar?.hide()

        progress = CustomProgress(this)


        pickImageHelper = PickImageHelper(this, null, true, imagePickerListener())
        hTitle.text = getString(R.string.text_add)

        cash_switch.tag = false
        credit_card_switch.tag = false

        frame_add.setOnClickListener {
            askPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) {
                pickImageHelper.pickImage(4)
            }.onDeclined { result ->
                if (result.hasDenied()) {
                    CustomDialog(this).showErrorDialog(getString(R.string.Permission_Required))
                } else if (result.hasForeverDenied()) {
                    result.foreverDenied.forEach {
                        CustomDialog(this).permissionDialog(getString(R.string.Please_Accept_Permission), object : DialogListener() {
                            override fun yesClick() {
                                super.yesClick()
                                result.goToSettings()
                            }
                        })
                    }
                }
            }
        }

        cash_switch.setOnClickListener {
            buttonOnOff(cash_switch)
        }

        credit_card_switch.setOnClickListener {
            buttonOnOff(credit_card_switch)
        }


        add_btn.setOnClickListener {
            /*val productModel = LiveProductModel()
            productModel.path = "/storage/emulated/0/DCIM/Camera/IMG20190205215354.jpg"
            productModel.serial = "c gb"
            productModel.description = "gingvu"
            productModel.paymentMode = "CASH, Card"
            productModel.price = "272"

            intent.putExtra("item", productModel)
            setResult(Activity.RESULT_OK, intent)
            finish()*/
            if (validation()) {
                moveNext()
            }

           /* val productModel = LiveProductModel()
           productModel.path = "/storage/emulated/0/Download/journey-man-1348520-unsplash.jpg"
           productModel.serial = "asdad"
           productModel.description = "gingvu"
           productModel.paymentMode = "CASH, Card"
           productModel.price = "272"

           intent.putExtra("item", productModel)
           setResult(Activity.RESULT_OK, intent)
           finish()*/
        }

        hBack.setOnClickListener {
            onBackPressed()
        }

    }

    private fun buttonOnOff(imageview: ImageView) {
        if (imageview.tag as Boolean) {
            imageview.setImageResource(R.drawable.ic_switch_off)
            imageview.tag = false
        } else {
            imageview.setImageResource(R.drawable.ic_switch_on)
            imageview.tag = true
        }
    }

    private fun moveNext() {

        var paymentMode: String = ""

        if (cash_switch.tag as Boolean) {
            paymentMode = "Cash"
        }
        if (credit_card_switch.tag as Boolean) {
            if (TextUtils.isEmpty(paymentMode)) {
                paymentMode = "Card"
            } else {
                paymentMode = "$paymentMode,Card"
            }

        }

        val intent = Intent()
        val productModel = LiveProductModel()

        productModel.path = imageFilePath
        productModel.serial = et_serial.text.toString().trim()
        productModel.description = et_des.text.toString().trim()
        productModel.paymentMode = paymentMode
        productModel.price = (DecimalFormat("##.##").format(et_price.text.toString().trim().toDouble()))

        intent.putExtra("item", productModel)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }


    private fun validation(): Boolean {

        if (TextUtils.isEmpty(imageFilePath)) {
            Toast.makeText(this, getString(R.string.select_product_image), Toast.LENGTH_SHORT).show()
            return false
        }

        if (TextUtils.isEmpty(et_serial.text.toString().trim())) {
            et_serial.error = getString(R.string.enter_product_details)
            return false
        }

        if (TextUtils.isEmpty(et_price.text.toString().trim())) {
            et_price.error = getString(R.string.enter_product_price)
            return false
        } else {

            try {
                val tempamount = et_price.text.toString().trim().toDouble()
                if (tempamount <= 0) {
                    Toast.makeText(this, getString(R.string.product_price_morethan_0), Toast.LENGTH_SHORT).show()
                    return false
                }
                if (tempamount > Constant.MAX_TRANSACTION_AMOUNT) {
                    Toast.makeText(this, "Enter amount less than ${DecimalFormat("##.##").format(Constant.MAX_TRANSACTION_AMOUNT)}", Toast.LENGTH_SHORT).show()
                    return false
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Toast.makeText(this, getString(R.string.enter_valid_product_price), Toast.LENGTH_SHORT).show()
                return false
            }

        }

        if (TextUtils.isEmpty(et_des.text.toString().trim())) {
            et_des.error = getString(R.string.enter_product_description)
            return false
        }

        if (!(cash_switch.tag as Boolean)) {
            if (!(credit_card_switch.tag as Boolean)) {
                Toast.makeText(this, getString(R.string.payment_mode_error), Toast.LENGTH_SHORT).show()
                return false
            }
        }

        if (!(credit_card_switch.tag as Boolean)) {
            if (!(cash_switch.tag as Boolean)) {
                Toast.makeText(this, getString(R.string.payment_mode_error), Toast.LENGTH_SHORT).show()
                return false
            }
        }



        return true
    }

    inner class imagePickerListener : PickImageHelper.OnImagePickerListener {

        override fun onImagePicked(imagePath: String, requestCode: Int) {
            if (requestCode == Constant.CAMERA_IMAGE_REQUEST) {
                imageFilePath = imagePath
                Log.e(TAG, "CAMERA *****")
                Glide.with(this@AddLiveProductActivity).asBitmap().load(imagePath).apply(RequestOptions().override(200)).into(product_image)

            } else if (requestCode == Constant.GALLARY_IMAGE_REQUEST) {
                Log.e(TAG, "GALLARY")
                imageFilePath = imagePath
                product_image.setPadding(0, 0, 0, 0)
                Glide.with(this@AddLiveProductActivity).asBitmap().load(imagePath).apply(RequestOptions().override(200)).into(product_image)

            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        pickImageHelper.onActivityResult(requestCode, resultCode, data)
    }
}
