package com.mazadlive.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.Window
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.TranslateAnimation
import android.widget.PopupMenu
import android.widget.Toast
import com.mazadlive.helper.PickImageHelper
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.activities.settings.AccountActivity
import com.mazadlive.activities.settings.FollowActivity
import com.mazadlive.activities.settings.SettingActivity
import com.mazadlive.adapters.FeedBuyerAdapter
import com.mazadlive.adapters.FeedSellerAdapter
import com.mazadlive.adapters.UserPostAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ApiUrl
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.database.MessageData
import com.mazadlive.helper.GridSpacingItemDecoration
import com.mazadlive.helper.MessageEvent
import com.mazadlive.models.PostModel
import com.mazadlive.models.SubscribeModel
import com.mazadlive.models.TapPaymentModel
import com.mazadlive.models.UserModel
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.customdialog_badge_alert.*
import kotlinx.android.synthetic.main.customdialog_badge_form.*
import kotlinx.android.synthetic.main.customdialog_post_view_recycle.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONObject

class UserProfileActivity : Activity() {

    private lateinit var fragmentView: View
    private var progress: CustomProgress? = null
    var postList = ArrayList<PostModel>()
    var isMe = true
    private var isFollowing = false
    private var isUserFollow = false

    private var userRef = MyApplication.instance.getUserPreferences()
    private var userModel: UserModel? = null
    private var totalFollower = 0
    private var totalFollowing = 0
    private var imageFilePath = ""
    private var docBitmap: Bitmap? = null
    private var dialogBadgeForm: Dialog? = null
    lateinit var pickImageHelper: PickImageHelper
    private var REQUEST_BUDGE_PAYMENT = 5000

    override fun onResume() {
        setMessageRead()
        super.onResume()
    }

    var isUserLive = "stopped"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentView = LayoutInflater.from(this).inflate(R.layout.fragment_profile,null)
        setContentView(fragmentView)
        progress = CustomProgress(this)
        pickImageHelper = PickImageHelper(this, null, true, PickerHelper())

        userModel = intent!!.getParcelableExtra("user")
        isMe = intent!!.getBooleanExtra("isMe",false)

        if (!isMe) {

            fragmentView.mGoLive.visibility = View.GONE
            fragmentView.menu.visibility = View.VISIBLE

            fragmentView.mMenu.setImageResource(R.drawable.ic_clear_white)
            if (isUserLive.equals("running", true)) {
                fragmentView.ripple_lay.startRippleAnimation()
                val anim2 = AnimationUtils.loadAnimation(this, R.anim.zoom_in_out)
                fragmentView.frame_profile.startAnimation(anim2)
            }

            // fragmentView.live.visibility = View.VISIBLE
        } else {

            fragmentView.mMenu.setImageResource(R.drawable.menu)

            if (userRef.appModeBuyer) {
                fragmentView.mGoLive.visibility = View.GONE
            } else {
                fragmentView.mGoLive.visibility = View.VISIBLE
                setBudgeData()
            }
            fragmentView.menu.visibility = View.GONE
        }

        fragmentView.mMenu.setOnClickListener {
            onBackPressed()
        }

        getUserData()
        saveUserData()

        val vto = fragmentView.viewTreeObserver
        vto.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            @SuppressLint("ObsoleteSdkInt")
            override fun onGlobalLayout() {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    fragmentView.viewTreeObserver.removeGlobalOnLayoutListener(this)
                } else {
                    fragmentView.viewTreeObserver.removeOnGlobalLayoutListener(this)
                }

                val height = fragmentView.measuredHeight
                fragmentView.relativeLayout2.requestLayout()
                fragmentView.relativeLayout2.layoutParams.height = (height * 56) / 100
            }
        })

        getUserPost()

        fragmentView.recycleview.adapter = UserPostAdapter(this, postList)
        fragmentView.recycleview.layoutManager = GridLayoutManager(this, 3)
        fragmentView.recycleview.addItemDecoration(GridSpacingItemDecoration(3, 6, false))

        (fragmentView.recycleview.adapter as UserPostAdapter).listener = OnItemClick()

        fragmentView.mGoLive.setOnClickListener {

            getStreamCount()



        }


        fragmentView.mSetting.setOnClickListener { startActivity(Intent(this, SettingActivity::class.java)) }

        fragmentView.mEdit.setOnClickListener {
            val intent = Intent(this, AccountActivity::class.java)
            startActivity(intent)
        }

        fragmentView.menu.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                showPopupMenu(it)
            }else{

                CustomDialog(this).createDialogBox()
            }
        }

        fragmentView.lay_following.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                val intent = Intent(this, FollowActivity::class.java)
                intent.putExtra("type", "following")
                intent.putExtra("isMe", isMe)

                if (!isMe) {
                    intent.putExtra("userModel", userModel)
                }
                startActivity(intent)
            }else{

                CustomDialog(this).createDialogBox()
            }
        }


        fragmentView.lay_follow.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                val intent = Intent(this, FollowActivity::class.java)
                intent.putExtra("type", "follower")
                intent.putExtra("isMe", isMe)

                if (!isMe) {
                    intent.putExtra("userModel", userModel)
                }

                startActivity(intent)
            }else{

                CustomDialog(this).createDialogBox()
            }
        }

    }


    private fun setBudgeData() {

        if (!userRef.verified){
            if (TextUtils.isEmpty(userRef.verificationStatus)) {
                // Show Badge Alert Dialog
                if (!userRef.badgeDailogShow) {
                    userRef.badgeDailogShow = true
                    showBadgeAlertDialog(false)
                } else {
                    fragmentView.mBadgeBottomBar.visibility = View.VISIBLE
                    fragmentView.mVerificationStatus.text = resources.getString(R.string.verification_upload)
                }
            } else {
                if (userRef.verificationStatus == "pending") {
                    fragmentView.mBadgeBottomBar.visibility = View.VISIBLE
                    fragmentView.mVerificationStatus.text = resources.getString(R.string.verification_padding)
                } else if (userRef.verificationStatus == "approved") {
                    fragmentView.mBadgeBottomBar.visibility = View.VISIBLE
                    fragmentView.mVerificationStatus.text = resources.getString(R.string.verification_approved)
                }
            }
        }else{
            fragmentView.mBadgeBottomBar.visibility = View.GONE
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.e("DOC_IMAGES", "*** onActivityResult")
        if ( resultCode == Activity.RESULT_OK && requestCode == REQUEST_BUDGE_PAYMENT){
            val tapPaymentModel = data!!.getParcelableExtra<TapPaymentModel>("model")

            ServiceRequest(object : ApiResponseListener{
                override fun onCompleted(`object`: Any) {
                    userRef.verified = true
                    userRef.verificationStatus == "verified"
                    setBudgeData()
                }

                override fun onError(errorMessage: String) {
                    CustomDialog(this@UserProfileActivity).showErrorDialog(getString(R.string.error_payment))
                }

            }).verificationBudgePayment(tapPaymentModel)
        }else{
            pickImageHelper.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun getStreamCount() {
        val listener = object : DialogListener() {
            override fun okClick() {

            }
        }

        progress!!.show()
        val param = JSONObject()
        param.put("version", "1.1")
        param.put("user_id", userRef!!.id)

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                val jsonObject = JSONObject(`object`.toString())
                if (jsonObject.has("description")) {
                    val obj = jsonObject.getJSONObject("description")
                    if (obj.has("available_live_count")) {
                        val count = obj.getInt("available_live_count")
                        if (count == 0) {
                            CustomDialog(this@UserProfileActivity).showAlertDialog(getString(R.string.alert), getString(R.string.warning_stream), listener)
                        }else{
                            val intent = Intent(this@UserProfileActivity, LiveOnActivity::class.java)
                            startActivity(intent)
                        }
                    }


                    if(obj.has("unlimited_live_count")){
                        val subObj  =obj.getJSONObject("unlimited_live_count")
                        val subModel = SubscribeModel()
                        if(subObj.has("status")){
                            subModel.status = subObj.getBoolean("status")
                            //  ad1.status = subObj.getBoolean("status")
                            //   ad4.status = subObj.getBoolean("status")
                        }

                        if(subObj.has("activation")){
                            subModel.activation = subObj.getString("activation")
                            //ad1.activation = subObj.getString("activation")
                            //     .//    ad4.activation = subObj.getString("activation")
                        }

                        if(subObj.has("expiry")){
                            subModel.expiry = subObj.getString("expiry")
                            /* ad1.expiry = subObj.getString("expiry")
                             ad4.expiry = subObj.getString("expiry")*/
                        }
                        if(subObj.has("count")){
                            subModel.previousCount = subObj.getInt("count")

                            /* ad4.available = subObj.getInt("count").toString()
                             ad1.available = subObj.getInt("count").toString()*/
                        }

                        val count = subModel.previousCount
                        if (!subModel.status && count == 0) {
                            CustomDialog(this@UserProfileActivity!!).showAlertDialog("Alert", getString(R.string.warning_stream), listener)
                        }
                        //   ad4.subModel = subModel
                    }
                }

            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(this@UserProfileActivity, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).showCounts(param)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(msg: MessageData) {
        setMessageRead()
    }

    private fun moveNext() {

        val intent = Intent(this@UserProfileActivity, StreamingActivity::class.java)
        startActivity(intent)
        // finish()
    }

    fun setMessageRead() {
        /* doAsync {
            *//* val count = AppDatabase.getAppDatabase().messageDao().getUnreadCount()
            Log.e("Count_Count", "Count : $count")
            (activity!!).runOnUiThread {
                if (count > 0) {
                    fragmentView.mMenu.setImageResource(R.drawable.ic_menu_alert)
                    // msg_count.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.border_round_red,0)
                } else {
                    // msg_count.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                    fragmentView.mMenu.setImageResource(R.drawable.menu)
                }
            }*//*
            // Log.e(TAG, "$count ** ")
        }*/
    }

    private fun showPopupMenu(view: View) {

        val popupMenu = PopupMenu(this@UserProfileActivity, view)
        popupMenu.inflate(R.menu.profile_menu)

        if (isFollowing) {
            popupMenu.menu.findItem(R.id.follow).title = getString(R.string.unfollow)
        } else {
            if (isUserFollow) {
                popupMenu.menu.findItem(R.id.follow).title = getString(R.string.follow_back)
            } else {
                popupMenu.menu.findItem(R.id.follow).title = getString(R.string.follow)
            }
        }
        popupMenu.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.follow -> {
                    followPeople()
                }

                R.id.mReport -> {
                    CustomDialog(this@UserProfileActivity).showReportDialog(getString(R.string.give_feedbak_for_user), object : DialogListener() {
                        override fun okClick(any: Any) {
                            reportUser(any.toString())
                        }
                    })
                }

                R.id.mBlock -> {
                    blockUser()
                }
            }

            true
        }

        popupMenu.show()
    }

    private fun showProfileDialog(position: Int) {

        val dialog = Dialog(this@UserProfileActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.customdialog_post_view_recycle)
        val wlp = dialog.window.attributes
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog.window.attributes = wlp

        Log.e("GET_LIST_POST", "** ${postList.size} $isMe")

        val tempPostList = ArrayList<PostModel>()
        tempPostList.add(postList.get(position))
        dialog.recyclerview.layoutManager = LinearLayoutManager(this@UserProfileActivity, LinearLayoutManager.HORIZONTAL, false)
        if (isMe) {
            dialog.recyclerview.adapter = FeedSellerAdapter(this@UserProfileActivity, tempPostList, object : FeedSellerAdapter.OnClikPost {
                override fun onClickPost(position: Int) {

                }
            })

            (dialog.recyclerview.adapter as FeedSellerAdapter).reportListener = object : FeedSellerAdapter.ReportListener() {
                override fun onReport(position: Int) {
                    dialog.dismiss()
                    getUserPost()
                }

            }

        } else {
            dialog.recyclerview.adapter = FeedBuyerAdapter(this@UserProfileActivity, tempPostList, object : FeedBuyerAdapter.OnClikPost {
                override fun onClickPost(position: Int) {

                }
            })

            (dialog.recyclerview.adapter as FeedBuyerAdapter).reportListener = object : FeedBuyerAdapter.ReportListener() {
                override fun onReport(position: Int) {
                    dialog.dismiss()
                    getUserPost()
                }

            }
        }
        dialog.recyclerview.adapter.notifyDataSetChanged()
        dialog.recyclerview.scrollToPosition(position)
        dialog.show()
    }

    private fun showBadgeAlertDialog(isForPayment : Boolean) {

        val dialog = Dialog(this@UserProfileActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.customdialog_badge_alert)
        val wlp = dialog.window.attributes
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog.window.attributes = wlp
        dialog.window.attributes.windowAnimations = R.style.dialog_anim


        Glide.with(this@UserProfileActivity).load(userRef.profile).apply(RequestOptions().override(500).error(R.drawable.dummy)).into(dialog.badge_alert_profileImage)

        val mAnimation = TranslateAnimation(0F, 0F, 0F, 50F);
        mAnimation.setDuration(500);
        mAnimation.setFillAfter(true);
        mAnimation.setRepeatCount(-1);
        mAnimation.setRepeatMode(Animation.REVERSE);

        dialog.badge_alert_im_thumb.setAnimation(mAnimation)
        val anim2 = AnimationUtils.loadAnimation(this@UserProfileActivity, R.anim.zoom_in_out)
        dialog.badge_alert_im_verify.startAnimation(anim2)

        if (isForPayment){
            dialog.badge_alert_des.text = resources.getString(R.string.verification_payment_dialog)
        }else{
            dialog.badge_alert_des.text = resources.getString(R.string.verification_id_dialog)
        }

        dialog.badge_alert_later_btn.setOnClickListener {
            dialog.dismiss()
            userRef.showVerifyDialog = false
        }

        dialog.setOnDismissListener {
            fragmentView.mBadgeBottomBar.visibility = View.VISIBLE
            fragmentView.mVerificationStatus.text = resources.getString(R.string.verification_upload)
        }
        dialog.show()
    }

    private fun updateValue() {
        fragmentView.post_count.setText("${postList.size}")
        fragmentView.follow.setText("$totalFollower")
        fragmentView.following.setText("$totalFollowing")
    }

    private fun saveUserData() {
        var profileUrl = userRef.profile
        var username = userRef.username
        var countryName = userRef.CountryName
        var des = userRef.description

        if (!isMe) {
            profileUrl = userModel!!.profile
            username = userModel!!.username
            countryName = userModel!!.countryName
            des = ""
            fragmentView.mEdit.visibility = View.GONE
            fragmentView.mSetting.visibility = View.GONE
            fragmentView.menu.visibility = View.VISIBLE

            if (userModel!!.verified) {
                fragmentView.im_verify.setImageResource(R.drawable.ic_verify)
            } else {
                fragmentView.im_verify.setImageResource(R.drawable.ic_unverified)
            }
        } else {
            if (userRef.verified) {
                fragmentView.im_verify.setImageResource(R.drawable.ic_verify)
            } else {
                fragmentView.im_verify.setImageResource(R.drawable.ic_unverified)
            }

        }
        Glide.with(this@UserProfileActivity).load(profileUrl).apply(RequestOptions().override(500).error(R.drawable.dummy)).into(fragmentView.mProfileImage)
        fragmentView.user_name.text = username
        fragmentView.loaction.text = countryName
        fragmentView.details.text = des
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        if (MyApplication.instance.getUserPreferences().appModeBuyer) {
            // fragmentView.mGoLive.visibility = View.GONE
        } else {
            // fragmentView.mGoLive.visibility = View.VISIBLE
        }
    }


    fun getUserPost() {
        val map = JSONObject()
        map.put("user_id", userRef!!.id)
        map.put("version", "1.1")

        postList.clear()
        if (!isMe) {
            map.put("user_id", userModel!!.id)
        }

        Log.e("PROFILE_PARAM", "${map.toString()}")
        progress!!.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                fragmentView.recycleview.adapter.notifyDataSetChanged()
                updateValue()
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(this@UserProfileActivity, "${getString(R.string.failed)} $errorMessage", Toast.LENGTH_SHORT).show()
                // CustomDialog(activity!!).showErrorDialog("Failed to get post $errorMessage")
            }

        }).getPost(postList, map, ApiUrl.getMyPosts, false)
    }

    private fun followPeople() {

        val map = JSONObject()
        map.put("self_id", userRef!!.id)
        map.put("his_id", userModel!!.id)
        map.put("version", "1.1")

        if (isFollowing) {
            map.put("is_follow", 0)
        } else {
            map.put("is_follow", 1)
        }


        progress!!.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                isFollowing = !isFollowing
                getUserData()

            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(this@UserProfileActivity, "${getString(R.string.failed)} $errorMessage", Toast.LENGTH_SHORT).show()
                // CustomDialog(activity!!).showErrorDialog("Failed to get post $errorMessage")
            }

        }).followPeople(map)
    }

    private fun getUserData() {
        val map = JSONObject()
        if (isMe) {
            map.put("user_id", userRef!!.id)
        } else {
            map.put("user_id", userModel!!.id)
        }
        map.put("version", "1.1")
        progress!!.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                val json = JSONObject(`object`.toString())
                if (json.has("description")) {
                    val userArray = json.getJSONArray("description")
                    for (i in 0 until userArray.length()) {
                        val user = UserModel()
                        val obj = userArray.getJSONObject(i)

                        if (obj.has("following")) {

                            val array1 = obj.getJSONArray("following")

                            totalFollowing = array1.length()

                            val followingList = ArrayList<UserModel>()
                            for (i in 0 until array1.length()) {

                                val userModel = UserModel()
                                val following = array1.getJSONObject(i)
                                if (following.has("_id")) {
                                    if (userRef.id.equals(following.getString("_id"))) {
                                        isUserFollow = true
                                        break
                                    } else {
                                        isUserFollow = false
                                    }
                                }

                                if (following.has("username")) {
                                    userModel.username = following.getString("username")
                                }

                                if (following.has("country_code")) {
                                    userModel.countryCode = following.getString("country_code")
                                }

                                if (following.has("country_name")) {
                                    userModel.countryName = following.getString("country_name")
                                }

                                if (following.has("phone")) {
                                    userModel.phone = following.getString("phone")
                                }

                                if (following.has("email")) {
                                    userModel.email = following.getString("email")
                                }
                                if (following.has("type")) {
                                    userModel.type = following.getString("type")
                                }

                                if (following.has("is_live")) {
                                    isUserLive = following.getString("is_live")
                                }

                                if (following.has("profilePhoto")) {
                                    userModel.profile = following.getString("profilePhoto")
                                }

                                if (obj.has("verification")) {
                                    val vObj = obj.getJSONObject("verification")
                                    if (vObj.has("status")) {
                                        if (vObj.getString("status").equals("approved", true)) {
                                            userModel.verified = true
                                        }
                                    }
                                }

                                followingList.add(userModel)
                            }
                        }

                        if (obj.has("followers")) {
                            val userModel = UserModel()
                            val array2 = obj.getJSONArray("followers")
                            totalFollower = array2.length()

                            val followerList = ArrayList<UserModel>()
                            for (i in 0 until array2.length()) {
                                val follower = array2.getJSONObject(i)

                                Log.e("FOLLOWER", " ** ${follower.toString()}")
                                if (follower.has("_id")) {

                                    if (userRef.id.equals(follower.getString("_id"))) {
                                        isFollowing = true
                                        break
                                    } else {
                                        isFollowing = false
                                    }
                                }

                                if (follower.has("username")) {
                                    userModel.username = follower.getString("username")
                                }

                                if (follower.has("country_code")) {
                                    userModel.countryCode = follower.getString("country_code")
                                }

                                if (follower.has("country_name")) {
                                    userModel.countryName = follower.getString("country_name")
                                }

                                if (follower.has("phone")) {
                                    userModel.phone = follower.getString("phone")
                                }

                                if (follower.has("email")) {
                                    userModel.email = follower.getString("email")
                                }
                                if (follower.has("type")) {
                                    userModel.type = follower.getString("type")
                                }

                                if (follower.has("profilePhoto")) {
                                    userModel.profile = follower.getString("profilePhoto")
                                }

                                followerList.add(userModel)
                            }
                        }
                        updateValue()
                    }
                }
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(this@UserProfileActivity, "${getString(R.string.failed)} $errorMessage", Toast.LENGTH_SHORT).show()
                // CustomDialog(activity!!).showErrorDialog("Failed to get post $errorMessage")
            }

        }).userDetails(map)
    }

    private fun blockUser() {

        val param = JSONObject()
        param.put("version", "1.1")
        param.put("his_id", userModel!!.id)
        param.put("self_id", userRef.id)
        param.put("is_block", 1)

        Log.e("PARAM:", "${param.toString()} **")
        progress!!.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                (this@UserProfileActivity).onBackPressed()
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(this@UserProfileActivity, "Could not block $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).blockUser(param)
    }

    private fun reportUser(msg: String) {
        val param = JSONObject()
        param.put("version", "1.1")
        param.put("his_id", userModel!!.id)
        param.put("self_id", userRef.id)
        param.put("reason", msg)
        param.put("message", msg)

        progress!!.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                (this@UserProfileActivity).onBackPressed()
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(this@UserProfileActivity, "${getString(R.string.failed)} $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).reportUser(param)
    }

    private fun uploadDocumentToServer(param: JSONObject) {

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()

                userRef.showVerifyDialog = false
                dialogBadgeForm!!.dismiss()
                CustomDialog(this@UserProfileActivity).showAlertDialog(getString(R.string.alert), getString(R.string.doc_uploaded), object : DialogListener() {
                    override fun okClick() {
                        userRef.verificationStatus = "pending"
                        setBudgeData()
                        getUserData()
                    }
                })
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(this@UserProfileActivity, "${getString(R.string.failed)} $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).userVerification(param)
    }

    inner class OnItemClick : UserPostAdapter.OnItemClick {
        override fun onItemClick(position: Int) {
            showProfileDialog(position)
        }
    }


    inner class PickerHelper : PickImageHelper.OnImagePickerListener {

        override fun onImagePicked(imagePath: String, requestCode: Int) {
            Log.e("DOC_IMAGES", "*** $imagePath")

            Glide.with(this@UserProfileActivity).asBitmap().load(imagePath).apply(RequestOptions().override(1500)).listener(object : RequestListener<Bitmap> {
                override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                    return false
                }

                override fun onResourceReady(resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                    Log.e("DOC_IMAGES", "*** $imagePath")

                    dialogBadgeForm!!.badge_form_doc_image.setImageBitmap(resource)
                    docBitmap = resource
                    return true
                }

            }).into(dialogBadgeForm!!.badge_form_doc_image)
        }
    }

}
