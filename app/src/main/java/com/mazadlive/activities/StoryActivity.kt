package com.mazadlive.activities

import android.app.Dialog
import android.content.Context
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.adapters.StoryListAdapter
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.fragments.Story.FullStoryFragment
import com.mazadlive.models.StoryModel
import kotlinx.android.synthetic.main.activity_account.*
import kotlinx.android.synthetic.main.activity_story.*
import kotlinx.android.synthetic.main.view_story_reply.*
import org.greenrobot.eventbus.EventBus

class StoryActivity : BaseActivity() {

    var timer: Int = 0
    var type: String? = null
    val storyList = ArrayList<StoryModel>()
     var currentPosition = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //requestWindowFeature(Window.FEATURE_NO_TITLE)
       // window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_story)
        supportActionBar!!.hide()

        val temp  = intent.getParcelableArrayListExtra<StoryModel>("storyList")
        storyList.addAll(temp)
        type = intent.getStringExtra("TYPE")

        setFragments()
    }


    private fun setFragments() {
        val listFrag = ArrayList<Fragment>()

        for(i in  0 until storyList.size){
            //Log.e("STORY_ACTI","${storyList[i].is_video} $")
            val frag  = FullStoryFragment()

            frag.fragPosition = i
            frag.storyActivity = this
            val bundle = Bundle()
            bundle.putParcelable("story",storyList[i])
            frag.arguments = bundle

            listFrag.add(frag)


        }

        currentPosition = intent.getIntExtra("position",0)
        val adapter = StoryListAdapter(supportFragmentManager,storyList,listFrag)

        view_pager.adapter = adapter

        adapter.notifyDataSetChanged()
        view_pager.setCurrentItem(currentPosition)



        view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {


            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {

                 //Log.e("StoryActivity","$currentPosition **1111111")
                 currentPosition = position
                 EventBus.getDefault().post(storyList[position])
            }

        })


    }


    fun moveNext() {
        Log.e("StoryActivity","$currentPosition **")
        if(currentPosition < storyList.size-1){
            view_pager.setCurrentItem(currentPosition+1)
        }else{
           onBackPressed()
        }
    }


}
