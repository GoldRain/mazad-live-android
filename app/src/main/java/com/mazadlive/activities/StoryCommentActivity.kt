package com.mazadlive.activities

import android.app.Activity
import android.app.Service
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.MediaController
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customviews.CustomEditText
import com.mazadlive.helper.SoftKeyboard
import kotlinx.android.synthetic.main.activity_story_comment.*
import android.media.MediaPlayer
import android.net.Uri
import android.widget.Toast
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.MappingTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.util.Util


class StoryCommentActivity : BaseActivity() {

    val TAG = "StoryCommentActivity"
    var softKeyboard: SoftKeyboard? = null

    private var player: SimpleExoPlayer? = null
    private lateinit var mediaDataSourceFactory: DataSource.Factory
    private val bandwidthMeter: BandwidthMeter = DefaultBandwidthMeter()
    private var trackSelector: DefaultTrackSelector? = null
    private var lastSeenTrackGroupArray: TrackGroupArray? = null

    private var currentWindow: Int = 0
    private var playbackPosition: Long = 0
    var SeekTo: Int = 0

    private var videoFile = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story_comment)
        supportActionBar?.hide()

        videoFile  = intent.getStringExtra("path")
        val fromFile  = intent.getBooleanExtra("fromFile",false)

        Log.e("VIDEO_PATH","$videoFile")
        val index = videoFile.lastIndexOf(".")
        if(index >0){
            videoFile = ("${videoFile.substring(0,index)}.mp4")
        }


      //  Log.e("VIDEO_PATH","@@ $videoFile")
     /*   videoView.setVideoPath(videoFile);
        videoView.setMediaController( MediaController(this));
        videoView.start();

        videoView.setOnErrorListener(MediaPlayer.OnErrorListener { mediaPlayer, i, i1 ->
            false })
        videoView.setOnPreparedListener(MediaPlayer.OnPreparedListener {
            progress_bar.visibility = View.GONE
            Log.e(TAG,"isReady to play")
        })
*/

        mediaDataSourceFactory = DefaultDataSourceFactory(this, Util.getUserAgent(this, "mediaPlayerSample"),
                bandwidthMeter as TransferListener<in DataSource>)

    }

    override fun onPause() {
        if (Util.SDK_INT <= 23) releasePlayer()
        super.onPause()
    }

    override fun onResume() {
        if (Util.SDK_INT <= 23 && player == null) {
            initializePlayer()
        }

        super.onResume()
    }

    public override fun onStart() {
        super.onStart()

        if (Util.SDK_INT > 23) initializePlayer()
    }

    public override fun onStop() {
        super.onStop()

        if (Util.SDK_INT > 23) releasePlayer()
    }


    private fun initializePlayer() {

        player_view.requestFocus()

        val videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)

        trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)
        //lastSeenTrackGroupArray = null

        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector)

        player_view.player = player

        with(player!!) {
            addListener(PlayerEventListener())
            playWhenReady = true
        }

        // Use Hls, Dash or other smooth streaming media source if you want to test the track quality selection.
        /*val mediaSource: MediaSource = HlsMediaSource(Uri.parse("https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"),
                mediaDataSourceFactory, mainHandler, null)*/

        val mediaSource = ExtractorMediaSource.Factory(mediaDataSourceFactory)
                .createMediaSource(Uri.parse(videoFile))

        var haveStartPosition = currentWindow != C.INDEX_UNSET
        if (haveStartPosition) {
            player!!.seekTo(currentWindow, playbackPosition)
        }
        haveStartPosition = currentWindow != SeekTo
        if (haveStartPosition) {
            player!!.seekTo(currentWindow, SeekTo.toLong())
        }
        player!!.prepare(mediaSource, !haveStartPosition, false)
      //  updateButtonVisibilities()

       // ivHideControllerButton.setOnClickListener { playerView.hideController() }
    }


    private inner class PlayerEventListener : Player.DefaultEventListener() {

        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            when (playbackState) {
                Player.STATE_IDLE       // The player does not have any media to play yet.
                -> progress_bar1.visibility = View.VISIBLE
                Player.STATE_BUFFERING  // The player is buffering (loading the content)
                -> progress_bar1.visibility = View.VISIBLE
                Player.STATE_READY      // The player is able to immediately play
                -> progress_bar1.visibility = View.GONE
                Player.STATE_ENDED      // The player has finished playing the media
                -> progress_bar1.visibility = View.GONE
            }
           // updateButtonVisibilities()
        }

        override fun onTracksChanged(trackGroups: TrackGroupArray?, trackSelections: TrackSelectionArray?) {
           // updateButtonVisibilities()
            // The video tracks are no supported in this device.
            /*if (trackGroups !== lastSeenTrackGroupArray) {
                val mappedTrackInfo = trackSelector!!.currentMappedTrackInfo
                if (mappedTrackInfo != null) {
                    if (mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_VIDEO) == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                        Toast.makeText(this@StoryCommentActivity, "Error unsupported track", Toast.LENGTH_SHORT).show()
                    }
                }
                //lastSeenTrackGroupArray = trackGroups
            }*/
        }
    }


    private fun releasePlayer() {
        if (player != null) {
         //   updateStartPosition()
           // shouldAutoPlay = player!!.playWhenReady
            player!!.release()
            player = null
            trackSelector = null
        }
    }

}

/* 1 . mediaDataSourceFactory = DefaultDataSourceFactory(this, Util.getUserAgent(this, "mediaPlayerSample"),
                bandwidthMeter as TransferListener<in DataSource>)

   2.  private fun initializePlayer() {

        playerView.requestFocus()

        val videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)

        trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)
        lastSeenTrackGroupArray = null

        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector)

        playerView.player = player

        with(player!!) {
            addListener(PlayerEventListener())
            playWhenReady = shouldAutoPlay
        }

        // Use Hls, Dash or other smooth streaming media source if you want to test the track quality selection.
        /*val mediaSource: MediaSource = HlsMediaSource(Uri.parse("https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"),
                mediaDataSourceFactory, mainHandler, null)*/

        val mediaSource = ExtractorMediaSource.Factory(mediaDataSourceFactory)
                .createMediaSource(Uri.parse(mVideoUrl))

        var haveStartPosition = currentWindow != C.INDEX_UNSET
        if (haveStartPosition) {
            player!!.seekTo(currentWindow, playbackPosition)
        }
        haveStartPosition = currentWindow != SeekTo
        if (haveStartPosition) {
            player!!.seekTo(currentWindow, SeekTo.toLong())
        }

        player!!.prepare(mediaSource, !haveStartPosition, false)
        updateButtonVisibilities()

        ivHideControllerButton.setOnClickListener { playerView.hideController() }
    }

    private fun releasePlayer() {
        if (player != null) {
            updateStartPosition()
            shouldAutoPlay = player!!.playWhenReady
            player!!.release()
            player = null
            trackSelector = null
        }
    }


    3.
    private inner class PlayerEventListener : Player.DefaultEventListener() {

        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            when (playbackState) {
                Player.STATE_IDLE       // The player does not have any media to play yet.
                -> progressBar.visibility = View.VISIBLE
                Player.STATE_BUFFERING  // The player is buffering (loading the content)
                -> progressBar.visibility = View.VISIBLE
                Player.STATE_READY      // The player is able to immediately play
                -> progressBar.visibility = View.GONE
                Player.STATE_ENDED      // The player has finished playing the media
                -> progressBar.visibility = View.GONE
            }
            updateButtonVisibilities()
        }

        override fun onTracksChanged(trackGroups: TrackGroupArray?, trackSelections: TrackSelectionArray?) {
            updateButtonVisibilities()
            // The video tracks are no supported in this device.
            if (trackGroups !== lastSeenTrackGroupArray) {
                val mappedTrackInfo = trackSelector!!.currentMappedTrackInfo
                if (mappedTrackInfo != null) {
                    if (mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_VIDEO) == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                        Toast.makeText(this@VideoPlayerActivity, "Error unsupported track", Toast.LENGTH_SHORT).show()
                    }
                }
                lastSeenTrackGroupArray = trackGroups
            }
        }
    }

*/
