package com.mazadlive.activities.payment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import com.mazadlive.R
import com.mazadlive.activities.BaseActivity
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.helper.Constant
import com.mazadlive.models.CardModel
import com.mazadlive.models.PaymentCard
import com.mazadlive.models.UserModel
import com.mazadlive.utils.CardType
import com.mazadlive.utils.DateValidator
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.activity_payment_form.*
import kotlinx.android.synthetic.main.header_view.*
import kotlinx.android.synthetic.main.view_cards_list.view.*
import java.util.ArrayList
import java.util.regex.Pattern
import company.tap.gosellapi.api.facade.GoSellError
import company.tap.gosellapi.api.facade.APIRequestCallback
import company.tap.gosellapi.api.requests.CardRequest
import company.tap.gosellapi.api.facade.GoSellAPI
import company.tap.gosellapi.api.model.*
import company.tap.gosellapi.api.requests.CreateChargeRequest
import company.tap.gosellapi.api.requests.CustomerRequest
import org.json.JSONObject


class PaymentFormActivity : BaseActivity() {

    companion object {
        val REQUEST_CODE = 103
        val START_FOR = "start_for"
        val PAYMENT_CARD = "payment_card"
        val CARD_POSITION = "card_position"
        val ADD = 0
        val EDIT = 1

        fun start(context: Context, bundle: Bundle?) {
            val intent = Intent(context, PaymentFormActivity::class.java)
            bundle?.let { intent.putExtras(bundle) }
            (context as Activity).startActivityForResult(intent, REQUEST_CODE)
            context.overridePendingTransition(R.anim.slide_in_from_right, R.anim.nothing)
        }
    }

    private val TAG = "PaymentActivity"
    private val userRef = MyApplication.instance.getUserPreferences()

    lateinit var progress:CustomProgress
    private lateinit var cardsList: ArrayList<CardModel>
    private var cardType: String? = null
    private var startFor: Int = ADD
    private lateinit var paymentCard: PaymentCard
    private var cardPosition = 0
    private var firstCard = true
    private var user: UserModel? = null
    private var amountPayable = ""
    var token:Token?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_form)
        supportActionBar?.hide()

        progress = CustomProgress(this)
        hTitle.text = resources.getString(R.string.payment_form_title_txt)
        hBack.setOnClickListener { onBackPressed() }

        card_type.setOnClickListener {
            if (card_list_layout.visibility == View.GONE) {
                card_list_layout.visibility = View.VISIBLE
            } else {
                card_list_layout.visibility = View.GONE
            }
        }

        et_card_number.addTextChangedListener(object : TextWatcher {
            private var current = ""
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val len = s?.length?:0
                if (len < 5) {
                    val card = findCardType(s.toString())
                    selected_card_icon.setImageResource(card.frontResource)
                    cardType = card.cardName
                }
                if (s.toString() != current) {
                    val userInput = s.toString().replace("[^\\d]".toRegex(), "")
                    if (userInput.length <= 16) {
                        val sb = StringBuilder()
                        for (i in 0 until userInput.length) {
                            if (i % 4 == 0 && i > 0) {
                                sb.append(" ")
                            }
                            sb.append(userInput[i])
                        }
                        current = sb.toString()
                    }
                    et_card_number.setText(current)
                    et_card_number.setSelection(current.length)
                }
            }
        })

        add_payment_method_btn.setOnClickListener {
            if(validateFields()){
                if (DateValidator.isValid(et_month.text.toString(), et_year.text.toString())){
                    createToken()
                }else{
                    Toast.makeText(this,getString(R.string.text_valid_date),Toast.LENGTH_SHORT).show()
                }
            }else{
                Toast.makeText(this,getString(R.string.text_fill_field),Toast.LENGTH_SHORT).show()
            }

        }
        makeCardList()
    }

    private fun makeCardList() {
        initCardList()
        for ((index, card) in cardsList.withIndex()) {
            val itemView = layoutInflater.inflate(R.layout.view_cards_list, null)
            itemView.card_icon.setImageResource(card.cardIcon)
            itemView.setOnClickListener(CardListItemClickListener(index))
            cards_list.addView(itemView)
        }
    }

    private fun initCardList() {
        cardsList = ArrayList()
        cardsList.add(CardModel(resources.getString(R.string.card_1), R.mipmap.visa))
        cardsList.add(CardModel(resources.getString(R.string.card_2), R.mipmap.master_card))
        cardsList.add(CardModel(resources.getString(R.string.card_3), R.mipmap.amex))
        cardsList.add(CardModel(resources.getString(R.string.card_4), R.mipmap.diners_club))
        cardsList.add(CardModel(resources.getString(R.string.card_5), R.mipmap.discover))
        cardsList.add(CardModel(resources.getString(R.string.card_6), R.mipmap.jcb_payment_ico))
        cardsList.add(CardModel(resources.getString(R.string.card_7), R.mipmap.payment_ic_verve))
        cardsList.add(CardModel(resources.getString(R.string.card_8), R.mipmap.unknown_cc))
    }

    private fun findCardType(number: String): CardType {
        if (number.length < 4) {
            return CardType.INVALID
        }

        for (type in CardType.values()) {
            if (type.typeRegex != null) {
                val pattern = Pattern.compile(type.typeRegex)
                val matcher = pattern.matcher(number.substring(0, 4))

                if (matcher.matches()) {
                    return type
                }
            }
        }

        return CardType.INVALID
    }

    private fun validateFields(): Boolean {
        if (TextUtils.isEmpty(et_name_on_card.text)) {
            return false
        }
        if (TextUtils.isEmpty(et_card_number.text)) {
            return false
        }
        if (TextUtils.isEmpty(et_ccv_number.text)) {
            return false
        }
        if (TextUtils.isEmpty(et_month.text)) {
            return false
        }
        if (TextUtils.isEmpty(et_year.text)) {
            return false
        }
        if (cardType == null) {
            return false
        }

        return true
    }

    fun createToken(){
        progress.show()
        GoSellAPI.getInstance(userRef.TAP_TOKEN).createToken(
                CardRequest.Builder(Constant.TEST_CARD_NUMBER, "10", "20", "123", userRef.TAP_API_KEY)
                        .address_city("Some city")
                        .address_line1("First line")
                        .address_line2("Second line")
                        .address_state("Royal State")
                        .address_zip("007")
                        .address_country("Some country")
                        .name("Testing VISA card")
                        .build(),
                object : APIRequestCallback<Token> {
                    override fun onSuccess(responseCode: Int, serializedResponse: Token) {
                        Log.d(TAG, "onSuccess createToken serializedResponse:$serializedResponse")
                        token = serializedResponse
                        if(TextUtils.isEmpty(userRef.tapCustomerId)){
                            createCustomer()
                        }else{
                            retriveCustomer()
                        }
                    }

                    override fun onFailure(errorDetails: GoSellError) {
                        progress.dismiss()
                        Log.d(TAG, "onFailure createToken, errorCode: " + errorDetails.errorCode + ", errorBody: " + errorDetails.errorBody + ", throwable: " + errorDetails.throwable)
                    }
                }
        )
    }

    private lateinit var customer: Customer

    private fun createCustomer() {
        val customerMetadata = java.util.HashMap<String, String>()
        customerMetadata["Gender"] = "Male"
        customerMetadata["Nationality"] = "Kuwaiti"
        GoSellAPI.getInstance(userRef.TAP_TOKEN).createCustomer(
                CustomerRequest.Builder("Test User", "0096598989898", "test@test.com")
                        .description("test description")
                        .metadata(customerMetadata)
                        .currency("KWT")
                        .build(),
                object : APIRequestCallback<Customer> {
                    override fun onSuccess(responseCode: Int, serializedResponse: Customer) {
                        Log.d(TAG, "onSuccess createCustomer: ")
                        customer = serializedResponse

                        createCharge(token)
                        updateCustomerOnServer()
                    }

                    override fun onFailure(errorDetails: GoSellError) {
                        Log.d(TAG, "onFailure createCustomer, errorCode: " + errorDetails.errorCode + ", errorBody: " + errorDetails.errorBody + ", throwable: " + errorDetails.throwable)
                    }
                }
        )
    }

    private fun updateCustomerOnServer() {
        val param = JSONObject()
        param.put("version","1.1")
        param.put("user_id",userRef.id)
        param.put("tap_customer_id",customer.id)
        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {

            }

            override fun onError(errorMessage: String) {

            }

        }).updateTapCustomerId(param)
    }

    private fun retriveCustomer() {
        val customerMetadata = java.util.HashMap<String, String>()
        customerMetadata["Gender"] = "Male"
        customerMetadata["Nationality"] = "Kuwaiti"
        GoSellAPI.getInstance(userRef.TAP_TOKEN).retrieveCustomer(userRef.tapCustomerId,
                object : APIRequestCallback<Customer> {
                    override fun onSuccess(responseCode: Int, serializedResponse: Customer) {
                        Log.d(TAG, "onSuccess retriveCustomer: $serializedResponse")
                        customer = serializedResponse

                        createCharge(null)
                    }

                    override fun onFailure(errorDetails: GoSellError) {
                        progress.dismiss()
                        Log.d(TAG, "onFailure retriveCustomer, errorCode: " + errorDetails.errorCode + ", errorBody: " + errorDetails.errorBody + ", throwable: " + errorDetails.throwable)
                    }
                }
        )
    }


    private fun createCharge(serializedResponse: Token?) {

        val chargeMetadata = HashMap<String,String>()
        chargeMetadata.put("Order Number", "ORD-1001")


        GoSellAPI.getInstance(userRef.TAP_TOKEN).createCharge(
                CreateChargeRequest.Builder(10.0, "KWD", Redirect("", ""))
                        .statement_descriptor("Test Txn 001")
                        .capture(true)
                        .receipt_email("tiwarinavadeep@gmail.com")
                        .receipt_sms("rSmas")
                        .first_name("Navadeep")
                        .last_name("Tiwari")
                        .description("Test Transaction")
                        .source(Source(token!!.getId()))
                        .metadata(chargeMetadata)
                        .build(),
                object : APIRequestCallback<Charge> {
                    override fun onSuccess(responseCode: Int,serializedResponse : Charge) {
                        progress.dismiss()
                        Toast.makeText(this@PaymentFormActivity,"",Toast.LENGTH_SHORT).show()
                        Log.d(TAG, "onSuccess createCharge: serializedResponse:$serializedResponse")
                    }

                    override fun onFailure(errorDetails: GoSellError) {
                        progress.dismiss()
                        Log.d(TAG, "onFailure createCharge, errorCode: " + errorDetails.errorCode + ", errorBody: " + errorDetails.errorBody + ", throwable: " + errorDetails.throwable)
                    }
                }
        )
    }


    private inner class CardListItemClickListener(private val position: Int): View.OnClickListener {
        override fun onClick(p0: View?) {
            cardType = cardsList[position].cardName
            selected_card_icon.setImageResource(cardsList[position].cardIcon)
            card_list_layout.visibility = View.GONE
        }
    }
}
