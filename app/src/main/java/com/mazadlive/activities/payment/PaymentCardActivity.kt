package com.mazadlive.activities.payment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.mazadlive.R
import com.mazadlive.activities.BaseActivity
import com.mazadlive.models.PaymentCard
import kotlinx.android.synthetic.main.activity_payment_card.*
import kotlinx.android.synthetic.main.header_view.*
import org.jetbrains.anko.doAsync

class PaymentCardActivity : BaseActivity() {

    private val TAG = "CardsActivity"
    private val cardsList = ArrayList<PaymentCard>()

    companion object {
        val REQUEST_CODE = 106

        fun start(context: Context, bundle: Bundle?) {
            val intent = Intent(context, PaymentCardActivity::class.java)
            bundle?.let { intent.putExtras(bundle) }
            (context as Activity).startActivityForResult(intent, REQUEST_CODE)
            context.overridePendingTransition(R.anim.slide_in_from_right, R.anim.nothing)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_card)
        supportActionBar?.hide()

        hTitle.text = resources.getString(R.string.card_title_txt)

        hBack.setOnClickListener { onBackPressed() }

        list_view.layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
        list_view.addItemDecoration(DividerItemDecoration(this, 5))
        loadPaymentCard()

        add_btn.setOnClickListener {
            if (cardsList.size < 3) {
                val bundle = Bundle()
                bundle.putInt(PaymentFormActivity.START_FOR, PaymentFormActivity.ADD)
                PaymentFormActivity.start(this, bundle)
            } else {
               // HelperDialog(this, null).displayFieldErrorDialog(resources.getString(R.string.payment_card_add_btn_error_txt))
            }
        }
    }

    private fun loadPaymentCard() {
        doAsync {
            /*val list = AppDatabase.getAppDatabase().paymentCardDao().getAllCards
            cardsList.clear()
            for (card in list) {
                cardsList.add(card)
            }
            uiThread {
                list_view.adapter = CardListAdapter(this@PaymentCardsActivity, cardsList, ListItemClickListener())
            }*/
        }
    }
}
