package com.mazadlive.activities

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.DeleteObjectRequest
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.facebook.imagepipeline.producers.ProducerContextCallbacks
import com.mazadlive.R
import com.mazadlive.adapters.AddressAdapter
import com.mazadlive.adapters.LiveProductAdapter
import com.mazadlive.helper.Constant
import com.mazadlive.helper.DividerItemDecorator
import com.mazadlive.helper.GridSpacingItemDecoration
import com.mazadlive.models.LiveProductModel
import kotlinx.android.synthetic.main.activity_live_product.*
import kotlinx.android.synthetic.main.customdialog_product_details_two.*
import kotlinx.android.synthetic.main.header_view.*
import org.jetbrains.anko.doAsync

class LiveProductActivity : BaseActivity() {
    var s3: AmazonS3Client? = null
    var transferUtility: TransferUtility? = null
    val productList = ArrayList<LiveProductModel>()
    val  ADD_PRODUCT_CODE =111

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_live_product)
        supportActionBar?.hide()

        hTitle.setText(getString(R.string.auction_items))
        h_im_add.visibility = View.VISIBLE

        h_im_add.setOnClickListener {
            val intent = Intent(this,AddLiveProductActivity::class.java)
            startActivityForResult(intent,ADD_PRODUCT_CODE)
        }

        hBack.setOnClickListener {
            onBackPressed()
        }

        recycleView.layoutManager = GridLayoutManager(this@LiveProductActivity,1)

       // recycleView.addItemDecoration(GridSpacingItemDecoration(1, 24, false))
        recycleView.addItemDecoration(DividerItemDecorator(ContextCompat.getDrawable(this, R.drawable.divider_line)))
        recycleView.adapter = LiveProductAdapter(this,productList,OnItemClick())

        recycleView.adapter.notifyDataSetChanged()

        start_btn.setOnClickListener {
           if(productList.size > 0){
               val intent1 = Intent(this,StreamingActivity::class.java)
               intent1.putExtra("sl",intent.getStringExtra("sl"))
               intent1.putExtra("sc",intent.getStringExtra("sc"))
               intent1.putParcelableArrayListExtra("product",productList)
               startActivity(intent1)
               finish()
           }else{
               Toast.makeText(this,getString(R.string.enter_atleast_one_item),Toast.LENGTH_SHORT).show()
           }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == ADD_PRODUCT_CODE && resultCode == Activity.RESULT_OK){
            val productModel = data!!.getParcelableExtra<LiveProductModel>("item")
            productList.add(productModel)
            recycleView.adapter.notifyDataSetChanged()

            tx_empty.visibility = View.GONE
        }
    }

    inner class OnItemClick:LiveProductAdapter.OnItemClick{
        override fun onClick(position: Int) {
            showProductDetails(position)
        }

        override fun onDelete(position: Int) {
            showDeleteDialog(position)
        }

    }

    private fun showProductDetails(position: Int) {

        val dialog = Dialog(this)

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.setContentView(R.layout.customdialog_product_details_two)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val model = productList[position]
        Glide.with(this).load(model.path).apply(RequestOptions().error(R.drawable.dummy)).into(dialog.product_info_image)

        dialog.product_info_itemNo.text = ("Product ${position+1}/${productList.size}")
        dialog.product_info_des.text = model.description
        dialog.product_info_serial.text = model.serial
        dialog.product_info_price.text = model.price
        dialog.product_info_pay_mode.text = model.paymentMode

        dialog.product_info_close.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun showDeleteDialog(position: Int) {

        val builder = AlertDialog.Builder(this)
                .setTitle(getString(R.string.confrom))
                .setMessage(getString(R.string.are_you_sure_to_delete))
                .setPositiveButton(getString(R.string.delete),object :DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        dialog!!.cancel()

                        productList.removeAt(position)
                        recycleView.adapter.notifyDataSetChanged()

                        if(productList.size<=0){
                            tx_empty.visibility = View.VISIBLE
                        }
                    }

                })
                .setNegativeButton(getString(R.string.cancel),object :DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {

                        dialog!!.cancel()
                    }

                })
                .show()

    }
}
