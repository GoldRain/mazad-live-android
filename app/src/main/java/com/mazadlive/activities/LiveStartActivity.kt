package com.mazadlive.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Window
import android.view.WindowManager
import com.mazadlive.R
import com.mazadlive.adapters.liveCommentAdapter
import com.mazadlive.models.CommentModel
import kotlinx.android.synthetic.main.activity_live_start.*

class LiveStartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_live_start)
        supportActionBar!!.hide()

        close.setOnClickListener { onBackPressed() }

        var commentList = ArrayList<CommentModel>()
        commentview.layoutManager = LinearLayoutManager(this)
        commentview.adapter = liveCommentAdapter(this, commentList)

    }
}
