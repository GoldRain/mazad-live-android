package com.mazadlive.activities

import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.Socket.SocketEventsListener
import com.mazadlive.Socket.SocketManager
import com.mazadlive.adapters.liveCommentAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.helper.Constant
import com.mazadlive.models.*
import com.mazadlive.parser.StremDataParse
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.UserPreferences
import com.red5pro.streaming.R5Connection
import com.red5pro.streaming.R5Stream
import com.red5pro.streaming.R5StreamProtocol
import com.red5pro.streaming.config.R5Configuration
import com.red5pro.streaming.event.R5ConnectionEvent
import com.red5pro.streaming.event.R5ConnectionListener
import kotlinx.android.synthetic.main.activity_player.*
import kotlinx.android.synthetic.main.customdialog_bid_awared_win.*
import kotlinx.android.synthetic.main.customdialog_makeabid.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.text.DecimalFormat

class PlayerActivity : BaseActivity(), R5ConnectionListener {
    companion object {

        var mapComment = HashMap<String, CommentModel>()
    }

    private val TAG = "PlayerActivity"
    lateinit var progress: CustomProgress
     var customDialog: Dialog?= null

    var shopShow = false

    override fun onConnectionEvent(event: R5ConnectionEvent) {
        Log.d("Subscriber", ":onConnectionEvent " + event.name +" "+event.message)
        runOnUiThread {
            connect.text = event.name
        }

        if (event.name === R5ConnectionEvent.LICENSE_ERROR.name) {
            val h = Handler(Looper.getMainLooper())
            h.post {
                val alertDialog = AlertDialog.Builder(this@PlayerActivity).create()
                alertDialog.setTitle(getString(R.string.error))
                alertDialog.setMessage(getString(R.string.license_inavlid))
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok)) { dialog, which ->
                    dialog.dismiss()
                    finish()
                }
                alertDialog.show()
            }
        }

        if (event.name == R5ConnectionEvent.STOP_STREAMING.name) {
            // finish()
        }

        if (event.name == "START_STREAMING") {
            runOnUiThread {
                connect.text = getString(R.string.hint_connected)
                connect.visibility = View.GONE

                cl_top.visibility = View.VISIBLE
                cl_bottom.visibility = View.VISIBLE
                mTimerView.visibility = View.VISIBLE
                mTimerView.startTimer()
                isPlaying = true
                if (liveStoryModel!!.currentProduct != null) {
                    im_item_info.visibility = View.VISIBLE
                } else {
                    getCurrentProduct()
                }
            }
        }
    }

    private val socketEventsListener = object : SocketEventsListener() {
        override fun onConnected(vararg args: Any?) {

        }

        override fun onDisconnected(vararg args: Any?) {

        }

        override fun onStartStream(args: String?) {
            Log.e(TAG, "onStartStream  $args")
        }

        override fun onUpdateProduct(args: String?) {
            Log.e(TAG, "onUpdateProduct  $args")
            val jsonObject = JSONObject(args)
            StremDataParse().parseStream(liveStoryModel!!, jsonObject)

            if (!liveStoryModel!!.status.equals("stopped",true)) {
                if (liveStoryModel!!.currentProduct != null) {
                    currentProduct = liveStoryModel!!.currentProduct
                    Log.e(TAG, "onUpdateProduct  ${liveStoryModel!!.currentProduct!!.url}")

                    if (isPlaying) {
                        im_item_info.visibility = View.VISIBLE
                    }

                }

                runOnUiThread {
                    if (customDialog != null) {
                        customDialog!!.dismiss()
                    }
                    /*commentList.clear()
                    mapApproveComment.clear()
                    mapComment.clear()
                    notifyComments()*/
                }
            }
        }

        override fun onUpdateStream(args: String?) {
            Log.e("PlayerActivity", "onUpdateStream ${Constant.CURRENT_STREAM_ID.equals(liveStoryModel!!.id)} $args")
            val holdSize = mapComment.size
            val jsonObject = JSONObject(args)

            //Log.e(TAG, "CHECK_IDD $holdSize  ${mapComment.size} ${liveStoryModel!!.viewerCounts} ${liveStoryModel!!.status}")
            StremDataParse().parseStream(liveStoryModel!!, jsonObject)
            if (liveStoryModel!!.status.equals("stopped")) {
                showStopDialog("")
            } else {
                doAsync {
                    var flag = false
                    var keys = ""

                    val keysList = ArrayList<String>()
                    keysList.addAll(mapComment.keys)

                    for (i in 0 until keysList.size) {
                        keys = keysList[i]
                        flag = mapComment.get(keys)!!.status.equals("approved", true)

                        if (flag) {
                            val commentAp = mapComment.get(keys)!!

                            if (!mapApproveComment.containsKey(commentAp.id)) {
                                mapApproveComment.put(commentAp.id, commentAp)
                                showBidAwarededDialog(commentAp)

                                break
                            }
                        }
                    }

                    runOnUiThread {

                        if (liveStoryModel!!.currentProduct != null) {
                            currentProduct = liveStoryModel!!.currentProduct
                            //  Log.e(TAG, "currentProduct  ${currentProduct!!.description}  ${currentProduct!!.url}")
                            if (isPlaying) {
                                im_item_info.visibility = View.VISIBLE
                            }

                        }
                        if (holdSize < mapComment.size && liveStoryModel!!.commentsList.size > 0) {
                            commentList.add(liveStoryModel!!.commentsList[liveStoryModel!!.commentsList.size - 1])
                            notifyComments()
                        }
                        saveUserData()
                    }
                }

            }
        }

    }

    //{"error":false,"message":"Done","stream":{"_id":"5c4192586e74ab2ee58dc929","user_id":"5c06183c71bbd11bde6d6fc1","stream_name":"shee_jinping_1547801173352","status":"running","viewerCount":0,"country":[{"country_id":"5c0110d1a942fb3bee0dd6e9"}],"start_price":"350","comment":[],"created_at":"Fri Jan 18 2019 14:16:16 GMT+0530 (IST)","__v":0,"thumbUrl":"https:\/\/mazadpostimages.s3.amazonaws.com\/streamimages\/IMG_1547801176837.jpg","current_product":{"stream_id":"5c4192586e74ab2ee58dc929","image_url":"https:\/\/mazadpostimages.s3.amazonaws.com\/streamimages\/IMG_1547801176355.jpg","product_serial":"Here is the product Serial","price":"350","payment_type":"CASH","is_sold":false,"description":"Here is the product Description"},"userDetails":[{"_id":"5c06183c71bbd11bde6d6fc1","username":"shee_jinping","country_code":"+973","country":{"_id":"5c0110d1a942fb3bee0dd6e9","countryName":"Bahrain","countryCode":"+973","flagImageUrl":"\/images\/bahrain.png","createdAt":"Fri Nov 30 2018 15:58:33 GMT+0530 (IST)","__v":0},"phone":"7891201189","email":"nk@gmail.com","password":"password","type":"seller","profilePhoto":"https:\/\/s3-ap-southeast-1.amazonaws.com\/datapost\/image\/IMG_1546087040841.jpg","__v":0,"followers":[{"_id":"5c11f51e5935ac2141033d33","user_id":"5c0619c93bf6c11c4f1b60e6","created_at":"Thu Dec 13 2018 11:28:54 GMT+0530 (IST)"},{"_id":"5c14f3b07976ee46a34638c1","user_id":"5c0a5b0707be2938de836e25","created_at":"Sat Dec 15 2018 17:59:36 GMT+0530 (IST)"},{"_id":"5c24b1de21a8f238f1fbd018","user_id":"5c08c6a41bf815286450659a","created_at":"Thu Dec 27 2018 16:35:02 GMT+0530 (IST)"},{"_id":"5c27ac32bcfd58634f750649","user_id":"5c0a5ac207be2938de836e24","created_at":"Sat Dec 29 2018 22:47:38 GMT+0530 (IST)"}],"following":[{"_id":"5c133b040989d21250867f52","user_id":"5c0a5ac207be2938de836e24","created_at":"Fri Dec 14 2018 10:39:24 GMT+0530 (IST)"},{"_id":"5c13d46f4abf8951794449dc","user_id":"5c0619c93bf6c11c4f1b60e6","created_at":"Fri Dec 14 2018 21:33:59 GMT+0530 (IST)"},{"_id":"5c19de4b103db614b277fa8a","user_id":"5c0a5b0707be2938de836e25","created_at":"Wed Dec 19 2018 11:29:39 GMT+0530 (IST)"},{"_id":"5c24b1fa21a8f238f1fbd01d","user_id":"5c08c6a41bf815286450659a","created_at":"Thu Dec 27 2018 16:35:30 GMT+0530 (IST)"},{"_id":"5c25e3ebe700e92d90e07900","user_id":"5c1348dc16c41018a61a014a","created_at":"Fri Dec 28 2018 14:20:51 GMT+0530 (IST)"}],"is_online":true,"socket_id":"XUrBXV0tPHny09K2AAAD","is_live":"running","favPost":[{"_id":"5c20a34b2aee322b571e90af","post_id":"5c1de4b722ca02247cf06a74"},{"_id":"5c20a36f2aee322b571e90b1","post_id":"5c1ca837f2de95231d241728"},{"_id":"5c2770ea6c5f51522867eccb","post_id":"5c1e00024186752e78911814"},{"_id":"5c306c57deede72d3a134e56","post_id":"5c2f2031d1010732575303c3"}],"description":"","block_list":[],"playerId":"57c9fdb4-6b7b-477b-8847-dd38ec69df6d","user_type":"android","push_notification":"true","is_activity":"true","is_auction":false,"is_buyNow":true,"is_admin_block":false,"last_login":"1547799670793","posts_block_list":["5c2f6d4c2cecfe548652fcee"]}]},"comments":[],"userDetails":[{"_id":"5c06183c71bbd11bde6d6fc1","username":"shee_jinping","country_code":"+973","country":{"_id":"5c0110d1a942fb3bee0dd6e9","countryName":"Bahrain","countryCode":"+973","flagImageUrl":"\/images\/bahrain.png","createdAt":"Fri Nov 30 2018 15:58:33 GMT+0530 (IST)","__v":0},"phone":"7891201189","email":"nk@gmail.com","password":"password","type":"seller","profilePhoto":"https:\/\/s3-ap-southeast-1.amazonaws.com\/datapost\/image\/IMG_1546087040841.jpg","__v":0,"followers":[{"_id":"5c11f51e5935ac2141033d33","user_id":"5c0619c93bf6c11c4f1b60e6","created_at":"Thu Dec 13 2018 11:28:54 GMT+0530 (IST)"},{"_id":"5c14f3b07976ee46a34638c1","user_id":"5c0a5b0707be2938de836e25","created_at":"Sat Dec 15 2018 17:59:36 GMT+0530 (IST)"},{"_id":"5c24b1de21a8f238f1fbd018","user_id":"5c08c6a41bf815286450659a","created_at":"Thu Dec 27 2018 16:35:02 GMT+0530 (IST)"},{"_id":"5c27ac32bcfd58634f750649","user_id":"5c0a5ac207be2938de836e24","created_at":"Sat Dec 29 2018 22:4
//{"error":false,"message":"Done","stream":{"_id":"5c4192586e74ab2ee58dc929","user_id":"5c06183c71bbd11bde6d6fc1","stream_name":"shee_jinping_1547801173352","status":"running","viewerCount":1,"country":[{"country_id":"5c0110d1a942fb3bee0dd6e9"}],"start_price":"350","comment":[],"created_at":"Fri Jan 18 2019 14:16:16 GMT+0530 (IST)","__v":0,"thumbUrl":"https:\/\/mazadpostimages.s3.amazonaws.com\/streamimages\/IMG_1547801176837.jpg","current_product":{"stream_id":"5c4192586e74ab2ee58dc929","image_url":"https:\/\/mazadpostimages.s3.amazonaws.com\/streamimages\/IMG_1547801214332.jpg","product_serial":"Here is the product Serial","price":"350","payment_type":"CASH","is_sold":false,"description":"Here is the product Description"},"userDetails":[{"_id":"5c06183c71bbd11bde6d6fc1","username":"shee_jinping","country_code":"+973","country":{"_id":"5c0110d1a942fb3bee0dd6e9","countryName":"Bahrain","countryCode":"+973","flagImageUrl":"\/images\/bahrain.png","createdAt":"Fri Nov 30 2018 15:58:33 GMT+0530 (IST)","__v":0},"phone":"7891201189","email":"nk@gmail.com","password":"password","type":"seller","profilePhoto":"https:\/\/s3-ap-southeast-1.amazonaws.com\/datapost\/image\/IMG_1546087040841.jpg","__v":0,"followers":[{"_id":"5c11f51e5935ac2141033d33","user_id":"5c0619c93bf6c11c4f1b60e6","created_at":"Thu Dec 13 2018 11:28:54 GMT+0530 (IST)"},{"_id":"5c14f3b07976ee46a34638c1","user_id":"5c0a5b0707be2938de836e25","created_at":"Sat Dec 15 2018 17:59:36 GMT+0530 (IST)"},{"_id":"5c24b1de21a8f238f1fbd018","user_id":"5c08c6a41bf815286450659a","created_at":"Thu Dec 27 2018 16:35:02 GMT+0530 (IST)"},{"_id":"5c27ac32bcfd58634f750649","user_id":"5c0a5ac207be2938de836e24","created_at":"Sat Dec 29 2018 22:47:38 GMT+0530 (IST)"}],"following":[{"_id":"5c133b040989d21250867f52","user_id":"5c0a5ac207be2938de836e24","created_at":"Fri Dec 14 2018 10:39:24 GMT+0530 (IST)"},{"_id":"5c13d46f4abf8951794449dc","user_id":"5c0619c93bf6c11c4f1b60e6","created_at":"Fri Dec 14 2018 21:33:59 GMT+0530 (IST)"},{"_id":"5c19de4b103db614b277fa8a","user_id":"5c0a5b0707be2938de836e25","created_at":"Wed Dec 19 2018 11:29:39 GMT+0530 (IST)"},{"_id":"5c24b1fa21a8f238f1fbd01d","user_id":"5c08c6a41bf815286450659a","created_at":"Thu Dec 27 2018 16:35:30 GMT+0530 (IST)"},{"_id":"5c25e3ebe700e92d90e07900","user_id":"5c1348dc16c41018a61a014a","created_at":"Fri Dec 28 2018 14:20:51 GMT+0530 (IST)"}],"is_online":true,"socket_id":"XUrBXV0tPHny09K2AAAD","is_live":"running","favPost":[{"_id":"5c20a34b2aee322b571e90af","post_id":"5c1de4b722ca02247cf06a74"},{"_id":"5c20a36f2aee322b571e90b1","post_id":"5c1ca837f2de95231d241728"},{"_id":"5c2770ea6c5f51522867eccb","post_id":"5c1e00024186752e78911814"},{"_id":"5c306c57deede72d3a134e56","post_id":"5c2f2031d1010732575303c3"}],"description":"","block_list":[],"playerId":"57c9fdb4-6b7b-477b-8847-dd38ec69df6d","user_type":"android","push_notification":"true","is_activity":"true","is_auction":false,"is_buyNow":true,"is_admin_block":false,"last_login":"1547799670793","posts_block_list":["5c2f6d4c2cecfe548652fcee"]}]},"comments":[],"userDetails":[{"_id":"5c06183c71bbd11bde6d6fc1","username":"shee_jinping","country_code":"+973","country":{"_id":"5c0110d1a942fb3bee0dd6e9","countryName":"Bahrain","countryCode":"+973","flagImageUrl":"\/images\/bahrain.png","createdAt":"Fri Nov 30 2018 15:58:33 GMT+0530 (IST)","__v":0},"phone":"7891201189","email":"nk@gmail.com","password":"password","type":"seller","profilePhoto":"https:\/\/s3-ap-southeast-1.amazonaws.com\/datapost\/image\/IMG_1546087040841.jpg","__v":0,"followers":[{"_id":"5c11f51e5935ac2141033d33","user_id":"5c0619c93bf6c11c4f1b60e6","created_at":"Thu Dec 13 2018 11:28:54 GMT+0530 (IST)"},{"_id":"5c14f3b07976ee46a34638c1","user_id":"5c0a5b0707be2938de836e25","created_at":"Sat Dec 15 2018 17:59:36 GMT+0530 (IST)"},{"_id":"5c24b1de21a8f238f1fbd018","user_id":"5c08c6a41bf815286450659a","created_at":"Thu Dec 27 2018 16:35:02 GMT+0530 (IST)"},{"_id":"5c27ac32bcfd58634f750649","user_id":"5c0a5ac207be2938de836e24","created_at":"Sat Dec 29 2018 22:4


    var mapApproveComment = HashMap<String, CommentModel>()
    protected var subscribe: R5Stream? = null

    private var userRef: UserPreferences? = null
    private var isPlaying = false
    private var minAmount = 1.toDouble()
    private var liveStoryModel: LiveStoryModel? = null
    private var socketManager: SocketManager? = null
    private var commentList = ArrayList<CommentModel>()

    private var joinTime = System.currentTimeMillis()
    private var currentProduct: LiveProductModel? = null



    private var onLineHandler = Handler()

    private val runable = object : Runnable {
        override fun run() {
            Constant.setOnLine(true)
            onLineHandler.postDelayed(this, 50000)
        }
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        Constant.SHOW_ADD = false


        super.onResume()
    }

    override fun onStop() {
        if (subscribe != null) {
            subscribe!!.stop()
        }

        super.onStop()
    }

    override fun onStart() {
        socketManager!!.setSocketEventsListener(false, socketEventsListener)
        Constant.CURRENT_STREAM_ID = liveStoryModel!!.id
        Log.e(TAG,"CHECK_FOR_ID_222 *** ${Constant.CURRENT_STREAM_ID} ${liveStoryModel!!.id}")
        super.onStart()
    }

    override fun onBackPressed() {
        if (isPlaying) {
            Constant.SHOW_ADD = true
            if (subscribe != null) {
                subscribe!!.stop()
                subscribe!!.removeListener()
            }
            joinStream(false)
        }
        Constant.CURRENT_STREAM_ID = ""
        onLineHandler.removeCallbacks(runable)
        socketManager!!.setSocketEventsListener(false, null)
        super.onBackPressed()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_player)
        supportActionBar!!.hide()
        joinTime = System.currentTimeMillis()
        progress = CustomProgress(this)
        mapComment = HashMap()
        userRef = MyApplication.instance.getUserPreferences()
        liveStoryModel = intent.getParcelableExtra("stream")

        Constant.CURRENT_STREAM_ID = liveStoryModel!!.id
      ///  Log.e(TAG,"CHECK_FOR_ID_111 *** ${Constant.CURRENT_STREAM_ID} ${liveStoryModel!!.id}")

        commentview.layoutManager = LinearLayoutManager(this)
        commentview.adapter = liveCommentAdapter(this, commentList)

        socketManager = MyApplication.instance.getSocketManager()


        if (liveStoryModel == null) {
            Toast.makeText(this, getString(R.string.unable_to_play_video), Toast.LENGTH_SHORT).show()
            finish()
        }

        publishButton.visibility = View.GONE

        mTimerView.setVisibility(View.GONE)
        cl_top.visibility = View.GONE
        cl_bottom.visibility = View.GONE

        volume.setTag(false)
        volume.setVisibility(View.GONE)

        /*   et_comment.addTextChangedListener(object : TextWatcher {
               override fun afterTextChanged(s: Editable?) {
                   if (TextUtils.isEmpty(et_comment.text.trim())) {
                       im_bid.visibility = View.VISIBLE
                       im_send.visibility = View.INVISIBLE
                       im_bid.isEnabled = true
                   } else {
                       im_bid.visibility = View.INVISIBLE
                       im_bid.isEnabled = false
                       im_send.visibility = View.VISIBLE
                   }
               }

               override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

               }

               override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

               }

           })

   */

        close.setOnClickListener {
            onBackPressed()
        }

        im_bid.tag = false

        im_bid.setOnClickListener {
            getHeighestBid()
        }

        im_send.setOnClickListener {
            if (!TextUtils.isEmpty(et_comment.text.toString())) {
                val msg = et_comment.text.toString().trim()
                sendComment(msg, "comment")

            } else {
                Toast.makeText(this, getString(R.string.enter_comment), Toast.LENGTH_SHORT).show()
            }
        }
        /*view_ext.setOnClickListener {
            if(cl_top.visibility == View.VISIBLE){
                hideTopLayout()
            }else{
                showTopLayout()
            }
        }*/

        im_item_info.setOnClickListener {
            doAsync {
                uiThread {
                    if (liveStoryModel!!.currentProduct != null) {
                        Log.e(TAG, "CURRENT_PRODUCT_URL ${liveStoryModel!!.currentProduct!!.url}")
                        val list = ArrayList<LiveProductModel>()
                        list.add(liveStoryModel!!.currentProduct!!)
                        CustomDialog(this@PlayerActivity).showItemInfoDialog(list, 0, object : DialogListener() {
                            override fun okClick() {

                            }

                        })
                    }
                }
            }
        }
        saveUserData()

        if (liveStoryModel!!.userData != null) {
            user_name.setText(liveStoryModel!!.userData!!.username)
            Glide.with(this).load(liveStoryModel!!.userData!!.profile).apply(RequestOptions().override(80).error(R.drawable.dummy)).into(image)
        }

        onLineHandler.post(runable)

        checkForUserCountry()

        if (TextUtils.isEmpty(liveStoryModel!!.thumbUrl)) {
            Glide.with(this).load(liveStoryModel!!.thumbUrl).apply(RequestOptions().override(100).error(R.drawable.splash)).into(back_image)
        }
    }

    private fun checkForUserCountry() {
        Log.e(TAG, "Country  ${liveStoryModel!!.countryList.size}  ${liveStoryModel!!.countries}")
        doAsync {
            var flag = false
            for (i in 0 until liveStoryModel!!.countryList.size) {
                val id = liveStoryModel!!.countryList[i].id
                Log.e(TAG, "Country $id *** ${userRef!!.countryId}")
                if (userRef!!.countryId.equals(id)) {
                    flag = true
                    break
                }
            }

            uiThread {
                if (flag) {

                    Constant.setOnLine(true)
                    Log.e(TAG, "$liveStoryModel *************LLLLLL")
                    joinStream(true)

                    try {
                        //minAmount = liveStoryModel!!.price.toDouble()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    playStream()

                } else {
                    val text = getString(R.string.stream_dnt_avalable)
                    CustomDialog(this@PlayerActivity).showAlertDialog("Alert", text, object : DialogListener() {
                        override fun okClick() {
                            joinStream(false)
                            finish()
                        }
                    })
                }
            }

        }
    }

    private fun joinStream(flag: Boolean) {
        if (!socketManager!!.isConnected) {
            socketManager!!.socket!!.close()
            socketManager!!.socket!!.open()
            socketManager!!.connect()
        }

        if (TextUtils.isEmpty(liveStoryModel!!.id) || TextUtils.isEmpty(liveStoryModel!!.streamName)) {
            return
        } else {
            val jsonJoin = JSONObject()
            jsonJoin.put("stream_name", liveStoryModel!!.streamName)
            jsonJoin.put("stream_id", liveStoryModel!!.id)
            jsonJoin.put("is_join", flag)
            jsonJoin.put("user_id", MyApplication.instance.getUserPreferences().id)

            Log.e(TAG, "JOIN_STREAM $flag")
            socketManager!!.socket!!.emit(SocketManager.EMIT_JOIN_LEAVE_STREAM, jsonJoin)

            if (flag) {
                Handler().postDelayed(object : Runnable {
                    override fun run() {
                        sendComment("Joined", "comment")
                    }

                }, 1000)
            }
        }

    }

    private fun saveUserData() {
        if (liveStoryModel!!.viewerCounts < 0) {
            count_viewer.setText("0")
        } else {
            count_viewer.setText("${liveStoryModel!!.viewerCounts}")
        }

    }

    private fun sendComment(message: String, type: String) {

        val jsonObject = JSONObject()
        jsonObject.put("user_id", userRef!!.id)
        jsonObject.put("type", type)
        jsonObject.put("text", message)
        jsonObject.put("stream_id", liveStoryModel!!.id)

        if (!socketManager!!.isConnected) {
            socketManager!!.socket!!.open()
            socketManager!!.connect()
        }

        et_comment.setText("")
        notifyComments()

        Log.e(TAG, "** ${jsonObject.toString()}")
        socketManager!!.socket!!.emit(SocketManager.EMIT_ADD_COMMENT, jsonObject)
    }

    private fun notifyComments() {
        runOnUiThread {

            commentview.adapter.notifyItemChanged(commentList.size - 1)
            commentview.scrollToPosition(commentList.size - 1)
        }
    }

    private fun hideTopLayout() {
        val anim = AnimationUtils.loadAnimation(this, R.anim.slide_bottom_to_top)
        val anim2 = AnimationUtils.loadAnimation(this, R.anim.abc_slide_out_bottom)
        cl_top.startAnimation(anim)
        cl_bottom.startAnimation(anim2)

        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                cl_top.visibility = View.GONE

            }

            override fun onAnimationStart(animation: Animation?) {

            }

        })

        anim2.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                //cl_top.visibility = View.GONE
                if (cl_bottom.visibility == View.VISIBLE) {
                    cl_bottom.visibility = View.GONE
                }
            }

            override fun onAnimationStart(animation: Animation?) {

            }

        })
    }

    private fun showTopLayout() {
        val anim = AnimationUtils.loadAnimation(this, R.anim.abc_slide_in_top)
        val anim2 = AnimationUtils.loadAnimation(this, R.anim.abc_slide_in_bottom)
        cl_top.startAnimation(anim)
        cl_bottom.startAnimation(anim2)
        cl_top.visibility = View.VISIBLE
        cl_bottom.visibility = View.VISIBLE

    }

    private fun showStopDialog(text: String) {
        runOnUiThread {
            shopShow = true
            CustomDialog(this).showLiveStreamEndDialog(text, object : DialogListener() {
                override fun okClick() {
                    shopShow = false
                    finish()
                }
            })
        }

    }

    private fun showBidAwarededDialog(commentModel: CommentModel) {

        runOnUiThread {
            val flag = false
            val nextText = getString(R.string.text_product_sold)

            if(customDialog == null){
                customDialog = Dialog(this)
                customDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
                customDialog!!.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                customDialog!!.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                customDialog!!.setContentView(R.layout.customdialog_bid_awared_win)
                customDialog!!.setCanceledOnTouchOutside(false)
                customDialog!!.setCancelable(false)


                customDialog!!.bid_awarded_win_des.text = nextText

                val anim = AnimationUtils.loadAnimation(this, R.anim.scale)
                val anim2 = AnimationUtils.loadAnimation(this, R.anim.scale_done)

                customDialog!!.bid_awarded_win_cir_image.startAnimation(anim)
                customDialog!!.bid_awarded_win_tic.startAnimation(anim2)

                customDialog!!.bid_awarded_win_close.setBackgroundResource(R.drawable.feedview_btn_background)

                customDialog!!.bid_awarded_win_close.setOnClickListener {
                    customDialog!!.dismiss()
                    onBackPressed()
                }
                customDialog!!.bid_awarded_win_des.text = nextText
                customDialog!!.show()
            }else{
                customDialog!!.bid_awarded_win_des.text = nextText
                customDialog!!.show()
            }


        }

    }

    private fun bidDialog() {

        Log.e(TAG, "MIN_AMOUNT $minAmount")
        /*CustomDialog(this).showBidialog(minAmount, object : DialogListener() {
            override fun okClick(any: Any) {
                sendComment(any.toString(), "amount")
            }
        })*/
//{"version":"1.1","stream_id":"5c41919b6e74ab2ee58dc91b","start_price":"350","user_id":"5c06183c71bbd11bde6d6fc1","image_url":"https:\/\/mazadpostimages.s3.amazonaws.com\/streamimages\/IMG_1547800986754.jpg","description":"Here is the product Description","product_serial":"Here is the product Serial","payment_type":"CASH","is_sold":false}

        val bidDialog = Dialog(this)
        bidDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        bidDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        bidDialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        bidDialog.setContentView(R.layout.customdialog_makeabid)
        bidDialog.setCancelable(true)

        bidDialog.mBidMinAmount.setText("${ getString(R.string.hint_min_bid_amount)} (KD${minAmount})")
        bidDialog.mFlagMakeaBid.setImageResource(resources.getIdentifier("drawable/flag_" + MyApplication.instance.getUserPreferences().CountryFlag, null, packageName))
        bidDialog.mBidCountry.text = MyApplication.instance.getUserPreferences().CountryName

        bidDialog.mMakeABid.setOnClickListener {

            try {
                val tempamount = bidDialog.mBidAmout.text.toString().trim().toDouble()
                if(tempamount <= 0){
                    bidDialog.mBidAmout.error = "Please Enter Correct Bid Amount"
                    return@setOnClickListener
                }
                if (tempamount > minAmount) {
                    sendComment(DecimalFormat("##.##").format(tempamount), "amount")
                    bidDialog.dismiss()
                }else{
                    bidDialog.mBidAmout.error = "Please Enter more Bid Amount"
                    return@setOnClickListener
                }

            }catch (e:Exception){
                e.printStackTrace()
                bidDialog.mBidAmout.error = "Enter valid amount"
            }
        }

        bidDialog.show()
    }

    private fun playStream() {
        val config = MyApplication.instance.getRedConfig()
        config.setLicenseKey(Constant.LICENSE_KEY)
        config.setBundleID(packageName)

        val connection = R5Connection(config)

        subscribe = R5Stream(connection)
        subscribe!!.client = this@PlayerActivity
        subscribe!!.setListener(this@PlayerActivity)


        //show all logging
        subscribe!!.setLogLevel(R5Stream.LOG_LEVEL_DEBUG)

        videoView.setZOrderOnTop(true);
        videoView.attachStream(subscribe)

        videoView.showDebugView(false)

        Log.e("liveStoryModel"," *** "+liveStoryModel!!.streamName)
        subscribe!!.play(liveStoryModel!!.streamName)

        /*if (isPlaying) {
            isPlaying = false
            publishButton.setImageResource(R.drawable.round_red)
            mTimerView.visibility = View.GONE
            mTimerView.stopTimer()
            subscribe!!.stop()

            volume.visibility = View.GONE
        } else {
            publishButton.setImageResource(R.drawable.ic_pause_red)
            isPlaying = true

            //volume.setVisibility(View.VISIBLE);
        }*/
    }

    private fun getHeighestBid() { ////type: stream / post / story, stream_id: streamId, story_id: storyId, post_id: postId .
        val param = JSONObject() // : bid_user_id, bid_amount: bid_amount.
        param.put("version", "1.1")

        param.put("stream_id", liveStoryModel!!.id)
        param.put("type", "stream")


        progress!!.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                val js = JSONObject(`object`.toString())//amount
                if (js.has("description")) {
                    val json = js.getJSONObject("description")
                    if (json.has("bid_amount")) {
                        try {
                            minAmount = json.get("bid_amount").toString().toDouble()

                        } catch (e: Exception) {

                        } finally {
                            bidDialog()
                        }
                    } else {
                        if (json.has("amount")) {
                            try {
                                minAmount = json.get("amount").toString().toDouble()

                            } catch (e: Exception) {

                            } finally {
                                bidDialog()
                            }
                        } else {
                            bidDialog()
                        }
                    }


                } else {
                    bidDialog()
                }
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                bidDialog()

            }

        }).getHighestBid(param)

    }

    private fun getCurrentProduct() {
        val param = JSONObject() // : bid_user_id, bid_amount: bid_amount.
        param.put("version", "1.1")
        param.put("stream_id", liveStoryModel!!.id)

        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                val jsonObject = JSONObject(`object`.toString())
                if (jsonObject.has("description")) {
                    val json = jsonObject.getJSONObject("description")
                    val model = StremDataParse().parseStream2(json)
                    Log.e(TAG, "CHECK_CURRENT_PRO ${model.currentProduct}")
                    runOnUiThread {
                        if (model.currentProduct != null) {
                            liveStoryModel!!.currentProduct = model.currentProduct
                            currentProduct = model.currentProduct
                            if (isPlaying) {
                                im_item_info.visibility = View.VISIBLE
                            }
                        }
                    }
                }
            }

            override fun onError(errorMessage: String) {

                progress.dismiss()
                val text = getString(R.string.no_product_available)

            }

        }).getCurrentProduct(param)
    }

}
