package com.mazadlive.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.view.Window
import com.mazadlive.R
import com.mazadlive.Socket.SocketEventsListener
import com.mazadlive.adapters.BlockUserAdapter
import com.mazadlive.adapters.ChatDialogAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.database.AppDatabase
import com.mazadlive.database.RoomData
import com.mazadlive.database.RoomUserData
import com.mazadlive.helper.Constant
import com.mazadlive.models.UserModel
import com.mazadlive.parser.MessageParser
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.activity_chat_list.*
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.util.*

class ChatListActivity : BaseActivity() {

    val TAG = "ChatListActivity"
    val roomList = ArrayList<RoomData>()
    val mapMsg = HashMap<String, RoomUserData>()
    private lateinit var progress: CustomProgress

    var clickPosition = -1
    val socketEventsListener = object : SocketEventsListener() {
        override fun onConnected(vararg args: Any?) {
        }

        override fun onDisconnected(vararg args: Any?) {
        }

        override fun onMessageReceived(args: String?) {
            val jsonObject = JSONObject(args)

            val msg = MessageParser().parse(jsonObject)
            Log.e(TAG, "MSG_ROOM ${msg.room_id}")
            if (!mapMsg.containsKey(msg.id)) {
                doAsync {
                    for (i in 0 until roomList.size) {
                        Log.e(TAG, "ROOM______ ${msg.room_id}  ${roomList[i].id}")
                        if (msg.room_id.equals(roomList[i].id)) {
                            roomList[i].messageData = msg
                            Collections.sort(roomList, SortList())
                            runOnUiThread { recycleView.adapter.notifyDataSetChanged() }
                            break
                        }
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_chat_list)
        supportActionBar?.hide()

        progress = CustomProgress(this)

        recycleView.layoutManager = LinearLayoutManager(this@ChatListActivity)
        recycleView.adapter = ChatDialogAdapter(this, roomList, OnItemClick())

        back.setOnClickListener { onBackPressed() }

        val isNoti = intent.getBooleanExtra(Constant.NOTOFICATION_KEY,false)
        val roomId = intent.getStringExtra(Constant.ROOM_KEY)

        if(isNoti){
            val bundle = intent.extras
            Log.e("EXTRA","BB $bundle  ")
            val intent = Intent(this@ChatListActivity, ChatActivity::class.java)
            intent.putExtra("userId", bundle.getString("user_id"))
            startActivity(intent)
        }

        getRoomsFromLocal()

      //  getMyChatRooms()
    }

    override fun onResume() {
        Constant.IS_CHAT_ACTIVE =true

        if(clickPosition >= 0){

            doAsync {
                val rm = AppDatabase.getAppDatabase().roomDao().getRoom(roomList[clickPosition].id)
                roomList[clickPosition].messageData = rm.messageData
                runOnUiThread {
                    recycleView.adapter.notifyDataSetChanged()
                }
            }
        }


        //  EventBus.getDefault().register(this)
        MyApplication.instance.getSocketManager()!!.setSocketEventsListener(false, socketEventsListener)
        super.onResume()
    }

    override fun onPause() {
        Constant.IS_CHAT_ACTIVE =false
        //  EventBus.getDefault().unregister(this)
        MyApplication.instance.getSocketManager()!!.setSocketEventsListener(false, null)
        super.onPause()
    }


    private fun getRoomsFromLocal() {
        doAsync {
            roomList.clear()
            val temp = AppDatabase.getAppDatabase().roomDao().getAllRooms()
            Log.e(TAG, " ALL_ROOMS ${temp.size}")

            if (temp.isNotEmpty()){
                roomList.addAll(temp)
                Collections.sort(roomList, SortList())
                runOnUiThread {
                    recycleView.adapter.notifyDataSetChanged()
                }
            }else{
                mNoMessage.visibility = View.VISIBLE
                recycleView.visibility = View.GONE
            }

        }
    }


    private fun getMyChatRooms() {

        val param = JSONObject()
        param.put("user_id", MyApplication.instance.getUserPreferences().id)
        param.put("room_id", "")
        param.put("version", "1.1")

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                getRoomsFromLocal()
            }

            override fun onError(errorMessage: String) {
            }

        }).getRooms(param)
    }

    inner class OnItemClick : ChatDialogAdapter.OnItemClick {

        override fun onClick(position: Int) {
            clickPosition = position
            val user = roomList[position].userList!![0]
            val intent = Intent(this@ChatListActivity, ChatActivity::class.java)
            intent.putExtra("userId", user.user_id)
            startActivity(intent)

        }

        override fun onSelect(position: Int) {

        }

    }

    inner class SortList : Comparator<RoomData> {
        override fun compare(o1: RoomData?, o2: RoomData?): Int {
            if (o1!!.messageData == null && o2!!.messageData == null) {

                return 0
            } else {
                if (o1!!.messageData == null) {
                    return 1
                } else {
                    if (o2!!.messageData == null) {
                        return -1
                    } else {
                        return (o2!!.messageData!!.created_at - o1!!.messageData!!.created_at).toInt()
                    }
                }
            }

        }

    }
}
