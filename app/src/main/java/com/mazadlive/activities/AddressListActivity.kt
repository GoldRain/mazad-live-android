package com.mazadlive.activities

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import com.mazadlive.R
import com.mazadlive.adapters.AddressAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.helper.Constant
import com.mazadlive.helper.GridSpacingItemDecoration
import com.mazadlive.models.AddressModel
import com.mazadlive.parser.AddressParser
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.activity_address_list.*
import org.json.JSONObject

class AddressListActivity : BaseActivity() {


    private lateinit var progress: CustomProgress


    private  var list =  ArrayList<AddressModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_address_list)
        supportActionBar?.hide()

        progress = CustomProgress(this)
        list = ArrayList<AddressModel>()

        recycleView.layoutManager = GridLayoutManager(this@AddressListActivity,1)
        recycleView.addItemDecoration(GridSpacingItemDecoration(1, 24, false))

        recycleView.adapter = AddressAdapter(this,list,OnItemClick())
        recycleView.adapter.notifyDataSetChanged()

        add.setOnClickListener {
            val intent = Intent(this,NewAddressActivity::class.java)
            startActivityForResult(intent,111)
        }
        getAddress()

    }

    override fun onResume() {

        super.onResume()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==111 && resultCode == Activity.RESULT_OK){
            getAddress()
        }
    }

    private fun getAddress() {
        list.clear()
        val param = JSONObject()
        param.put("version","1.1")
        param.put("user_id", MyApplication.instance.getUserPreferences().id)
        param.put("type","read")
        param.put("is_active",true)

        progress.show()
        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                progress.dismiss()

                AddressParser().parseList(JSONObject(`object`.toString()),list)

                runOnUiThread {
                    recycleView.adapter.notifyDataSetChanged()
                }
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
            }

        }).addAddress(param)
    }


    inner class OnItemClick:AddressAdapter.OnItemClick{
        override fun onClick(position: Int) {
           val intent = Intent()
            intent.putExtra("address",list[position])
            setResult(Activity.RESULT_OK,intent)
            finish()
        }

    }
}
