package com.mazadlive.activities.settings

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.widget.ImageView
import android.widget.Toast
import com.mazadlive.R
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.activity_privacy_policy.*
import org.json.JSONObject

class PrivacyPolicyActivity : AppCompatActivity() {

    val userRef = MyApplication.instance.getUserPreferences()

    lateinit var progress:CustomProgress
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_privacy_policy)
        supportActionBar!!.hide()

        progress =CustomProgress(this)
        mActivityStatus.tag = userRef.activityStatus
        mSaveLoginInfo.tag = userRef.isLoginInfoSave

        saveData()

        mActivityStatus.setOnClickListener {

            changeActivityStatus()

        }

        mSaveLoginInfo.setOnClickListener {
            if (mSaveLoginInfo.tag as Boolean) {
                mSaveLoginInfo.setImageResource(R.drawable.ic_switch_off)
                mSaveLoginInfo.tag = false
            } else {
                mSaveLoginInfo.setImageResource(R.drawable.ic_switch_on)
                mSaveLoginInfo.tag = true
            }

            userRef.loginStatus = (mSaveLoginInfo.tag as Boolean)
        }

        back.setOnClickListener { onBackPressed() }

        mBlock.setOnClickListener { startActivity(Intent(this@PrivacyPolicyActivity,BlockAccountsActivity::class.java)) }


    }

    private fun saveData() {
        if (mSaveLoginInfo.tag as Boolean) {
            mSaveLoginInfo.setImageResource(R.drawable.ic_switch_on)
            mSaveLoginInfo.tag = true

        } else {
            mSaveLoginInfo.setImageResource(R.drawable.ic_switch_off)
            mSaveLoginInfo.tag = false
        }

        if (mActivityStatus.tag as Boolean) {
            mActivityStatus.setImageResource(R.drawable.ic_switch_on)
            mActivityStatus.tag = true

        } else {
            mActivityStatus.setImageResource(R.drawable.ic_switch_off)
            mActivityStatus.tag = false
        }
    }


    fun changeActivityStatus(){
        val param = JSONObject()
        param.put("version", "1.1")
        param.put("user_id", MyApplication.instance.getUserPreferences().id)
        param.put("status", !(mActivityStatus.tag as Boolean))

        progress.dismiss()

        ServiceRequest(object :ApiResponseListener{

            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                if (mActivityStatus.tag as Boolean) {
                    mActivityStatus.setImageResource(R.drawable.ic_switch_off)
                    mActivityStatus.tag = false
                } else {
                    mActivityStatus.setImageResource(R.drawable.ic_switch_on)
                    mActivityStatus.tag = true
                }

                userRef.activityStatus = (mActivityStatus.tag as Boolean)
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                Toast.makeText(this@PrivacyPolicyActivity,"Failed $errorMessage",Toast.LENGTH_SHORT).show()
            }

        }).updateActivityStatus(param)
    }
}
