package com.mazadlive.activities.settings

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Toast
import com.mazadlive.R
import com.mazadlive.activities.BaseActivity
import com.mazadlive.adapters.FollowUserAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ApiUrl
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.models.UserModel
import com.mazadlive.parser.UserParser
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.activity_follow.*
import org.json.JSONObject


class FollowActivity : BaseActivity() {


    private lateinit var progress: CustomProgress

    private val userList = ArrayList<UserModel>()
    private var type = "follower"
    private var userModel: UserModel? = null
    private var isMe: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_follow)
        supportActionBar?.hide()


        type = intent.getStringExtra("type")

        isMe = intent.getBooleanExtra("isMe", true)
        progress = CustomProgress(this)


        if (!isMe) {
            userModel = intent.getParcelableExtra("userModel")
        } else {
            userModel = UserModel()
        }

        when(type){
            "following" ->{
                header_text.text = "Following"
            }

            "Followers" ->{
                header_text.text = "Followers"
            }
            else ->{
                header_text.setText("Follow People")
            }
        }


        recycleView.layoutManager = LinearLayoutManager(this@FollowActivity)
        recycleView.adapter = FollowUserAdapter(this, userList, type, OnItemClick())
        recycleView.adapter.notifyDataSetChanged()

        back.setOnClickListener { onBackPressed() }

        if(intent.getBooleanExtra("showAll", false)) {

            getAllUsers()
        }else{
            getFollowers()
        }
    }

    private fun getAllUsers(){

    //    fragmentView.progress_bar.visibility = View.VISIBLE

        val param = JSONObject()
        param.put("version","1.1")
        param.put("text","")
        param.put("user_id",MyApplication.instance.getUserPreferences().id)
        userList.clear()
        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                // fragmentView.progress_bar.visibility = View.GONE

                val jsonObject = JSONObject(`object`.toString())
                if(jsonObject.has("description")){

                    val array = jsonObject.getJSONArray("description")

                    val parser = UserParser()
                    for (i in 0 until array.length()){
                        val obj = array.getJSONObject(i)

                        val userModel = parser.parse(obj)
                        Log.e("CHECK_FOLLOWER","${userModel.follower.size} *** ")
                        if (obj.has("followers")) {
                            val userModel2 = UserModel()
                            val array2 = obj.getJSONArray("followers")

                            for (j in 0 until array2.length()) {
                                val follower = array2.getJSONObject(j)

                                if (follower.has("user_id")) {
                                    val key_id = follower.getString("user_id")
                                    if (MyApplication.instance.getUserPreferences().id.equals(key_id)) {
                                        (recycleView.adapter as FollowUserAdapter).map.put(userModel.id, userModel2)
                                        break
                                    }
                                }

                                /*if (follower.has("_id")) {
                                    val key_id = follower.getString("_id")
                                    if (MyApplication.instance.getUserPreferences().id.equals(key_id)) {
                                        (recycleView.adapter as FollowUserAdapter).map.put(userModel.id, userModel2)
                                        break
                                    }
                                }*/

                            }
                        }

                        if(! MyApplication.instance.getUserPreferences().id.equals(userModel.id))
                            userList.add(userModel)
                    }

                    runOnUiThread {

                        if (userList.size <= 0) {
                            tx_empty.visibility = View.VISIBLE
                        } else {
                            tx_empty.visibility = View.GONE
                        }
                        Log.e("USER_LIST", "*** ${userList.size}")
                        recycleView.adapter.notifyDataSetChanged()
                    }

                }
            }

            override fun onError(errorMessage: String) {
             //   fragmentView.progress_bar.visibility = View.GONE
                Toast.makeText(this@FollowActivity!!, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).searchUser(param)
    }


    private fun getFollowers() {

        var url = ApiUrl.follower
        if (type.equals("following")) {
            url = ApiUrl.following
        }
        val json = JSONObject()
        json.put("version", "1.1")
        if (isMe) {
            json.put("user_id", MyApplication.instance.getUserPreferences().id)
        } else {
            json.put("user_id", userModel!!.id)
        }

        progress.show()

        ServiceRequest(object : ApiResponseListener {

            override fun onCompleted(`object`: Any) {

                progress.dismiss()

                val jsonObject = JSONObject(`object`.toString())
                if (jsonObject.has("description")) {
                    val array = jsonObject.getJSONArray("description")
                    val userparser = UserParser()
                    for (i in 0 until array.length()) {

                        val obj = array.getJSONObject(i)

                        val userModel = userparser.parse(obj)
                        Log.e("CHECK_FOLLOWER","${userModel.follower.size} *** ")
                        if (obj.has("followers")) {
                            val userModel2 = UserModel()
                            val array2 = obj.getJSONArray("followers")

                            for (j in 0 until array2.length()) {
                                val follower = array2.getJSONObject(j)

                                if (follower.has("user_id")) {
                                    val key_id = follower.getString("user_id")
                                    if (MyApplication.instance.getUserPreferences().id.equals(key_id)) {
                                        (recycleView.adapter as FollowUserAdapter).map.put(userModel.id, userModel2)
                                        break
                                    }
                                }

                                /*if (follower.has("_id")) {
                                    val key_id = follower.getString("_id")
                                    if (MyApplication.instance.getUserPreferences().id.equals(key_id)) {
                                        (recycleView.adapter as FollowUserAdapter).map.put(userModel.id, userModel2)
                                        break
                                    }
                                }*/

                            }
                        }
                        userList.add(userModel)
                    }

                    runOnUiThread {

                        if (userList.size <= 0) {
                            tx_empty.visibility = View.VISIBLE
                        } else {
                            tx_empty.visibility = View.GONE
                        }
                        Log.e("USER_LIST", "*** ${userList.size}")
                        recycleView.adapter.notifyDataSetChanged()
                    }

                }
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                Toast.makeText(this@FollowActivity, "Failed to get follower list $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).getFollowers(json, url)
    }

    private fun followPeople(position: Int) {

        val map = JSONObject()
        map.put("his_id", userList[position].id)
        map.put("version", "1.1")
        map.put("self_id", MyApplication.instance.getUserPreferences()!!.id)

        if ((recycleView.adapter as FollowUserAdapter).map.containsKey(userList[position].id)) {
            map.put("is_follow", 0)
        } else {
            map.put("is_follow", 1)
        }


        progress!!.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()

                runOnUiThread {
                    if ((recycleView.adapter as FollowUserAdapter).map.containsKey(userList[position].id)) {
                        (recycleView.adapter as FollowUserAdapter).map.remove(userList[position].id)
                    } else {
                        (recycleView.adapter as FollowUserAdapter).map.put(userList[position].id, userList[position])
                    }

                    recycleView.adapter.notifyItemChanged(position)
                }
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(this@FollowActivity!!, "Request failed $errorMessage", Toast.LENGTH_SHORT).show()

            }

        }).followPeople(map)
    }


    inner class OnItemClick : FollowUserAdapter.OnClick {
        override fun onFollow(position: Int) {
            followPeople(position)
        }

    }
}
