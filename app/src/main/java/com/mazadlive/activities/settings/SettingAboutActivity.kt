package com.mazadlive.activities.settings

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import com.mazadlive.R
import com.mazadlive.activities.TermPrivacyActivity
import com.mazadlive.activities.WebViewActivity
import com.mazadlive.api.ApiUrl
import kotlinx.android.synthetic.main.activity_about.*

class SettingAboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_about)
        supportActionBar!!.hide()

        back.setOnClickListener { onBackPressed() }

        mDataPrivacy.setOnClickListener {
            val intent = Intent(this@SettingAboutActivity, TermPrivacyActivity::class.java)
            intent.putExtra("type","privacy")
            startActivity(intent)
        }

        mTremCondition.setOnClickListener {
            //startActivity(Intent(this@SettingAboutActivity, TremAndConditionActivity::class.java)) }
            val intent = Intent(this@SettingAboutActivity, TermPrivacyActivity::class.java)
            intent.putExtra("type","term")
            startActivity(intent)
        }
    }
}
