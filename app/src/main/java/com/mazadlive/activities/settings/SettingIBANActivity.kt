package com.mazadlive.activities.settings

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.Window
import android.widget.Toast
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.activities.BaseActivity
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.models.CountryModel
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.UserPreferences
import kotlinx.android.synthetic.main.activity_ibansetting.*
import org.json.JSONObject

class SettingIBANActivity : BaseActivity() {


    private lateinit var userRef: UserPreferences
    private var progress: CustomProgress? = null
    private var selectedCountry = CountryModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_ibansetting)
        supportActionBar!!.hide()
        userRef = MyApplication.instance.getUserPreferences()
        progress = CustomProgress(this)
        back.setOnClickListener { onBackPressed() }

        if(!TextUtils.isEmpty(userRef.countryCode)){
            mCountry.setText(userRef.countryCode)
        }
        if(!TextUtils.isEmpty(userRef.ibanBankName)){
            mBankName.setText(userRef.ibanBankName)
        }
        if(!TextUtils.isEmpty(userRef.ibanPhoneNumber)){
            mPhoneNmuber.setText(userRef.ibanPhoneNumber)
        }
        if(!TextUtils.isEmpty(userRef.ibanBanNumber)){
            mbanNumber.setText(userRef.ibanBanNumber)
        }

        mSave.setOnClickListener {
            if (validation()) {
                saveData()
            }
        }

        country_view.setOnClickListener {
            CustomDialog(this).showCountryDialog(object : DialogListener() {
                override fun okClick(any: Any) {
                    super.okClick(any)
                    val countryModel = any as CountryModel
                    mCountry.setText(countryModel.code)
                    selectedCountry = countryModel
                    mPhoneNmuber.setText("")
                }
            }, true)
        }


    }

    private fun validation(): Boolean {
        if(TextUtils.isEmpty(selectedCountry.code)){
            CustomDialog(this).showErrorDialog(getString(R.string.select_country))
            return false
        }

        if(TextUtils.isEmpty(mbanNumber.text.toString().trim())){
            CustomDialog(this).showErrorDialog(getString(R.string.enetr_ban_number))
            return false
        }

        if(TextUtils.isEmpty(mBankName.text.toString().trim())){
            CustomDialog(this).showErrorDialog(getString(R.string.bank_name))
            return false
        }

        if(TextUtils.isEmpty(mPhoneNmuber.text.toString().trim())){
            CustomDialog(this).showErrorDialog(getString(R.string.phone_no))
            return false
        }

        return true
    }

    private fun saveData() { //user_id:  userId, country:  countryCode, banNumber: ban number , bankName: Bank Name , phoneNumber: Contact.
        val code = selectedCountry.code.replace("+","")
        val param = JSONObject()
        param.put("version", "1.1")
        param.put("user_id", userRef.id)
        param.put("country", code)
        param.put("banNumber",mbanNumber.text.toString().trim())
        param.put("bankName", mBankName.text.toString().trim())
        param.put("phoneNumber", mPhoneNmuber.text.toString().trim())

        progress!!.show()
        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                Toast.makeText(this@SettingIBANActivity,getString(R.string.setting_updated),Toast.LENGTH_SHORT).show()
            }

            override fun onError(errorMessage: String) {
                Toast.makeText(this@SettingIBANActivity,getString(R.string.failed_to_update),Toast.LENGTH_SHORT).show()
                progress!!.dismiss()
            }

        }).updateIbanSettings(param)
    }


}
