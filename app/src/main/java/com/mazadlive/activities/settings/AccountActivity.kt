package com.mazadlive.activities.settings

import android.Manifest
import android.content.Intent
import android.graphics.Bitmap
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.Window
import android.widget.Toast
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.DeleteObjectRequest
import com.mazadlive.helper.PickImageHelper
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.github.florent37.runtimepermission.kotlin.askPermission
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.helper.Constant
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.activity_account.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.io.File

class AccountActivity : AppCompatActivity() {


    val userRef = MyApplication.instance.getUserPreferences()
    private lateinit var pickImageHelper: PickImageHelper
    private var s3: AmazonS3Client? = null
    private var transferUtility: TransferUtility? = null
    private var bitmap: Bitmap? = null;
    private var imageFilePath = ""
    private var countryCode = "91"

    private var progress: CustomProgress?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_account)
        supportActionBar?.hide()
        progress = CustomProgress(this)
        pickImageHelper = PickImageHelper(this, null, true, imagePickerListener())
        doAsync {
            credentialsProvider()
            setTransferUtility(); //https://i.diawi.com/GSxRHv
        }

        if(Constant.countryList.size <= 0){
            Constant.getAllCountries()
        }


        saveData()
        back.setOnClickListener { onBackPressed() }

        mProfileImage.setOnClickListener {
            askPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) {
                pickImageHelper.pickImage(4)
            }.onDeclined { result ->
                if (result.hasDenied()) {
                    CustomDialog(this).showErrorDialog(getString(R.string.permission_required))
                } else if (result.hasForeverDenied()) {
                    result.foreverDenied.forEach {
                        CustomDialog(this).permissionDialog(getString(R.string.accept_permission), object : DialogListener() {
                            override fun yesClick() {
                                super.yesClick()
                                result.goToSettings()
                            }
                        })
                    }
                }
            }
        }

        mSave.setOnClickListener {
            if(TextUtils.isEmpty(imageFilePath)){
                progress!!.show()
                updateUserProfile("")
            }else{
                setFileToUpload()
            }
        }

        email_view.setOnClickListener {  }
        phone_view.setOnClickListener {  }
    }


    private fun saveData() {
        Glide.with(this).load(userRef.profile).apply(RequestOptions().override(500).error(R.drawable.dummy)).into(mProfileImage)
        mUsername.setText(userRef.username)
        mEmail.setText(userRef.email)
        mNumber.setText(userRef.phone)
        mPassword1.setText(userRef.password)
        mPassword2.setText(userRef.password)
        codeinput.setText(userRef.countryCode)
        et_des.setText(userRef.description)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        pickImageHelper.onActivityResult(requestCode, resultCode, data)
    }

    // private var bitmapString = ""

    inner class imagePickerListener : PickImageHelper.OnImagePickerListener {

        override fun onImagePicked(imagePath: String, requestCode: Int) {
            if (requestCode == Constant.CAMERA_IMAGE_REQUEST) {
                imageFilePath = imagePath
                mProfileImage.setPadding(0, 0, 0, 0)
                // bitmap = getBitmapFromPath(imagePath)
                Glide.with(this@AccountActivity).asBitmap().load(imagePath).apply(RequestOptions().override(200)).listener(object : RequestListener<Bitmap> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        bitmap = resource
                        //bitmapString = Constant.getStringFromBitmap(bitmap!!)
                        mProfileImage.setImageBitmap(resource)
                        return true
                    }

                }).into(mProfileImage)

            } else if (requestCode == Constant.GALLARY_IMAGE_REQUEST) {
                // bitmap = getBitmapFromPath(imagePath)
                imageFilePath = imagePath
                mProfileImage.setPadding(0, 0, 0, 0)
                Glide.with(this@AccountActivity).asBitmap().load(imagePath).apply(RequestOptions().override(200)).listener(object : RequestListener<Bitmap> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        bitmap = resource
                        mProfileImage.setImageBitmap(resource)

                        return true
                    }

                }).into(mProfileImage)

            }
        }
    }


    fun credentialsProvider(){

        // Initialize the Amazon Cognito credentials provider
        val credentialsProvider =  CognitoCachingCredentialsProvider(
                getApplicationContext(),
                userRef.POOL_ID, //indentity pool id
                Constant.REGION)

        setAmazonS3Client(credentialsProvider);
    }

    fun setAmazonS3Client(credentialsProvider: CognitoCachingCredentialsProvider) {

        // Create an S3 client
        s3 = AmazonS3Client(credentialsProvider)

    }

    fun setTransferUtility() {
        transferUtility = TransferUtility(s3, applicationContext)
    }

    fun setFileToUpload() {
        doAsync {
            try {
                bitmap = Constant.getResizedBitmap(bitmap!!,300)
                val file = Constant.saveUserImage(bitmap!!)
                val transferObserver = transferUtility!!.upload(Constant.BUCKET_NAME,"${Constant.USER_IMAGE_KEY}/${file!!.name}",file)

                Log.e("transferObserverList","setFileToUpload")

                transferObserverListener(transferObserver,file)
            }catch (e: java.lang.Exception){
                Toast.makeText(this@AccountActivity,getString(R.string.pic_not_upload), Toast.LENGTH_SHORT).show()
                updateUserProfile("")
                e.printStackTrace()
            }
        }
    }

    fun transferObserverListener(transferObserver: TransferObserver, file: File) {
        Log.e("transferObserverList","onState ")
        transferObserver.setTransferListener(object : TransferListener {
            override fun onStateChanged(id: Int, state: TransferState?) {
                if(state!!.name.equals("COMPLETED",true)){
                    val url = s3!!.getResourceUrl(Constant.BUCKET_NAME,"${Constant.USER_IMAGE_KEY}/${file.name}")
                    deleteImage(userRef.profile)
                    updateUserProfile(url)
                }else{
                    if(state!!.name.equals("FAILED",true)){

                        updateUserProfile("")
                    }
                }
                Log.e("transferObserverList","onStateChanged ${state!!.name}")
            }

            override fun onError(id: Int, ex: java.lang.Exception?) {
                Log.e("transferObserverList","onError")
                Toast.makeText(this@AccountActivity,getString(R.string.pic_not_upload), Toast.LENGTH_SHORT).show()
                updateUserProfile("")
            }

            override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {

                val per = (bytesCurrent/bytesTotal)*100

                //progress!!.setMessage("Upload Image : $per%")
                //Log.e("transferObserverList","onProgressChanged $id   **${bytesCurrent} *** $bytesTotal  ${per}")
            }
        });
    }

    private fun deleteImage(profile: String) {
        doAsync {
            val fName = profile.substring(profile.lastIndexOf("/"))
            s3!!.deleteObject(DeleteObjectRequest(Constant.BUCKET_NAME,"${Constant.USER_IMAGE_KEY}/$fName"))
        }
    }

    private fun updateUserProfile(url: String) {
        val json = JSONObject()
        json.put("version","1.1")
        json.put("user_id",userRef.id)

        if(TextUtils.isEmpty(mUsername.text.toString().trim())){
            json.put("user_name",userRef.username)
        }else{
            json.put("user_name",mUsername.text.toString().trim())
        }

        if(TextUtils.isEmpty(mPassword1.text.toString().trim())){
            json.put("password",userRef.password)
        }else{
            json.put("password",mPassword1.text.toString().trim())
        }

        if(TextUtils.isEmpty(et_des.text.toString().trim())){
            json.put("description",userRef.description)
        }else{
            json.put("description",et_des.text.toString().trim())
        }


        /*if(TextUtils.isEmpty(mEmail.text.toString().trim())){
            json.put("user_id",userRef.id)
        }else{
            json.put("user_id",userRef.id)
        }
*/

      /*  if(TextUtils.isEmpty(codeinput.text.toString().trim())){
            json.put("user_id",userRef.id)
        }else{
            json.put("user_id",userRef.id)
        }*/

        if(TextUtils.isEmpty(url)){
            json.put("image_url",userRef.profile)
        }else{
            json.put("image_url",url)
        }

        /*{
    "error": false,
    "message": "Done",
    "description": {
        "username": "Lakshya Saini",
        "country_code": "+973",
        "phone": "9680751662",
        "email": "ls@gmail.com",
        "password": "asdfghjkl",
        "type": "buyer",
        "profilePhoto": "https://s3-ap-southeast-1.amazonaws.com/datapost/image/IMG_1544079010247.jpg",
        "description": "Go hard or Go home",
        "_id": "5c08c6a41bf815286450659a",
        "__v": 0,
        "is_online": true,
        "socket_id": "lLmnkw20Tid9p6EZAAAA",
        "is_live": "stopped",
        "favPost": [
            {
                "_id": "5c08e4e8543dd033ba3f1b42",
                "post_id": "5c061c5e3bf6c11c4f1b60e7"
            }
        ]
    }
}*/

        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                Toast.makeText(this@AccountActivity,getString(R.string.acc_updated),Toast.LENGTH_SHORT).show()
                progress!!.dismiss()
                val json = JSONObject(`object`.toString())
                if(json.has("description")){
                    val obj  = json.getJSONObject("description")
                    if (obj.has("username")) {
                        userRef.username = obj.getString("username")
                    }

                    if (obj.has("country_code")) {
                        userRef.countryCode = obj.getString("country_code")
                    }

                    if (obj.has("phone")) {
                        userRef.phone = obj.getString("phone")
                    }

                    if (obj.has("email")) {
                        userRef.email = obj.getString("email")
                    }

                    if (obj.has("password")) {
                        userRef.password = obj.getString("password")
                    }

                    if (obj.has("type")) {
                        userRef.type = obj.getString("type")
                    }

                    if (obj.has("profilePhoto")) {
                        userRef.profile = obj.getString("profilePhoto")
                    }

                    if (obj.has("_id")) {
                        userRef.id = obj.getString("_id")
                    }



                    if(obj.has("country_id")){
                        userRef.countryId = obj.getString("country_id")
                    }

                    if(obj.has("country_name")){
                        userRef.CountryName = obj.getString("country_name")
                    }

                    if(obj.has("description")){
                        userRef.description = obj.getString("description")
                    }
                    saveData()
                }
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(this@AccountActivity,"Failed to update account $errorMessage",Toast.LENGTH_SHORT).show()
            }

        }).updateUserDetails(json)

    }
}
