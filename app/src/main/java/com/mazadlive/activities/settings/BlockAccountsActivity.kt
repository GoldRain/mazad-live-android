package com.mazadlive.activities.settings

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Toast
import com.mazadlive.R
import com.mazadlive.adapters.BlockUserAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.helper.MessageEvent
import com.mazadlive.models.UserModel
import com.mazadlive.parser.UserParser
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.activity_block_accounts.*
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject

class BlockAccountsActivity : AppCompatActivity() {


    private lateinit var progress: CustomProgress
    var blockUser = ArrayList<UserModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_block_accounts)
        supportActionBar?.hide()

        progress = CustomProgress(this)


        recycleView.layoutManager = LinearLayoutManager(this@BlockAccountsActivity)
        recycleView.adapter = BlockUserAdapter(this, blockUser, OnItemClick())
        recycleView.adapter.notifyDataSetChanged()

        back.setOnClickListener { onBackPressed() }

        getAllBlockUser()

        EventBus.getDefault().post(MessageEvent(MyApplication.instance.getUserPreferences()!!.appModeBuyer))
    }

    private fun updateList() {

        recycleView.adapter.notifyDataSetChanged()
        if(blockUser.size >0){
            recycleView.visibility = View.VISIBLE
            tx_empty.visibility = View.GONE
        }else{
            recycleView.visibility = View.GONE
            tx_empty.visibility = View.VISIBLE
        }
    }



    private fun getAllBlockUser() {

        val json = JSONObject()
        json.put("version", "1.1")
        json.put("user_id", MyApplication.instance.getUserPreferences().id)
        progress.show()
        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                val jsonObject = JSONObject(`object`.toString())
                if(jsonObject.has("description")){
                    val array = jsonObject.getJSONArray("description")
                    val parser = UserParser()
                    for (i in 0 until array.length()){
                        val obj  = array.getJSONObject(i)
                        val userModel = parser.parse(obj)
                        blockUser.add(userModel)
                    }

                   updateList()
                }
                progress.dismiss()
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                Toast.makeText(this@BlockAccountsActivity,"Failed to get list $errorMessage ",Toast.LENGTH_SHORT).show()
            }

        }).blockUserList(json)
    }



    private fun unBlockUser(position: Int) {
        /*blockUser.removeAt(position)
            recycleView.adapter.notifyDataSetChanged()*/

        val param = JSONObject()
        param.put("version","1.1")
        param.put("his_id", blockUser[position].id)
        param.put("self_id",MyApplication.instance.getUserPreferences().id)
        param.put("is_block",0)

        Log.e("PARAM:","${param.toString()} **")
        progress.show()
        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                blockUser.removeAt(position)
                updateList()
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                Toast.makeText(this@BlockAccountsActivity,"Could not Unblock $errorMessage",Toast.LENGTH_SHORT).show()
            }

        }).blockUser(param)
    }

    inner class OnItemClick():BlockUserAdapter.OnClick{
        override fun unBlock(position: Int) {
            unBlockUser(position)
        }
    }
}
