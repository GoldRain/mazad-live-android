package com.mazadlive.activities.settings

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.view.Window
import com.mazadlive.R
import com.mazadlive.Socket.SocketManager
import com.mazadlive.activities.*
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.database.AppDatabase
import com.mazadlive.helper.MessageEvent
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.customdialog_badge_form.view.*
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.doAsync
import org.json.JSONObject

class SettingActivity : BaseActivity() {

    private lateinit var progress: CustomProgress

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_setting)
        supportActionBar!!.hide()

        progress = CustomProgress(this@SettingActivity)

        if(MyApplication.instance.getUserPreferences().loginStatus){
            tx_logout.setText( getString(R.string.logout))
        }else{
            tx_logout.setText( getString(R.string.login))
        }

        invite_btn.tag = false
        invite_btn.setOnClickListener {
            /*if (invite_btn.tag as Boolean) {
                invite_arrow_icon.rotation = 180F
                invite_layout.visibility = View.GONE
                invite_btn.tag = false
            } else {
                invite_arrow_icon.rotation = 270F
                invite_layout.visibility = View.VISIBLE
                invite_btn.tag = true
            }*/

            // Open Contact Activity (Facebook Friend Invitation Padding - On When App review by Facebook)
            if(checkPermissionContact()){
                openContact()
            }
        }


        follow_people_btn.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                val intent = Intent(this, FollowActivity::class.java)
                intent.putExtra("type", "")
                intent.putExtra("showAll", true)
                startActivity(intent)
            }else{

                CustomDialog(this).createDialogBox()
            }
        }

        logout_btn.setOnClickListener {
            val intent  = Intent(this, SignInActivity::class.java)

            if(MyApplication.instance.getUserPreferences().loginStatus){

                doAsync {
                    AppDatabase.getAppDatabase().favPostDao().deleteAllFavPost()
                    AppDatabase.getAppDatabase().roomDao().deleteAllRooms()
                    AppDatabase.getAppDatabase().messageDao().deleteAllMessages()
                    AppDatabase.getAppDatabase().roomUserDao().deleteAllRoomUser()
                }

                setOnLine()
                MyApplication.instance.getUserPreferences().clearAll()
                MyApplication.instance.getUserPreferences().loginStatus = false
                startActivity(Intent(this@SettingActivity, SignInActivity::class.java))

                MyApplication.instance.getSocketManager().disConnect()

                ActivityCompat.finishAffinity(this@SettingActivity)
            }else{
                intent.putExtra("fromDialog",true)
            }
            startActivity(intent)
        }

        account_btn.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                val intent = Intent(this, AccountActivity::class.java)
                startActivity(intent)
            }else{

                CustomDialog(this).createDialogBox()
            }
        }

        notifications_btn.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                val intent = Intent(this, SettingNotificationActivity::class.java)
                startActivity(intent)
            }else{

                CustomDialog(this).createDialogBox()
            }
        }

        privacy_btn.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                val intent = Intent(this, PrivacyPolicyActivity::class.java)
                startActivity(intent)
            }else{

                CustomDialog(this).createDialogBox()
            }
        }

        support_btn.setOnClickListener {
            val intent = Intent(this, SupportActivity::class.java)
            startActivity(intent)
        }

        about_btn.setOnClickListener {
            val intent = Intent(this, SettingAboutActivity::class.java)
            startActivity(intent)
        }

        mNewPost.setOnClickListener {
            val intent = Intent(this, PostActivity::class.java)
            startActivity(intent)
        }

        ecommerce_setting_btn.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                val intent = Intent(this, SettingECommerceActivity::class.java)
                startActivity(intent)
            }else{

                CustomDialog(this).createDialogBox()
            }
        }

        contacts_btn.setOnClickListener {
            if(checkPermissionContact()){
                openContact()
            }
        }

        switch_account_btn.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                changeUserType()
            }else{

                CustomDialog(this).createDialogBox()
            }
        }

        mLanguage.setOnClickListener {
            val intent = Intent(this, LanguageActivity::class.java)
            startActivity(intent)
        }

        back.setOnClickListener { onBackPressed() }

    }


    fun setOnLine() {
        val socketManager = MyApplication.instance.getSocketManager()
        if (!socketManager.isConnected) {
            socketManager.socket!!.open()
            socketManager.connect()
        }
        val map = JSONObject()
        map.put("user_id", MyApplication.instance.getUserPreferences().id)
        map.put("is_online", false)
        map.put("user_type", "android")
        map.put("last_login", MyApplication.instance.getUserPreferences().lastLoginTime)
        map.put("playerId", "")

        socketManager.socket!!.emit(SocketManager.EMIT_UPDATE_ONLINE, map)
    }

    private fun openContact() {
        ContactListActivity.start(this)
    }


    private fun checkPermissionContact(): Boolean{
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.READ_CONTACTS,
                            Manifest.permission.WRITE_CONTACTS), 11)

            return false
        }

        return true

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == 11){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                openContact()
            }
        }
    }

    private fun changeUserType() {

        progress.show()
        val ref = MyApplication.instance.getUserPreferences()
        val map =  HashMap<String,String>()
        map.put("version","1.1")

        map.put("id", ref.id)

        if (ref.appModeBuyer){
            map.put("type","seller")
        }else{
            map.put("type","buyer")
        }

        //Log.e("USER_TYPE","type ${ref!!.appModeBuyer}")

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {

                progress.dismiss()
                val json = JSONObject(`object`.toString())

                if (json.has("data")) {
                    val obj = json.getJSONObject("data")
                    if (obj.has("username")) {
                        ref.username = obj.getString("username")
                    }

                    if (obj.has("country_code")) {
                        ref.countryCode = obj.getString("country_code")
                    }

                    if (obj.has("phone")) {
                        ref.phone = obj.getString("phone")
                    }

                    if (obj.has("email")) {
                        ref.email = obj.getString("email")
                    }

                    if (obj.has("password")) {
                        ref.password = obj.getString("password")
                    }

                    if (obj.has("type")) {
                        ref.type = obj.getString("type")
                    }

                    if (obj.has("profilePhoto")) {
                        ref.profile = obj.getString("profilePhoto")
                    }

                    if (obj.has("_id")) {
                        ref.id = obj.getString("_id")
                    }

                    ref.appModeBuyer = ref.type.equals("buyer",true)

                }
                if (ref.appModeBuyer) {
                    // MyApplication.instance.getUserPreferences().appModeBuyer = false
                    //fragmentView.switch_account.setImageResource(R.drawable.switch_account_buyer)
                    mSwitchText.text = "Switch to Seller Account"
                    EventBus.getDefault().post(MessageEvent(false))

                } else {
                    // MyApplication.instance.getUserPreferences().appModeBuyer = true
                  // fragmentView.switch_account.setImageResource(R.drawable.switch_account_seller)
                    mSwitchText.text = "Switch to Buyer Account"
                    EventBus.getDefault().post(MessageEvent(true))
                }
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                CustomDialog(this@SettingActivity).showErrorDialog("Failed to connect $errorMessage")
            }

        }).changeUserType(map)

    }
}
