package com.mazadlive.activities.settings

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Window
import com.mazadlive.R
import com.mazadlive.activities.SplashActivity
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.UserPreferences
import kotlinx.android.synthetic.main.activity_language.*
import java.util.*

class LanguageActivity : AppCompatActivity() {

    lateinit var userPreferences: UserPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_language)
        supportActionBar!!.hide()

        userPreferences = MyApplication.instance.getUserPreferences()

        setData(userPreferences.appLanguage,false)

        mLangEnglish.setOnClickListener {
            setLocale("en")
        }

        mLangArabic.setOnClickListener {
            setLocale("ar")
        }

        mBack.setOnClickListener { onBackPressed() }

    }


    private fun setLocale(lang: String) {

        Log.e("LANGUAGE", "$lang   *** ")
        val myLocale = Locale(lang)
        Locale.setDefault(myLocale)
        val res = resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = myLocale
        baseContext.resources.updateConfiguration(conf, dm)
        userPreferences.appLanguage = lang
        setData(lang,true)

    }

    private fun setData(value: String,isUserSet : Boolean) {
        if (value == "en") {
            mArabic.setImageResource(0)
            mEnglish.setImageResource(R.drawable.ic_right)
        } else {
            mArabic.setImageResource(R.drawable.ic_right)
            mEnglish.setImageResource(0)
        }

        if (isUserSet){
            val intent = Intent(this, SplashActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
    }
}
