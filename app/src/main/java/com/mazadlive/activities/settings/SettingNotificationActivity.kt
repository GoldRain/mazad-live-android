package com.mazadlive.activities.settings

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Window
import com.mazadlive.R
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.UserPreferences
import kotlinx.android.synthetic.main.activity_setting_notification.*
import org.json.JSONObject

class SettingNotificationActivity : AppCompatActivity() {

    private lateinit var userPreferences: UserPreferences
    private lateinit var progress: CustomProgress

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_setting_notification)
        supportActionBar!!.hide()

        userPreferences = MyApplication.instance.getUserPreferences()
        progress = CustomProgress(this)

        mEmailSMS.tag = userPreferences.emailAndNotification
        mPush.tag = userPreferences.pushNotification

        setButtonData()

        back.setOnClickListener { onBackPressed() }

        mPush.setOnClickListener {
            setNotificationSetting(!userPreferences.pushNotification,userPreferences.emailAndNotification)
        }

        mEmailSMS.setOnClickListener {
            setNotificationSetting(userPreferences.pushNotification,!userPreferences.emailAndNotification)
        }
    }

    private fun setButtonData(){

        if (userPreferences.emailAndNotification){
            mEmailSMS.setImageResource(R.drawable.ic_switch_on)
        }else{
            mEmailSMS.setImageResource(R.drawable.ic_switch_off)
        }

        if (userPreferences.pushNotification){
            mPush.setImageResource(R.drawable.ic_switch_on)
        }else{
            mPush.setImageResource(R.drawable.ic_switch_off)
        }
    }

    private fun setNotificationSetting(isPush : Boolean, isEmailAndSms : Boolean) {

        progress.show()
        val jsonObject  = JSONObject()

        jsonObject.put("user_id",userPreferences.id)
        jsonObject.put("push_notification",isPush)
        jsonObject.put("email_and_notification",isEmailAndSms)

        Log.e("ASDASDASDSADSAD",(!userPreferences.pushNotification).toString())
        Log.e("ASDASDASDSADSAD",(!userPreferences.emailAndNotification).toString())

        ServiceRequest(object : ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                setButtonData()
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
            }
        }).notificationSetting(jsonObject)

    }
}
