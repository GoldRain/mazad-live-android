package com.mazadlive.activities.settings

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Telephony
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Window
import com.mazadlive.R
import com.mazadlive.adapters.ContactListAdapter
import com.mazadlive.database.AppDatabase
import com.mazadlive.database.ContactData
import com.mazadlive.helper.Constant
import kotlinx.android.synthetic.main.activity_contact_list.*
import org.jetbrains.anko.doAsync
import java.util.*
import com.mazadlive.helper.GridSpacingItemDecoration


class ContactListActivity : AppCompatActivity() {

    companion object {
        fun start(context: Context){
            val intent = Intent(context, ContactListActivity::class.java)
            context.startActivity(intent)
        }
    }

    val list  = ArrayList<ContactData>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_contact_list)
        supportActionBar?.hide()

        recycleView.layoutManager = LinearLayoutManager(this@ContactListActivity)
        recycleView.addItemDecoration(GridSpacingItemDecoration(1, 16, false))
        recycleView.adapter = ContactListAdapter(this,list,OnItemClick())
        recycleView.adapter.notifyDataSetChanged()

        getAllContacts()
        back.setOnClickListener {
            onBackPressed()
        }
    }

    private fun getAllContacts() {
        doAsync {
           // Log.e("COUNT:","ALL ${AppDatabase.getAppDatabase().contactDao().getTotalCount()}")
            val listTemp  = AppDatabase.getAppDatabase().contactDao().getAllContact()

            Collections.sort(listTemp,Sort())
            list.addAll(listTemp)

            runOnUiThread {
                recycleView.adapter.notifyDataSetChanged()
            }
        }
    }

  //  @RequiresApi(Build.VERSION_CODES.KITKAT)
    private fun sendSMS(phone:String) {

      try {
         /* val smsIntent = Intent(android.content.Intent.ACTION_VIEW)
          smsIntent.type = "vnd.android-dir/mms-sms"
          smsIntent.putExtra("address", "$phone")
          smsIntent.putExtra("sms_body", "hey, i am using mazad live app.")
          startActivity(Intent.createChooser(smsIntent,"Select"))*/


          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
              val defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(this) // Need to change the build to API 19

              val sendIntent = Intent(Intent.ACTION_VIEW)
              sendIntent.type = "vnd.android-dir/mms-sms"
              sendIntent.setData(Uri.parse("smsto:$phone"))
           //   sendIntent.putExtra(Intent.EXTRA_TEXT, "hey, i am using mazad live app.")
              sendIntent.putExtra("sms_body", "Hey friend - Free Download Mazad Live from ${Constant.PLAYSTORE_LINK}")

              if (defaultSmsPackageName != null)
              {
               //   sendIntent.`package` = defaultSmsPackageName
                  sendIntent.setPackage(defaultSmsPackageName)
              }
          //    startActivity(Intent.createChooser(intent,"Select"))
              startActivity(sendIntent)

          } else {
              val smsIntent = Intent(android.content.Intent.ACTION_VIEW)
              smsIntent.type = "vnd.android-dir/mms-sms"
              smsIntent.putExtra("address", "$phone")
              smsIntent.putExtra("sms_body", "Hey friend - Free Download Mazad Live from ${Constant.PLAYSTORE_LINK}")
              startActivity(Intent.createChooser(smsIntent,"Select"))
          }
      }catch (e: Exception){

          Log.e("fhgfdh","exception is: ${e.message}")
      }

    }

    inner class OnItemClick:ContactListAdapter.OnClick{
        override fun onInvite(position: Int) {
            sendSMS(list[position].phone)
        }
    }

    inner class Sort:Comparator<ContactData>{
        override fun compare(o1: ContactData?, o2: ContactData?): Int {
            val u1 = o1!!.username.toLowerCase()
            val u2 = o2!!.username.toLowerCase()
            return (u1.compareTo(u2))
        }

    }
}
