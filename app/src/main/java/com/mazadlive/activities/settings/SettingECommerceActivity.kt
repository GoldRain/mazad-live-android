package com.mazadlive.activities.settings

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import com.mazadlive.R
import com.mazadlive.activities.BaseActivity
import com.mazadlive.activities.StorySettingsActivity
import kotlinx.android.synthetic.main.activity_setting_ecommerce.*

class SettingECommerceActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_setting_ecommerce)
        supportActionBar!!.hide()

        back.setOnClickListener { onBackPressed() }

        mIBANSetting.setOnClickListener { startActivity(Intent(this@SettingECommerceActivity, SettingIBANActivity::class.java)) }

        mStorySetting.setOnClickListener { startActivity(Intent(this@SettingECommerceActivity, StorySettingsActivity::class.java)) }
    }
}
