package com.mazadlive.activities

import android.opengl.Visibility
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.view.Window
import com.mazadlive.R
import com.mazadlive.adapters.SellerFeedbackListAdapter
import com.mazadlive.helper.DividerItemDecorator
import com.mazadlive.models.PostFeedbackModel
import kotlinx.android.synthetic.main.activity_feeds.*

class FeedsActivity : AppCompatActivity() {

    val TAG = "FeedsActivity"
    val feedbackList = ArrayList<PostFeedbackModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_feeds)
        supportActionBar!!.hide()

        if(intent.hasExtra("isYourFeedback")){
            if (intent.getBooleanExtra("isYourFeedback",false)){
                mYourFeedback.visibility = View.VISIBLE
            }
        }

        back.setOnClickListener { onBackPressed() }

        val list = intent.getParcelableArrayListExtra<PostFeedbackModel>("feeds")
        feedbackList.addAll(list)
        Log.e(TAG,"${feedbackList.size}")

        feedback_list.layoutManager = LinearLayoutManager(this@FeedsActivity)
        feedback_list.addItemDecoration(DividerItemDecorator(ContextCompat.getDrawable(this@FeedsActivity, R.drawable.divider_line)))
        feedback_list.adapter = SellerFeedbackListAdapter(this@FeedsActivity, feedbackList)
    }
}
