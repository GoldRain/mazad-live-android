package com.mazadlive.activities

import android.os.Bundle
import android.view.Window
import com.mazadlive.R
import kotlinx.android.synthetic.main.activity_post_edit.*

class PostEditActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_post_edit)
        supportActionBar!!.hide()

        back.setOnClickListener { onBackPressed() }
    }
}
