package com.mazadlive.activities

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.Window
import com.mazadlive.R
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgressDialog
import kotlinx.android.synthetic.main.activity_otp_forgot_password.*

class OtpForgotPasswordActivity : BaseActivity() {

    companion object {
        fun start(context: Context, bundle: Bundle?){

            val intent = Intent(context,OtpForgotPasswordActivity::class.java)
            bundle.let { intent.putExtras(bundle) }
            context.startActivity(intent)
        }

    }

    var number = ""
    var code = ""
    var email = ""
    private var type = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTheme(R.style.whiteStatusTheme)
        setContentView(R.layout.activity_otp_forgot_password)
        supportActionBar!!.hide()


         type = intent.getStringExtra("type")
        if(type.equals("email")){

            customTextView2.text = (getString(R.string.sms_verification_code_has_been_sent_to2))
            email = intent.getStringExtra("EMAIL")
            mNumber.text = (email)

        }else{
            customTextView2.text = (getString(R.string.sms_verification_code_has_been_sent_to))
            code = intent.getStringExtra("CODE")

            code = code.replace("+","")
            number = intent.getStringExtra("NUMBER")
            mNumber.text = ("+$code $number")
        }
        back.setOnClickListener { onBackPressed() }

        next.setOnClickListener {
            verifyCode()

            /*val bundle = Bundle()
            bundle.putString("type",type)

            if(type.equals("email")){
                //intent.putExtra("EMAIL",email)
                bundle.putString("EMAIL",email)
            }else{
                *//*intent.putExtra("CODE",number)
                intent.putExtra("NUMBER",code)*//*
                bundle.putString("CODE",number)
                bundle.putString("NUMBER",code)
            }
            ChangePasswordActivity.start(this@OtpForgotPasswordActivity,bundle)
            finish()*/
        }
    }

    private fun verifyCode() {
        val otp = code_pin.value.toString()
        Log.e("OTP_LLE","${otp.length} *** ")
        if(TextUtils.isEmpty(otp)){
            CustomDialog(this@OtpForgotPasswordActivity).showErrorDialog(getString(R.string.enetr_otp))
            return
        }else{
            if(otp.length !=6){
                CustomDialog(this@OtpForgotPasswordActivity).showErrorDialog(getString(R.string.enter_sixdigit_otp))
                return
            }
        }
        val param  = HashMap<String,String>()
        param.put("version","1.1")
        param.put("code", otp)

        if(type.equals("email")){
            param.put("email",email)
            param.put("type","email")
        }else{
            param.put("country_code",code)
            param.put("phone",number)
            param.put("type","phone")
        }

        var progressDialog = CustomProgressDialog(this)
        progressDialog.displayDialog(true)

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {

                try {
                    progressDialog.hideDialog()

                    val intent = Intent( this@OtpForgotPasswordActivity,ChangePasswordActivity::class.java)
                    intent.putExtra("type",type)
                    if(type.equals("email")){
                        intent.putExtra("EMAIL",email)
                    }else{
                        intent.putExtra("CODE",code)
                        intent.putExtra("NUMBER",number)
                    }


                    startActivity(intent)
                    finish()
                }catch (e:Exception){
                    e.printStackTrace()
                    CustomDialog(this@OtpForgotPasswordActivity).showErrorDialog(getString(R.string.failed_toveirfy_otp))
                }
            }

            override fun onError(errorMessage: String) {
                progressDialog.hideDialog()
                CustomDialog(this@OtpForgotPasswordActivity).showErrorDialog("Failed to verify otp try again. $errorMessage")
            }

        }).verifyOtp(param)

        /*version:1.1
type:email
email:paruhack@gmail.com
code:850576*/
    }
}
