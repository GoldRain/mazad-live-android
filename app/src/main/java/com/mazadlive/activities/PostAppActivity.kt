package com.mazadlive.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Toast
import com.mazadlive.R
import kotlinx.android.synthetic.main.activity_post_app.*
import java.io.File

class PostAppActivity : AppCompatActivity() {

    var path : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_post_app)
        supportActionBar?.hide()

        if (intent.hasExtra("filePath")){
            path = intent.getStringExtra("filePath")
        }
        if (intent.hasExtra("insta")){
            if (intent.getBooleanExtra("insta",false)){
                mInstagram.visibility = View.VISIBLE
            }
        }
        if (intent.hasExtra("twitter")){
            if (intent.getBooleanExtra("twitter",false)){
                mTwitter.visibility = View.VISIBLE
            }
        }

        mDone.setOnClickListener {
            finish()
        }

        back.setOnClickListener {
            finish()
        }

        mInstagram.setOnClickListener {
            try {
                val shareIntent = Intent(android.content.Intent.ACTION_SEND)
                shareIntent.type = "image/*"
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(File(path)))
                shareIntent.setPackage("com.instagram.android")
                startActivity(shareIntent)
            }catch (e : Exception){
                Toast.makeText(this,"Instagram App don't found",Toast.LENGTH_SHORT).show()
            }
        }

        mTwitter.setOnClickListener {
            try {
                val tweetIntent = Intent(android.content.Intent.ACTION_SEND);
                tweetIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(File(path)))
                tweetIntent.type = "image/*";
                tweetIntent.setPackage("com.twitter.android");
                startActivity(tweetIntent);
            }catch (e : Exception){
                Toast.makeText(this,"Twitter App don't found",Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun finish() {
        setResult(Activity.RESULT_OK)
        super.finish()
    }

}
