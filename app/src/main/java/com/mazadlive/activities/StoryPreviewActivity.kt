package com.mazadlive.activities

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.media.MediaPlayer
import android.media.ThumbnailUtils
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.MediaController
import android.widget.Toast
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.mazadlive.R
import com.mazadlive.Socket.SocketManager
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.helper.Constant
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.activity_story_preview.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.io.File
import java.lang.Exception
import java.text.DecimalFormat
import java.util.ArrayList

class StoryPreviewActivity : BaseActivity() {

    val socketManager = MyApplication.instance.getSocketManager()
    val userRef = MyApplication.instance.getUserPreferences()
    var s3: AmazonS3Client? = null
    var transferUtility: TransferUtility? = null


    private lateinit var progress: CustomProgress

    private var imageList =  ArrayList<String>()

    override fun onBackPressed() {

        super.onBackPressed()
    }

    private var amount = 0.toDouble()
    var isVideo = false

    override fun onPause() {
        if(isVideo && videoView.isPlaying){

            videoView.stopPlayback()
        }
        super.onPause()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story_preview)
        supportActionBar?.hide()
        progress = CustomProgress(this)
        imageList = intent!!.getStringArrayListExtra("images")
        isVideo = intent.getBooleanExtra("isVideo",false)

        doAsync {
            credentialsProvider()
            setTransferUtility();
        }

        if(isVideo){
            im_video.visibility = View.VISIBLE

        }else{
            im_video.visibility = View.GONE
            image.visibility = View.VISIBLE
            videoView.visibility = View.GONE
        }

        Glide.with(this).asBitmap().load(imageList[0]).apply(RequestOptions().override(1500)).into(image)

        im_video.setOnClickListener {
            im_video.visibility = View.GONE
            image.visibility = View.GONE
            videoView.visibility = View.VISIBLE
            playVideo()
        }

        send.setOnClickListener {
            if(isVideo && videoView.isPlaying){
                videoView.stopPlayback()
            }

            if(!TextUtils.isEmpty(mMessageRep.text.toString().trim())){
                if(!TextUtils.isEmpty(mStorySerial.text.toString().trim())){
                    try {
                        amount = mMessageRep.text.toString().toDouble()
                        if(amount >0){

                            if (amount > Constant.MAX_TRANSACTION_AMOUNT){
                                Toast.makeText(this,"Enter amount less than ${Constant.MAX_TRANSACTION_AMOUNT}",Toast.LENGTH_SHORT).show()
                                return@setOnClickListener
                            }

                            if(!TextUtils.isEmpty(mMessageDes.text.toString().trim())){
                                progress.show()
                                if(isVideo){
                                    setVideoFileToUpload()
                                }else{
                                    setFileToUpload()
                                }
                            }else{
                                Toast.makeText(this,"Enter product description",Toast.LENGTH_SHORT).show()
                            }
                        }else{
                            Toast.makeText(this,getString(R.string.bid_amount_more),Toast.LENGTH_SHORT).show()
                        }

                    }catch (e:Exception){
                        Toast.makeText(this,getString(R.string.enter_valid_bid),Toast.LENGTH_SHORT).show()
                    }

                }else{
                    Toast.makeText(this,getString(R.string.enter_bid),Toast.LENGTH_SHORT).show()
                }
            }else{
                Toast.makeText(this,getString(R.string.enter_story_serial),Toast.LENGTH_SHORT).show()
            }
        }

        story_setting.setOnClickListener {
            startActivity(Intent(this@StoryPreviewActivity, StorySettingsActivity::class.java))
        }

        Constant.setOnLine(true)
    }

    private fun playVideo() {

        videoView.setVideoPath(imageList[0]);
        videoView.setMediaController( MediaController(this));
        videoView.start();

        videoView.setOnErrorListener(MediaPlayer.OnErrorListener { mediaPlayer, i, i1 ->
            false })
        videoView.setOnPreparedListener(MediaPlayer.OnPreparedListener {
        })
    }


    private fun credentialsProvider(){

        // Initialize the Amazon Cognito credentials provider
        val credentialsProvider =  CognitoCachingCredentialsProvider(
                getApplicationContext(),
                MyApplication.instance.getUserPreferences().POOL_ID, //indentity pool id
                Constant.REGION)

        setAmazonS3Client(credentialsProvider);
    }

    private fun setAmazonS3Client(credentialsProvider: CognitoCachingCredentialsProvider) {

        // Create an S3 client
        s3 = AmazonS3Client(credentialsProvider)
    }

    private fun setTransferUtility() {
        transferUtility = TransferUtility(s3, applicationContext)
    }

    private fun setFileToUpload() {

        doAsync {
            try {
                val imageCounter = 0
                var file =  File(imageList[imageCounter])
                var btm = Constant.getBitmapFromPath(file.path)
                btm = Constant.getResizedBitmap(btm!!,1500)
                val file2 = Constant.saveStoryImage(btm)
                if(file2 != null){
                    file = file2
                }

                val transferObserver = transferUtility!!.upload(Constant.BUCKET_NAME,"${Constant.STORY_IMAGE_KEY}/${file!!.name}",file)

                transferObserverListener(transferObserver,imageCounter,file)
            }catch (e: Exception){
                e.printStackTrace()
                Toast.makeText(this@StoryPreviewActivity,getString(R.string.failed),Toast.LENGTH_SHORT).show()
                progress.dismiss()
            }
        }
    }

    private val imageCounter =0

    private fun setVideoFileToUpload() {
        doAsync {
            try {

                val name = "IMG_${System.currentTimeMillis()}"
                var path  =  imageList[imageCounter]
                var btm = ThumbnailUtils.createVideoThumbnail(imageList[imageCounter], MediaStore.Video.Thumbnails.FULL_SCREEN_KIND)
                btm = Constant.getResizedBitmap(btm!!,200)

                val file2 = Constant.savePostImage(btm,name)
                val file = File(path)


                val transferObserver = transferUtility!!.upload(Constant.BUCKET_NAME,"${Constant.STORY_IMAGE_KEY}/$name.mp4",file)
                transferObserver.setTransferListener(object : TransferListener {
                    override fun onStateChanged(id: Int, state: TransferState?) {
                        if(state!!.name.equals("COMPLETED",true)){
                            val transferObserver1 = transferUtility!!.upload(Constant.BUCKET_NAME,"${Constant.STORY_IMAGE_KEY}/$name.jpg",file2)
                            transferObserverListener(transferObserver1,imageCounter,file2!!)
                        }

                    }

                    override fun onError(id: Int, ex: Exception?) {
                        Toast.makeText(this@StoryPreviewActivity,"failed to upload story try again",Toast.LENGTH_SHORT).show()
                    }

                    override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {

                        val per = (bytesCurrent/bytesTotal)*100
                    }
                })
            }catch (e: Exception){
                e.printStackTrace()
                Toast.makeText(this@StoryPreviewActivity,getString(R.string.failed_to_upload_story),Toast.LENGTH_SHORT).show()
            }
        }
    }



    private fun transferObserverListener(transferObserver: TransferObserver, position: Int, file: File) {
       // Log.e("transferObserverList","onState $position")
        transferObserver.setTransferListener(object : TransferListener {
            override fun onStateChanged(id: Int, state: TransferState?) {
                if(state!!.name.equals("COMPLETED",true)){
                   progress.dismiss()
                    Constant.deleteStoryImage()
                    val url =  s3!!.getResourceUrl(Constant.BUCKET_NAME,"${Constant.STORY_IMAGE_KEY}/${file.name}")
                    sendStory(url)
                }else{
                    if(state!!.name.equals("FAILED",true)){
                        progress.dismiss()
                        Toast.makeText(this@StoryPreviewActivity,getString(R.string.failed_to_upload_story),Toast.LENGTH_SHORT).show()
                    }

                }

            }

            override fun onError(id: Int, ex: Exception?) {
                ex!!.printStackTrace()

                Toast.makeText(this@StoryPreviewActivity,getString(R.string.failed),Toast.LENGTH_SHORT).show()

            }

            override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                val per = (bytesCurrent/bytesTotal)*100
            }
        });
    }


    private fun sendStory(url:String) {
        if(!socketManager.isConnected){
            socketManager.socket!!.open()
            socketManager.connect()
        }

        val param = JSONObject()
        param.put("version","1.1")
        param.put("url",url)
        param.put("buyNowPrice", DecimalFormat("##.##").format(amount))
        param.put("status","running")
        param.put("user_id",userRef.id)
        param.put("is_buyNow",userRef.isBuyNow)
        param.put("is_video",isVideo)
        param.put("whatsapp_only",userRef.isWhatsAppOnly)
        param.put("whatsapp_and_call",userRef.isWhatsAndCall)
        param.put("product_serial",mStorySerial.text.toString().trim())
        param.put("description",mMessageDes.text.toString().trim())
        param.put("product_description",mMessageDes.text.toString().trim())
        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                Toast.makeText(this@StoryPreviewActivity,getString(R.string.story_success),Toast.LENGTH_SHORT).show()

                setResult(Activity.RESULT_OK)
                finish()
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                Toast.makeText(this@StoryPreviewActivity,"Failed $errorMessage",Toast.LENGTH_SHORT).show()
            }

        }).postStories(param)

    }
}
