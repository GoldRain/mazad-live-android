package com.mazadlive.activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.media.ThumbnailUtils
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.services.s3.AmazonS3Client
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.location.places.ui.PlacePicker
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.adapters.CountryAdapterMultiple
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.helper.Constant
import com.mazadlive.models.CountryModel
import com.mazadlive.models.SubscribeModel
import com.mazadlive.models.TagModel
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.UserPreferences
import kotlinx.android.synthetic.main.activity_post.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.io.File
import java.lang.Exception
import java.text.DecimalFormat

//onPurchasesUpdated 24 GPA.3369-3774-2928-61441 dhfchmiehlblcenfpkojcajm.AO-J1OyJARXNUxyfPkFASW9Aq9jZx6eJ-0UStFfry3yGvvtXreJ3HGqK9KS0iPxjdd-QkMD_hPTx42G8ddFPSwr8P64v-D2F01NNkNHTnXfgVwn6YDi9HJw
class PostActivity : BaseActivity() {

    val PLACE_CODE = 12
    var s3: AmazonS3Client? = null
    var transferUtility: TransferUtility? = null

    var scrollFlag: Boolean = false

    private var imagesList = ArrayList<String>()

    private var tagList = ArrayList<TagModel>()

    private var countryList = ArrayList<CountryModel>()


    private var selectLanguage = "en"
    private var seletedCountryCheck = false
    private var selectTag = false
    private var selectAddress = ""
    private var selectLat = 0.0
    private var selectLon = 0.0

    private var imgFileList = ArrayList<File>()
    var userRef: UserPreferences? = null

    //private var progressDialog: CustomProgressDialog?= null
    private var progress: CustomProgress? = null
    var imageCounter = 0
    var isVideo = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_post)
        supportActionBar!!.hide()

        isVideo = intent.getBooleanExtra("isVideo", false)
        userRef = MyApplication.instance.getUserPreferences()
        progress = CustomProgress(this)
        progress!!.setMessage("Please Wait")

        doAsync {
            credentialsProvider()
            setTransferUtility();
        }
        if (Constant.countryList.size <= 0) {
            getAllCountries()
        }
        getAllTags()

        imagesList = intent.getStringArrayListExtra("images")

        saveData()
        saveImages()


        cash_switch.tag = false
        insta_switch.tag = false
        twitter_switch.tag = false
        auction_switch.tag = true
        buy_now_switch.tag = false
        whatsapp_switch.tag = false
        credit_card_switch.tag = false
        whatsapp_call_switch.tag = false

        cash_switch.setOnClickListener { buttonOnOff(cash_switch) }
        insta_switch.setOnClickListener { buttonOnOff(insta_switch) }
        twitter_switch.setOnClickListener { buttonOnOff(twitter_switch) }

        auction_switch.setOnClickListener {

            buttonOnOff(auction_switch)

        }
        buy_now_switch.setOnClickListener {
            buttonOnOff(buy_now_switch)
        }

        whatsapp_switch.setOnClickListener {

            if ((whatsapp_switch.tag as Boolean)) {
                /*whatsapp_call_switch.tag = true
                whatsapp_call_switch.setImageResource(R.drawable.ic_switch_on)*/
            } else {

                if ((whatsapp_call_switch.tag as Boolean)) {
                    whatsapp_call_switch.tag = false
                    whatsapp_call_switch.setImageResource(R.drawable.ic_switch_off)
                }

            }

            buttonOnOff(whatsapp_switch)
        }

        credit_card_switch.setOnClickListener { buttonOnOff(credit_card_switch) }

        whatsapp_call_switch.setOnClickListener {
            if ((whatsapp_call_switch.tag as Boolean)) {
                /*whatsapp_switch.tag = true
                whatsapp_switch.setImageResource(R.drawable.ic_switch_on)*/

            } else {
                if ((whatsapp_switch.tag as Boolean)) {
                    whatsapp_switch.tag = false
                    whatsapp_switch.setImageResource(R.drawable.ic_switch_off)
                }

            }
            buttonOnOff(whatsapp_call_switch)
        }

        trem.setText(Html.fromHtml(getString(R.string.post_check_box_txt)), TextView.BufferType.SPANNABLE)

        /*seek_bar.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> v.parent.requestDisallowInterceptTouchEvent(true)
                MotionEvent.ACTION_UP -> v.parent.requestDisallowInterceptTouchEvent(false)
            }
            v.onTouchEvent(event)
            true
        }*/

        countryList = ArrayList<CountryModel>()
        countryList.addAll(Constant.countryList)
        countryList.forEach { it ->
            it.isCheck = it.id.equals(MyApplication.instance.getUserPreferences().countryId)
        }
        //for ( i in 1..100 ) { countryList.add(CountryModel("a")) }

        val adapter = CountryAdapterMultiple(this, countryList, object : DialogListener() {})
        val linarLayoutManager = LinearLayoutManager(this)
        countries_view.layoutManager = linarLayoutManager
        countries_view.adapter = adapter

        seek_bar.max = 100
        seek_bar.progress = 10

        val listener: SeekBar.OnSeekBarChangeListener = object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                countries_view.smoothScrollToPosition((adapter.countryList.size * i) / 100)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                scrollFlag = true
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                scrollFlag = false
            }
        }

        seek_bar.setSeekListener(listener)


        countries_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                if (!scrollFlag) {
                    val seekbarPosition = ((linarLayoutManager.findLastVisibleItemPosition() * 100) / adapter.countryList.size);
                    seek_bar.progress = seekbarPosition
                    Log.e("DATA_CHANGE", (linarLayoutManager.findLastVisibleItemPosition().toString() + "------ RECYCLER_VIEW"))
                }

            }
        })

        back.setOnClickListener { onBackPressed() }

        create_post_btn.setOnClickListener {

            if (validation()) {
                progress!!.show()
                if (isVideo) {
                    setVideoFileToUpload()
                } else {
                    setFileToUpload()
                }
            }

            /*val intent = Intent(this, PostEditActivity::class.java)
            startActivity(intent)*/
        }

        post_image_layout.setOnClickListener {
            ImageListActivity.start(this, imagesList, isVideo)
        }


        tags_btn.setOnClickListener {

            CustomDialog(this).showDialogForTag(tagList, object : DialogListener() {
                override fun okClick() {
                    setTag()
                }
            })
        }

        location_btn.setOnClickListener {
            if (checkPermissions()) {
                openLocationActivity()
            }
        }

        language_layout.setOnClickListener {
            CustomDialog(this).showLanguagePickerDialog(object : DialogListener() {
                override fun okClick(any: Any) {
                    language_text.setText(any.toString())
                    when (any.toString()) {
                        "ENGLISH" -> {
                            selectLanguage = "en"
                            language_icon.setImageResource(R.drawable.english_language_icon)

                        }
                        "ARABIC" -> {
                            selectLanguage = "ar"
                            language_icon.setImageResource(R.drawable.ic_saudi_arabia_icon)
                        }
                    }
                    selectLanguage = any.toString()

                }
            })
        }


        getPostCount()
        addTextListener()
    }

    private fun getPostCount() {
        val listener = object : DialogListener() {
            override fun okClick() {

                finish()
            }
        }

        progress!!.show()
        val param = JSONObject()
        param.put("version", "1.1")
        param.put("user_id", userRef!!.id)

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                val jsonObject = JSONObject(`object`.toString())
                if (jsonObject.has("description")) {
                    val obj = jsonObject.getJSONObject("description")
                    if (obj.has("available_post_count")) {
                        val count = obj.getInt("available_post_count")
                        if (count == 0) {
                            CustomDialog(this@PostActivity!!).showAlertDialog("Alert", getString(R.string.alaer_omly_ten_post), listener)
                        }

                    }

                    if(obj.has("unlimited_post_count")){
                        val subObj  =obj.getJSONObject("unlimited_post_count")
                        val subModel = SubscribeModel()
                        if(subObj.has("status")){
                            subModel.status = subObj.getBoolean("status")
                            //  ad1.status = subObj.getBoolean("status")
                         //   ad4.status = subObj.getBoolean("status")
                        }

                        if(subObj.has("activation")){
                            subModel.activation = subObj.getString("activation")
                            //ad1.activation = subObj.getString("activation")
                               //     .//    ad4.activation = subObj.getString("activation")
                        }

                        if(subObj.has("expiry")){
                            subModel.expiry = subObj.getString("expiry")
                           /* ad1.expiry = subObj.getString("expiry")
                            ad4.expiry = subObj.getString("expiry")*/
                        }
                        if(subObj.has("count")){
                            subModel.previousCount = subObj.getInt("count")

                           /* ad4.available = subObj.getInt("count").toString()
                            ad1.available = subObj.getInt("count").toString()*/
                        }

                        val count = subModel.previousCount
                        if (!subModel.status && count == 0) {
                            CustomDialog(this@PostActivity!!).showAlertDialog("Alert", getString(R.string.alaer_omly_ten_post), listener)
                        }


                        //   ad4.subModel = subModel
                    }

                }

            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(this@PostActivity!!, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).showCounts(param)
    }

    private fun addTextListener() {
        //et_about.addTextChangedListener(CharCountListener(et_about,char_count_des,100))
        // et_serial.addTextChangedListener(CharCountListener(et_serial,char_count_serial,50))
    }

    private fun saveData() {

        username.setText(userRef!!.username)
        Glide.with(this).load(userRef!!.profile).apply(RequestOptions().override(80).error(R.drawable.dummy)).into(circleImageView)
    }


    private fun openLocationActivity() {

        val buider = PlacePicker.IntentBuilder()
        startActivityForResult(buider.build(this), PLACE_CODE)
        // val intent = Intent(this, LocationSelectActivity::class.java)
        // startActivityForResult(intent,LOCATION_PERMISSION)
    }

    private val POST_IMAGE_CODE = 111
    private val LOCATION_PERMISSION = 112

    private fun checkPermissions(): Boolean {


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION), LOCATION_PERMISSION)
            return false
        }

        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == LOCATION_PERMISSION) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                openLocationActivity()
            }
        }

    }


    private fun setTag() {

        var tag = ""

        for (i in 0 until tagList.size) {
            if (tagList[i].isCheck) {
                tag += "${tagList[i].name},"
            }
        }

        val index = tag.lastIndexOf(",")
        if (index != -1) {
            tag = tag.substring(0, index)
        }

        val font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        if (TextUtils.isEmpty(tag)) {
            // tag = getString(R.string.post_tags_txt)
            tag_name.typeface = font
            tag_name.setTextColor(ContextCompat.getColor(this@PostActivity, R.color.tag_color))
            selectTag = false
        } else {
            tag_name.typeface = null
            tag_name.setTextColor(ContextCompat.getColor(this@PostActivity, R.color.colorBlack))
            selectTag = true
        }
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == POST_IMAGE_CODE && resultCode == Activity.RESULT_OK) {
            imagesList = data!!.getStringArrayListExtra("images_list")
            Log.e("IMAGES_SIZE", "** ${imagesList.size}")

            if (imagesList.size <= 0) {
                finish()
            } else {
                saveImages()
            }

        }

        if (requestCode == LOCATION_PERMISSION && resultCode == Activity.RESULT_OK) {

            val bundle = data!!.getBundleExtra("data")

            selectAddress = "${bundle.getString("address")} "
            selectLat = bundle.getDouble("latitude")
            selectLon = bundle.getDouble("longitude")

            if (TextUtils.isEmpty(selectAddress)) {
                val font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
                location_text.typeface = font
                location_text.setTextColor(ContextCompat.getColor(this@PostActivity, R.color.tag_color))
            } else {
                location_text.typeface = null
                location_text.setTextColor(ContextCompat.getColor(this@PostActivity, R.color.colorBlack))
            }

        }

        if (requestCode == PLACE_CODE && resultCode == Activity.RESULT_OK) {

            val place = PlacePicker.getPlace(data, this);

            // val bundle = data!!.getBundleExtra("data")

            selectAddress = ("${place!!.name}")
            selectLat = place.latLng.latitude
            selectLon = place.latLng.longitude

            if (TextUtils.isEmpty(selectAddress)) {
                val font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
                location_text.typeface = font
                location_text.setTextColor(ContextCompat.getColor(this@PostActivity, R.color.tag_color))
            } else {
                location_text.typeface = null
                location_text.setTextColor(ContextCompat.getColor(this@PostActivity, R.color.colorBlack))
            }

        }

        if (resultCode == Activity.RESULT_OK && requestCode == 6000){
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    private fun saveImages() {
        val imageId = arrayOfNulls<ImageView>(3)
        imageId[0] = post_image1
        imageId[1] = post_image2
        imageId[2] = post_image3

        post_image1.visibility = View.GONE
        post_image2.visibility = View.GONE
        post_image3.visibility = View.GONE
        var size = imagesList.size

        if (size > 3) {
            post_image_text_more.visibility = View.VISIBLE
            val temp = imagesList.size - 3
            post_image_text_more.setText("+$temp more...")

            size = 3

        } else {
            post_image_text_more.visibility = View.GONE
        }

        for (i in 0 until size) {
            imageId[i]!!.visibility = View.VISIBLE
            Glide.with(this).load(imagesList[i]).apply(RequestOptions().override(80).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true)).into(imageId[i]!!)
        }

        if (isVideo) {
            im_video.visibility = View.VISIBLE
        } else {
            im_video.visibility = View.GONE
        }

    }

    private fun buttonOnOff(imageview: ImageView) {
        if (imageview.tag as Boolean) {
            imageview.setImageResource(R.drawable.ic_switch_off)
            imageview.tag = false
        } else {
            imageview.setImageResource(R.drawable.ic_switch_on)
            imageview.tag = true
        }
    }


    private fun getAllTags() {
        val param = HashMap<String, String>()
        param.put("version", "1.1")
        param.put("type", "read")

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                try {

                    val json = JSONObject(`object` as String)

                    if (json.has("description")) {
                        val tagArray = json.getJSONArray("description")
                        for (i in 0 until tagArray.length()) {
                            val obj = tagArray.getJSONObject(i)

                            if(!obj.getBoolean("isDeleted")){
                                val tagModel = TagModel()
                                tagModel.name = obj.getString("name")
                                tagModel.id = obj.getString("_id")
                                tagList.add(tagModel)
                            }

                        }
                    }
                }catch (e:Exception){
                    e.printStackTrace()
                }

            }

            override fun onError(errorMessage: String) {
                Toast.makeText(this@PostActivity, "Failed to get tags $errorMessage", Toast.LENGTH_SHORT).show()

            }

        }).getTags(param)
    }

    private fun getAllCountries() {

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                countryList.clear()
                countryList.addAll(Constant.countryList)
                countries_view.adapter.notifyDataSetChanged()

                if(countryList.size <=0 ){
                    country_empty.visibility = View.VISIBLE
                }else{
                    country_empty.visibility = View.GONE
                }
            }

            override fun onError(errorMessage: String) {
                if(countryList.size <=0 ){
                    country_empty.visibility = View.VISIBLE
                }else{
                    country_empty.visibility = View.GONE
                }
            }

        }).getAllCountry(Constant.countryList)
    }


    private fun validation(): Boolean {
        if (imagesList.size <= 0) {
            CustomDialog(this).showErrorDialog(getString(R.string.select_atleast_one))
            return false
        }

        if (TextUtils.isEmpty(et_serial.text.toString().trim())) {
            CustomDialog(this).showErrorDialog(getString(R.string.enetr_post_serial))
            return false
        }

        if (TextUtils.isEmpty(et_about.text.toString().trim())) {
            CustomDialog(this).showErrorDialog(getString(R.string.enetr_description))
            return false
        }

        if (!selectTag) {
            CustomDialog(this).showErrorDialog(getString(R.string.choose_atleast_one))
            return false
        }

        /* if(TextUtils.isEmpty(selectAddress)){
             CustomDialog(this).showErrorDialog("Choose Location")
             return false
         }*/

        if (TextUtils.isEmpty(et_amount.text.toString().trim())) {
            CustomDialog(this).showErrorDialog(getString(R.string.enetr_amount))
            return false
        } else {

            try {
                val amount = et_amount.text.toString().trim().toDouble()
                if(amount <= 0){
                    CustomDialog(this).showErrorDialog("Enter valid amount")
                    return false
                }
                if (amount > Constant.MAX_TRANSACTION_AMOUNT){
                    CustomDialog(this).showErrorDialog("Enter amount less than ${Constant.MAX_TRANSACTION_AMOUNT}")
                    return false
                }

            }catch (e:Exception){
                e.printStackTrace()
                CustomDialog(this).showErrorDialog("Enter valid amount")
                return false
            }
        }

        if (!(credit_card_switch.tag as Boolean) && !(cash_switch.tag as Boolean)) {
            CustomDialog(this).showErrorDialog(getString(R.string.payment_mode_error))
            return false
        }


        if (!(auction_switch.tag as Boolean) && !(buy_now_switch.tag as Boolean)) {
            CustomDialog(this).showErrorDialog("Select at least one option ( Auction Or Buy Now )")
            return false
        }

        return true

        //details, latitude, longitude, place_name, user_id, is_instagram, is_twitter, amount, is_auction, buy_now, whatsapp_only, whatsapp_and_call, payment_type:"cash,cc"

        /* cash_switch.tag = false
        insta_switch.tag = false
        twitter_switch.tag = false
        auction_switch.tag = false
        buy_now_switch.tag = false
        whatsapp_switch.tag = false
        credit_card_switch.tag = false
        whatsapp_call_switch.tag = false*/

    }


    private fun credentialsProvider() {

        // Initialize the Amazon Cognito credentials provider
        val credentialsProvider = CognitoCachingCredentialsProvider(
                getApplicationContext(),
                userRef!!.POOL_ID, //indentity pool id
                Constant.REGION)

        setAmazonS3Client(credentialsProvider);
    }

    private fun setAmazonS3Client(credentialsProvider: CognitoCachingCredentialsProvider) {

        // Create an S3 client
        s3 = AmazonS3Client(credentialsProvider)

    }

    private fun setTransferUtility() {
        transferUtility = TransferUtility(s3, applicationContext)
    }

    private fun setFileToUpload() {
        doAsync {
            try {
                Log.e("IMAGE_CUNTER", "***  $imageCounter")
                var file = File(imagesList[imageCounter])
                var btm = Constant.getBitmapFromPath(file.path)

                btm = Constant.getResizedBitmap(btm!!, 1000)
                val file2 = Constant.savePostImage(btm, "")
                if (file2 != null) {
                    file = file2
                }

                val transferObserver = transferUtility!!.upload(Constant.BUCKET_NAME, "${Constant.POST_IMAGE_KEY}/${file!!.name}", file)

                Log.e("transferObserverList", "setFileToUpload")

                transferObserverListener(transferObserver, imageCounter, file)
            } catch (e: Exception) {
                e.printStackTrace()
                imageCounter++
                if (imageCounter == imagesList.size) {
                    uploadPost()
                } else {
                    setFileToUpload()
                }

            }
        }
    }

    private fun setVideoFileToUpload() {
        doAsync {
            try {

                val name = "IMG_${System.currentTimeMillis()}"
                var path = imagesList[imageCounter]
                var btm = ThumbnailUtils.createVideoThumbnail(imagesList[imageCounter], MediaStore.Video.Thumbnails.FULL_SCREEN_KIND)
                btm = Constant.getResizedBitmap(btm!!, 200)

                val file2 = Constant.savePostImage(btm, name)
                val file = File(path)


                val transferObserver = transferUtility!!.upload(Constant.BUCKET_NAME, "${Constant.POST_IMAGE_KEY}/$name.mp4", file)
                transferObserver.setTransferListener(object : TransferListener {
                    override fun onStateChanged(id: Int, state: TransferState?) {
                        if (state!!.name.equals("COMPLETED", true)) {
                            val transferObserver1 = transferUtility!!.upload(Constant.BUCKET_NAME, "${Constant.POST_IMAGE_KEY}/$name.jpg", file2)
                            transferObserverListener(transferObserver1, imageCounter, file2!!)
                        } else if (state!!.name.equals("FAILED", true)) {
                            progress!!.dismiss()
                            Toast.makeText(this@PostActivity, getString(R.string.failed_to_upload), Toast.LENGTH_SHORT).show()
                        }
                        // Log.e("transferObserverList","onStateChanged ${state!!.name}  $imageCounter")
                    }

                    override fun onError(id: Int, ex: Exception?) {
                        Toast.makeText(this@PostActivity, getString(R.string.failed_to_upload), Toast.LENGTH_SHORT).show()
                        //Log.e("transferObserverList","onError")
                    }

                    override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                        val per = (bytesCurrent / bytesTotal) * 100
                        //Log.e("transferObserverList","onProgressChanged $id   **${bytesCurrent} *** $bytesTotal  ${per}")
                    }
                })
            } catch (e: Exception) {
                e.printStackTrace()
                imageCounter++
                if (imageCounter == imagesList.size) {
                    uploadPost()
                } else {
                    setFileToUpload()
                }

            }
        }
    }


    private fun transferObserverListener(transferObserver: TransferObserver, position: Int, file: File) {

        transferObserver.setTransferListener(object : TransferListener {
            override fun onStateChanged(id: Int, state: TransferState?) {
                if (state!!.name.equals("COMPLETED", true)) {
                    imgFileList.add(file)
                    imageCounter++
                    if (imageCounter == imagesList.size) {
                        imageCounter = 0
                        uploadPost()
                    } else {
                        setFileToUpload()
                    }
                } else {
                    if (state!!.name.equals("FAILED", true)) {
                        imageCounter++
                        if (imageCounter == imagesList.size) {
                            uploadPost()
                        } else {
                            setFileToUpload()
                        }
                    }
                }
                Log.e("transferObserverList", "onStateChanged ${state!!.name}  $position")
            }

            override fun onError(id: Int, ex: Exception?) {
                ex!!.printStackTrace()
                Log.e("transferObserverList", "onError ${ex.message}")
                imageCounter++
                if (imageCounter == imagesList.size) {
                    uploadPost()
                } else {
                    setFileToUpload()
                }
            }

            override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {

                val per = (bytesCurrent / bytesTotal) * 100

                //progress!!.setMessage("Upload Image ${imageCounter+1} : $per%")
                Log.e("transferObserverList", "onProgressChanged $id   **${bytesCurrent} *** $bytesTotal  ${per}")
            }
        });
    }


    private fun uploadPost() {
        Constant.deletePostImage()

        //progress!!.setMessage("Uploading Post")
        val param = JSONObject()
        param.put("version", "1.1")
        param.put("post_user_id", MyApplication.instance.getUserPreferences().id)
        param.put("is_video", isVideo)
        param.put("is_sold", false)
        param.put("is_story", false)
        param.put("status", "Available")
        param.put("is_twitter", (twitter_switch.tag as Boolean))
        param.put("is_instagram", (insta_switch.tag as Boolean))
        param.put("is_auction", (auction_switch.tag as Boolean))
        param.put("buy_now", (buy_now_switch.tag as Boolean))
        param.put("whatsapp_only", (whatsapp_switch.tag as Boolean))
        param.put("whatsapp_and_call", (whatsapp_call_switch.tag as Boolean))
        param.put("place_name", selectAddress)
        param.put("language", selectLanguage)
        param.put("description", et_about.text.toString().trim())
        param.put("product_serial", et_serial.text.toString().trim())
        param.put("only_user_from_selected_country", check_box.isChecked)

        if(TextUtils.isEmpty(selectAddress)){
            param.put("latitude", "")
            param.put("longitude", "")
        }else{
            param.put("latitude", "$selectLat")
            param.put("longitude", "$selectLon")

        }

        var selectedCountries = ""
        countryList.forEach {
            if (it.isCheck) {
                selectedCountries += "${it.id},"
            }
        }

        var selectedTags = ""
        tagList.forEach {
            if (it.isCheck) {
                selectedTags += "${it.id},"
            }
        }

        var imageUrl = ""
        imgFileList.forEach {
            imageUrl += "${s3!!.getResourceUrl(Constant.BUCKET_NAME, "${Constant.POST_IMAGE_KEY}/${it.name}")},"
        }


        if (!TextUtils.isEmpty(selectedTags)) {
            val index = selectedTags.lastIndexOf(",")
            if (index > 0) {
                selectedTags = selectedTags.substring(0, index)
            }
        }

        if (!TextUtils.isEmpty(selectedCountries)) {
            val index = selectedCountries.lastIndexOf(",")
            selectedCountries = selectedCountries.substring(0, index)
        }

        if (!TextUtils.isEmpty(imageUrl)) {
            val index = imageUrl.lastIndexOf(",")
            imageUrl = imageUrl.substring(0, index)
        }

        param.put("country", selectedCountries)
        param.put("image_url", imageUrl)
        param.put("tag", selectedTags)

        var payment_type = ""

        if ((credit_card_switch.tag as Boolean)) {
            payment_type = "cc"
        }

        if ((cash_switch.tag as Boolean)) {
            if (TextUtils.isEmpty(payment_type)) {
                payment_type = "cash"
            } else {
                payment_type += ",cash"
            }
        }

        param.put("payment_type", payment_type)
        param.put("amount", DecimalFormat("##.##").format(et_amount.text.toString().trim().toDouble()))


        Log.e("POST_PARAM", "$param")
        progress!!.dismiss()


        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                val json = JSONObject(`object`.toString())
                Toast.makeText(this@PostActivity, getString(R.string.post_success_upload), Toast.LENGTH_SHORT).show()

                if ((insta_switch.tag as Boolean) || (twitter_switch.tag as Boolean)){
                    val intent = Intent(this@PostActivity,PostAppActivity::class.java)
                    intent.putExtra("filePath",imagesList[0])
                    intent.putExtra("insta",(insta_switch.tag as Boolean))
                    intent.putExtra("twitter",(twitter_switch.tag as Boolean))
                    //intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivityForResult(intent,6000)
                    finish()
                }else{
                    Log.e("ASDASDSADASDASDSAD","herfs 60dsf00")
                    setResult(Activity.RESULT_OK)
                    finish()
                }

            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                CustomDialog(this@PostActivity).showErrorDialog("Failed to post $errorMessage")
            }

        }).postCreate(param)
    }


}
