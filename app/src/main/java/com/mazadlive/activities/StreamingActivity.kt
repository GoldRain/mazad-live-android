package com.mazadlive.activities

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Rect
import android.hardware.Camera
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.Surface
import android.view.SurfaceHolder
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.DeleteObjectRequest
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.Socket.SocketEventsListener
import com.mazadlive.Socket.SocketManager
import com.mazadlive.adapters.liveCommentAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.helper.Constant
import com.mazadlive.models.CommentModel
import com.mazadlive.models.LiveProductModel
import com.mazadlive.models.LiveStoryModel
import com.mazadlive.models.UserModel
import com.mazadlive.parser.StremDataParse
import com.mazadlive.utils.MyApplication
import com.red5pro.streaming.R5Connection
import com.red5pro.streaming.R5Stream
import com.red5pro.streaming.R5StreamProtocol
import com.red5pro.streaming.config.R5Configuration
import com.red5pro.streaming.event.R5ConnectionEvent
import com.red5pro.streaming.event.R5ConnectionListener
import com.red5pro.streaming.source.R5Camera
import com.red5pro.streaming.source.R5Microphone
import kotlinx.android.synthetic.main.activity_streaming.*
import kotlinx.android.synthetic.main.view_ads.view.*
import kotlinx.android.synthetic.main.zz_view_zztest.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.io.File
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.forEach

class StreamingActivity : BaseActivity(), SurfaceHolder.Callback, R5ConnectionListener {

    private val TAG = "StreamingActivity"
    var TIMER_NEXT_PRODUCT : Long = 3
    var countDown = CountDown()
    var isCountDownRun = false

    companion object {
        var mapComment = HashMap<String, CommentModel>()
        var isStream = false
    }

    override fun surfaceCreated(surfaceHolder: SurfaceHolder) {
        try {
            camera!!.setPreviewDisplay(surfaceHolder)

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {

    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {

    }

    /* connection listioner.................*/
    override fun onConnectionEvent(event: R5ConnectionEvent) {
        Log.d(TAG, ":onConnectionEvent " + " ${event.name}  **** "+ event.message)
        if (event.name === R5ConnectionEvent.DISCONNECTED.name) {
          runOnUiThread {
              stop()
          }
            /*val h = Handler(Looper.getMainLooper())
            h.post {
                val alertDialog = AlertDialog.Builder(this@StreamingActivity).create()
                alertDialog.setTitle("Error")
                alertDialog.setMessage("License is Invalid")
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK"

                ) { dialog, which -> dialog.dismiss() }
                alertDialog.show()
            }*/
        }

        if(event.name === R5ConnectionEvent.NET_STATUS.name && (""+event.message).equals("NetStream.Play.StreamDry")){
            runOnUiThread {
                stop()
            }
        }
        if (event.name === R5ConnectionEvent.START_STREAMING.name) {
            runOnUiThread {

                cl_top.visibility = View.VISIBLE
                cl_bottom1.visibility = View.VISIBLE
                mTimerLay.visibility = View.VISIBLE
                mTimerView.visibility = View.VISIBLE
                im_item_info.visibility = View.VISIBLE
                if (productList.size < 2){
                    mNextItem.visibility = View.GONE
                }
                if (!mTimerView.isRunning()) {
                    mTimerView.visibility = View.VISIBLE
                    mTimerView.startTimer()
                }

                if (!socketManager.isConnected) {
                    socketManager.socket!!.close()
                    socketManager.socket!!.open()
                    socketManager.connect()
                }

                callApi()

            }
        }
    }

    private val socketEventsListener = object : SocketEventsListener() {
        override fun onConnected(vararg args: Any?) {

        }

        override fun onDisconnected(vararg args: Any?) {

        }

        override fun onStartStream(args: String?) {
            Log.e(TAG, "onStartStream  $args")
            //create storyModel object..
        }

        override fun onUpdateStream(args: String?) {
            Log.e(TAG, "onUpdateStream ${Constant.CURRENT_STREAM_ID.equals(storyModel.id)} $args")
            val jsonObject = JSONObject(args)


            val holdSize = mapComment.size
            StremDataParse().parseStream(storyModel!!, jsonObject)

            Log.e(TAG, "CHECK_IDD $holdSize  ${mapComment.size} ${storyModel!!.commentsList.size} ${storyModel.status}")
            if (storyModel.status.equals("stopped")) {
                showStopDialog("")

            } else {
                runOnUiThread {

                    if (holdSize < mapComment.size && storyModel!!.commentsList.size > 0) {
                        commentList.add(storyModel!!.commentsList[storyModel!!.commentsList.size - 1])
                        dupCommentList.add(storyModel!!.commentsList[storyModel!!.commentsList.size - 1])
                        notifyComments()
                    }

                    if (storyModel!!.viewerCounts < 0) {
                        count_viewer.setText("0")
                    } else {
                        count_viewer.setText("${storyModel!!.viewerCounts}")
                    }

                }
            }

        }
    }

    private val THUMB_IMAGE_DELAY = (50 * 1000).toLong()
    private val ONLINE_DELAY = (60000).toLong()
    private var count = 1
    private var sName = ""
    private var currentBid: CommentModel? = null
    private var currentProductPosition = 0

    private lateinit var progress: CustomProgress

    private var productList = ArrayList<LiveProductModel>()
    private var commentList = ArrayList<CommentModel>()
    private var dupCommentList = ArrayList<CommentModel>()
    private var commentListBookMark = ArrayList<CommentModel>()
    private var currentCamMode = Camera.CameraInfo.CAMERA_FACING_FRONT

    protected var camera: R5Camera? = null
    protected var isPublishing = false
    protected var stream: R5Stream? = null
    protected var camOrientation: Int = 0
    internal var count_val = 4
    protected var cam: Camera? = null
    private lateinit var socketManager: SocketManager
    private var s3: AmazonS3Client? = null
    private var transferUtility: TransferUtility? = null
    private val handlerThumb = Handler()
    private var storyModel = LiveStoryModel()
    private var onLineHandler = Handler()
    private var stopShow = false

    val bookMarkComment = HashMap<String, CommentModel>()

    private val runalbeThumb = object : Runnable {
        override fun run() {
            try {
                if (stream == null) {
                    return
                }

                var btm = stream!!.streamImage
                btm = Constant.getResizedBitmap(btm!!, 200)
                val file2 = Constant.saveStreamThumbImage(btm)
                val transferObserver = transferUtility!!.upload(Constant.BUCKET_NAME, "${Constant.STREAM_IMAGE_KEY}/${file2!!.name}", file2)

                transferObserverListener(transferObserver, file2)
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }

    private val runableOnline = object : Runnable {
        override fun run() {
            Constant.setOnLine(true)
            onLineHandler.postDelayed(this, 60000)
        }
    }

    private fun notifyComments() {
        commentview.adapter.notifyItemChanged(dupCommentList.size - 1)
        commentview.scrollToPosition(dupCommentList.size - 1)
    }

    override fun onStart() {
        Constant.SHOW_ADD = false
        socketManager.setSocketEventsListener(false, socketEventsListener)
        isStream = true
        super.onStart()
    }

    override fun onResume() {

        super.onResume()
    }

    override fun onPause() {

        super.onPause()
    }

    override fun onStop() {
        Constant.SHOW_ADD = true
        socketManager.setSocketEventsListener(false, null)
        isStream = false
        Constant.CURRENT_STREAM_ID = ""
        if (isPublishing) {
            stop()
        }
        onLineHandler.removeCallbacks(runableOnline)
        super.onStop()
    }

    override fun onBackPressed() {
        backAction()
    }

    private fun backAction() {

        if (isPublishing && commentview.visibility == View.GONE) {
            hideBookMarkView()
        } else {

            if (isPublishing) {
                if (bookMarkComment.size > 0) {
                    stop()
                    CustomDialog(this).showDialogBidComment("You have bookmarked on ${bookMarkComment.size} bids.", object : DialogListener() {
                        override fun okClick(any: Any) {
                            if (any == 1) {
                                openBookMarkView()
                                tx_slide.setText(getString(R.string.hint_exit))
                            } else {
                                Constant.CURRENT_STREAM_ID = ""
                                super@StreamingActivity.onBackPressed()
                            }
                        }
                    })

                } else {
                    Constant.CURRENT_STREAM_ID = ""
                    super.onBackPressed()
                }
            } else {
                Constant.CURRENT_STREAM_ID = ""
                super.onBackPressed()
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_streaming)
        supportActionBar!!.hide()
        mapComment = HashMap()
        progress = CustomProgress(this)
        storyModel = LiveStoryModel()
        productList = intent.getParcelableArrayListExtra("product")

        checkAndRequestPermissions()
        flash.tag = false
        tx_slide.visibility = View.GONE
        doAsync {
            credentialsProvider()
            setTransferUtility();
        }

        socketManager = MyApplication.instance.getSocketManager()
        commentview.layoutManager = LinearLayoutManager(this)
        commentview.adapter = liveCommentAdapter(this, dupCommentList)
        (commentview.adapter as liveCommentAdapter).isAdmin = true
        (commentview.adapter as liveCommentAdapter).listener = OnItemclick()

        commentviewBookmark.layoutManager = LinearLayoutManager(this)
        commentviewBookmark.adapter = liveCommentAdapter(this, commentListBookMark)
        (commentviewBookmark.adapter as liveCommentAdapter).isAdmin = true
        (commentviewBookmark.adapter as liveCommentAdapter).listener = OnItemclick()

        cl_bottom1.visibility = View.GONE
        counting.setVisibility(View.INVISIBLE)

        mic.setTag(false)

        swap.setOnClickListener {
            swapCamera()
        }

        flash.setOnClickListener {
            flashLight()
        }

        mic.setOnClickListener(View.OnClickListener {
            if ((mic.getTag() as Boolean)) {
                mic.setImageResource(R.drawable.ic_mic_on_black)
                if (isPublishing && stream != null) {
                    stream!!.restrainAudio(false)

                }
                mic.setTag(false)

            } else {
                mic.setImageResource(R.drawable.ic_mic_off_black)
                if (stream != null) {
                    stream!!.restrainAudio(true)

                }
                mic.setTag(true)
            }
        })

        publishButton.setOnClickListener {
            showAnimation()
        }

        close.setOnClickListener {
            backAction()
        }

        tx_slide.setOnClickListener {
            when (tx_slide.text.toString().trim()) {
                getString(R.string.hint_view_bookmark_bids) -> {
                    openBookMarkView()
                }

                getString(R.string.hint_view_live_bids) -> {
                    hideBookMarkView()
                }

                getString(R.string.hint_exit) -> {
                    super.onBackPressed()
                }
            }
        }

        im_item_info.setOnClickListener {
            showIntemInfoDialog()
        }

        mNextItem.setOnClickListener {
            if (currentProductPosition < productList.size){
                currentProductPosition += 1
                uploadProductImage()
                sendComment(getString(R.string.action_item_changed))
            }

            if (currentProductPosition == (productList.size-1)){
                mNextItem.visibility = View.GONE
            }
        }

        mTimer3.setOnClickListener {
            if (isCountDownRun){
                countDown.cancel()
                mTimer3.text = "Begin in 3min Timer"
                isCountDownRun = false
            }else{
                countDown.start()
                isCountDownRun = true
            }
        }

        saveData()

        showAnimation()

        onLineHandler.post(runableOnline)
    }

    inner class CountDown() : CountDownTimer(TIMER_NEXT_PRODUCT*60000,1000){
        override fun onFinish() {
            isCountDownRun = false
            mTimer3.text = "Begin in 3min Timer"
            showDialogForAwardList()
        }

        override fun onTick(millisUntilFinished: Long) {
            val showTime = millisecondsToTime(millisUntilFinished)
            mTimer3.text = "Ending $showTime"
            when(showTime){
                "3:00" -> {sendComment("Hurry Up!! yo have 3 minutes to win the product")}
                "2:00" -> {sendComment("Hurry Up!! yo have 2 minutes to win the product")}
                "1:00" -> {sendComment("Hurry Up!! yo have 1 minutes to win the product")}
                "0:30" -> {sendComment("Hurry Up!! yo have 30 secound to win the product")}
                "0:10" -> {sendComment("Hurry Up!! yo have 10 secound to win the product")}
            }
        }
    }

    private fun millisecondsToTime(milliseconds: Long): String {
        val minutes = milliseconds / 1000 / 60
        val seconds = milliseconds / 1000 % 60
        val secondsStr = java.lang.Long.toString(seconds)
        val secs: String
        if (secondsStr.length >= 2) {
            secs = secondsStr.substring(0, 2)
        } else {
            secs = "0$secondsStr"
        }
        return minutes.toString() + ":" + secs
    }


    private fun credentialsProvider() {

        // Initialize the Amazon Cognito credentials provider
        val credentialsProvider = CognitoCachingCredentialsProvider(
                getApplicationContext(),
                MyApplication.instance.getUserPreferences().POOL_ID, //indentity pool id
                Constant.REGION)

        setAmazonS3Client(credentialsProvider);
    }

    private fun setAmazonS3Client(credentialsProvider: CognitoCachingCredentialsProvider) {
        // Create an S3 client
        s3 = AmazonS3Client(credentialsProvider)
    }

    private fun setTransferUtility() {
        transferUtility = TransferUtility(s3, applicationContext)
    }

    private fun updateCommentView(position: Int) {

       /* if (commentview.visibility == View.VISIBLE) {
            val com = dupCommentList[position]
            if (com.isBookmark) {
                bookMarkComment.remove(com.id)
            } else {
                bookMarkComment.put(com.id, com)
            }
            commentList[position].isBookmark = !com.isBookmark
            (commentview.adapter as liveCommentAdapter).notifyItemChanged(position)
        } else {
            val com = commentListBookMark[position]
            if (com.isBookmark) {
                bookMarkComment.remove(com.id)
            } else {
                bookMarkComment.put(com.id, com)
            }

            commentListBookMark[position].isBookmark = !com.isBookmark
            (commentviewBookmark.adapter as liveCommentAdapter).notifyItemChanged(position)
        }*/

        val com = dupCommentList[position]
        if (com.isBookmark) {
            bookMarkComment.remove(com.id)
            dupCommentList[position].isBookmark = false
        } else {
            bookMarkComment.put(com.id, com)
            dupCommentList[position].isBookmark = true
        }

        commentview.adapter.notifyItemChanged(position)

        if (bookMarkComment.size > 0) {
            tx_slide.visibility = View.VISIBLE
        } else {
            if (commentview.visibility == View.VISIBLE) {
                tx_slide.visibility = View.GONE
            }
        }
    }

    private fun openBookMarkView() {
        commentListBookMark.clear()
        bookMarkComment.keys.forEach {
            commentListBookMark.add(bookMarkComment.get(it)!!)
        }

        dupCommentList.clear()
        dupCommentList.addAll(commentListBookMark)




        val anim = AnimationUtils.loadAnimation(this, R.anim.abc_slide_in_bottom)
        val anim2 = AnimationUtils.loadAnimation(this, R.anim.fade_out)
        //  anim2.duration = 1500
        commentview.startAnimation(anim2)

        anim2.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                commentview.visibility = View.GONE
                commentview.startAnimation(anim)
                commentview.visibility = View.VISIBLE
                commentview.adapter.notifyDataSetChanged()

            }

            override fun onAnimationStart(animation: Animation?) {

            }

        })
        tx_slide.setText(getString(R.string.hint_view_live_bids))

    }

    private fun hideBookMarkView() {
        val anim = AnimationUtils.loadAnimation(this, R.anim.abc_slide_in_bottom)
        val anim2 = AnimationUtils.loadAnimation(this, R.anim.fade_out)
        // anim2.duration = 1500

        dupCommentList.clear()
        dupCommentList.addAll(commentList)
        commentview.startAnimation(anim2)

        anim2.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                commentview.visibility = View.GONE
                commentview.startAnimation(anim)
                commentview.visibility = View.VISIBLE
                commentview.adapter.notifyDataSetChanged()
            }

            override fun onAnimationStart(animation: Animation?) {

            }

        })
        tx_slide.setText(getString(R.string.hint_view_bookmark_bids))
    }

    private fun showStopDialog(text: String) {

        if (!stopShow) {
            runOnUiThread {
                stopShow = true
                CustomDialog(this@StreamingActivity).showLiveStreamEndDialog(text, object : DialogListener() {
                    override fun okClick() {
                        stopShow = false
                        finish()
                    }
                })
            }
        }

    }

    private fun showIntemInfoDialog() {

        CustomDialog(this).showItemInfoDialog(productList, currentProductPosition, object : DialogListener() {

            override fun okClick() {

            }
        })
    }

    private fun showBidAwarededDialog() {

        CustomDialog(this@StreamingActivity).showBidAwardedDialog(CommentModel(), object : DialogListener() {
            override fun okClick(any: Any) {
                when (any.toString()) {

                    "next" -> {
                        currentProductPosition += 1
                        uploadProductImage()
                    }

                    "stop" -> {
                        stop()
                    }
                }

            }
        }, (currentProductPosition == productList.size - 1))


    }

    private fun checkAndRequestPermissions() {
        val permissionReadStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        val permissionWriteStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val permissionContacts = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
        val readSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val listPermissionsNeeded = ArrayList<String>()

        val array = arrayOfNulls<String>(4)
        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
            array[0] = Manifest.permission.CAMERA
        }
        if (permissionReadStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
            array[1] = Manifest.permission.READ_EXTERNAL_STORAGE
        }
        if (permissionWriteStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            array[2] = Manifest.permission.WRITE_EXTERNAL_STORAGE
        }
        if (permissionContacts != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECORD_AUDIO)
            array[3] = Manifest.permission.RECORD_AUDIO
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, array, 1)
        }
    }

    private fun showAnimation() {

        val anim = AnimationUtils.loadAnimation(this@StreamingActivity, R.anim.fade_in)
        val anim2 = AnimationUtils.loadAnimation(this@StreamingActivity, R.anim.fade_out)

        anim2.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {

            }

            override fun onAnimationEnd(animation: Animation) {
                counting.visibility = View.INVISIBLE
            }

            override fun onAnimationRepeat(animation: Animation) {

            }
        })


        val listener = object : Animation.AnimationListener {

            override fun onAnimationStart(animation: Animation) {

            }

            override fun onAnimationEnd(animation: Animation) {
                counting.startAnimation(anim2)
            }

            override fun onAnimationRepeat(animation: Animation) {

            }
        }

        anim.setAnimationListener(listener)

        if (isPublishing) {

            runOnUiThread {
                onPublishToggle()
            }
        } else {
            val handler = Handler()
            handler.post(object : Runnable {
                override fun run() {
                    if (count_val > 1) {
                        count_val--
                        counting.visibility = View.VISIBLE
                        counting.startAnimation(anim)
                        counting.text = count_val.toString() + ""

                        handler.postDelayed(this, 1200)

                    } else {
                        counting.visibility = View.VISIBLE
                        counting.startAnimation(anim)
                        counting.text = getString(R.string.hint_go)
                        count_val = 4
                        runOnUiThread {
                            onPublishToggle()
                        }
                    }
                }
            })
        }
    }

    private fun saveData() {

        count_viewer.text = "${storyModel.viewerCounts}"
        val userRef = MyApplication.instance.getUserPreferences()
        user_name.text = userRef.username
        if (!isFinishing) {
            Glide.with(this).load(userRef.profile).apply(RequestOptions().override(80).error(R.drawable.dummy)).into(image)
        }
    }

    protected fun openFrontFacingCameraGingerbread(): Camera? {
        var cameraCount = 0
        var cam: Camera? = null
        val cameraInfo = Camera.CameraInfo()
        cameraCount = Camera.getNumberOfCameras()
        for (camIdx in 0 until cameraCount) {
            Camera.getCameraInfo(camIdx, cameraInfo)
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                try {
                    cam = Camera.open(camIdx)
                    camOrientation = cameraInfo.orientation
                    applyDeviceRotation()
                    break
                } catch (e: RuntimeException) {
                    e.printStackTrace()
                }
            }
        }

        return cam
    }

    protected fun applyDeviceRotation() {
        val rotation = windowManager.defaultDisplay.rotation
        var degrees = 0
        when (rotation) {
            Surface.ROTATION_0 -> degrees = 0
            Surface.ROTATION_90 -> degrees = 270
            Surface.ROTATION_180 -> degrees = 180
            Surface.ROTATION_270 -> degrees = 90
        }

        val screenSize = Rect()
        windowManager.defaultDisplay.getRectSize(screenSize)
        val screenAR = screenSize.width() * 1.0f / (screenSize.height() * 1.0f)
        if (screenAR > 1 && degrees % 180 == 0 || screenAR < 1 && degrees % 180 > 0)
            degrees += 180

        //  println("Apply Device Rotation: $rotation, degrees: $degrees")

        camOrientation += degrees

        camOrientation %= 360
    }

    private fun onPublishToggle() {

        if (isPublishing) {
            stop()
            publishButton.setImageResource(R.drawable.round_red)
            mTimerView.setVisibility(View.GONE)
            mTimerView.stopTimer()
        } else {
            start()
            /*publishButton.setImageResource(R.drawable.ic_pause_red)
            if (!mTimerView.isRunning()) {
                mTimerView.setVisibility(View.VISIBLE)
                mTimerView.startTimer()
            }*/
        }
        isPublishing = !isPublishing

    }

    private fun start() {
        //Create the configuration from the values.xml

        MyApplication.instance

        val config = MyApplication.instance.getRedConfig()

        val connection = R5Connection(config)

        stream = R5Stream(connection)

        cam = openFrontFacingCameraGingerbread()

        cam!!.setDisplayOrientation((camOrientation + 180) % 360)

        camera = R5Camera(cam, 640, 360)
        camera!!.setBitrate(1024)
        camera!!.setOrientation(camOrientation)
        camera!!.setFramerate(15)

        val mic = R5Microphone()
        stream!!.attachMic(mic)
        preview.attachStream(stream)

        stream!!.attachCamera(camera)

        preview.showDebugView(false)

        stream!!.setListener(this)

        sName = "${MyApplication.instance.getUserPreferences().username}_${System.currentTimeMillis()}"
        sName = sName.replace(" ", "")
        stream!!.publish(sName, R5Stream.RecordType.Live)
        cam!!.startPreview()

    }

    private fun stop() {

        stream?.let {
            it.stop()
            if (it.getVideoSource() != null) {
                val c = (it.getVideoSource() as R5Camera).camera
                c.stopPreview()
                c.release()
            }else{
                cam?.stopPreview()
                cam?.release()
            }


            stream = null
        }

        mTimerView.stopTimer()

       Handler().postDelayed(object :Runnable{
           override fun run() {



               isPublishing = false
               val jsonObject = JSONObject()
               jsonObject.put("user_id", MyApplication.instance.getUserPreferences().id)

               if (storyModel != null) {
                   jsonObject.put("stream_id", storyModel!!.id)
               }

               if (!socketManager.isConnected) {
                   socketManager.socket!!.close()
                   socketManager.socket!!.open()
                   socketManager.connect()
               }


               ServiceRequest(object : ApiResponseListener {
                   override fun onCompleted(`object`: Any) {

                   }
                   override fun onError(errorMessage: String) {

                   }

               }).stopMyStrem()



               handlerThumb.removeCallbacks(runalbeThumb)
               socketManager.socket!!.emit(SocketManager.EMIT_STOP_STREAM, jsonObject)
           }

       },200)

    }

    private fun transferObserverListener(transferObserver: TransferObserver, file2: File?) {

        transferObserver.setTransferListener(object : TransferListener {
            override fun onStateChanged(id: Int, state: TransferState?) {
                if (state!!.name.equals("COMPLETED", true)) {
                    val url = s3!!.getResourceUrl(Constant.BUCKET_NAME, "${Constant.STREAM_IMAGE_KEY}/${file2!!.name}")
                    Constant.deleteStreameThumbImage()
                    uploadThumb(url)
                } else if (state.name.equals("FAILED", true)) {
                    handlerThumb.postDelayed(runalbeThumb, ONLINE_DELAY)
                }

            }

            override fun onError(id: Int, ex: java.lang.Exception?) {
                Log.e(TAG, "thumb upload onError")
                handlerThumb.postDelayed(runalbeThumb, ONLINE_DELAY)
            }

            override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                val per = (bytesCurrent / bytesTotal) * 100
            }
        });
    }

    private fun deleteThumb(oldThumb: String) {
        if (!TextUtils.isEmpty(oldThumb)) {
            doAsync {
                val fName = oldThumb.substring(oldThumb.lastIndexOf("/"))

                s3!!.deleteObject(DeleteObjectRequest(Constant.BUCKET_NAME, "${Constant.STREAM_IMAGE_KEY}/$fName"))
            }
        }
    }

    private fun swapCamera() {
        if (stream == null) {
            return
        }
        flash.tag = false
        flash.setImageResource(R.drawable.ic_flash_off_black)
        val publishCam = stream!!.getVideoSource() as R5Camera

        var newCam: Camera? = null

        //NOTE: Some devices will throw errors if you have a camera open when you attempt to open another
        publishCam.camera.stopPreview()
        publishCam.camera.release()

        //NOTE: The front facing camera needs to be 180 degrees further rotated than the back facing camera
        var rotate = 0
        if (currentCamMode == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            newCam = openBackFacingCameraGingerbread()
            rotate = 0
            if (newCam != null)
                currentCamMode = Camera.CameraInfo.CAMERA_FACING_BACK
        } else {
            newCam = openFrontFacingCameraGingerbread()
            rotate = 180
            if (newCam != null)
                currentCamMode = Camera.CameraInfo.CAMERA_FACING_FRONT
        }

        if (newCam != null) {

            newCam.setDisplayOrientation((camOrientation + rotate) % 360)

            publishCam.camera = newCam
            publishCam.orientation = camOrientation

            newCam.startPreview()
            cam = newCam
        }
    }

    private fun hasFlash(): Boolean {
        if (camera == null) {
            return false;
        }

        val parameters = cam!!.getParameters();

        if (parameters.getFlashMode() == null) {
            return false;
        }

        val supportedFlashModes = parameters.getSupportedFlashModes();
        if (supportedFlashModes == null || supportedFlashModes.isEmpty() || supportedFlashModes.size == 1 && supportedFlashModes.get(0).equals(Camera.Parameters.FLASH_MODE_OFF)) {
            return false;
        }

        return true;
    }

    private fun flashLight() {
        Log.e(TAG, "HAS_FLASH ${hasFlash()}")
        if (hasFlash()) {
            if (flash.tag as Boolean) {
                val param = cam!!.parameters
                param.flashMode = Camera.Parameters.FLASH_MODE_OFF
                cam!!.parameters = param
                cam!!.startPreview()
                flash.tag = false
                flash.setImageResource(R.drawable.ic_flash_off_black)
            } else {
                val param = cam!!.parameters
                param.flashMode = Camera.Parameters.FLASH_MODE_TORCH
                cam!!.parameters = param

                stream!!.attachCamera(camera)
                cam!!.startPreview()
                flash.tag = true
                flash.setImageResource(R.drawable.ic_flash_on_black)
            }
        } else {
            Toast.makeText(this, getString(R.string.dont_have_flash), Toast.LENGTH_SHORT).show()
        }
    }

    protected fun openBackFacingCameraGingerbread(): Camera? {
        var cameraCount = 0
        var cam: Camera? = null
        val cameraInfo = Camera.CameraInfo()
        cameraCount = Camera.getNumberOfCameras()
        println("Number of cameras: $cameraCount")
        for (camIdx in 0 until cameraCount) {
            Camera.getCameraInfo(camIdx, cameraInfo)
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                try {
                    cam = Camera.open(camIdx)
                    camOrientation = cameraInfo.orientation
                    applyInverseDeviceRotation()
                    break
                } catch (e: RuntimeException) {
                    e.printStackTrace()
                }

            }
        }

        return cam
    }

    protected fun applyInverseDeviceRotation() {
        val rotation = windowManager.defaultDisplay.rotation
        var degrees = 0
        when (rotation) {
            Surface.ROTATION_0 -> degrees = 0
            Surface.ROTATION_90 -> degrees = 270
            Surface.ROTATION_180 -> degrees = 180
            Surface.ROTATION_270 -> degrees = 90
        }

        camOrientation += degrees

        camOrientation = camOrientation % 360
    }

    private fun handleStreamResponse(str: String) {
        val jsonObject = JSONObject(str)
        if (!jsonObject.getBoolean("error")) {

            if (jsonObject.has("stream")) {
                val streamObj = jsonObject.getJSONObject("stream")


                if (streamObj.has("_id")) {
                    storyModel!!.id = streamObj.getString("_id")
                }

                if (streamObj.has("user_id")) {
                    storyModel!!.user_id = streamObj.getString("user_id")
                }

                if (streamObj.has("stream_name")) {
                    storyModel!!.streamName = streamObj.getString("stream_name")
                }
                if (streamObj.has("status")) {
                    storyModel!!.status = streamObj.getString("status")
                }

                if (streamObj.has("viewerCount")) {
                    storyModel!!.viewerCounts = streamObj.getInt("viewerCount")
                }

                if (streamObj.has("start_price")) {
                    storyModel.price = streamObj.getString("start_price")
                }

                if (streamObj.has("comment")) {
                    val comArray = streamObj.getJSONArray("comment")
                    for (i in 0 until comArray.length()) {
                        val comObj = comArray.getJSONObject(i)
                    }
                }

            }

            if (jsonObject.has("userDetails")) {

                val userObj = jsonObject.getJSONObject("userDetails")
                val userData = UserModel()
                if (userObj.has("username")) {
                    userData.username = userObj.getString("username")
                }

                if (userObj.has("country_code")) {
                    userData.countryCode = userObj.getString("country_code")
                }

                if (userObj.has("phone")) {
                    userData.phone = userObj.getString("phone")
                }

                if (userObj.has("email")) {
                    userData.email = userObj.getString("email")
                }

                if (userObj.has("password")) {
                    //userData.password = userObj.getString("password")
                }

                if (userObj.has("type")) {
                    userData.type = userObj.getString("type")
                }

                if (userObj.has("profilePhoto")) {
                    userData.profile = userObj.getString("profilePhoto")
                }

                if (userObj.has("_id")) {
                    userData.id = userObj.getString("_id")
                }

                storyModel!!.userData = userData

            }

            Constant.CURRENT_STREAM_ID = storyModel.id

            runOnUiThread {
                saveData()
            }

        }
    }

    inner class OnItemclick : liveCommentAdapter.OnClick {
        override fun pin(position: Int) {
            Log.e(TAG, "PIN_SHOW")


            val model = dupCommentList[position]
            if (!model.isBookmark) {
                bookmarkComment(position, true)
            }else{
              showDialogForAwardList()
            }


        }

        override fun addBookmark(position: Int) {
            bookmarkComment(position)
            //bookmarkComment(position)
        }

    }

    private fun showDialogForAwardList() {

        val list = ArrayList<CommentModel>()
        bookMarkComment.keys.forEach {
            if(bookMarkComment.get(it)!!.isBookmark){
                list.add(bookMarkComment.get(it)!!)
            }
        }

        CustomDialog(this@StreamingActivity).showAwardBidList(list, object : DialogListener() {
            override fun okClick() {

                Log.e("ASDASDASDASDASD","Come herer ${list.size}")
                var allBids = ""
                for (i in 0 until list.size){
                    if (list[i].isCheck){
                        allBids = allBids+list[i].id+","
                    }
                }
                if (allBids.length > 0){
                    awardBidCall(allBids.substring(0,allBids.lastIndex))
                }

            }
        })
    }

    private fun sendComment(message: String) {

        val jsonObject = JSONObject()
        jsonObject.put("user_id", MyApplication.instance.getUserPreferences().id)
        jsonObject.put("type", "comment")
        jsonObject.put("text", message)
        jsonObject.put("stream_id", storyModel.id)
        if (!socketManager.isConnected) {
            socketManager.socket!!.open()
            socketManager.connect()
        }
        socketManager.socket!!.emit(SocketManager.EMIT_ADD_COMMENT, jsonObject)
    }

    private fun callApi() {
        val sc = intent.getStringExtra("sc")


        val json = JSONObject()
        json.put("version", "1.1")
        json.put("stream_name", sName)
        json.put("status", "running")
        json.put("start_price", productList[0].price)
        json.put("user_id", MyApplication.instance.getUserPreferences().id)

        json.put("country_id", sc)

        Log.e("StreamingActivity", " callApi >>> ${json.toString()}")
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                handleStreamResponse(`object`.toString())
                // Log.e("UPDATE_THUMB"," callApi >>>")
                handlerThumb.postDelayed(runalbeThumb, 500)

                uploadProductImage()
            }

            override fun onError(errorMessage: String) {
                handlerThumb.postDelayed(runalbeThumb, ONLINE_DELAY)
            }

        }).createStream(json)

    }

    private fun uploadThumb(url: String) {
        //Log.e("UPDATE_THUMB"," uploadThumb_PA >>>$url")
        if (TextUtils.isEmpty(url)) {
            handlerThumb.postDelayed(runalbeThumb, ONLINE_DELAY)

            return
        }
        val json = JSONObject()
        json.put("version", "1.1")
        json.put("image_url", url)
        json.put("stream_id", "")
        json.put("stream_id", storyModel.id)

        // Log.e("UPDATE_THUMB"," uploadThumb_PA >>>${json.toString()}")
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                    val oldThumb = storyModel.thumbUrl
                    deleteThumb(oldThumb)
                handlerThumb.postDelayed(runalbeThumb, THUMB_IMAGE_DELAY)

            }

            override fun onError(errorMessage: String) {
                handlerThumb.postDelayed(runalbeThumb, ONLINE_DELAY)
            }

        }).updateStreamThumb(json)
    }

    private fun uploadProductImage() {

        dupCommentList.clear()
        commentList.clear()
        bookMarkComment.clear()
        commentview.adapter.notifyDataSetChanged()

        progress.show()
        try {
            var file = File(productList[currentProductPosition].path)
            var btm = Constant.getBitmapFromPath(file.path)
            btm = Constant.getResizedBitmap(btm!!, 800)
            val file2 = Constant.savePostImage(btm, "")
            if (file2 != null) {
                file = file2
            }
            val transferObserver = transferUtility!!.upload(Constant.BUCKET_NAME, "${Constant.STREAM_IMAGE_KEY}/${file!!.name}", file)

            transferObserver.setTransferListener(object : TransferListener {
                override fun onStateChanged(id: Int, state: TransferState?) {
                    if (state!!.name.equals("COMPLETED", true)) {

                        val imageFileUrl = s3!!.getResourceUrl(Constant.BUCKET_NAME, "${Constant.STREAM_IMAGE_KEY}/${file.name}")
                        productList[currentProductPosition].url = imageFileUrl
                        updateProduct()
                    } else {
                        if (state!!.name.equals("FAILED", true)) {
                            progress.dismiss()
                            Toast.makeText(this@StreamingActivity, getString(R.string.failed_to_add), Toast.LENGTH_SHORT).show()
                        }
                    }
                }

                override fun onError(id: Int, ex: java.lang.Exception?) {
                    ex!!.printStackTrace()
                    progress.dismiss()
                    Toast.makeText(this@StreamingActivity, getString(R.string.failed_to_add), Toast.LENGTH_SHORT).show()

                }

                override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                    val per = (bytesCurrent / bytesTotal) * 100
                }
            });
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun awardBidCall(awardbids: String) {

        countDown.cancel()
        Log.e("ASDASDASDASDASD",awardbids)
       currentBid = commentList[0]

        //currentBid = dupCommentList[position]
        val param = JSONObject()
        param.put("version", "1.1")

        param.put("user_id", MyApplication.instance.getUserPreferences().id)

        param.put("stream_id", storyModel.id)
        param.put("bid_id", awardbids)
       /* if (commentview.visibility == View.VISIBLE) {
            param.put("bid_id", commentList[position].id)
        } else {
            param.put("bid_id", commentListBookMark[position].id)
        }
*/
        param.put("image_url", productList[currentProductPosition].url)
        param.put("description", productList[currentProductPosition].description)
       // param.put("amount", commentList[position].message)
        param.put("amount", productList[currentProductPosition].price)
        param.put("product_serial", productList[currentProductPosition].serial)

        if(productList[currentProductPosition].paymentMode.equals("Cash,Card",true)){
            param.put("payment_type", "cash,cc")
        }else{
            if(productList[currentProductPosition].paymentMode.equals("Cash",true)){
                param.put("payment_type", "cash")
            }else{
                param.put("payment_type", "cc")
            }

        }

        param.put("is_sold", "false")

        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                showBidAwarededDialog()
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(this@StreamingActivity, "${getString(R.string.failed)} $errorMessage", Toast.LENGTH_SHORT).show()
            }
        }).awardBid(param)
    }

    private fun bookmarkComment(position: Int,flag:Boolean = false) {//stream_id, comment_id, is_bookMarked

        val param = JSONObject()

        param.put("version", "1.1")
        param.put("stream_id", storyModel!!.id)

        Log.e("BOOK_MARK_COM", "${param.toString()}")

        param.put("comment_id", dupCommentList[position].id)
        param.put("is_bookMarked", !(dupCommentList[position].isBookmark))
       /* if (commentview.visibility == View.VISIBLE) {
            param.put("comment_id", commentList[position].id)
            param.put("is_bookMarked", !(commentList[position].isBookmark))
        } else {
            param.put("comment_id", commentListBookMark[position].id)
            param.put("is_bookMarked", !(commentListBookMark[position].isBookmark))
        }
*/
        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                updateCommentView(position)
                if(flag){

                    showDialogForAwardList()
                }
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
//                if(flag){
//                    showDialogForAwardList()
//                }

                Toast.makeText(this@StreamingActivity, getString(R.string.failed_to_bookmark), Toast.LENGTH_SHORT).show()
            }

        }).bookmarkComment(param)

    }

    private fun updateProduct() {
        progress.show()
        val json = JSONObject()
        json.put("version", "1.1")
        json.put("stream_id", storyModel.id)
        json.put("start_price", productList[currentProductPosition].price)
        json.put("user_id", MyApplication.instance.getUserPreferences().id)

        json.put("image_url", productList[currentProductPosition].url)
        json.put("description", productList[currentProductPosition].description)
        json.put("product_serial", productList[currentProductPosition].serial)
        json.put("payment_type", productList[currentProductPosition].paymentMode)
        json.put("is_sold", false)


        Log.e(TAG, "UPFATE_PRODUCT ${json.toString()}")
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                /*  commentList.clear()
                  commentListBookMark.clear()
                  mapComment.clear()
                  notifyComments()*/
                Toast.makeText(this@StreamingActivity, getString(R.string.product_success), Toast.LENGTH_SHORT).show()
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                Toast.makeText(this@StreamingActivity, "${getString(R.string.failed)} $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).updateProduct(json)

    }

}
