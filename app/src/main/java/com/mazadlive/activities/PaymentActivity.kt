package com.mazadlive.activities

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Toast
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.activities.payment.PaymentCardActivity
import com.mazadlive.activities.payment.PaymentFormActivity
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ApiUrl
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.helper.Constant
import com.mazadlive.models.*
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.view_payment_failed.*
import org.json.JSONObject
import company.tap.gosellapi.api.facade.GoSellError
import company.tap.gosellapi.api.model.Charge
import company.tap.gosellapi.api.facade.APIRequestCallback
import company.tap.gosellapi.api.requests.CreateChargeRequest
import company.tap.gosellapi.api.model.Customer
import io.fabric.sdk.android.services.settings.IconRequest.build
import company.tap.gosellapi.api.facade.GoSellAPI
import company.tap.gosellapi.api.model.Redirect


class PaymentActivity : BaseActivity() {

    private val TAG = PaymentActivity::class.java.name
    var REQUEST_PAYMENT_WEBVIEW = 5000
    lateinit var progress: CustomProgress
    var addressModel: AddressModel? = null
    var post: PostModel? = null
    var story: StoryModel? = null
    var type = "post"
    var paymentModeChangeable = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        supportActionBar?.hide()

        progress = CustomProgress(this)
        cod.tag = true
        cc.tag = false

        post = intent.getParcelableExtra("post")
        type = intent.getStringExtra("type")
        story = intent.getParcelableExtra("story")


        when (type) {
            "post" -> {
                total_amount.text = String.format("${getString(R.string.amount_to_be_paid)} ${getString(R.string.currency)} ${post!!.amount}")
                Log.e("SADASDADASDASD",post!!.payment_type)
                if (post!!.payment_type == "cash"){
                    cc.setTextColor(ContextCompat.getColor(this,R.color.screen_text_hint_color))
                    cc.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(this,R.drawable.ic_radio_button_unchecked),null,null,null)
                    cod.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(this,R.drawable.ic_radio_button_checked),null,null,null)
                    next_btn.text = "Confirm"
                    cod.tag = true
                    cc.tag = false
                }else if (post!!.payment_type == "cc"){
                    cod.setTextColor(ContextCompat.getColor(this,R.color.screen_text_hint_color))
                    cod.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(this,R.drawable.ic_radio_button_unchecked),null,null,null)
                    cc.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(this,R.drawable.ic_radio_button_checked),null,null,null)
                    next_btn.text = "Next"
                    cc.tag = true
                    cod.tag = false
                }else{
                    paymentModeChangeable = true
                }
            }
            "story" -> {
                paymentModeChangeable = true
                total_amount.text = String.format("${getString(R.string.amount_to_be_paid)} ${getString(R.string.currency)} ${story!!.buyNowPrice}")
            }
        }

        cod.setOnClickListener {

            if (paymentModeChangeable){
                if ((cod.tag as Boolean)) {
                    cod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_radio_button_unchecked, 0, 0, 0)
                    cod.tag = false
                    next_btn.text = "Confirm"
                } else {
                    cc.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_radio_button_unchecked, 0, 0, 0)
                    cod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_radio_button_checked, 0, 0, 0)
                    cod.tag = true
                    cc.tag = false
                    next_btn.text = "Next"
                }
            }

        }

        cc.setOnClickListener {

            if(paymentModeChangeable){
                if ((cc.tag as Boolean)) {
                    cc.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_radio_button_unchecked, 0, 0, 0)
                    cc.tag = false
                    next_btn.text = "Confirm"
                } else {
                    cod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_radio_button_unchecked, 0, 0, 0)
                    cc.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_radio_button_checked, 0, 0, 0)
                    cod.tag = false
                    cc.tag = true
                    next_btn.text = "Next"
                }
            }
        }

        address_hint.setOnClickListener {
            val intent = Intent(this, AddressListActivity::class.java)
            startActivityForResult(intent, Constant.ADDRESS_CODE)
        }

        next_btn.setOnClickListener {
            if (validation()) {
                if ((cod.tag as Boolean)) {
                    sendOtp()
                } else {

                    val intent = Intent(this@PaymentActivity, WebViewActivity::class.java)
                    val tapPaymentModel = TapPaymentModel()
                    when (type) {
                        "post" -> tapPaymentModel.amount = post!!.amount
                        "story" -> total_amount.text = story!!.buyNowPrice
                    }

                    intent.putExtra("url", ApiUrl.getPaymetLoader)
                    intent.putExtra("tapModel", tapPaymentModel)
                    startActivityForResult(intent, REQUEST_PAYMENT_WEBVIEW)
                }
            }
        }

        back.setOnClickListener {
            onBackPressed()
        }

    }

    private fun startBuyNow(token: String) {
        /*val dropInRequest = DropInRequest().clientToken(token)
        dropInRequest.disablePayPal()
        startActivityForResult(dropInRequest.getIntent(this), Constant.BUY_NOW_CODE)*/
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constant.ADDRESS_CODE && resultCode == Activity.RESULT_OK) {

            addressModel = data!!.getParcelableExtra<AddressModel>("address")

            if (addressModel != null) {
                var ads = "${addressModel!!.username}\n${addressModel!!.addressLine1},${addressModel!!.addressLine2},${addressModel!!.city}\n${addressModel!!.state} ${addressModel!!.pincode}"
                if (addressModel!!.countryModel != null) {
                    ads += "\n${addressModel!!.countryModel!!.name}.\n${addressModel!!.phone}"
                } else {
                    ads += ".\n${addressModel!!.phone}"
                }
                address.setText(ads)
                address.visibility = View.VISIBLE
            }
        }

        if (requestCode == Constant.BUY_NOW_CODE && resultCode == Activity.RESULT_OK) {
            /*if (resultCode == Activity.RESULT_OK) {
                val result = data!!.getParcelableExtra<DropInResult>(DropInResult.EXTRA_DROP_IN_RESULT)

                Log.e("ERROR_PAY", " SUCCESS_STEP 1")
                matchNonce(result)

            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.e("ERROR_PAY", " USER_CANCELLED")
            } else {
                // handle errors here, an exception may be available in
                val error = data!!.getSerializableExtra(DropInActivity.EXTRA_ERROR) as Exception
                error.printStackTrace()


                Log.e("ERROR_PAY", error.message + " *")
            }*/
        }

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_PAYMENT_WEBVIEW) {
            val tapPaymentModel = data!!.getParcelableExtra<TapPaymentModel>("model")
            placeOrder(tapPaymentModel)
        }
    }

    private fun validation(): Boolean {
        if (addressModel == null) {
            CustomDialog(this).showErrorDialog("Select Address")
            return false
        }

        if (!(cod.tag as Boolean) && !(cc.tag as Boolean)) {
            CustomDialog(this).showErrorDialog("Select a payment method")
            return false
        }


        return true
    }

    private fun getClientToken() {

        val param = JSONObject()
        param.put("version", "1.1")
        param.put("user_id", MyApplication.instance.getUserPreferences().id)
        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                val jsonObject = JSONObject(`object`.toString())
                if (jsonObject.has("token")) {
                    val token = jsonObject.getString("token")
                    startBuyNow(token)
                }
            }

            override fun onError(errorMessage: String) {
                Toast.makeText(this@PaymentActivity, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
                progress.dismiss()
            }
        }).getToken(param)
    }

    private fun sendOtp() {

        val param = JSONObject()
        param.put("version", "1.1")
        param.put("user_id", MyApplication.instance.getUserPreferences().id)

        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                CustomDialog(this@PaymentActivity).showOtpDialog(object : DialogListener() {
                    override fun okClick(any: Any) {
                        verifyOtp(any.toString())
                    }
                })
            }

            override fun onError(errorMessage: String) {
                Toast.makeText(this@PaymentActivity, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
                progress.dismiss()
            }
        }).orderOtp(param)


    }

    private fun verifyOtp(otp: String) {
        var cCode = MyApplication.instance.getUserPreferences().countryCode
        cCode = cCode.replace("+", "")
        val param = JSONObject()
        param.put("version", "1.1")
        param.put("user_id", MyApplication.instance.getUserPreferences().id)
        param.put("code", "123456")
        param.put("country_code", cCode)
        param.put("phone", MyApplication.instance.getUserPreferences().phone)

        Log.e("PARAM", "${param.toString()}")

        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                placeOrder(TapPaymentModel())
            }

            override fun onError(errorMessage: String) {
                Toast.makeText(this@PaymentActivity, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
                placeOrder(TapPaymentModel())
                progress.dismiss()
            }

        }).verifyOrderOtp(param)
    }

    /* private fun matchNonce(result: DropInResult?) {
         val param = JSONObject()
         param.put("version", "1.1")
         param.put("payment_method_nonce", result!!.paymentMethodNonce!!.nonce.toString())

         when (type) {
             "post" -> {
                 param.put("amount", post!!.amount)
             }
             "story" -> {
                 param.put("amount", story!!.buyNowPrice)
             }
         }

         Log.e("PARAM", "NONCE ${param.toString()}")
         progress.show()
         ServiceRequest(object : ApiResponseListener {
             override fun onCompleted(`object`: Any) {
                 progress.dismiss()
                 val jsonObject = JSONObject(`object`.toString())
                 if (jsonObject.has("description")) {
                     val desObj = jsonObject.getJSONObject("description")
                     val transObj = desObj.getJSONObject("transaction")
                     val status = transObj.getString("status")
                     val tranId = transObj.getString("id")
                     placeOrder(status, tranId)
                 }
             }

             override fun onError(errorMessage: String) {
                 Toast.makeText(this@PaymentActivity, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
                 progress.dismiss()
             }
         }).checkout(param)
     }
 */

    private fun placeOrder(tapPaymentModel: TapPaymentModel) {

        val param = JSONObject()
        param.put("version", "1.1")
        param.put("user_id", MyApplication.instance.getUserPreferences().id)

        param.put("address_id", addressModel!!.id)
        param.put("user_payment_status", tapPaymentModel.result)
        param.put("transaction_id", tapPaymentModel.chargeid)
        param.put("chargeid", tapPaymentModel.chargeid)
        param.put("crd", tapPaymentModel.crd)
        param.put("crdtype", tapPaymentModel.crdtype)
        param.put("hash", tapPaymentModel.hash)
        param.put("payid", tapPaymentModel.payid)
        param.put("ref", tapPaymentModel.ref)
        param.put("result", tapPaymentModel.result)
        param.put("trackid", tapPaymentModel.trackid)


        if (cod.tag as Boolean) {
            param.put("payment_method", "cod")
        } else {
            param.put("payment_method", "card")
        }
        param.put("type", type)
        when (type) {
            "post" -> {
                param.put("type_id", post!!.id)
                param.put("seller_id", post!!.post_user_id)
                param.put("amount", post!!.amount)
            }
            "story" -> {
                param.put("type_id", story!!.id)
                param.put("seller_id", story!!.userData!!.id)
                param.put("amount", story!!.buyNowPrice)
            }
        }



        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                Toast.makeText(this@PaymentActivity, getString(R.string.order_placed_success), Toast.LENGTH_SHORT).show()

                val jsonObject = JSONObject(`object`.toString())
                if (!jsonObject.getBoolean("error")) {

                    val obj = jsonObject.getJSONObject("description")
                    Log.e("PAYMENTY", "${obj.toString()}")
                    val orderModel = OrderModel()

                    orderModel.paymenMethod = "cc"
                    orderModel.amount = param.getString("amount")
                    orderModel.orderId = obj.getString("_id")
                    orderModel.addressModel = addressModel
                    orderModel.orderStatus = obj.getString("status")
                    orderModel.paymentStatus = obj.getString("user_payment_status")
                    orderModel.paymenMethod = obj.getString("payment_method")
                    orderModel.created_at = System.currentTimeMillis()

                    if (!TextUtils.isEmpty(param.getString("transaction_id")))
                        orderModel.txnId = param.getString("transaction_id")


                    Log.e("PAYMENTY", "${orderModel.amount} ${orderModel.orderStatus} ${orderModel.paymentStatus} ${orderModel.txnId}")
                    val intent = Intent(this@PaymentActivity, PaymentDoneActivity::class.java)
                    intent.putExtra("order", orderModel)
                    startActivity(intent)
                    finish()


                } else {
                    if (cc.tag as Boolean) {
                        showPaymentFailedDialog(param)
                    }
                }

            }

            override fun onError(errorMessage: String) {
                Toast.makeText(this@PaymentActivity, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
                if (cc.tag as Boolean) {
                    showPaymentFailedDialog(param)
                }
                progress.dismiss()

            }

        }).createOrder(param)
    }

    private fun showPaymentFailedDialog(jsonObject: JSONObject) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.view_payment_failed)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)

        dialog.order_id.text = jsonObject.getString("transaction_id")

        dialog.cl.visibility = View.VISIBLE
        dialog.mid_view3.visibility = View.VISIBLE

        dialog.time.text = (Constant.getDateAndTime(System.currentTimeMillis()))

        dialog.continue_btn.setOnClickListener {
            dialog.dismiss()
            finish()
            //Toast.makeText(this,"Currently this feature will not work",Toast.LENGTH_SHORT).show()
        }

        dialog.back_dialog.setOnClickListener {
            dialog.dismiss()
            finish()
        }

        dialog.show()
    }

}
