package com.mazadlive.activities

import android.Manifest
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.text.Html
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.services.s3.AmazonS3Client
import com.mazadlive.helper.PickImageHelper
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.github.florent37.runtimepermission.kotlin.askPermission
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.helper.Constant
import com.mazadlive.models.CountryModel
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.activity_sign_up.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.io.*

class SignUpActivity : BaseActivity() {

    private lateinit var pickImageHelper: PickImageHelper

    var s3: AmazonS3Client? = null
    var transferUtility: TransferUtility? = null

    private var bitmap: Bitmap? = null;
    private var imageFilePath = ""
    private var countryCode = "973"

    private var progress:CustomProgress?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_sign_up)
        supportActionBar!!.hide()
        im_dummy.visibility = View.GONE

        progress = CustomProgress(this)
        pickImageHelper = PickImageHelper(this, null, true, imagePickerListener())
        getAllCountries()
        doAsync {
            credentialsProvider()
            setTransferUtility();
        }

        if(Constant.countryList.size <= 0){
            Constant.getAllCountries()
        }

        mTremCondition.setText(Html.fromHtml(getString(R.string.our_terms)), TextView.BufferType.SPANNABLE)
        back.setOnClickListener { onBackPressed() }

        mProfileImage.setOnClickListener {
            askPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) {
                pickImageHelper.pickImage(4)
            }.onDeclined { result ->
                if (result.hasDenied()) {
                    CustomDialog(this).showErrorDialog(getString(R.string.permission_required))
                } else if (result.hasForeverDenied()) {
                    result.foreverDenied.forEach {
                        CustomDialog(this).permissionDialog(getString(R.string.accept_permmission), object : DialogListener() {
                            override fun yesClick() {
                                super.yesClick()
                                result.goToSettings()
                            }
                        })
                    }
                }
            }
        }

        signup.setOnClickListener {

            if (validations()) {
                progress!!.show()
                if(TextUtils.isEmpty(imageFilePath)){
                    verifyPhone("")
                    //registerUser((""))
                }else{
                    setFileToUpload()
                }
            }
        }

        mOpenCountry.setOnClickListener {
            CustomDialog(this).showCountryDialog(object : DialogListener() {
                override fun okClick(any: Any) {
                    super.okClick(any)
                    val countryModel = any as CountryModel
                    codeinput.setText(String.format("%s", countryModel.code))
                    countryCode = countryModel.code.toString()
                    mNumber.setText("")
                }
            }, true)
        }

        mTremCondition.setOnClickListener {
            val intent = Intent(this@SignUpActivity,TermPrivacyActivity::class.java)
            intent.putExtra("type","term")
            startActivity(intent)
        }

        mLogin.setOnClickListener { super.onBackPressed() }


        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {

            }

            override fun onError(errorMessage: String) {

            }

        }).getCredentials()
    }
    private fun getAllCountries() {

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {

            }

            override fun onError(errorMessage: String) {

            }

        }).getAllCountry(Constant.countryList)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        pickImageHelper.onActivityResult(requestCode, resultCode, data)
    }

   // private var bitmapString = ""

    inner class imagePickerListener : PickImageHelper.OnImagePickerListener {

        override fun onImagePicked(imagePath: String, requestCode: Int) {
            if (requestCode == Constant.CAMERA_IMAGE_REQUEST) {
                imageFilePath = imagePath
                mProfileImage.setPadding(0, 0, 0, 0)
                // bitmap = getBitmapFromPath(imagePath)
                Glide.with(this@SignUpActivity).asBitmap().load(imagePath).apply(RequestOptions().override(200)).listener(object : RequestListener<Bitmap> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        bitmap = resource
                       //bitmapString = Constant.getStringFromBitmap(bitmap!!)
                        mProfileImage.setImageBitmap(resource)
                        return true
                    }

                }).into(im_dummy)

            } else if (requestCode == Constant.GALLARY_IMAGE_REQUEST) {
                // bitmap = getBitmapFromPath(imagePath)
                imageFilePath = imagePath
                mProfileImage.setPadding(0, 0, 0, 0)
                Glide.with(this@SignUpActivity).asBitmap().load(imagePath).apply(RequestOptions().override(200)).listener(object : RequestListener<Bitmap> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        bitmap = resource
                       // bitmapString = Constant.getStringFromBitmap(bitmap!!)
                        mProfileImage.setImageBitmap(resource)
                        im_dummy.setImageBitmap(resource)
                        //writeToFile(bitmapString)
                        return true
                    }

                }).into(im_dummy)

            }
        }
    }


    fun credentialsProvider(){

        // Initialize the Amazon Cognito credentials provider
        val credentialsProvider =  CognitoCachingCredentialsProvider(
                getApplicationContext(),
                MyApplication.instance.getUserPreferences().POOL_ID, //indentity pool id
                Constant.REGION)

        setAmazonS3Client(credentialsProvider);
    }

    fun setAmazonS3Client(credentialsProvider: CognitoCachingCredentialsProvider) {

        // Create an S3 client
        s3 = AmazonS3Client(credentialsProvider)

        // Set the region of your S3 bucket
       // s3!!.setRegion(com.amazonaws.regions.Region.getRegion(Regions.US_EAST_1))

    }

    fun setTransferUtility() {
        transferUtility = TransferUtility(s3, applicationContext)
    }

    fun setFileToUpload() {
        doAsync {
            try {

                bitmap = Constant.getResizedBitmap(bitmap!!,1000)
                val file = Constant.saveUserImage(bitmap!!)
                val transferObserver = transferUtility!!.upload(Constant.BUCKET_NAME,"${Constant.USER_IMAGE_KEY}/${file!!.name}",file)
                transferObserverListener(transferObserver,file)
            }catch (e: java.lang.Exception){
                Toast.makeText(this@SignUpActivity,getString(R.string.profile_not_upload),Toast.LENGTH_SHORT).show()
                verifyPhone("")
                e.printStackTrace()
            }
        }
    }

    fun transferObserverListener(transferObserver: TransferObserver, file: File) {
        Log.e("transferObserverList","onState ")
        transferObserver.setTransferListener(object : TransferListener {
            override fun onStateChanged(id: Int, state: TransferState?) {
                if(state!!.name.equals("COMPLETED",true)){
                    val name = s3!!.getResourceUrl(Constant.BUCKET_NAME,"${Constant.USER_IMAGE_KEY}/${file.name}")
                    verifyPhone(name)
                }else if(state!!.name.equals("FAILED",true)){

                    verifyPhone("")

                }
                Log.e("transferObserverList","onStateChanged ${state!!.name}")
            }

            override fun onError(id: Int, ex: java.lang.Exception?) {
                Log.e("transferObserverList","onError")
                Toast.makeText(this@SignUpActivity,getString(R.string.pic_not_found),Toast.LENGTH_SHORT).show()
                verifyPhone("")
            }

            override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {

                val per = (bytesCurrent/bytesTotal)*100

                //progress!!.setMessage("Upload Image : $per%")
                //Log.e("transferObserverList","onProgressChanged $id   **${bytesCurrent} *** $bytesTotal  ${per}")
            }
        });
    }



    private fun validations(): Boolean {
        if (TextUtils.isEmpty(mUsername.text.toString().trim())) {
            CustomDialog(this).showErrorDialog(getString(R.string.fill_username))
            return false
        }else{
            val name = mUsername.text.toString().trim()
            if(name.contains(" ")){
                CustomDialog(this).showErrorDialog(getString(R.string.invalid_username))
                return false
            }
        }

        if (TextUtils.isEmpty(countryCode)) {
            CustomDialog(this).showErrorDialog(getString(R.string.select_country_code))
            return false
        }

        if (TextUtils.isEmpty(mNumber.text.toString().trim())) {
            CustomDialog(this).showErrorDialog(getString(R.string.fill_phone_no))
            return false
        }
        if (TextUtils.isEmpty(mEmail.text.toString().trim())) {
            CustomDialog(this).showErrorDialog(getString(R.string.fill_email))
            return false
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(mEmail.text.toString()).matches()) {
            CustomDialog(this).showErrorDialog(getString(R.string.email_suggestion1))
            return false
        }
        if (TextUtils.isEmpty(mPassword1.text.toString().trim()) || mPassword1.text.toString().trim().length < 8) {
            CustomDialog(this).showErrorDialog(getString(R.string.password_suggestion))
            return false
        }
        if (TextUtils.isEmpty(mPassword2.text.toString().trim()) || mPassword2.text.toString().trim().length < 8) {
            CustomDialog(this).showErrorDialog(getString(R.string.password_suggestion))
            return false
        }
        if (mPassword1.text.toString().trim() != mPassword2.text.toString().trim()) {
            CustomDialog(this).showErrorDialog(getString(R.string.password_dnt_match))
            return false
        }
        if (!checkbox.isChecked) {
            CustomDialog(this).showErrorDialog(getString(R.string.agree_term_condition))
            return false
        }
        return true
    }


    /*version:1.1
  country_code:91
  phone:9314669334
  email:paruhack@gmail.com
  username:rocco
  password:password
  userType:buyer
  profilePic:*/


    private fun verifyPhone(profileUrl:String) {

        var code = codeinput.text.toString().trim().replace("+", "")

        val param = JSONObject()
        param.put("version", "1.1")
        param.put("country_code", code)
        param.put("phone", mNumber.text.toString().trim())

        val param2 = JSONObject()
        param2.put("version", "1.1")
        param2.put("country_code", code)
        param2.put("phone", mNumber.text.toString().trim())
        param2.put("email", mEmail.text.toString().trim())
        param2.put("username", mUsername.text.toString())
        param2.put("password", mPassword1.text.toString())
        param2.put("userType", "buyer")
        param2.put("profilePic", "")


        if (!TextUtils.isEmpty(profileUrl)) {
            param2.put("profilePic", profileUrl)
        }
        Log.e("PARAMS__", "*** $param")

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                val bundle = Bundle()
                bundle.putString("NUMBER", mNumber.text.toString().trim())
                bundle.putString("CODE", code)
                bundle.putString("EMAIL", mEmail.text.toString())
                bundle.putString("PARAM", param2.toString())

                OTPActivity.start(this@SignUpActivity,bundle)
                finish()
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                CustomDialog(this@SignUpActivity).showErrorDialog("Failed to register try again. $errorMessage")
            }

        }).verifyPhone(param)

    }

}
