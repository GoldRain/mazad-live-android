package com.mazadlive.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.Toast
import com.facebook.drawee.backends.pipeline.Fresco
import com.mazadlive.R
import com.mazadlive.adapters.FeedBuyerAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ApiUrl
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.customviews.ImageOverLayView
import com.mazadlive.database.AppDatabase
import com.mazadlive.models.CategoryModel
import com.mazadlive.models.LiveStoryModel
import com.mazadlive.models.PostImages
import com.mazadlive.models.PostModel
import com.mazadlive.parser.PostDataParser
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.UserPreferences
import com.stfalcon.frescoimageviewer.ImageViewer
import kotlinx.android.synthetic.main.activity_search_post.*
import kotlinx.android.synthetic.main.view_image_overlay.view.*
import org.jetbrains.anko.doAsync
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class SearchPostActivity : AppCompatActivity(), FeedBuyerAdapter.OnClikPost {

    lateinit var userRef: UserPreferences
    var progress: CustomProgress? = null
    var postList = ArrayList<PostModel>()
    lateinit var model : CategoryModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_post)
        supportActionBar?.hide()
        userRef = MyApplication.instance.getUserPreferences()
        progress = CustomProgress(this)
        val linearLayoutManager1 = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycleviewfeeds.layoutManager = linearLayoutManager1
        recycleviewfeeds.adapter = FeedBuyerAdapter(this, postList, this)

        if (intent.hasExtra("model")){
            model = intent.getParcelableExtra("model")
        }else{
            return
        }

        mTitle.text = model.name

        searchData(model.id.toString())

        swipe_to_refresh.setOnRefreshListener {
            searchData(model.id.toString())
        }

        back.setOnClickListener {
            onBackPressed()
        }
    }

    fun searchData(text: String) {
        val param = JSONObject()
        param.put("version", "1.1")

        if (model.isTag!!){
            param.put("category", text)
        }else{
            param.put("placeName", text)
        }

        postList.clear()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {

                if (swipe_to_refresh.isRefreshing) {
                    swipe_to_refresh.isRefreshing = false
                }

                val jsonObject = JSONObject(`object`.toString())
                if (jsonObject.has("description")) {

                    val postObjArray = jsonObject.getJSONArray("description")
                    for (i in 0 until postObjArray.length()) {

                        val postObj = postObjArray.getJSONObject(i)
                        val postModel = PostDataParser().parse(postObj)

                        doAsync {
                            val favPost = AppDatabase.getAppDatabase().favPostDao().getFavPost(postModel.id)
                                postModel.isFav = favPost.post_id.equals(postModel.id, true)
                        }
                        postList.add(postModel)
                    }

                    recycleviewfeeds.adapter.notifyDataSetChanged()
                }

            }

            override fun onError(errorMessage: String) {
                //updateList()
                if (swipe_to_refresh.isRefreshing) {
                    swipe_to_refresh.isRefreshing = false
                }
                Toast.makeText(this@SearchPostActivity, "${getString(R.string.failed)} $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).searchCategories(param,model.URL.toString())

    }

    fun parsePostList(postObjArray: JSONArray) {

        for (i in 0 until postObjArray.length()) {
            val postObj = postObjArray.getJSONObject(i)
            val postModel = PostDataParser().parse(postObj)

            doAsync {
                val favPost = AppDatabase.getAppDatabase().favPostDao().getFavPost(postModel.id)
                if (favPost != null) {
                    postModel.isFav = favPost.post_id.equals(postModel.id, true)
                }
            }
            postList.add(postModel)
        }


    }

    /*private fun updateList() {
        runOnUiThread {
            if(postList.size >0){
                fragmentView.tx_empty.visibility = View.GONE
            }else{
                fragmentView.tx_empty.visibility = View.VISIBLE
            }
            fragmentView.recycleview!!.adapter.notifyDataSetChanged()
        }
    }*/

    private fun showAllImage(position: Int) {

        val imageList2 = postList[position].postImages

        Fresco.initialize(this);
        val overView = ImageOverLayView(this)

        val mImageViewerBuilder = ImageViewer.Builder<PostImages>(this, imageList2)
        val mImageViewer = mImageViewerBuilder.setStartPosition(position)
                .setOverlayView(overView)
                .setStartPosition(0)
                .setBackgroundColor(ContextCompat.getColor(this, R.color.colorBlack))
                .setFormatter(object : ImageViewer.Formatter<PostImages> {
                    override fun format(t: PostImages?): String {
                        return t!!.url
                    }

                })

                .show()


        overView.back.setOnClickListener {
            mImageViewer.onDismiss()
        }


    }


    override fun onClickPost(position: Int) {
        val post = postList[position]
        if (post.postImages.size > 0) {

            if (post.postImages[0].isVideo) {
                var path = post.postImages[0].url
                val ind = path.lastIndexOf(".")
                if (ind > 0) {
                    path = path.substring(0, ind) + ".mp4"
                }

                val intent = Intent(this@SearchPostActivity, StoryCommentActivity::class.java)
                intent.putExtra("isVideo", true)
                intent.putExtra("path", path)
                startActivity(intent)
            } else {
                showAllImage(position)
            }
        }
    }

    //type: all/one , country_code: id .
    private fun getPostFromCountry() {

        val map = JSONObject()
        map.put("type", "one")
        map.put("country_id", userRef.countryId)
        map.put("version", "1.1")

        Log.e("MAP_DATA", "*** ${map}")

        progress_bar.visibility = View.VISIBLE
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                //   progress!!.dismiss()
                progress_bar.visibility = View.GONE
                if (postList.size > 0) {
                    Collections.reverse(postList)
                    tx_empty.visibility = View.GONE
                } else {
                    tx_empty.visibility = View.VISIBLE
                }

                if (recycleviewfeeds.adapter != null) {
                    recycleviewfeeds.adapter.notifyDataSetChanged()
                }

                if (swipe_to_refresh.isRefreshing) {
                    swipe_to_refresh.isRefreshing = false
                }

            }

            override fun onError(errorMessage: String) {
                //progress!!.dismiss()
                progress_bar.visibility = View.GONE
                Toast.makeText(this@SearchPostActivity, "Failed to get post $errorMessage", Toast.LENGTH_SHORT).show()
                tx_empty.visibility = View.VISIBLE
            }

        }).getPost(postList, map, ApiUrl.getPost, false)
    }
}
