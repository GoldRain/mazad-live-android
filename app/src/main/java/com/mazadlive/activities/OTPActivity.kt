package com.mazadlive.activities

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.text.TextUtils
import android.util.Log
import android.view.Window
import com.mazadlive.R
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.customdialogs.CustomProgressDialog
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.activity_otp_forgot_password.*
import org.json.JSONObject

class OTPActivity : BaseActivity() {
    companion object {
        fun start(context: Context, bundle: Bundle?){

            val intent = Intent(context,OTPActivity::class.java)

            bundle?.let { intent.putExtras(bundle) }
            context.startActivity(intent)
        }

    }

    var number = ""
    var code = ""
    var email = ""
    var paramString = ""

    lateinit var progress:CustomProgress

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setTheme(R.style.whiteStatusTheme)
        setContentView(R.layout.activity_otp)
        supportActionBar!!.hide()

        progress = CustomProgress(this)
        code = intent.extras.getString("CODE")
        number = intent.extras.getString("NUMBER")
        email = intent.extras.getString("EMAIL")
        paramString = intent.extras.getString("PARAM")
        mNumber.text = ("$code $number")

        back.setOnClickListener { onBackPressed() }

        next.setOnClickListener {
            verifyCode()
             }
    }

    private fun verifyCode() {
        val otp = code_pin.value
        Log.e("OTP_LLE",otp+" *** ")
        if(TextUtils.isEmpty(otp)){
            CustomDialog(this@OTPActivity).showErrorDialog("Enter otp.")
            return
        }else{
            if(otp.length <6){
                CustomDialog(this@OTPActivity).showErrorDialog("Enter 6 digit otp.")
                return
            }
        }
        val param  = HashMap<String,String>()
        param.put("version","1.1")
        param.put("type","phone")
        param.put("phone",number)
        param.put("country_code",code)
        //param.put("email",email)
        param.put("code",otp)

        Log.e("PARAM_OTP","${param.toString()}")
        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                registerUser()
            }

            override fun onError(errorMessage: String) {
               progress.dismiss()

                CustomDialog(this@OTPActivity).showAlertDialog("Failed","Failed to verify otp try again. $errorMessage")
            }

        }).verifyOtp(param)

    }



    private fun registerUser() {

        val param = JSONObject(paramString)

        Log.e("PARAM_REGISTER","R:: ${param.toString()}")
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                try {

                    val json = JSONObject(`object` as String)

                    val ref = MyApplication.instance.getUserPreferences()

                    if (json.has("data")) {
                        val obj = json.getJSONObject("data")
                        if (obj.has("username")) {
                            ref.username = obj.getString("username")
                        }

                        if (obj.has("country_code")) {
                            ref.countryCode = obj.getString("country_code")
                        }

                        if (obj.has("phone")) {
                            ref.phone = obj.getString("phone")
                        }

                        if (obj.has("email")) {
                            ref.email = obj.getString("email")
                        }

                        if (obj.has("password")) {
                            ref.password = obj.getString("password")
                        }

                        if (obj.has("type")) {
                            ref.type = obj.getString("type")
                        }

                        if (obj.has("profilePhoto")) {
                            ref.profile = obj.getString("profilePhoto")
                        }

                        if (obj.has("_id")) {
                            ref.id = obj.getString("_id")
                        }

                        if(obj.has("country_id")){
                            ref.countryId = obj.getString("country_id")
                        }

                        if(obj.has("country_name")){
                            ref.CountryName = obj.getString("country_name")
                        }


                        ref.appModeBuyer = ref.type.equals("buyer",true)

                    }

                } catch (ex: Exception) {
                    ex.printStackTrace()
                   // CustomDialog(this@OTPActivity).showErrorDialog("Failed to register try again. Server error")
                }finally {

                    startActivity(Intent( this@OTPActivity,SignInActivity::class.java))

                    finish()
                }
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()

                CustomDialog(this@OTPActivity).showAlertDialog("Failed to register try again. $errorMessage")
               // CustomDialog(this@OTPActivity).showErrorDialog("Failed to register try again. $errorMessage")
            }

        }).signup(param)

    }

}
