package com.mazadlive.activities

import android.annotation.TargetApi
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.mazadlive.R
import com.mazadlive.api.ApiUrl
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.activity_term_privacy.*

class TermPrivacyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_term_privacy)
        supportActionBar?.hide()

        val type = intent.getStringExtra("type")
        mWebView.clearCache(true);
        mWebView.settings.javaScriptEnabled = true;
        mWebView.webViewClient = MyClient()

        val lan = MyApplication.instance.getUserPreferences().appLanguage

        Log.e("TYPE_CHECK","$type $lan")
        Log.e("TYPE_CHECK","${ApiUrl.getTremAndConditionAr}")
        Log.e("TYPE_CHECK","${ApiUrl.getTremAndConditionEn}")
        Log.e("TYPE_CHECK","${ApiUrl.getPrivacyEn}")
        Log.e("TYPE_CHECK","${ApiUrl.getPrivacyAr}")
        when(lan){
            "en" ->{
                if(type.equals("term")){
                    mWebView.loadUrl(ApiUrl.getTremAndConditionEn)
                }else{
                    mWebView.loadUrl(ApiUrl.getPrivacyEn)
                }
            }

            "ar" ->{
                if(type.equals("term")){
                    mWebView.loadUrl(ApiUrl.getTremAndConditionAr)
                }else{
                    mWebView.loadUrl(ApiUrl.getPrivacyAr)
                }
            }

            else ->{
                if(type.equals("term")){
                    mWebView.loadUrl(ApiUrl.getTremAndConditionEn)
                }else{
                    mWebView.loadUrl(ApiUrl.getPrivacyEn)
                }
            }
        }


        back.setOnClickListener {
           onBackPressed()
        }
    }

    override fun onBackPressed() {
        if(mWebView.canGoBack()){
            mWebView.goBack()
        }else{
            super.onBackPressed()
        }


    }

    inner class MyClient: WebViewClient() {

        @TargetApi(Build.VERSION_CODES.N)
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            view!!.loadUrl(request!!.url!!.toString())
            return super.shouldOverrideUrlLoading(view, request)
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            progress_bar.visibility = View.VISIBLE
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            progress_bar.visibility = View.GONE

        }

        override fun onReceivedError(view: WebView?, errorCode: Int, description: String?, failingUrl: String?) {
            super.onReceivedError(view, errorCode, description, failingUrl)

        }

    }
}
