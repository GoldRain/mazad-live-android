package com.mazadlive.activities

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.Window
import com.facebook.common.util.UriUtil
import com.facebook.drawee.backends.pipeline.Fresco
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.adapters.ListImageAdapter
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customviews.ImageOverLayView
import com.stfalcon.frescoimageviewer.ImageViewer
import kotlinx.android.synthetic.main.activity_image_list.*
import kotlinx.android.synthetic.main.view_image_overlay.view.*
import java.io.File
import java.util.*

class ImageListActivity : AppCompatActivity() {

    companion object {
        fun start(context: Context, list: ArrayList<String>?,isVideo:Boolean) {
            val intent = Intent(context, ImageListActivity::class.java)
            list?.let {
                intent.putExtra("images", list)
                intent.putExtra("isVideo", isVideo)
            }
            (context as Activity).startActivityForResult(intent,111)
        }
    }

    private var isUpdate = false
    private var imageList =  ArrayList<String>()
    private var imageList2 =  ArrayList<String>()

    private var isVideo = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_image_list)
        supportActionBar!!.hide()

        imageList = intent.getStringArrayListExtra("images")
        isVideo = intent.getBooleanExtra("isVideo",false)

        imageList.forEach {
            val im = UriUtil.getUriForFile(File(it)).toString()
            imageList2.add(im)
        }
        recycleView.layoutManager = GridLayoutManager(this,3)
        recycleView.adapter = ListImageAdapter(this, imageList,OnItemClick())
        recycleView.adapter.notifyDataSetChanged()


        back.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        if(isUpdate){
            val intent = Intent()
            intent.putExtra("images_list",imageList)
            setResult(Activity.RESULT_OK,intent)
        }
        Log.e("IMAGES_SIZE","** ${imageList.size} list")
        super.onBackPressed()
    }

    inner class OnItemClick :ListImageAdapter.OnItemClick{
        override fun onItemClick(position: Int) {
            var tx1 = ""
            var tx2 = ""
            if(isVideo){
                 tx1 = getString(R.string.play_video)
                 tx2 = getString(R.string.delete_video)
            }else{
                 tx1 = getString(R.string.view_image)
                 tx2 = getString(R.string.delete_image)
            }

            CustomDialog(this@ImageListActivity).showDialogForViewImage(tx1,tx2,object : DialogListener() {
                override fun okClick(any: Any) {
                    if(any.toString().equals("view",true)){
                        if(isVideo){
                            val intent = Intent(this@ImageListActivity,StoryCommentActivity::class.java)
                            intent.putExtra("isVideo",true)
                            intent.putExtra("path",imageList[0])
                            startActivity(intent)
                        }else{
                            showAllImage(position)
                        }
                    }else if(any.toString().equals("delete",true)){
                        isUpdate = true;
                        imageList2.removeAt(position)
                        imageList.removeAt(position)
                        recycleView.adapter.notifyDataSetChanged()
                    }
                }
            })
        }
    }

    private fun showAllImage(position: Int) {

        var currentImagePositon = position
        Fresco.initialize(this@ImageListActivity);
        val overView = ImageOverLayView(this@ImageListActivity)

        val mImageViewerBuilder = ImageViewer.Builder<String>(this@ImageListActivity, imageList2)
        val mImageViewer = mImageViewerBuilder.setStartPosition(position)
                .setOverlayView(overView)
                .setBackgroundColor(ContextCompat.getColor(this@ImageListActivity, R.color.colorBlack))
                .show()


        overView.back.setOnClickListener {
            mImageViewer.onDismiss()
        }


    }
}
