package com.mazadlive.activities

import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.text.TextUtils
import android.view.Window
import com.mazadlive.R
import com.mazadlive.customdialogs.CustomDialog
import kotlinx.android.synthetic.main.activity_support.*

class SupportActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_support)
        supportActionBar!!.hide()

        mReport.setOnClickListener { sendMail() }

        mPhone.setOnClickListener {
            openCallDialog()
        }

        back.setOnClickListener { onBackPressed() }
    }

    private fun openCallDialog() {

        val supportContact = resources.getString(R.string.support_number)
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$supportContact")
        startActivity(intent)


        /*val builder = AlertDialog.Builder(this)
        builder.setTitle("Confirm")
                .setMessage("Do you want to make a call to: 0123456789")
                .setPositiveButton("Call",object :DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        dialog!!.dismiss()
                        makeCalls()
                    }

                })

                .setNegativeButton("Cancel",object :DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        dialog!!.dismiss()
                    }

                })*/
    }

    private fun makeCalls() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(this@SupportActivity, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(android.Manifest.permission.CALL_PHONE), 111)
                return
            }
        }

        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:0123456789"))
        startActivity(intent)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (ContextCompat.checkSelfPermission(this@SupportActivity, android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            makeCalls()
        }
    }

    private fun sendMail() {
        if (!TextUtils.isEmpty(mDescription.text.toString().trim())) {
            try {
                val intent = Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:${resources.getString(R.string.support_email)}"))
                intent.putExtra(Intent.EXTRA_EMAIL, resources.getString(R.string.support_email))
                intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback")
                intent.putExtra(Intent.EXTRA_TEXT, mDescription.text.toString())
                startActivity(Intent.createChooser(intent, "Select"))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else {
            CustomDialog(this).showErrorDialog(getString(R.string.write_descrption))
        }
    }

}
