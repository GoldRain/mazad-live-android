package com.mazadlive.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import android.widget.Toast
import com.android.billingclient.api.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import com.mazadlive.adapters.AdsAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ApiUrl
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.helper.Constant
import com.mazadlive.models.AdsModel
import com.mazadlive.models.TapPaymentModel
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.UserPreferences
import kotlinx.android.synthetic.main.activity_upgrade.*
import kotlinx.android.synthetic.main.customdialog_purchase_more.*
import org.json.JSONObject
import com.mazadlive.helper.Security
import com.mazadlive.models.SubscribeModel
import java.io.IOException
import java.util.ArrayList

class UpgradeActivity : BaseActivity(), PurchasesUpdatedListener {

    @SuppressLint("SwitchIntDef")
    override fun onPurchasesUpdated(responseCode: Int, purchases: MutableList<Purchase>?) {

        purchases?.forEach {
            Log.e(TAG_BILL, "onPurchasesUpdated ${it.sku} ${it.orderId} ${it.purchaseToken}")
        }
        when (responseCode) {

            BillingClient.BillingResponse.OK -> {

                purchases?.forEach {
                    if (verifyValidSignature(it.originalJson, it.signature)) {

                        if (currentProduct.type == "adsRemove") {
                            updateRemoveAds()
                        } else {
                            updateAllCount(it)
                        }

                        setPurchasedData(it.originalJson)
                    } else {
                        Toast.makeText(this, "Purchases failed ", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            BillingClient.BillingResponse.ERROR -> {
                Toast.makeText(this, "Purchases ERROR ", Toast.LENGTH_SHORT).show()
            }

            BillingClient.BillingResponse.BILLING_UNAVAILABLE -> {
                Toast.makeText(this, "Purchases Failed BILLING_UNAVAILABLE ", Toast.LENGTH_SHORT).show()
            }

            BillingClient.BillingResponse.ITEM_ALREADY_OWNED -> {
                Toast.makeText(this, "Purchases Failed You Have Already Purchased  ", Toast.LENGTH_SHORT).show()
            }
            BillingClient.BillingResponse.ITEM_UNAVAILABLE -> {
                Toast.makeText(this, "Purchases Failed Currently You Can't Purchased", Toast.LENGTH_SHORT).show()
            }
            BillingClient.BillingResponse.USER_CANCELED -> {
                //Toast.makeText(this,"Purchases Failed USER_CANCELED ",Toast.LENGTH_SHORT).show()
            }

        }

        Log.e(TAG_BILL, "onPurchasesUpdated $responseCode")
    }


    private fun initBillingClient() {
        billingClient = BillingClient.newBuilder(this).setListener(this).build()


        billingClient!!.startConnection(object : BillingClientStateListener {

            override fun onBillingSetupFinished(@BillingClient.BillingResponse responseCode: Int) {
                Log.e(TAG_BILL, "onBillingSetupFinished")
                setPrice(BillingClient.SkuType.SUBS)
                setPrice(BillingClient.SkuType.INAPP)
            }

            override fun onBillingServiceDisconnected() {
                Log.e(TAG_BILL, "onBillingServiceDisconnected")
            }
        })
    }

    private fun setPrice(subs: String) {

        val skuList = ArrayList<String>()

        if (subs.equals(BillingClient.SkuType.SUBS)) {
            skuList.add("11")
            skuList.add("12")
            skuList.add("13")
        } else {
            skuList.add("22")
            skuList.add("23")
            skuList.add("24")
            skuList.add("101")
        }

        val params = SkuDetailsParams.newBuilder()
        params.setSkusList(skuList).setType(subs)

        billingClient!!.querySkuDetailsAsync(params.build()) { responseCode, skuDetailsList ->

            skuDetailsList?.forEach {
                Log.e(TAG_BILL, "querySkuDetailsAsync ${it.price} ** ${it.sku} ** ${it.introductoryPriceCycles} ** ${it.introductoryPricePeriod} ** ${it.introductoryPriceCycles}")

                it.introductoryPriceCycles

                if (subs.equals(BillingClient.SkuType.SUBS)) {
                    ad4.amount = it.price
                    ad5.amount = it.price
                    ad6.amount = it.price

                } else {

                    if(it.sku.equals("101")){

                        adsRemove.amount = it.price

                        mRemoveAmount.text = it.price

                    }else{

                        ad1.amount = it.price
                        ad2.amount = it.price
                        ad3.amount = it.price
                    }
                }
            }
        }


        billingClient!!.queryPurchaseHistoryAsync(subs,
                object : PurchaseHistoryResponseListener {
                    override fun onPurchaseHistoryResponse(responseCode: Int, purchasesList: MutableList<Purchase>?) {

                        if (responseCode == BillingClient.BillingResponse.OK
                                && purchasesList != null) {
                          purchasesList?.forEach {
                              Log.e(TAG_BILL, "onPurchaseHistoryResponse ${it.isAutoRenewing} ** ${it.sku} ${it.originalJson}")

                          }
                        }
                    }

                });
    }

    private fun iniitPucrchaseFlow(adsModel: AdsModel) {
        var responseCode = -1

        val flowParams = BillingFlowParams.newBuilder()
                .setSku(adsModel.id)
                .setType(adsModel.purchaseType)

                .build()

        responseCode = billingClient!!.launchBillingFlow(this@UpgradeActivity, flowParams)

        Log.e(TAG_BILL, " *** *** $responseCode ")
        when (responseCode) {

            BillingClient.BillingResponse.OK -> {
                // Toast.makeText(this,"Purchases Updated ",Toast.LENGTH_SHORT).show()
            }

            BillingClient.BillingResponse.ERROR -> {
                Toast.makeText(this, " ERROR ", Toast.LENGTH_SHORT).show()
            }

            BillingClient.BillingResponse.BILLING_UNAVAILABLE -> {
                Toast.makeText(this, " BILLING_UNAVAILABLE ", Toast.LENGTH_SHORT).show()
            }

            BillingClient.BillingResponse.ITEM_ALREADY_OWNED -> {
                /*  if (adsModel.available!!.toInt() == 0) {
                      getPurchaseData(adsModel)
                  } else {
                    //  Toast.makeText(this, "You have already purchsed this item... ", Toast.LENGTH_SHORT).show()
                      showErrorAlertDialog(adsModel)
                  }*/

                getPurchaseData(adsModel)


            }
            BillingClient.BillingResponse.ITEM_UNAVAILABLE -> {
                Toast.makeText(this, "currently item not available ", Toast.LENGTH_SHORT).show()
            }
            BillingClient.BillingResponse.USER_CANCELED -> {
                // Toast.makeText(this,"USER_CANCELED ",Toast.LENGTH_SHORT).show()
            }

        }
    }


    var billingClient: BillingClient? = null
    val TAG_BILL = "BillingProcessor"


    var postCount: String = ""
    var storyCount: String = ""
    var liveCount: String = ""
    var REQUEST_PAYMENT_WEBVIEW = 5000
    private var progress: CustomProgress? = null
    private lateinit var userPreferences: UserPreferences

    lateinit var ad1: AdsModel
    lateinit var ad2: AdsModel
    lateinit var ad3: AdsModel
    lateinit var ad4: AdsModel
    lateinit var ad5: AdsModel
    lateinit var ad6: AdsModel
    val adsRemove = AdsModel()

    //lateinit var bp :BillingProcessor

    var currentProduct = AdsModel()


    override fun onDestroy() {
        if (billingClient != null && billingClient!!.isReady()) {
            billingClient!!.endConnection();
            billingClient = null

        }

        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_upgrade)
        supportActionBar!!.hide()

        //  bp = BillingProcessor(this, Constant.SUBSCRIPTION_KEY, this)
        //  bp.initialize()

        ad1 = AdsModel()
        ad2 = AdsModel()
        ad3 = AdsModel()
        ad4 = AdsModel()
        ad5 = AdsModel()
        ad6 = AdsModel()

        initBillingClient()

        userPreferences = MyApplication.instance.getUserPreferences()
        progress = CustomProgress(this)

        if (userPreferences.showAds) {
            mAdslay.visibility = View.VISIBLE
        }

        mRemoveAmount.text = String.format("${getString(R.string.currency)} 3")



        back.setOnClickListener { onBackPressed() }

        mAdslay.setOnClickListener {

            showPayDialog(adsRemove)
        }

        recycleview.layoutManager = GridLayoutManager(this, 2)
        recycleview.adapter = AdsAdapter(this, object : AdsAdapter.Listener {
            override fun onClick(adsModel: AdsModel) {

                showPayDialog(adsModel)

            }
        })

        Handler().postDelayed(object : Runnable {
            override fun run() {
                adsModels()
                getCount()
            }

        }, 100)

    }

    fun showErrorAlertDialog(adsModel: AdsModel) {
        runOnUiThread {
            frame_alt.visibility = View.VISIBLE

            aletOk.setOnClickListener {
                frame_alt.visibility = View.GONE
                getPurchaseData(adsModel)
            }
            aletCancel.setOnClickListener {
                frame_alt.visibility = View.GONE

            }
        }
    }

    private fun adsModels() {

        adsRemove.amount = 3.toString()
        adsRemove.id = "101"
        adsRemove.available = "1"
        adsRemove.purchaseType = BillingClient.SkuType.INAPP
        adsRemove.type = "adsRemove"
        adsRemove.details = getString(R.string.remove_Add)
        adsRemove.imageUrl = R.drawable.border_gradient_upgrade_adsbtn


        ad1.purchaseType = BillingClient.SkuType.INAPP
        ad1.id = "24"
        ad1.type = "post"
        ad1.details = getString(R.string.extra_post)
        ad1.order = "10"
        ad1.imageUrl = R.drawable.grid1


        ad2.purchaseType = BillingClient.SkuType.INAPP
        ad2.id = "23"
        ad2.type = "story"
        ad2.details = getString(R.string.extra_story)
        ad2.order = "10"
        ad2.imageUrl = R.drawable.grid2


        ad3.id = "22"
        ad3.purchaseType = BillingClient.SkuType.INAPP
        ad3.type = "live"
        ad3.details = getString(R.string.extra_live)
        ad3.order = "10"
        ad3.imageUrl = R.drawable.grid3


        ad4.id = "13"
        ad4.purchaseType = BillingClient.SkuType.SUBS
        ad4.type = "post"
        ad4.details = getString(R.string.unlimited_post)
        ad4.order = "-1"
        ad4.imageUrl = R.drawable.grid4


        ad5.id = "12"
        ad5.purchaseType = BillingClient.SkuType.SUBS
        ad5.type = "story"
        ad5.details = getString(R.string.unlimited_story)
        ad5.order = "-1"
        ad5.imageUrl = R.drawable.grid5


        ad6.id = "11"
        ad6.purchaseType = BillingClient.SkuType.SUBS
        ad6.type = "live"
        ad6.details = getString(R.string.unlimited_live)
        ad6.order = "-1"
        ad6.imageUrl = R.drawable.grid6

        (recycleview.adapter as AdsAdapter).adsList.add(ad1)
        (recycleview.adapter as AdsAdapter).adsList.add(ad4)

        (recycleview.adapter as AdsAdapter).adsList.add(ad2)
        (recycleview.adapter as AdsAdapter).adsList.add(ad5)

        (recycleview.adapter as AdsAdapter).adsList.add(ad3)
        (recycleview.adapter as AdsAdapter).adsList.add(ad6)

        (recycleview.adapter as AdsAdapter).notifyDataSetChanged()

    }

    private fun getCount() {
        val param = JSONObject()
        param.put("version", "1.1")
        param.put("user_id", MyApplication.instance.getUserPreferences().id)

        progress!!.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()

                val jsonObject = JSONObject(`object` as String)

                if (jsonObject.has("description")) {


                    val obj = jsonObject.getJSONObject("description")

                    if (obj.has("available_post_count")) {
                        postCount = obj.getString("available_post_count").toString()
                        if (postCount != "-1") {
                            ad1.available = postCount
                            ad4.available = postCount
                            (recycleview.adapter as AdsAdapter).adsList.add(ad1)
                            (recycleview.adapter as AdsAdapter).adsList.add(ad4)
                        }
                    }
                    if (obj.has("available_story_count")) {
                        storyCount = obj.getString("available_story_count").toString()
                        if (storyCount != "-1") {
                            ad2.available = storyCount
                            ad5.available = storyCount
                            (recycleview.adapter as AdsAdapter).adsList.add(ad2)
                            (recycleview.adapter as AdsAdapter).adsList.add(ad5)
                        }
                    }
                    if (obj.has("available_live_count")) {
                        liveCount = obj.getString("available_live_count").toString()
                        if (liveCount != "-1") {
                            ad3.available = liveCount
                            ad6.available = liveCount
                            (recycleview.adapter as AdsAdapter).adsList.add(ad3)
                            (recycleview.adapter as AdsAdapter).adsList.add(ad6)
                        }
                    }


                    if (obj.has("unlimited_post_count")) {
                        val subObj = obj.getJSONObject("unlimited_post_count")
                        val subModel = SubscribeModel()
                        if (subObj.has("status")) {
                            subModel.status = subObj.getBoolean("status")
                            //  ad1.status = subObj.getBoolean("status")
                            ad4.status = subObj.getBoolean("status")
                        }

                        if (subObj.has("activation")) {
                            subModel.activation = subObj.getString("activation")
                            //ad1.activation = subObj.getString("activation")
                            ad4.activation = subObj.getString("activation")
                        }

                        if (subObj.has("expiry")) {
                            subModel.expiry = subObj.getString("expiry")
                            ad1.expiry = subObj.getString("expiry")
                            ad4.expiry = subObj.getString("expiry")
                        }
                        if (subObj.has("count")) {
                            subModel.previousCount = subObj.getInt("count")

                            ad4.available = subObj.getInt("count").toString()
                            ad1.available = subObj.getInt("count").toString()
                        }

                    }


                    if (obj.has("unlimited_story_count")) {
                        val subObj = obj.getJSONObject("unlimited_story_count")
                        val subModel = SubscribeModel()
                        if (subObj.has("status")) {
                            subModel.status = subObj.getBoolean("status")
                            //  ad1.status = subObj.getBoolean("status")
                            ad5.status = subObj.getBoolean("status")
                        }

                        if (subObj.has("activation")) {
                            subModel.activation = subObj.getString("activation")
                            //ad1.activation = subObj.getString("activation")
                            ad5.activation = subObj.getString("activation")
                        }

                        if (subObj.has("expiry")) {
                            subModel.expiry = subObj.getString("expiry")
                            ad2.expiry = subObj.getString("expiry")
                            ad5.expiry = subObj.getString("expiry")
                        }
                        if (subObj.has("count")) {
                            subModel.previousCount = subObj.getInt("count")

                            ad2.available = subObj.getInt("count").toString()
                            ad5.available = subObj.getInt("count").toString()
                        }

                        //   ad4.subModel = subModel
                    }


                    if (obj.has("unlimited_live_count")) {
                        val subObj = obj.getJSONObject("unlimited_live_count")
                        val subModel = SubscribeModel()
                        if (subObj.has("status")) {
                            subModel.status = subObj.getBoolean("status")
                            //  ad1.status = subObj.getBoolean("status")
                            ad6.status = subObj.getBoolean("status")
                        }

                        if (subObj.has("activation")) {
                            subModel.activation = subObj.getString("activation")
                            ad3.activation = subObj.getString("activation")
                            ad6.activation = subObj.getString("activation")
                        }

                        if (subObj.has("expiry")) {
                            subModel.expiry = subObj.getString("expiry")
                            ad3.expiry = subObj.getString("expiry")
                            ad6.expiry = subObj.getString("expiry")
                        }
                        if (subObj.has("count")) {
                            subModel.previousCount = subObj.getInt("count")

                            ad3.available = subObj.getInt("count").toString()
                            ad6.available = subObj.getInt("count").toString()
                        }


                    }


                    runOnUiThread {

                        (recycleview.adapter as AdsAdapter).adsList[0] = ad1
                        (recycleview.adapter as AdsAdapter).adsList[1] = ad4

                        (recycleview.adapter as AdsAdapter).adsList[2] = ad2
                        (recycleview.adapter as AdsAdapter).adsList[3] = ad5

                        (recycleview.adapter as AdsAdapter).adsList[4] = ad3
                        (recycleview.adapter as AdsAdapter).adsList[5] = ad6

                        (recycleview.adapter as AdsAdapter).notifyDataSetChanged()
                    }

                }
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
            }
        }).showCounts(param)
    }

    private fun consumePurchase(adsModel: AdsModel, token: String) {
        progress!!.show()
        billingClient!!.consumeAsync(token) { responseCode2, outToken ->
            Log.e(TAG_BILL, " *** $responseCode2  *** $outToken")
            progress!!.dismiss()

            if (responseCode2 == BillingClient.BillingResponse.OK) {
                iniitPucrchaseFlow(adsModel)
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.e(TAG_BILL, "onActivityResult $requestCode")

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_PAYMENT_WEBVIEW) {
            val tapPaymentModel = data!!.getParcelableExtra<TapPaymentModel>("model")
            ServiceRequest(object : ApiResponseListener {
                override fun onCompleted(`object`: Any) {
                    ServiceRequest(object : ApiResponseListener {
                        override fun onCompleted(`object`: Any) {
                            if (tapPaymentModel.description == "adsRemove") { // For Remove Ads and else for Pay For Purchase Free Story, Post and Live Stream
                                updateRemoveAds(tapPaymentModel)
                            } else {
                                //updateAllCount(tapPaymentModel)
                            }
                        }

                        override fun onError(errorMessage: String) {
                            CustomDialog(this@UpgradeActivity).showErrorDialog(getString(R.string.payment_update))
                        }

                    }).updateCount("post")
                }

                override fun onError(errorMessage: String) {
                    CustomDialog(this@UpgradeActivity).showErrorDialog(getString(R.string.payment_update))
                }

            }).verificationBudgePayment(tapPaymentModel)

        }
    }

    private fun openPaymentActivity(adsModel: AdsModel) {

        val tapPaymentModel = TapPaymentModel()
        tapPaymentModel.amount = adsModel.amount.toString()
        tapPaymentModel.description = adsModel.type.toString()
        tapPaymentModel.orderNumber = adsModel.order.toString()

        val intent = Intent(this@UpgradeActivity, WebViewActivity::class.java)
        intent.putExtra("url", ApiUrl.getPaymetLoader)
        intent.putExtra("tapModel", tapPaymentModel)
        startActivityForResult(intent, REQUEST_PAYMENT_WEBVIEW)
    }

    private fun updateRemoveAds(tapPaymentModel: TapPaymentModel? = null) {

        progress!!.show()
        val param = JSONObject()
        param.put("version", "1.1")
        param.put("user_id", userPreferences.id)
        param.put("is_ads", false)

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                CustomDialog(this@UpgradeActivity).showErrorDialog(getString(R.string.purcahse_success))
                userPreferences.showAds = false
                mAdslay.visibility = View.GONE
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(this@UpgradeActivity, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).noMoreAds(param)
    }

    private fun updateAllCount(tapPaymentModel: TapPaymentModel) {

        progress!!.show()
        val param = JSONObject()
        param.put("version", "1.1")
        param.put("user_id", userPreferences.id)
        param.put("type", tapPaymentModel.description)
        param.put("count", tapPaymentModel.orderNumber)

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                CustomDialog(this@UpgradeActivity).showErrorDialog(getString(R.string.purcahse_success))
                getCount()
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                CustomDialog(this@UpgradeActivity).showErrorDialog(getString(R.string.hint_payment_failed_text))
                Toast.makeText(this@UpgradeActivity, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).updateAllCount(param)
    }

    private fun updateAllCount(purchase: Purchase) {

        progress!!.show()
        val param = JSONObject()
        param.put("version", "1.1")
        param.put("user_id", userPreferences.id)
        param.put("type", currentProduct.type)
        param.put("is_ads", false)

        if (currentProduct.purchaseType.equals(BillingClient.SkuType.INAPP)) {
            param.put("count", "10")
        } else {
            param.put("count", "-1")
        }

        Log.e("updateAllCount", "**  ${param.toString()}")
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                CustomDialog(this@UpgradeActivity).showErrorDialog(getString(R.string.purcahse_success))
                getCount()
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                CustomDialog(this@UpgradeActivity).showErrorDialog(getString(R.string.hint_payment_failed_text))
                Toast.makeText(this@UpgradeActivity, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).updateAllCount(param)
    }

    private fun showPayDialog(adsModel: AdsModel) {

        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.customdialog_purchase_more)
        val wlp = dialog.window.attributes
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog.window.attributes = wlp
        dialog.window.attributes.windowAnimations = R.style.dialog_anim

        val mAnimation = TranslateAnimation(0F, 0F, 0F, 50F);
        mAnimation.duration = 500;
        mAnimation.fillAfter = true;
        mAnimation.repeatCount = -1;
        mAnimation.repeatMode = Animation.REVERSE
        dialog.badge_alert_im_thumb.animation = mAnimation

        if (adsModel.type == "adsRemove") {
            dialog.mDescription.text = String.format("${getString(R.string.purchase_remove_ads_description)}\n\n${adsModel.details}")
        } else {
            dialog.mDescription.text = String.format("${getString(R.string.purchase_more_description)}\n\n${adsModel.details}")
        }

        dialog.mDetails.text = adsModel.details
        dialog.mAmount.text = String.format("${getString(R.string.currency)} ${adsModel.amount}")

        Glide.with(this).load(adsModel.imageUrl).apply(RequestOptions().override(200)).into(dialog.mImage)

        dialog.mPay.setOnClickListener {
            dialog.dismiss()
            currentProduct = adsModel
            //openPaymentActivity(adsModel)
            iniitPucrchaseFlow(adsModel)
        }

        dialog.mCancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun verifyValidSignature(signedData: String, signature: String): Boolean {
        try {
            return Security.verifyPurchase(Constant.SUBSCRIPTION_KEY, signedData, signature)
        } catch (e: IOException) {
            Log.e(TAG_BILL, "Got an exception trying to validate a purchase: $e")
            return false
        }

    }


    private fun getPurchaseData(adsModel: AdsModel) {

        // val obj = JSONObject(originalJson)
        val param = JSONObject()
        param.put("version", "1.1")
        param.put("productId", adsModel.id)
        param.put("user_id", userPreferences.id)
        // param.put("androidObject", obj)

        progress!!.show()
        Log.e("updateAllCount", "**  ${param.toString()}")
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()

                val jsonObject = `object` as JSONObject
                if (!jsonObject.getBoolean("error")) {
                    if (jsonObject.has("description")) {
                        val array = jsonObject.getJSONArray("description")
                        for (i in 0 until array.length()) {
                            val obj = array.getJSONObject(i)
                            if (obj.has("android_response")) {
                                val tokenObj = obj.getJSONObject("android_response")

                                if (tokenObj.has("purchaseToken")) {
                                    val token = tokenObj.getString("purchaseToken")
                                    adsModel.purchaseToken = token
                                    consumePurchase(adsModel, token)
                                }
                            }
                        }
                    }
                }
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                CustomDialog(this@UpgradeActivity).showErrorDialog(errorMessage)
                // Toast.makeText(this@UpgradeActivity, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).getPurchaseData(param)
    }

    fun setPurchasedData(originalJson: String) {

        // val str = "{\"productId\":\"21\",\"purchaseToken\":\"pkofecnlcgeoffeedgakboil.AO-J1OwIh_P-U6Dz3aAUxjVzMv9oTLm1-yNghjthJTi1ccjDKf5rLoHsKYlFKIHCV5Y0naSKS21rzdpTZGReZzt1uIfrgrt8eL4jovFMIoxB7FQGTG9Hi5M\",\"purchaseTime\":1554989233065,\"developerPayload\":null}\n"
        val obj = JSONObject(originalJson)
        val param = JSONObject()
        param.put("version", "1.1")
        param.put("user_type", "android")
        param.put("user_id", userPreferences.id)
        param.put("androidObject", obj)

        Log.e("updateAllCount", "**  ${param.toString()}")
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()

            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                CustomDialog(this@UpgradeActivity).showErrorDialog(errorMessage)
                // Toast.makeText(this@UpgradeActivity, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).updatePurchaseData(param)

    }

    /*  val skuList =ArrayList<String>()
                skuList.add("11")
                skuList.add("12")
                skuList.add("13")

                val params = SkuDetailsParams.newBuilder()
                params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS)

                billingClient!!.querySkuDetailsAsync(params.build()) { responseCode1, skuDetailsList ->
                    skuDetailsList?.forEach {
                        Log.e(TAG_BILL,"querySkuDetailsAsync ${it.sku} ${it.description}  ${it.subscriptionPeriod}")
                    }
                }

                billingClient!!.queryPurchaseHistoryAsync(BillingClient.SkuType.INAPP) {
                    responseCode2, result ->
                    Log.e(TAG_BILL,"queryPurchaseHistoryAsync ${result.size}")
                    result?.forEach {
                        Log.e(TAG_BILL,"queryPurchaseHistoryAsync 22 ${it.sku} ${it.purchaseTime}  ${it.isAutoRenewing} ${it.originalJson} ")
                    }
                }
                */
}
