package com.mazadlive.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.models.CountryModel
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.activity_new_address.*
import org.json.JSONObject

class NewAddressActivity : BaseActivity() {

    var code = ""
    private  var countryModel: CountryModel?= null
    lateinit var progress:CustomProgress

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_address)
        supportActionBar?.hide()

        progress = CustomProgress(this)
        icon1.visibility = View.GONE
        icon2.visibility = View.GONE
        icon3.visibility = View.GONE
        icon4.visibility = View.GONE
        icon5.visibility = View.GONE
        icon6.visibility = View.GONE
        icon7.visibility = View.GONE
        icon8.visibility = View.GONE

        save_btn.setOnClickListener {

            if(valdation()){
                addMyAddress()
            }

        }

        mOpenCountry.setOnClickListener {
            CustomDialog(this).showCountryDialog(object : DialogListener() {
                @SuppressLint("SetTextI18n")
                override fun okClick(any: Any) {

                    countryModel = any as CountryModel
                    code = countryModel!!.code
                    code = code.replace("+","")
                    codeinput.setText("+$code")
                    mNumber.setText("")
                    mCountry.setText(countryModel!!.name)
                }
            },false,false)
        }

        viewCountry.setOnClickListener {
        }

    }

    private fun addMyAddress() {
        val param = JSONObject()
        param.put("version","1.1")
        param.put("user_id",MyApplication.instance.getUserPreferences().id)
        param.put("addressLineOne",adLine1.text.toString().trim())
        param.put("addressLineTwo",adLine2.text.toString().trim())
        param.put("city",mCity.text.toString().trim())
        param.put("state",mState.text.toString().trim())
        param.put("country_id",countryModel!!.id)
        param.put("pincode",mPin.text.toString().trim())
        param.put("type","create")
        param.put("is_active",true)
        param.put("address_id","")
        param.put("phone",mNumber.text.toString())
        param.put("name",mUsername.text.toString())

        Log.e("PARAM","${param.toString()}")
        progress.show()
        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                progress.dismiss()

                Toast.makeText(this@NewAddressActivity,getString(R.string.new_address_saved),Toast.LENGTH_SHORT).show()
                setResult(Activity.RESULT_OK)
                finish()
            }

            override fun onError(errorMessage: String) {
              progress.dismiss()
                Toast.makeText(this@NewAddressActivity,"Failed $errorMessage",Toast.LENGTH_SHORT).show()
            }

        }).addAddress(param)
    }

    private fun valdation(): Boolean {

        if(TextUtils.isEmpty(mUsername.text.toString().trim())){
            mUsername.error = getString(R.string.user_cant_empty)
            return false
        }

        if(TextUtils.isEmpty(code)){
            mNumber.error = getString(R.string.select_country_code)
            return false
        }

        if(TextUtils.isEmpty(mNumber.text.toString().trim())){
            mNumber.error = getString(R.string.mobile_cant_empty)
            return false
        }
        if(TextUtils.isEmpty(adLine1.text.toString().trim())){
            adLine1.error = getString(R.string.fill_field)
            return false
        }

        if(TextUtils.isEmpty(adLine2.text.toString().trim())){
            adLine2.error = getString(R.string.fill_field)
            return false
        }

        if(TextUtils.isEmpty(mCity.text.toString().trim())){
            mCity.error = getString(R.string.enter_city_name)
            return false
        }

        if(TextUtils.isEmpty(mState.text.toString().trim())){
            mState.error = getString(R.string.enetr_state_name)
            return false
        }

        if(TextUtils.isEmpty(mPin.text.toString().trim())){
            mPin.error = getString(R.string.enter_pin)
            return false
        }


        return true
    }
}
