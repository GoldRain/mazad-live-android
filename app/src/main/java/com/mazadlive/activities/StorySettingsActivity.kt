package com.mazadlive.activities

import android.os.Bundle
import android.util.Log
import android.view.Window
import android.widget.ImageView
import android.widget.Toast
import com.mazadlive.R
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.UserPreferences
import kotlinx.android.synthetic.main.activity_story_settings.*
import org.json.JSONObject
import java.util.*

class StorySettingsActivity : BaseActivity() {

    private var userRef: UserPreferences? = null
    private var progress: CustomProgress? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_story_settings)
        supportActionBar!!.hide()
        userRef = MyApplication.instance.getUserPreferences()
        progress = CustomProgress(this)



        setData()

        mAuction.setOnClickListener {
            val param = JSONObject()
            param.put("user_id", userRef!!.id)
            param.put("is_auction", !(userRef!!.isAuction))
            param.put("is_buyNow",(userRef!!.isBuyNow))
            changeSetting(param, 0)
        }

        mBuyNow.setOnClickListener {
            val param = JSONObject()
            param.put("user_id", userRef!!.id)
            param.put("is_buyNow", !(userRef!!.isBuyNow))
            param.put("is_auction", (userRef!!.isAuction))
            changeSetting(param, 1)

        }
        mWhatsappOnly.setOnClickListener {
            val param = JSONObject()
            param.put("user_id", userRef!!.id)
            param.put("is_watsappOnly", !(userRef!!.isWhatsAppOnly))
            param.put("is_watsappAndCall", (userRef!!.isWhatsAndCall))
            changeSetting(param, 2)

        }
        mWhatsCall.setOnClickListener {
            val param = JSONObject()
            param.put("user_id", userRef!!.id)
            param.put("is_watsappAndCall", !(userRef!!.isWhatsAndCall))
            param.put("is_watsappOnly", (userRef!!.isWhatsAppOnly))
            changeSetting(param, 3)
        }

        back.setOnClickListener { onBackPressed() }

        mSave.setOnClickListener { finish() }

    }

    private fun setData() {
        mAuction.tag = userRef!!.isAuction
        mBuyNow.tag = userRef!!.isBuyNow
        mWhatsappOnly.tag = userRef!!.isWhatsAppOnly
        mWhatsCall.tag = userRef!!.isWhatsAndCall

        performSwitch(mAuction)
        performSwitch(mBuyNow)
        performSwitch(mWhatsappOnly)
        performSwitch(mWhatsCall)
    }

    private fun performSwitch(imageView: ImageView) {
        if (imageView.tag as Boolean) {
            imageView.setImageResource(R.drawable.ic_switch_on)
        } else {
            imageView.setImageResource(R.drawable.ic_switch_off)
        }
    }

    fun changeSetting(param: JSONObject, index: Int) {
        param.put("version", "1.1")
        Log.e("SETTING_PARAM", "$param")
        progress!!.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()

                when (index) {
                    0 -> {
                        userRef!!.isAuction = !(userRef!!.isAuction)
                        userRef!!.isBuyNow = !(userRef!!.isAuction)


                    }
                    1 -> {
                        userRef!!.isBuyNow = !(userRef!!.isBuyNow)
                        userRef!!.isAuction = !(userRef!!.isBuyNow)

                    }
                    2 -> {
                        userRef!!.isWhatsAppOnly = !(userRef!!.isWhatsAppOnly)
                        userRef!!.isWhatsAndCall = !(userRef!!.isWhatsAppOnly)


                    }
                    3 -> {
                        userRef!!.isWhatsAndCall = !(userRef!!.isWhatsAndCall)
                        userRef!!.isWhatsAppOnly = !(userRef!!.isWhatsAndCall)

                    }
                }
                setData()
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                Toast.makeText(this@StorySettingsActivity, getString(R.string.failed_change_settings), Toast.LENGTH_SHORT).show()
            }
        }).updateStorySettings(param)


    }
}
