package com.mazadlive.activities;

import android.annotation.TargetApi;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.mazadlive.R;
import com.mazadlive.helper.Constant;
import com.mazadlive.utils.AbstractAppPauseApplication;
import com.mazadlive.utils.MyApplication;

public abstract class BaseActivity extends AppCompatActivity {

   // private final String TAG = "BaseActivity";
    private final String TAG = "Advertise";
    private AbstractAppPauseApplication mApplication;

    private boolean mRotated;

    private boolean mTreatRotationAsAppPause = false;
    private InterstitialAd  mInterstitialAd;
    final Handler adHandler = new Handler();

    private String APP_ID = "1340327703072906~6071211849";
    private String UNIT_ID = "1340327703072906/5466170594";

    private Thread adThread= new Thread(new Runnable() {
        @Override
        public void run() {

            if(mInterstitialAd.isLoaded()){
                mInterstitialAd.show();
            }else{
                //mInterstitialAd.loadAd( new AdRequest.Builder().build());
                adHandler.postDelayed(this,10000);
                Log.d(TAG, "The interstitial wasn't loaded yet.");
            }

        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mApplication = (AbstractAppPauseApplication)getApplication();

        Boolean nonConfigState = (Boolean)getLastCustomNonConfigurationInstance();
        if (nonConfigState == null) {
            mRotated = false;
        } else {
            mRotated = nonConfigState.booleanValue();
        }
        Log.d(TAG, "onCreate");

        MobileAds.initialize(this, getString(R.string.ad_app_id));
        mInterstitialAd =  new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.ad_unit_id));


    }

    private void startHandler() {
        if(MyApplication.Companion.getInstance().getUserPreferences().getLoginStatus()){
            mInterstitialAd.loadAd( new AdRequest.Builder().build());
            adHandler.postDelayed(adThread, Constant.Companion.getAD_TIME());
            if(mInterstitialAd.getAdListener() == null){
                mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        Log.d(TAG, "onAdLoaded");
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        Log.d(TAG, "onAdFailedToLoad");
                    }

                    @Override
                    public void onAdOpened() {
                        Log.d(TAG, "onAdOpened");
                        adHandler.removeCallbacks(adThread);
                    }

                    @Override
                    public void onAdLeftApplication() {
                        Log.d(TAG, "onAdLeftApplication.");
                    }

                    @Override
                    public void onAdClosed() {
                        Log.d(TAG, "onAdClosed.");
                        mInterstitialAd.loadAd( new AdRequest.Builder().build());
                        adHandler.postDelayed(adThread, Constant.Companion.getAD_TIME());
                    }
                });
            }
        }
    }

    @Override
    protected void onPause() {
        adHandler.removeCallbacks(adThread);

        super.onPause();
    }

    @Override
    protected void onResume() {
        if(MyApplication.Companion.getInstance().getUserPreferences().getLoginStatus())
            if(MyApplication.Companion.getInstance().getUserPreferences().getShowAds())
                if(Constant.Companion.getSHOW_ADD())
                    startHandler();


        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();

        /*
         * We check to see if onStart() is being called as a result of device
         * orientation change. If NOT, only then we bind to the application. If
         * mRotated is true, we are already bound to the application.
         */
        if (!mRotated) {
            mApplication.bind();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mRotated = false;

        /*
         * First, check if rotations should be treated as app pause. If yes,
         * then _always unbind_ in onStop().
         */
        if (mTreatRotationAsAppPause) {
            mApplication.unbind();
            return;
        }

        /*
         * The following code is only required to be executed if device
         * rotations should NOT be treated as app pauses (this is the default behaviour). 
         * 
         * It checks to see if the onStop() is being called because of an orientation change. 
         * If so, it does NOT perform the unbind.
         */

        mRotated = isBeingRotated();
        if (!mRotated) {
            mApplication.unbind();
        }
    }
    
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private boolean isBeingRotated(){
        /*
         * Fine-tuning of the logic to detect if we are being rotated.
         * >= HC, we have an isChangingConfigurations() which we can check before actual getChangingConfigurations()
         * 
         * This is not available pre-HC. In those cases, we directly call getChangingConfigurations()
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            if(!isChangingConfigurations()){
                return false;
            }
        }
        int changingConfig = getChangingConfigurations();
       // if (BuildConfig.DEBUG) {
            Log.d("android-app-pause",
                    String.format("Changing Config: %d", changingConfig));
        //}
        if ((changingConfig & ActivityInfo.CONFIG_ORIENTATION) == ActivityInfo.CONFIG_ORIENTATION) {
            return true;
        }
        return false;
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return mRotated ? Boolean.TRUE : Boolean.FALSE;
    }

    /**
     * Controls whether device orientation changes get treated as app pause
     * events. If you want device orientation changes to trigger app pause
     * events, call this method with an argument of {@code true}. <br/>
     * <br/>
     * <strong>Note:</strong> This method <strong>must be called from
     * {@code onCreate()} method</strong> of your sub-class of this
     * {@code Activity}.
     * 
     * @param treatRotationAsAppPause If {@code true}, device rotations will
     *            trigger app pause events. Defaults to {@code false}.
     */
    protected void setTreatRotationAsAppPause(boolean treatRotationAsAppPause) {
        this.mTreatRotationAsAppPause = treatRotationAsAppPause;
    }

}
