package com.mazadlive.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.mazadlive.R
import com.mazadlive.adapters.BidsListAdapter
import com.mazadlive.adapters.BidsListSellerAdapter
import com.mazadlive.adapters.PostBidsListAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.helper.GridSpacingItemDecoration
import com.mazadlive.models.BidModel
import com.mazadlive.models.PostModel
import com.mazadlive.models.StoryModel
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.activity_bid_list.*
import org.jetbrains.anko.runOnUiThread
import org.json.JSONObject

class BidListActivity : BaseActivity() {
    val TAG = "BidListActivity"
    var postId = ""
    var storyId = ""
    var type = ""
    var post:PostModel?= null
    var story:StoryModel?= null

    var bidsList = ArrayList<BidModel>()

    lateinit var progress: CustomProgress
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bid_list)
        supportActionBar?.hide()

        progress = CustomProgress(this)
       // Log.e(TAG,"*** ${intent.getStringExtra("post_id")}")
        //Log.e(TAG,"*** ${intent.getParcelableExtra<PostModel>("post")}")
         postId = intent.getStringExtra("type_id");
         storyId = intent.getStringExtra("type_id");
        type = intent.getStringExtra("type");


        if(type.equals("post")){
            post= intent.getParcelableExtra<PostModel>("post")
            bidsList.addAll(post!!.bidsList)

            Log.e(TAG,"TYPE ${post!!.bidsList[0].username}")
        }else{
            if(type.equals("story")){
                story= intent.getParcelableExtra<StoryModel>("story")
                bidsList.addAll(story!!.bidList)
            }

        }




        Log.e(TAG,"$postId ${storyId} ${bidsList.size}")

        bids_list.layoutManager = LinearLayoutManager(this)
        bids_list.addItemDecoration(GridSpacingItemDecoration(1,20,false))
        bids_list.adapter = BidsListSellerAdapter(this, bidsList)
        (bids_list.adapter as BidsListSellerAdapter).listener = OnItemClick()


        back.setOnClickListener {
            onBackPressed()
        }

        if(bidsList.size <=0){
            tx_empty.visibility = View.VISIBLE
            cl_bid.visibility = View.GONE
        }else{
            tx_empty.visibility = View.GONE
            cl_bid.visibility = View.VISIBLE
        }
    }



    inner class OnItemClick:BidsListSellerAdapter.OnItemClick{
        override fun changeBidStatus(status: String, position: Int) {
            changeStatus(status, position)
        }

    }

    private fun changeStatus(status: String, position: Int) {
        val param = JSONObject()
        param.put("user_id",MyApplication.instance.getUserPreferences().id)
        param.put("version","1.1")
        param.put("status",status.toLowerCase())
        param.put("id",postId)
        param.put("bid_id",bidsList[position].id)
        param.put("type",type)

        Log.e(TAG,"STATUS_PARAM ${param.toString()}")
        progress.show()
        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                runOnUiThread {
                    bidsList[position].status = status
                    bids_list.adapter.notifyItemChanged(position)
                }
            }

            override fun onError(errorMessage: String) {
               progress.dismiss()
            }

        }).changeBidStatus(param)
    }
}
