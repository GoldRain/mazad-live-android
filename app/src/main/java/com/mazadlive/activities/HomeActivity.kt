package com.mazadlive.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.github.florent37.runtimepermission.kotlin.askPermission
import com.lite.imagepickerlib.GalleryActivity
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.Socket.SocketEventsListener
import com.mazadlive.Socket.SocketManager
import com.mazadlive.activities.settings.*
import com.mazadlive.service.ContactService
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.database.AppDatabase
import com.mazadlive.fragments.Main.*
import com.mazadlive.helper.Constant
import com.mazadlive.helper.MessageEvent
import com.mazadlive.helper.PicEvent
import com.mazadlive.models.LiveStoryModel
import com.mazadlive.models.TapPaymentModel
import com.mazadlive.models.VoteModel
import com.mazadlive.parser.MessageParser
import com.mazadlive.parser.StoryDataParse
import com.mazadlive.parser.StremDataParse
import com.mazadlive.utils.Alerts
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.UserPreferences
import kotlinx.android.synthetic.main.activity_home.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.lang.Exception

class HomeActivity : BaseActivity() {

    val TAG = "HomeActivity"

    companion object {
        fun start(context: Context, bundle: Bundle?) {

            val intent = Intent(context, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            bundle?.let { intent.putExtras(bundle) }
            context.startActivity(intent)
        }

    }

    private lateinit var ref: UserPreferences

    private val CONATCT_PERMISSION = 11
    var currentFragment: Fragment? = null

    lateinit var progress: CustomProgress

    val onlineHandler = Handler()
    val onLineRunable = object : Runnable {
        override fun run() {
            onlineHandler.postDelayed(this, 60000)
        }
    }


    val socketEventsListener = object : SocketEventsListener() {
        override fun onConnected(vararg args: Any?) {

        }

        override fun onDisconnected(vararg args: Any?) {

        }

        override fun onStartStream(args: String?) {
            Log.e("CategoriesBuyerFragment", "onStartStream  $args")
        }

        override fun onUpdateStream(args: String?) {
            val jsonObject = JSONObject(args)
            if (!jsonObject.getBoolean("error")) {

                val storyModel = LiveStoryModel()
                StremDataParse().parseStream(storyModel, jsonObject)

                EventBus.getDefault().post(storyModel)
            }

        }

        override fun onUpdateProduct(args: String?) {
            val jsonObject = JSONObject(args)
            val storyModel = LiveStoryModel()
            StremDataParse().parseStream(storyModel, jsonObject)

            EventBus.getDefault().post(storyModel)
        }

        override fun onUpdateStory(args: String?) {
            Log.e("CategoriesBuyerFragment", "onUpdateStory  $args")
            // Toast.makeText(activity!!,"onUpdateStory",Toast.LENGTH_SHORT).show()
            val jsonObject = JSONObject(args)
            if (!jsonObject.getBoolean("error")) {
                if (jsonObject.has("description")) {
                    val array = jsonObject.getJSONArray("description")
                    for (i in 0 until array.length()) {
                        val obj = array.getJSONObject(i)
                        val storyModel = StoryDataParse().parse(obj)
                        EventBus.getDefault().post(storyModel)
                    }
                }
            }

        }

        //{"error":false,"message":"Vote updated","post_id":"5c1d178777c5534a7d404517","voteStatus":1,"percent":"100%"}
        override fun onUpdateVote(args: String?) {
            val jsonObject = JSONObject(args)
            if (!jsonObject.getBoolean("error")) {
                val voteModel = VoteModel()
                if (jsonObject.has("post_id")) {
                    voteModel.postId = jsonObject.getString("post_id")
                }

                if (jsonObject.has("voteStatus")) {
                    voteModel.vote = jsonObject.getInt("voteStatus")
                }

                if (jsonObject.has("percent")) {
                    voteModel.percent = jsonObject.getString("percent")
                }

                EventBus.getDefault().post(voteModel)
            }

        }

        override fun onMessageReceived(args: String?) {
            val jsonObject = JSONObject(args)

            val messageData = MessageParser().parse(jsonObject)
            Log.e(TAG, "onMessageReceived ${messageData.room_id} ")
            if (!messageData.sender_id.equals(ref.id)) {

                messageData.message_send = 1
                doAsync {
                    AppDatabase.getAppDatabase().messageDao().insert(messageData)
                    val room = AppDatabase.getAppDatabase().roomDao().getRoom(messageData.room_id)
                    Log.e(TAG, "onMessageReceived ${room.id} ")
                    room.messageData = messageData
                    AppDatabase.getAppDatabase().roomDao().update(room)
                    setMessageRead()
                }
                EventBus.getDefault().post(messageData)
            }

        }
    }

    private fun updateMessageIcon() {

        //updateBottomTab(0)
    }

    override fun onBackPressed() {

        if (currentFragment is HomeFragment) {
            finish()
        } else {
            setHomeFragment()
        }

    }

    override fun onDestroy() {
        onlineHandler.removeCallbacks(onLineRunable)
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)  // Change From Setting Switch account not detected ,so unregister in onDestroy
        }

        super.onDestroy()
    }

    override fun onResume() {
        Constant.CURRENT_STREAM_ID = ""
        Alerts.register(this);
        setMessageRead()
        MyApplication.instance.getSocketManager().setSocketEventsListener(false, socketEventsListener)
        super.onResume()
    }

    override fun onPause() {
        Alerts.unregister(this);
        MyApplication.instance.getSocketManager().setSocketEventsListener(false, null)
        super.onPause()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_home)
        supportActionBar!!.hide()
        progress = CustomProgress(this)


        Constant.setOnLine(true)


        ref = MyApplication.instance.getUserPreferences()
        saveData()
        home.setOnClickListener {
            setHomeFragment()
            closeDrawer()

        }

        invite.setOnClickListener {
            if (checkPermissionContact()) {
                ContactListActivity.start(this)
            }
            closeDrawer()
        }

        followpeople.setOnClickListener {

            if (MyApplication.instance.getUserPreferences().loginStatus) {
                val intent = Intent(this, FollowActivity::class.java)
                intent.putExtra("type", "")
                intent.putExtra("showAll", true)
                startActivity(intent)
            } else {
                CustomDialog(this).createDialogBox()
            }
            closeDrawer()
        }
        ecommerce.setOnClickListener {
            val intent = Intent(this, SettingECommerceActivity::class.java)
            startActivity(intent)
            closeDrawer()
        }
        notification.setOnClickListener {
            setNotificationFragment()
            closeDrawer()
        }
        privacy.setOnClickListener {
            startActivityForResult(Intent(this@HomeActivity, PrivacyPolicyActivity::class.java), 100)
            closeDrawer()
        }
        support.setOnClickListener {
            startActivityForResult(Intent(this@HomeActivity, SupportActivity::class.java), 100)
            closeDrawer()
        }
        setting.setOnClickListener {
            startActivityForResult(Intent(this@HomeActivity, SettingActivity::class.java), 100)
            closeDrawer()
        }
        switch_account.setOnClickListener {
            changeUserType()
            closeDrawer()
        }

        chatList.setOnClickListener {
            startActivity(Intent(this@HomeActivity, ChatListActivity::class.java))
            closeDrawer()
        }

        mLanuage.setOnClickListener {
            startActivity(Intent(this@HomeActivity, LanguageActivity::class.java))
            closeDrawer()
        }

        logout.setOnClickListener {

            doAsync {
                AppDatabase.getAppDatabase().favPostDao().deleteAllFavPost()
                AppDatabase.getAppDatabase().roomDao().deleteAllRooms()
                AppDatabase.getAppDatabase().messageDao().deleteAllMessages()
                AppDatabase.getAppDatabase().roomUserDao().deleteAllRoomUser()
            }


            setOnLine()
            ref.clearAll()
            ref.loginStatus = false
            startActivity(Intent(this@HomeActivity, SignInActivity::class.java))
            closeDrawer()
            MyApplication.instance.getSocketManager().disConnect()
            ActivityCompat.finishAffinity(this@HomeActivity)
        }


        loadlayout()

        onlineHandler.post(onLineRunable)

        getMyChatRooms()


        // Log.e(TAG,"ISNOTI ${intent.getBooleanExtra(Constant.NOTOFICATION_KEY,false)}")
        if (intent.getBooleanExtra(Constant.NOTOFICATION_KEY, false)) {
            val room = intent.getStringExtra(Constant.ROOM_KEY)
            if (!TextUtils.isEmpty(room)) {
                val bundle = intent.extras
                if (bundle != null && bundle.getBoolean(Constant.NOTOFICATION_KEY)) {
                    val intentChat = Intent(this, ChatListActivity::class.java)
                    intentChat.putExtra(Constant.NOTOFICATION_KEY, true)
                    intentChat.putExtra(Constant.ROOM_KEY, room)
                    intentChat.putExtras(bundle)
                    startActivity(intentChat)
                }
            }

        }

        handleInviteLink()
    }

    private fun handleInviteLink() {
        if (!TextUtils.isEmpty(Constant.LINK_URL)) {
            val link = Constant.LINK_URL
            Constant.LINK_URL = "";
            val uri = Uri.parse(link)
            val id = uri.getQueryParameter("id")
            val type = uri.getQueryParameter("type")
            Log.e("linkPostId"," *handleInviteLink* "+id)
            if (type.equals("post")) {
                setHomeFragment()
                setSearchFragment(id)
            } else {
                setHomeFragment(id)
            }
        }else{

            if (checkPermissionContact()) {
                startService(Intent(this, ContactService::class.java))
            }

            setHomeFragment()
        }
    }


    fun setOnLine() {
        val socketManager = MyApplication.instance.getSocketManager()
        if (!socketManager.isConnected) {
            socketManager.socket!!.open()
            socketManager.connect()
        }
        val map = JSONObject()
        map.put("user_id", MyApplication.instance.getUserPreferences().id)
        map.put("is_online", false)
        map.put("user_type", "android")
        map.put("last_login", MyApplication.instance.getUserPreferences().lastLoginTime)
        map.put("playerId", "")

        socketManager.socket!!.emit(SocketManager.EMIT_UPDATE_ONLINE, map)
    }

    fun setMessageRead() {
        doAsync {
            val count = AppDatabase.getAppDatabase().messageDao().getUnreadCount()
            Log.e(TAG, "Count : $count")
            runOnUiThread {
                if (count > 0) {
                    im_msg.setImageResource(R.drawable.ic_msg_alert)
                    // msg_count.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.border_round_red,0)
                } else {
                    // msg_count.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                    im_msg.setImageResource(R.drawable.ic_message)
                }
            }
            Log.e(TAG, "$count ** ")
        }
    }


    fun hideTopLayout(flag: Boolean) {
        Log.e("HIDE_TOP", "CALLLL")
        if (flag) {

            linearLayout.visibility = View.GONE
            /* val anim = AnimationUtils.loadAnimation(this, R.anim.abc_slide_out_bottom)
             linearLayout.startAnimation(anim)
             anim.setAnimationListener(object : Animation.AnimationListener {
                 override fun onAnimationRepeat(animation: Animation?) {

                 }

                 override fun onAnimationEnd(animation: Animation?) {

                     if (linearLayout.visibility == View.VISIBLE) {
                         linearLayout.visibility = View.GONE
                     }
                 }

                 override fun onAnimationStart(animation: Animation?) {

                 }

             })*/
        } else {

            if (linearLayout.visibility == View.GONE) {
                linearLayout.visibility = View.VISIBLE
                /*val anim = AnimationUtils.loadAnimation(this, R.anim.abc_slide_in_bottom)
                linearLayout.visibility = View.VISIBLE
                linearLayout.startAnimation(anim)*/
            }

        }
    }


    private fun checkPermissionContact(): Boolean {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.READ_CONTACTS,
                            Manifest.permission.WRITE_CONTACTS), CONATCT_PERMISSION)

            return false

        }

        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CONATCT_PERMISSION) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

                startService(Intent(this, ContactService::class.java))
            }
        }
    }


    private fun saveData() {
        username.setText(ref.username)
        userEmail.setText(ref.email)
        // Log.e("ProfilePAth: ", "** ${ApiUrl.server}/$profilePath")
        // Glide.with(this@HomeActivity).load(profilePath).apply(RequestOptions().override(100).error(R.drawable.dummy)).into(profile)
    }

    fun openDrawer() {
        drawer.openDrawer(Gravity.START)
    }

    fun closeDrawer() {
        drawer.closeDrawer(Gravity.START)
    }

    @SuppressLint("SetTextI18n")
    fun updateBottomTab(position: Int) {

        val imageViews = ArrayList<ImageView>()
        imageViews.add(image_home)
        imageViews.add(image_search)
        if (MyApplication.instance.getUserPreferences().appModeBuyer) {
            image_orders.setImageResource(R.drawable.ic_orders)
        } else {
            image_orders.setImageResource(android.R.drawable.ic_menu_camera)
        }
        imageViews.add(image_orders)
        imageViews.add(image_notification)
        imageViews.add(image_profile)

        for (i in imageViews.indices) {
            imageViews[i].setColorFilter(ContextCompat.getColor(this, R.color.color_taboff), PorterDuff.Mode.MULTIPLY)
        }
        imageViews[position].setColorFilter(ContextCompat.getColor(this, R.color.color_tabon), PorterDuff.Mode.MULTIPLY)

        val textViews = ArrayList<TextView>()
        textViews.add(text_home)
        textViews.add(text_search)
        if (MyApplication.instance.getUserPreferences().appModeBuyer) {
            text_orders.text = getString(R.string.hint_orders)
        } else {
            text_orders.text = getString(R.string.hint_camera)
        }
        textViews.add(text_orders)
        textViews.add(text_notification)
        textViews.add(text_profile)

        for (i in textViews.indices) {
            textViews[i].setTextColor(ContextCompat.getColor(this, R.color.color_taboff))
        }
        textViews[position].setTextColor(ContextCompat.getColor(this, R.color.color_tabon))


    }

    private fun loadlayout() {


        tab_home.setOnClickListener { setHomeFragment() }

        tab_search.setOnClickListener { setSearchFragment() }

        tab_orders.setOnClickListener { setOrdersFragment() }

        tab_notification.setOnClickListener {
            if (MyApplication.instance.getUserPreferences().loginStatus) {
                setNotificationFragment()
            } else {

                CustomDialog(this).createDialogBox()
            }
        }

        tab_profile.setOnClickListener {

            if (MyApplication.instance.getUserPreferences().loginStatus) {
                val bundle = Bundle()
                bundle.putBoolean("isMe", true)
                setProfileFragment(bundle)
            } else {

                CustomDialog(this).createDialogBox()
            }
        }
    }

    fun setHomeFragment(id: String = "") {

        if (TextUtils.isEmpty(id) && (currentFragment != null && currentFragment is HomeFragment)) {
            return
        }
        Log.e("linkPostId"," *setHomeFragment* "+id)
        val homeFragment = HomeFragment()
        homeFragment.linkPostId = id
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_container, homeFragment, getString(R.string.hint_home))
        updateBottomTab(0)
        transaction.commit()
        currentFragment = homeFragment
    }

    fun setSearchFragment(id: String = "") {
        if (currentFragment != null && currentFragment is SearchFragment) {
            return
        }

        val searchFragment = SearchFragment()
        searchFragment.linkPostId = id

        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_container, searchFragment, getString(R.string.hint_search))
        updateBottomTab(1)
        transaction.commit()

        currentFragment = searchFragment
    }

    fun setOrdersFragment() {

        if (!MyApplication.instance.getUserPreferences().loginStatus) {

            CustomDialog(this).createDialogBox()

            return
        }


        if (MyApplication.instance.getUserPreferences().appModeBuyer) {
            if (currentFragment != null && currentFragment is OrderFragment) {
                return
            }

            val orderFragment = OrderFragment()
            val transaction = supportFragmentManager.beginTransaction()
            transaction.addToBackStack(null)
            transaction.replace(R.id.frame_container, orderFragment, getString(R.string.hint_orders))
            updateBottomTab(2)
            transaction.commit()

            currentFragment = orderFragment
        } else {
            askPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO) {

                startActivityForResult(Intent(this@HomeActivity, GalleryActivity::class.java), 1000)


            }.onDeclined { result ->
                if (result.hasDenied()) {
                    CustomDialog(this).showErrorDialog(getString(R.string.permission_required))
                } else if (result.hasForeverDenied()) {
                    result.foreverDenied.forEach {
                        CustomDialog(this).permissionDialog(getString(R.string.please_accept_permission), object : DialogListener() {
                            override fun yesClick() {
                                super.yesClick()
                                result.goToSettings()
                            }
                        })
                    }
                }
            }

        }

    }

    fun setNotificationFragment() {
        if (currentFragment != null && currentFragment is NotificationsFragment) {
            return
        }

        val notificationFragment = NotificationsFragment()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_container, notificationFragment, "Notification")
        updateBottomTab(3)
        transaction.commit()

        currentFragment = notificationFragment
    }

    fun setProfileFragment(bundle: Bundle?) {
        if (currentFragment != null && currentFragment is ProfileFragment) {
            return
        }

        val profileFragment = ProfileFragment()
        bundle?.let {
            profileFragment.arguments = it
        }
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_container, profileFragment, "Profile")
        transaction.addToBackStack(null)
        updateBottomTab(4)
        transaction.commit()
        currentFragment = profileFragment
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.e("ASDASDSADASDASDSAD", "herfs 6000")
        if (resultCode == Activity.RESULT_OK && requestCode == 100) {
            setNotificationFragment()
        }
        if (resultCode == Activity.RESULT_OK && requestCode == 1000) {


            //  val arrayList = data!!.getStringArrayListExtra("path_list")
            val arrayList = data!!.getStringArrayListExtra("images")
            val isVideo = data.getBooleanExtra("isVideo", false)
            Log.e("onActivityResult", "${arrayList[0]} $isVideo")
            val intent = Intent(this@HomeActivity, PostActivity::class.java)
            intent.putExtra("images", arrayList)
            intent.putExtra("isVideo", isVideo)
            startActivityForResult(intent, 6000)
        }

        if (resultCode == Activity.RESULT_OK && requestCode == 5000) {
            if (currentFragment != null && currentFragment is HomeFragment) {
                val tapPaymentModel = data!!.getParcelableExtra<TapPaymentModel>("model")
                updatePromote(tapPaymentModel.orderNumber)
            }
        }

        if (resultCode == Activity.RESULT_OK && requestCode == 6000) {
            Log.e("ASDASDSADASDASDSAD", "herfs 6000")
            if (currentFragment != null && currentFragment is HomeFragment) {
                (currentFragment as HomeFragment).setRefreshData()
            }
            if (currentFragment != null && currentFragment is ProfileFragment) {
                (currentFragment as ProfileFragment).getUserPost()
            }
        }

        try {
            if (currentFragment != null && currentFragment is ProfileFragment) {

                (currentFragment as ProfileFragment).setPic(requestCode, resultCode, data)

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun updatePromote(postId: String) {
        val jsonObject = JSONObject()
        jsonObject.put("version", "1.1")
        jsonObject.put("post_id", postId)
        jsonObject.put("is_promote", true)

        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                (currentFragment as HomeFragment).setRefreshData()
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                Toast.makeText(this@HomeActivity, "Failed to Promte", Toast.LENGTH_SHORT).show()
            }

        }).updatePromote(jsonObject)
    }

    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    /*override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }*/

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        updateBottomTab(0)
    }

    private fun getAllCountries() {
        autoConfirmedOrders()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {

            }

            override fun onError(errorMessage: String) {

            }

        }).getAllCountry(Constant.countryList)
    }

    private fun autoConfirmedOrders() {
        val param = JSONObject()
        param.put("version", "1.1")
        param.put("user_id", MyApplication.instance.getUserPreferences().id)
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {

            }

            override fun onError(errorMessage: String) {

            }

        }).autoConfirmed(param)

    }

    private fun changeUserType() {

        val map = HashMap<String, String>()
        map.put("version", "1.1")

        map.put("id", ref.id)

        if (ref.appModeBuyer) {
            map.put("type", "seller")
        } else {
            map.put("type", "buyer")
        }

        //Log.e("USER_TYPE","type ${ref!!.appModeBuyer}")

        progress!!.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                //val json = JSONObject(`object`.toString())
                val json = JSONObject(`object`.toString())

                if (json.has("data")) {
                    val obj = json.getJSONObject("data")
                    if (obj.has("username")) {
                        ref.username = obj.getString("username")
                    }

                    if (obj.has("country_code")) {
                        ref.countryCode = obj.getString("country_code")
                    }

                    if (obj.has("phone")) {
                        ref.phone = obj.getString("phone")
                    }

                    if (obj.has("email")) {
                        ref.email = obj.getString("email")
                    }

                    if (obj.has("password")) {
                        ref.password = obj.getString("password")
                    }

                    if (obj.has("type")) {
                        ref.type = obj.getString("type")
                    }

                    if (obj.has("profilePhoto")) {
                        ref.profile = obj.getString("profilePhoto")
                    }

                    if (obj.has("_id")) {
                        ref.id = obj.getString("_id")
                    }

                    ref.appModeBuyer = ref.type.equals("buyer", true)

                }


                if (ref.appModeBuyer) {
                    // MyApplication.instance.getUserPreferences().appModeBuyer = false
                    //fragmentView.switch_account.setImageResource(R.drawable.switch_account_buyer)
                    EventBus.getDefault().post(MessageEvent(true))

                } else {
                    // MyApplication.instance.getUserPreferences().appModeBuyer = true
                    // fragmentView.switch_account.setImageResource(R.drawable.switch_account_seller)
                    EventBus.getDefault().post(MessageEvent(false))
                }
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                CustomDialog(this@HomeActivity).showErrorDialog("Failed to switch account $errorMessage")
            }

        }).changeUserType(map)

    }


    private fun getMyChatRooms() {

        if (!MyApplication.instance.getUserPreferences().loginStatus) {

            return
        }
        val param = JSONObject()
        param.put("user_id", MyApplication.instance.getUserPreferences().id)
        param.put("room_id", "")
        param.put("version", "1.1")
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
            }

            override fun onError(errorMessage: String) {
            }

        }).getRooms(param)
    }


}
