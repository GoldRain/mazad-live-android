package com.mazadlive.activities

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Window
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.mazadlive.R
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import kotlinx.android.synthetic.main.activity_web_view.*
import android.os.Build
import android.annotation.TargetApi
import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.webkit.WebChromeClient
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ApiUrl
import com.mazadlive.api.ServiceRequest
import com.mazadlive.models.TapPaymentModel
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.UserPreferences
import org.json.JSONObject


class WebViewActivity : AppCompatActivity() {

    var TAG = WebViewActivity::class.java.name
    var URL: String = "http://www.google.co.in/"
    lateinit var progress: CustomProgress
    lateinit var tapPaymentModel: TapPaymentModel
    lateinit var userPreferences: UserPreferences

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_web_view)
        supportActionBar!!.hide()

        userPreferences = MyApplication.instance.getUserPreferences()

        if (intent.hasExtra("title")) {
            mTitle.text = intent.getStringExtra("title")
        }
        if (intent.hasExtra("url")) {
            URL = intent.getStringExtra("url")
            if (intent.hasExtra("tapModel")){
                tapPaymentModel = intent.getParcelableExtra("tapModel")
                Log.e("Order_Amount",tapPaymentModel.amount+"   model recevied form upgrade activity")
            }
        }

        if (URL == ApiUrl.getPaymetLoader){

            tapPaymentModel.firstName = userPreferences.username
            tapPaymentModel.receiptEmail = userPreferences.email
            tapPaymentModel.receiptSms = userPreferences.ibanPhoneNumber

            Log.e("Order_Amount",tapPaymentModel.amount+"   send for payment")

            ServiceRequest(object : ApiResponseListener{
                override fun onCompleted(`object`: Any) {
                    mWebView.clearCache(true)
                    mWebView.loadUrl(`object`.toString())
                }

                override fun onError(errorMessage: String) {
                    Log.e(TAG,"On Error = $errorMessage")
                }
            }).getChargeRequest(tapPaymentModel)
        }

        progress = CustomProgress(this@WebViewActivity)
        progress.show()
        mBack.setOnClickListener { onBackPressed() }

        mWebView.clearCache(true);
        mWebView.webChromeClient = WebChromeClient()
        mWebView.settings.javaScriptEnabled = true;
        mWebView.webViewClient = object : WebViewClient() {

            @TargetApi(Build.VERSION_CODES.N)
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                view!!.loadUrl(request!!.url!!.toString())
                return super.shouldOverrideUrlLoading(view, request)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                progress.dismiss()
                Log.e(TAG, "on Finish = $url")
                val uri = Uri.parse(url)
                if(uri!= null){
                    if (url!!.startsWith("${ApiUrl.getPaymetLoader}?chargeid=")) {
                        if (uri.getQueryParameter("result") == "SUCCESS"){
                            tapPaymentModel.chargeid = uri.getQueryParameter("chargeid")
                            tapPaymentModel.crd = uri.getQueryParameter("crd")
                            tapPaymentModel.crdtype = uri.getQueryParameter("crdtype")
                            tapPaymentModel.hash = uri.getQueryParameter("hash")
                            tapPaymentModel.payid = uri.getQueryParameter("payid")
                            tapPaymentModel.ref = uri.getQueryParameter("ref")
                            tapPaymentModel.result = uri.getQueryParameter("result")
                            tapPaymentModel.trackid = uri.getQueryParameter("trackid")
                            setResult(Activity.RESULT_OK,Intent().putExtra("model",tapPaymentModel))
                            finish()
                        }
                    }
                }
            }

            override fun onReceivedError(view: WebView?, errorCode: Int, description: String?, failingUrl: String?) {
                super.onReceivedError(view, errorCode, description, failingUrl)
                progress.dismiss()
                CustomDialog(this@WebViewActivity).showAlertDialog(getString(R.string.srever_error))
                Log.e(TAG, "on Error $errorCode")
            }
        }
        mWebView.loadUrl(URL)

    }
}
