package com.mazadlive.activities

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.RatingBar
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.facebook.drawee.backends.pipeline.Fresco
import com.mazadlive.R
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ApiUrl
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.customviews.ImageOverLayView
import com.mazadlive.helper.Constant
import com.mazadlive.models.OrderModel
import com.mazadlive.models.PostImages
import com.mazadlive.utils.MyApplication
import com.stfalcon.frescoimageviewer.ImageViewer
import kotlinx.android.synthetic.main.activity_feed_back.*
import kotlinx.android.synthetic.main.activity_feed_back.view.*
import org.json.JSONObject

class FeedBackActivity : BaseActivity() {
    val TAG = "FeedBackActivity"
    var order: OrderModel?= null
    lateinit var progress:CustomProgress

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feed_back)
        supportActionBar?.hide()
        progress = CustomProgress(this)
        order = intent.getParcelableExtra("order")

        saveData()
        back.setOnClickListener {
            onBackPressed()
        }
        continue_btn.setOnClickListener {
            addFeedBack()
        }
        continue_btn.visibility = View.GONE

        rating_bar.onRatingBarChangeListener = object :RatingBar.OnRatingBarChangeListener{
            override fun onRatingChanged(ratingBar: RatingBar?, rating: Float, fromUser: Boolean) {
                if(rating > 0){
                    continue_btn.visibility = View.VISIBLE
                }else{
                    continue_btn.visibility = View.GONE
                }
            }

        }

        et_comment.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if(TextUtils.isEmpty(et_comment.text.toString().trim())){
                    continue_btn.visibility = View.GONE
                }else{
                    continue_btn.visibility = View.VISIBLE
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        image.setOnClickListener {

            if(order!!.post != null){
                if (order!!.post!!.postImages[0].isVideo) {
                    var path = order!!.post!!.postImages[0].url
                    val ind = path.lastIndexOf(".")
                    if (ind > 0) {
                        path = path.substring(0, ind) + ".mp4"
                    }

                    val intent = Intent(this!!, StoryCommentActivity::class.java)
                    intent.putExtra("isVideo", true)
                    intent.putExtra("path", path)
                    startActivity(intent)
                } else {
                    showAllImage(order!!.post!!.postImages)
                }
            }else if(order!!.story != null){
                if (order!!.story!!.is_video) {
                    var path = order!!.story!!.url
                    val ind = path.lastIndexOf(".")
                    if (ind > 0) {
                        path = path.substring(0, ind) + ".mp4"
                    }

                    val intent = Intent(this, StoryCommentActivity::class.java)
                    intent.putExtra("isVideo", true)
                    intent.putExtra("path", path)
                    startActivity(intent)
                } else {

                    val imodel = PostImages()
                    imodel.url = order!!.story!!.url
                    val list = ArrayList<PostImages>()
                    list.add(imodel)

                    showAllImage(list )
                }
            }


        }
    }



    private fun showAllImage(list :ArrayList<PostImages>) {
        var currentImagePositon = 0
        Fresco.initialize(this);
        val overView = ImageOverLayView(this)

        val mImageViewerBuilder = ImageViewer.Builder<PostImages>(this, list)
        val mImageViewer = mImageViewerBuilder.setStartPosition(0)
                .setOverlayView(overView)
                .setStartPosition(0)
                .setBackgroundColor(ContextCompat.getColor(this, R.color.colorBlack))
                .setFormatter(object : ImageViewer.Formatter<PostImages> {
                    override fun format(t: PostImages?): String {
                        return t!!.url
                    }

                })

                .show()


        mImageViewerBuilder.setImageChangeListener(object : ImageViewer.OnImageChangeListener {
            override fun onImageChange(positionImage: Int) {
                currentImagePositon = positionImage
            }

        })

        overView.back.setOnClickListener {
            mImageViewer.onDismiss()
        }


    }


    private fun saveData() {
        if(order != null){

            Log.e(TAG," ${order!!.post} ")
            if(order!!.post != null){
                val im  = (order!!.post)!!.postImages[0].url

                Glide.with(this).load(im).apply(RequestOptions().override(500)).into(image)
                if((order!!.post)!!.postImages[0].isVideo){
                    im_video.visibility = View.VISIBLE
                }else{
                    im_video.visibility = View.GONE
                }

                mDes.setText(order!!.post!!.description)
            }else if(order!!.story != null){
                val im  = (order!!.story)!!.url

                Glide.with(this).load(im).apply(RequestOptions().override(500)).into(image)
                if(order!!.story!!.is_video){
                    im_video.visibility = View.VISIBLE
                }else{
                    im_video.visibility = View.GONE
                }

                mDes.text = order!!.story!!.buyNowPrice
            }


        }

    }

    fun addFeedBack(){//type_id = type_id, user_id = user_id, rating = rating, comment = comment, order_id = order_id

        val param = JSONObject()

        param.put("version","1.1")
        param.put("type_id",order!!.typeId)
        param.put("rating","${rating_bar.rating}")
        param.put("comment",et_comment.text.toString().trim())
        param.put("order_id",order!!.orderId)
        param.put("user_id",MyApplication.instance.getUserPreferences().id)

        var url = ApiUrl.addFeedbackOnPost
        if(order!!.orderType.equals("story",true)){
            url = ApiUrl.addFeedbackOnStory
        }

        Log.e(TAG,"PARAM ${param.toString()}")
        progress.show()
        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                Toast.makeText(this@FeedBackActivity,getString(R.string.feedback_sent),Toast.LENGTH_SHORT).show()
                setResult(Activity.RESULT_OK)
                finish()
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                Toast.makeText(this@FeedBackActivity,"Failed $errorMessage",Toast.LENGTH_SHORT).show()
            }

        }).addFeedback(param,url)
    }


}
