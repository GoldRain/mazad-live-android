package com.mazadlive.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Toast
import com.mazadlive.R
import com.mazadlive.Socket.SocketEventsListener
import com.mazadlive.Socket.SocketManager
import com.mazadlive.adapters.ChatListAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.database.AppDatabase
import com.mazadlive.database.MessageData
import com.mazadlive.database.RoomData
import com.mazadlive.database.RoomUserData
import com.mazadlive.helper.Constant
import com.mazadlive.models.ChatMessage
import com.mazadlive.models.UserModel
import com.mazadlive.parser.MessageParser
import com.mazadlive.utils.MyApplication
import com.mazadlive.utils.NetworkUtil
import kotlinx.android.synthetic.main.activity_chat.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ChatActivity : BaseActivity() {

    val TAG = "ChatActivity"
    lateinit var roomId: String
    lateinit var receiverId: String
    lateinit var roomData: RoomData
    lateinit var userData: UserModel

    lateinit var progress: CustomProgress

      var socketManager:SocketManager?= null
    val userRef = MyApplication.instance.getUserPreferences()
    val messageList = ArrayList<MessageData>()

    val onlineHandler = Handler()
    val onLineRunable = object : Runnable {
        override fun run() {
            if (!MyApplication.instance.getSocketManager()!!.isConnected) {
                MyApplication.instance.getSocketManager()!!.connect()
            }

            onlineHandler.postDelayed(this, 10000)
        }
    }


    val socketEventsListener = object : SocketEventsListener() {
        override fun onConnected(vararg args: Any?) {
        }

        override fun onDisconnected(vararg args: Any?) {
        }

        override fun onStartStream(args: String?) {
        }

        override fun onUpdateStream(args: String?) {
        }

        override fun onUpdateStory(args: String?) {
        }

        override fun onUpdateVote(args: String?) {
        }

        override fun onMessageReceived(args: String?) {
            val jsonObject = JSONObject(args)


            val messageData = MessageParser().parse(jsonObject)
            if (!msgMap.containsKey(messageData.id)) {
                Log.e(TAG, "onMessageReceived ${messageData.room_id} ${roomData.id}  ${roomUserdata!!.room_id}")
                if (!messageData.sender_id.equals(userRef.id)) {
                    if (messageData.room_id.equals(roomData.id)) {
                        messageData.message_read = 1
                        messageData.message_send = 1
                        messageList.add(messageData)
                        runOnUiThread {
                            chat_list.adapter.notifyDataSetChanged()
                            scrollToPositon()
                        }
                    }
                }
            }

            msgMap.put(messageData.id, messageData)
        }
    }


    private var roomUserdata: RoomUserData? = null
    private val msgMap = HashMap<String, MessageData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_chat)
        supportActionBar?.hide()


        progress = CustomProgress(this)

        roomData = RoomData()
        receiverId = intent.getStringExtra("userId")

        back.setOnClickListener { onBackPressed() }
        socketManager = MyApplication.instance.getSocketManager()

        chat_list.layoutManager = LinearLayoutManager(this@ChatActivity)
        chat_list.adapter = ChatListAdapter(this, messageList)
        chat_list.adapter.notifyDataSetChanged()

        et_msg.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (TextUtils.isEmpty(et_msg.text.toString().trim())) {
                    send_btn.visibility = View.GONE
                } else {
                    send_btn.visibility = View.VISIBLE
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })


        send_btn.setOnClickListener {
            sendMessage()
        }

        initList()
    }

    private fun initList() {

        doAsync {
            messageList.clear()
            roomUserdata = AppDatabase.getAppDatabase().roomUserDao().getRoomUser(receiverId)
            roomData = AppDatabase.getAppDatabase().roomDao().getRoom(roomUserdata!!.room_id)

            val list = AppDatabase.getAppDatabase().messageDao().getAllMessage(roomUserdata!!.room_id)

           // Log.e(TAG, "MSG_SIZE ${list.size} ${roomUserdata!!.room_id}  ${roomData.id}")
            messageList.addAll(list)
            runOnUiThread {
                chat_list.adapter.notifyDataSetChanged()
                chat_list.scrollToPosition(chat_list.adapter.itemCount - 1)
            }

            Constant.CURRENT_ROOM_ID = roomData.id
            updateRead()

        }
    }

    private fun updateRead() {
        doAsync {
            if (roomData != null) {
                AppDatabase.getAppDatabase().messageDao().updateMessageReadStatus(roomData.id)
            }
        }
    }

    override fun onDestroy() {

        super.onDestroy()
    }

    private lateinit var netWorkChangeReceiver: BroadcastReceiver

    override fun onResume() {

        Log.e(TAG, "onResume")

        if (roomData != null) {

            Constant.CURRENT_ROOM_ID = roomData.id
        }
        Constant.setOnLine(true)

        if (messageList.size > 0) {
            updateRead()
        }

        MyApplication.instance.getSocketManager()!!.setSocketEventsListener(false, socketEventsListener)

        onlineHandler.post(onLineRunable)

        val intentFilterNetwork = IntentFilter()
        intentFilterNetwork.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
         netWorkChangeReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {

                val status = NetworkUtil.getConnectivityStatusString(context)
                Log.e(TAG,"NET> STATUS $status")
                if (status != NetworkUtil.NETWORK_STATUS_NOT_CONNECTED) {
                    if (!socketManager!!.isConnected) {
                        socketManager!!.socket!!.open()
                        socketManager!!.connect()

                    }
                    sendFailedMessage()
                }
            }
        }

        registerReceiver(netWorkChangeReceiver,intentFilterNetwork)

        super.onResume()
    }

    private fun sendFailedMessage() {
        doAsync {
            val messageData = AppDatabase.getAppDatabase().messageDao().getFailedMessage(roomData.id)

            if(messageData != null && (messageData.message_send).toInt() ==0 ){
                Log.e(TAG,"NET> STATUS ${messageData.content}")

                val param = JSONObject()
                param.put("version", "1.1")
                param.put("sender_id", MyApplication.instance.getUserPreferences().id)
                param.put("room_id", roomData.id)
                param.put("message", messageData.content)
                param.put("messageType", messageData.content_type)
                ServiceRequest(object : ApiResponseListener {
                    override fun onCompleted(`object`: Any) {

                        val jsonObject = JSONObject(`object`.toString())
                        val msg = MessageParser().parse(jsonObject)
                        messageData.id = msg.id
                        messageData!!.message_send = 1
                        messageData!!.message_read = 1

                        doAsync {
                            AppDatabase.getAppDatabase().messageDao().updateMessageSend(messageData!!.local_id)

                        }

                        sendFailedMessage()
                    }

                    override fun onError(errorMessage: String) {
                        //  progress.dismiss()
                        //messageData.id = "L_${System.currentTimeMillis()}"
                       // AppDatabase.getAppDatabase().messageDao().insert(messageData)
                        //Toast.makeText(this@ChatActivity, "Message not send", Toast.LENGTH_SHORT).show()
                    }

                }).sendMessage(param)
            }else{
                initList()
            }
        }
    }

    override fun onPause() {

        if (roomData != null) {

            Constant.CURRENT_ROOM_ID = ""
        }

        unregisterReceiver(netWorkChangeReceiver)
        MyApplication.instance.getSocketManager()!!.setSocketEventsListener(false, null)
        onlineHandler.removeCallbacks(onLineRunable)
        super.onPause()
    }

    private fun scrollToPositon() {
        runOnUiThread {
            (chat_list.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(chat_list.adapter.itemCount - 1, 10)
        }
    }

    fun sendMessage() {

        val param = JSONObject()
        param.put("version", "1.1")
        param.put("sender_id", MyApplication.instance.getUserPreferences().id)
        param.put("room_id", roomData.id)
        param.put("message", et_msg.text.toString().trim())
        param.put("messageType", "text")


        var messageData = MessageData()
        messageData.content_type = "text"
        messageData.room_id = roomData.id
        messageData.local_id = "L${System.currentTimeMillis()}"
        messageData.id = "L${System.currentTimeMillis()}"
        messageData.content = et_msg.text.toString()
        messageData.created_at = System.currentTimeMillis()
        messageData.sender_id = MyApplication.instance.getUserPreferences().id
        messageData.message_read = 1

        et_msg.setText("")

        messageList.add(messageData)
        chat_list.adapter.notifyItemChanged(chat_list.adapter.itemCount - 1)
        scrollToPositon()

        doAsync {
            roomData.messageData = messageData
            AppDatabase.getAppDatabase().roomDao().update(roomData)
        }

        //   progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                //  progress.dismiss()
                val jsonObject = JSONObject(`object`.toString())
                messageData = MessageParser().parse(jsonObject)
                messageData.message_send = 1
                messageData.message_read = 1
                runOnUiThread {
                    chat_list.adapter.notifyItemChanged(chat_list.adapter.itemCount - 1)

                }
                doAsync {
                    roomData.messageData = messageData
                    AppDatabase.getAppDatabase().roomDao().update(roomData)
                    AppDatabase.getAppDatabase().messageDao().insert(messageData)
                }

            }

            override fun onError(errorMessage: String) {
                //  progress.dismiss()

               doAsync {
                   AppDatabase.getAppDatabase().messageDao().insert(messageData)
               }

                Toast.makeText(this@ChatActivity, getString(R.string.message_not_send), Toast.LENGTH_SHORT).show()
            }

        }).sendMessage(param)

    }

}
