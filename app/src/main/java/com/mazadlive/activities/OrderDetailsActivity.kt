package com.mazadlive.activities

import android.app.Activity
import android.content.Intent
import android.opengl.Visibility
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.facebook.drawee.backends.pipeline.Fresco
import com.mazadlive.R
import com.mazadlive.customviews.ImageOverLayView
import com.mazadlive.helper.Constant
import com.mazadlive.models.OrderModel
import com.mazadlive.models.PostImages
import com.mazadlive.models.PostModel
import com.mazadlive.utils.MyApplication
import com.stfalcon.frescoimageviewer.ImageViewer
import kotlinx.android.synthetic.main.activity_order_details.*
import kotlinx.android.synthetic.main.view_image_overlay.view.*

class OrderDetailsActivity : BaseActivity() {

    val TAG = "OrderDetailsActivity"
    var order:OrderModel?= null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_details)
        supportActionBar?.hide()

        if (intent.hasExtra(Constant.ORDER_KEY)){
            order = intent.getParcelableExtra(Constant.ORDER_KEY)
        }else{
            return
        }

        if(!MyApplication.instance.getUserPreferences().appModeBuyer){
            hint2.visibility = View.GONE
            card2.visibility = View.GONE
        }

        back.setOnClickListener {
            onBackPressed()
        }

        continue_btn.setOnClickListener {
            val intent = Intent(this,SupportActivity::class.java)
            startActivity(intent)

        }

        image.setOnClickListener {

            if(order!!.post != null){
                if (order!!.post!!.postImages[0].isVideo) {
                    var path = order!!.post!!.postImages[0].url
                    val ind = path.lastIndexOf(".")
                    if (ind > 0) {
                        path = path.substring(0, ind) + ".mp4"
                    }

                    val intent = Intent(this!!, StoryCommentActivity::class.java)
                    intent.putExtra("isVideo", true)
                    intent.putExtra("path", path)
                    startActivity(intent)
                } else {
                    showAllImage(order!!.post!!.postImages)
                }
            }


        }

        hintRate.setOnClickListener {
            val intent = Intent(this,FeedBackActivity::class.java)
            intent.putExtra("order",order)
            startActivityForResult(intent,Constant.FEEDBACK_CODE)
        }

        if(order!!.feedback.equals("pending",true)){
            hintRate.visibility = View.GONE
        }else{
            hintRate.visibility = View.GONE
        }

        saveData()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode ==Constant.FEEDBACK_CODE && resultCode == Activity.RESULT_OK){
            hintRate.visibility = View.GONE
        }
    }
    private fun showAllImage(list :ArrayList<PostImages>) {
        var currentImagePositon = 0
        Fresco.initialize(this);
        val overView = ImageOverLayView(this)

        val mImageViewerBuilder = ImageViewer.Builder<PostImages>(this, list)
        val mImageViewer = mImageViewerBuilder.setStartPosition(0)
                .setOverlayView(overView)
                .setStartPosition(0)
                .setBackgroundColor(ContextCompat.getColor(this, R.color.colorBlack))
                .setFormatter(object : ImageViewer.Formatter<PostImages> {
                    override fun format(t: PostImages?): String {
                        return t!!.url
                    }

                })

                .show()


        mImageViewerBuilder.setImageChangeListener(object : ImageViewer.OnImageChangeListener {
            override fun onImageChange(positionImage: Int) {
                currentImagePositon = positionImage
            }

        })

        overView.back.setOnClickListener {
            mImageViewer.onDismiss()
        }


    }


    private fun saveData() {


        if(order != null){

            Log.e(TAG," ${order!!.post} ")
            if(order!!.post != null){
                val im  = (order!!.post)!!.postImages[0].url

                Glide.with(this).load(im).apply(RequestOptions().override(500)).into(image)
                if((order!!.post)!!.postImages[0].isVideo){
                    im_video.visibility = View.VISIBLE
                }else{
                    im_video.visibility = View.GONE
                }
            }else{
                if(order!!.story != null){
                    Log.e(TAG,"STORY ${order!!.story!!.url}")
                    val im  = order!!.story!!.url

                    Glide.with(this).load(im).apply(RequestOptions().override(500)).into(image)
                    if(order!!.story!!.is_video){
                        im_video.visibility = View.VISIBLE
                    }else{
                        im_video.visibility = View.GONE
                    }
                }

            }
            Log.e("TIME_ACTIVTTY", "${order!!.created_at}")
            order_date.text = Constant.getDateAndTime(order!!.created_at)
            order_id.text = order!!.displayId
            amount.text = order!!.amount

            pay_status.text = order!!.paymentStatus
            order_status.text = order!!.orderStatus
            pay_mode.text = order!!.paymenMethod
            txn_id.text = order!!.txnId


            if (order!!.addressModel != null) {
                var ads = "${order!!.addressModel!!.username}\n${order!!.addressModel!!.addressLine1},${order!!.addressModel!!.addressLine2},${order!!.addressModel!!.city}\n${order!!.addressModel!!.state} ${order!!.addressModel!!.pincode}"
                if (order!!.addressModel!!.countryModel != null) {
                    ads += "\n${order!!.addressModel!!.countryModel!!.name}.\n${order!!.addressModel!!.phone}"
                } else {
                    ads += ".\n${order!!.addressModel!!.phone}"
                }
                address.setText(ads)

            }else{
                address.setText(getString(R.string.address_notfound))
            }
        }

    }
}
