package com.mazadlive.activities

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.animation.AnimationUtils
import com.mazadlive.R
import com.mazadlive.helper.Constant
import com.mazadlive.models.OrderModel
import kotlinx.android.synthetic.main.activity_payment_done.*

class PaymentDoneActivity : BaseActivity() {

    lateinit var orderModel:OrderModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_done)
        supportActionBar?.hide()

        if (intent.hasExtra("order")){
            orderModel = intent.getParcelableExtra("order")
        }

        val anim = AnimationUtils.loadAnimation(this, R.anim.scale)
        val anim2 = AnimationUtils.loadAnimation(this, R.anim.scale_done)

        rocket_image.startAnimation(anim)
        tic.startAnimation(anim2)

        done.setOnClickListener {
            onBackPressed()
        }
        setData()
    }



    private fun setData() {

        paid_amount.text = String.format("${getString(R.string.currency)} ${orderModel.amount}")

        order_id.text = orderModel.orderId
        txn_id.text = orderModel.txnId
        time.text = Constant.getDateAndTime(orderModel.created_at)

        Log.e("SDSADASDSAD",orderModel.paymenMethod+"asdsd")

        if(orderModel.paymenMethod == "card"){
            bank_name.text = "Card"
            mStatus.text = getString(R.string.order_card_status)
        }else{
            bank_name.text = "Cash On Delivery "
            mStatus.text = getString(R.string.order_cash_status)
        }

    }
}
