package com.mazadlive.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.Window
import com.mazadlive.R
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgressDialog
import kotlinx.android.synthetic.main.activity_change_password.*

class ChangePasswordActivity : BaseActivity() {


    companion object {
        fun start(context: Context,bundle: Bundle?){

            val intent = Intent(context,ChangePasswordActivity::class.java)
            bundle.let { intent.putExtras(bundle) }
            context.startActivity(intent)
        }

    }

    var number = ""
    var code = ""
    var email = ""
    private var type = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_change_password)
        supportActionBar!!.hide()

        type = intent.extras.getString("type")

        if(type.equals("email")){
            email = intent.extras.getString("EMAIL")

        }else{
            code = intent.extras.getString("CODE")
            number = intent.extras.getString("NUMBER")

        }
        back.setOnClickListener { super.onBackPressed() }

        change_password.setOnClickListener {
           if(validation()){
               callApi()
           }
        }

    }

    private fun validation(): Boolean {
        if (TextUtils.isEmpty(mPassword1.text.toString().trim()) || mPassword1.text.toString().trim().length < 8) {
            CustomDialog(this).showErrorDialog(getString(R.string.password_suggestion))
            return false
        }
        if (TextUtils.isEmpty(mPassword2.text.toString().trim()) || mPassword2.text.toString().trim().length < 8) {
            CustomDialog(this).showErrorDialog(getString(R.string.password_suggestion))
            return false
        }
        if (mPassword1.text.toString().trim() != mPassword2.text.toString().trim()) {
            CustomDialog(this).showErrorDialog(getString(R.string.password_dont_match))
            return false
        }
        return true
    }

    private fun callApi() {
        val param = HashMap<String,String>()
        param.put("version","1.1")
        param.put("password",mPassword1.text.toString().trim())

        if(type.equals("email")){
            param.put("type","email")
            param.put("email",email)

        }else{
            param.put("country_code",code)
            param.put("type","phone")
            param.put("phone",number)
        }

        var progressDialog = CustomProgressDialog(this)
        progressDialog.displayDialog(true)

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {

                try {
                    progressDialog.hideDialog()

                    val intent = Intent( this@ChangePasswordActivity,SignInActivity::class.java)
                    startActivity(intent)
                    finishAffinity()
                    finish()
                }catch (e:Exception){
                    e.printStackTrace()
                    CustomDialog(this@ChangePasswordActivity).showErrorDialog(getString(R.string.failed_to_change_password))
                }
            }

            override fun onError(errorMessage: String) {
                progressDialog.hideDialog()
                CustomDialog(this@ChangePasswordActivity).showErrorDialog(getString(R.string.failed_to_change_password))     // Change
            }

        }).changePassword(param)
    }
}
