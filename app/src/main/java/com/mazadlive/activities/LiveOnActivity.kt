package com.mazadlive.activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.Window
import android.widget.SeekBar
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.activities.settings.SettingActivity
import com.mazadlive.adapters.CountryAdapter
import com.mazadlive.adapters.CountryAdapterMultiple
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.helper.Constant
import com.mazadlive.models.CountryModel
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.activity_live_on.*
import java.util.ArrayList

class LiveOnActivity : BaseActivity() {

    var scrollFlag: Boolean = false

    private var selectLanguage = "en"

    private val countryList = ArrayList<CountryModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_live_on)
        setTheme(R.style.whiteStatusTheme)
        supportActionBar!!.hide()

        checkAndRequestPermissions()


        countryList.addAll(Constant.countryList)

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorBlack)
        }

        back.setOnClickListener { onBackPressed() }
        start.setOnClickListener {

            if(checkAndRequestPermissions()){
               moveNext()
            }
        }

        setting.setOnClickListener {
            val intent = Intent(this, SettingActivity::class.java)
            startActivity(intent)
        }

        countryList.forEach { it ->
            it.isCheck = it.id.equals(MyApplication.instance.getUserPreferences().countryId)
        }
        val adapter = CountryAdapterMultiple(this,countryList, object : DialogListener() {})
        val linarLayoutManager = LinearLayoutManager(this)
        countries_view.layoutManager = linarLayoutManager
        countries_view.adapter = adapter

        seek_bar.max = 100
        seek_bar.progress = 10

        val listener: SeekBar.OnSeekBarChangeListener = object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                //countries_view.smoothScrollToPosition((adapter.countryList.size * i) / 100)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                scrollFlag = true
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                scrollFlag = false
            }
        }

        seek_bar.setSeekListener(listener)


        countries_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                if (!scrollFlag) {
                    val seekbarPosition = ((linarLayoutManager.findLastVisibleItemPosition() * 100) / adapter.countryList.size);
                    seek_bar.progress = seekbarPosition
                    Log.e("DATA_CHANGE", (linarLayoutManager.findLastVisibleItemPosition().toString() + "------ RECYCLER_VIEW"))
                }

            }
        })

        language_text.setOnClickListener {
            CustomDialog(this).showLanguagePickerDialog(object :DialogListener(){
                override fun okClick(any: Any) {
                    language_text.setText(any.toString())
                    when(any.toString()){
                        "ENGLISH" ->{
                            selectLanguage = "en"
                            language_icon.setImageResource(R.drawable.english_language_icon)

                        }
                        "ARABIC" ->{
                            selectLanguage = "ar"
                            language_icon.setImageResource(R.drawable.ic_saudi_arabia_icon)
                        }
                    }
                    selectLanguage = any.toString()

                }
            })
        }


    }

    private fun moveNext() {

        var selectedCountries = ""
        countryList.forEach {
            if(it.isCheck){
                selectedCountries += "${it.id},"
            }
        }
        if(!TextUtils.isEmpty(selectedCountries)){
            val index = selectedCountries.lastIndexOf(",")
            selectedCountries = selectedCountries.substring(0,index)
        }

        val intent = Intent(this, LiveProductActivity::class.java)
        intent.putExtra("sl",selectLanguage)
        intent.putExtra("sc",selectedCountries)
        startActivity(intent)
        finish()
    }


    private fun checkAndRequestPermissions():Boolean {
        val permissionReadStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        val permissionWriteStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val permissionContacts = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
        val readSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        //val flash = ContextCompat.checkSelfPermission(this, Manifest.permission.)
        val listPermissionsNeeded = ArrayList<String>()


        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
           // array[0] = Manifest.permission.CAMERA
        }
        if (permissionReadStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
           // array[1] = Manifest.permission.READ_EXTERNAL_STORAGE
        }
        if (permissionWriteStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            //array[2] = Manifest.permission.WRITE_EXTERNAL_STORAGE
        }
        if (permissionContacts != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECORD_AUDIO)
           // array[3] = Manifest.permission.RECORD_AUDIO
        }

        val array = arrayOfNulls<String>(listPermissionsNeeded.size)

        for (i in 0 until listPermissionsNeeded.size){
            array[i] = listPermissionsNeeded.get(i)
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    array,
                    1)

            return false
        }

        return true
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode ==1 ){
            val permissionReadStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            val permissionWriteStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            val permissionContacts = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
            val readSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
            val listPermissionsNeeded = ArrayList<String>()

            val array = arrayOfNulls<String>(4)
            if (readSMS != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA)
                array[0] = Manifest.permission.CAMERA
            }
            if (permissionReadStorage != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
                array[1] = Manifest.permission.READ_EXTERNAL_STORAGE
            }
            if (permissionWriteStorage != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                array[2] = Manifest.permission.WRITE_EXTERNAL_STORAGE
            }
            if (permissionContacts != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.RECORD_AUDIO)
                array[3] = Manifest.permission.RECORD_AUDIO
            }

            if (listPermissionsNeeded.isEmpty()) {
                moveNext()
            }
        }
    }
}
