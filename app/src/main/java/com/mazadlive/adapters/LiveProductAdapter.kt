package com.mazadlive.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import com.mazadlive.models.LiveProductModel
import kotlinx.android.synthetic.main.view_live_product.view.*

class LiveProductAdapter(private var context:Context, private  var productsList : ArrayList<LiveProductModel>, val listener: OnItemClick) : RecyclerView.Adapter<LiveProductAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.view_live_product,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val productModel = productsList[position]
        holder.itemView.mSerial.text = productModel.serial
        holder.itemView.mDescription.text = productModel.description
        holder.itemView.mPrice.text = productModel.price

        if(TextUtils.isEmpty(productModel.path)){
            holder.itemView.image_view.setImageResource(R.drawable.dummy)
        }else{
            Glide.with(context).load(productModel.path).apply(RequestOptions().override(80).error(R.drawable.dummy)).into(holder.itemView.image_view)
        }
        holder.itemView.im_delete.setOnClickListener {
            listener.onDelete(position)
        }
        holder.itemView.setOnClickListener {
            listener.onClick(position)
        }
    }

    override fun getItemCount(): Int {
        return productsList.size
    }

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView)

    interface OnItemClick{
        fun onClick(position: Int)
        fun onDelete(position: Int)
    }
}