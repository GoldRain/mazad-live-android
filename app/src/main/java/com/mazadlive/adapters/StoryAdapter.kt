package com.mazadlive.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import com.mazadlive.activities.StoryActivity
import com.mazadlive.controllers.StoryController
import com.mazadlive.helper.Constant
import com.mazadlive.models.StoryModel
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.list_item_story.view.*

class StoryAdapter(var context : Context, var liveStoryList : ArrayList<StoryModel>, val listener:OnClikPost) : RecyclerView.Adapter<StoryAdapter.ViewHolder>() {

    inner class ViewHolder(var view:View) : RecyclerView.ViewHolder(view)

    override fun getItemViewType(position: Int): Int {
        return position
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item_story,parent,false)
        Log.e("StoryAdapter","Notified")
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        Log.e("StoryAdapter","Notified  ${liveStoryList.size}")
        return liveStoryList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val story = liveStoryList[position]

        StoryController(context,holder,story,this,position,listener).control()

    }

    private fun showPopupMenu(view: View,position: Int) {
        val  story  = liveStoryList[position]
        val popupMenu = PopupMenu(context!!, view)
        popupMenu.inflate(R.menu.post_menu)


        if(story.userData!!.id.equals(MyApplication.instance.getUserPreferences().id)){
            popupMenu.menu.findItem(R.id.mHide).setVisible(true)
        }else{
            popupMenu.menu.findItem(R.id.mHide).setVisible(false)
        }

        popupMenu.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.mHide -> {

                }

            }

            true
        }

        popupMenu.show()
    }

    fun openAllStory(position: Int){
        val intent = Intent(context, StoryActivity::class.java)
        intent.putExtra("TYPE", "STORY")
        intent.putExtra("position", position)
        intent.putParcelableArrayListExtra("storyList", liveStoryList)
        context.startActivity(intent)
    }
    interface OnClikPost {
        fun onClickPost(position: Int)
     /*   fun onLikePost(position: Int, param: JSONObject)
        fun onDisLikePost(position: Int, param: JSONObject)
        fun onVoteOnPost(position: Int, param: JSONObject)
        fun onFavoritePost(position: Int, param: JSONObject)
        fun addBidOnPost(position: Int, param: JSONObject)*/
    }
}