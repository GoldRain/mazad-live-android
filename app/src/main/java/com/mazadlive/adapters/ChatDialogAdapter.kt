package com.mazadlive.adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import com.mazadlive.activities.ChatActivity
import com.mazadlive.activities.ChatListActivity
import com.mazadlive.database.AppDatabase
import com.mazadlive.database.RoomData
import com.mazadlive.helper.Constant
import com.mazadlive.models.UserModel
import kotlinx.android.synthetic.main.view_chatdialog_list.view.*
import org.jetbrains.anko.doAsync
import java.util.ArrayList

class ChatDialogAdapter(var context: Context,val list : ArrayList<RoomData>, val listener:OnItemClick ) : RecyclerView.Adapter<ChatDialogAdapter.ViewHolder>() {

val TAG = "ChatDialogAdapter"
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.view_chatdialog_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val room = list[position]
        doAsync {
            val count = AppDatabase.getAppDatabase().messageDao().getUnreadCountForGroup(room.id)
         //   Log.e(TAG,"UnreadCount $count")

            (context as Activity).runOnUiThread {
                if(count >0){
                    holder.itemView.mProfileImage.setBackgroundResource(R.drawable.round_border_primary)
                }else{
                    holder.itemView.mProfileImage.setBackgroundResource(R.drawable.round_border_gray)
                }
            }
        }

        Log.e(TAG,"userList ${room.id}")
        if(room.userList!!.size >0){
            val user = room.userList!![0]

            holder.itemView.mUsername.text = (user.username)
            Glide.with(context!!).load(user.profile).apply(RequestOptions().override(80).error(R.drawable.dummy)).into(holder.itemView.mProfileImage)
        }

        if(room!!.messageData!= null){
            holder.itemView.mDate.visibility = View.VISIBLE
            holder.itemView.mLastMessage.visibility = View.VISIBLE
            holder.itemView.mDate.text = (Constant.getTime(room!!.messageData!!.created_at))
            holder.itemView.mLastMessage.text = room.messageData!!.content
        }else{
            holder.itemView.mDate.visibility = View.GONE
            holder.itemView.mLastMessage.visibility = View.GONE

        }


        holder.itemView.setOnClickListener {
            listener.onClick(position)
        }
    }

    interface OnItemClick{

        fun onClick(position:Int);

        fun onSelect(position:Int);
    }
}