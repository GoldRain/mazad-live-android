package com.mazadlive.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import com.mazadlive.activities.StoryActivity
import kotlinx.android.synthetic.main.list_item_post_image.view.*

class ListImageAdapter(private var context: Context, val list :ArrayList<String>,val listener:OnItemClick) : RecyclerView.Adapter<ListImageAdapter.ViewHolder>() {

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView)

    var isVideo = false
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item_post_image,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        Glide.with(context).load(list[position]).apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).override(250)).into(holder.itemView.image)

        if(isVideo){
            holder.itemView.im_video.visibility = View.VISIBLE
        }else{
            holder.itemView.im_video.visibility = View.GONE
        }

        holder.itemView.setOnClickListener {
            /*val intent = Intent(context, StoryActivity::class.java)
            intent.putExtra("TYPE","PROFILE")
            context.startActivity(intent)*/
            listener.onItemClick(position)
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }


    interface OnItemClick{
        fun  onItemClick(position: Int);
    }
}