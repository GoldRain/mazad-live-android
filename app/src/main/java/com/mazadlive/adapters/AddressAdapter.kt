package com.mazadlive.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import com.mazadlive.models.AddressModel
import com.mazadlive.models.AdsModel
import kotlinx.android.synthetic.main.view_address_list.view.*
import kotlinx.android.synthetic.main.view_ads.view.*

class AddressAdapter(private var context:Context, private  var adsList : ArrayList<AddressModel>,val listener: OnItemClick) : RecyclerView.Adapter<AddressAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.view_address_list,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val address = adsList[position]
        var ads = "${address.addressLine1},${address.addressLine2},${address.city}\n${address.state} ${address.pincode}"
        if(address.countryModel != null){
            ads = "${address.addressLine1},${address.addressLine2},${address.city}\n${address.state} ${address.pincode}\n${address.countryModel!!.name}."
        }

        holder.itemView.mUsername.setText(address.username)
        holder.itemView.phone.setText(address.phone)
        holder.itemView.address.text = ads
        holder.itemView.setOnClickListener {
            listener.onClick(position)
        }
    }

    override fun getItemCount(): Int {
        return adsList.size
    }

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
    }

    interface OnItemClick{
        fun onClick(position: Int)
    }
}