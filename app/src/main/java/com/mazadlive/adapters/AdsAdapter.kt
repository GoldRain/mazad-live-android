package com.mazadlive.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import com.mazadlive.helper.Constant
import com.mazadlive.models.AdsModel
import kotlinx.android.synthetic.main.view_ads.view.*

class AdsAdapter(private var context:Context,var listener : Listener) : RecyclerView.Adapter<AdsAdapter.ViewHolder>() {

    var adsList = ArrayList<AdsModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.view_ads,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val adsModel = adsList.get(index = position)

        holder.itemView.details.text = adsModel.details
        holder.itemView.amount.text = String.format("${adsModel.amount}")
        if (adsModel.available != null){

            if(adsModel.status){
                holder.itemView.expiry.visibility = View.VISIBLE
                holder.itemView.mAvailable.text = String.format("Unlimited")
                if(!TextUtils.isEmpty(adsModel.expiry)){

                    holder.itemView.expiry.text = "Expires on ${Constant.getDate(adsModel.expiry.toLong())}"
                }

            }else{
                holder.itemView.expiry.visibility = View.GONE
                holder.itemView.mAvailable.text = String.format("Available ${adsModel.available}")
            }

        }



        Glide.with(context).load(adsModel.imageUrl).apply(RequestOptions().override(200)).into(holder.itemView.image)
        holder.itemView.setOnClickListener { listener.onClick(adsModel) }
    }

    override fun getItemCount(): Int {
        return adsList.size
    }

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView)

    interface Listener{
        fun onClick(adsModel: AdsModel)
    }
}