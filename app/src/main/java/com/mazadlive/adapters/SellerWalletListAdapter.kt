package com.mazadlive.adapters

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.mazadlive.R
import com.mazadlive.activities.OrderDetailsActivity
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.helper.Constant
import com.mazadlive.models.WalletModel
import com.mazadlive.parser.OrderParser
import kotlinx.android.synthetic.main.wallet_list_item_layout.view.*
import org.json.JSONObject

class SellerWalletListAdapter(private val context: Context, private val list: ArrayList<WalletModel>) :
        RecyclerView.Adapter<SellerWalletListAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    lateinit var progress: CustomProgress

    init {
        progress = CustomProgress(context)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.wallet_list_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val walletModel = list[position]

        walletModel.message = "${context.getString(R.string.hint_you_received)} ${context.getString(R.string.currency)} ${walletModel.orderModel!!.amount} for order number ${walletModel.orderModel!!.displayId} "
        holder.itemView.message_txt.text = walletModel.message
        holder.itemView.action_btn.text = walletModel.status

        var walletStatus = walletModel.status.toLowerCase()
        var orderDeliveryStatus = walletModel.orderModel?.orderStatus?.toLowerCase()
        var userPaymnetStatus = walletModel.orderModel?.paymentStatus?.toLowerCase()


        when ((walletStatus).toLowerCase().replace(" ", "")) {
            "completed" -> {
                holder.itemView.action_btn.text = context.getString(R.string.settled)
                holder.itemView.action_btn.setTextColor(ContextCompat.getColor(context, R.color.status_blue_color))
                holder.itemView.action_btn.setBackgroundResource(0)
            }

            "requested" -> {
                holder.itemView.action_btn.text = context.getString(R.string.hint_deposite)
                holder.itemView.action_btn.setTextColor(ContextCompat.getColor(context, R.color.status_blue_color))
                holder.itemView.action_btn.setBackgroundResource(0)
            }

            "pending" -> {
                if (orderDeliveryStatus == "pending" || userPaymnetStatus == "pending") {
                    holder.itemView.action_btn.text = context.getString(R.string.pending)
                    holder.itemView.action_btn.setTextColor(ContextCompat.getColor(context, R.color.status_blue_color))
                    holder.itemView.action_btn.setBackgroundResource(0)
                } else {
                    holder.itemView.action_btn.text = context.getString(R.string.ask)
                    holder.itemView.action_btn.setBackgroundResource(R.drawable.btn_background)
                }
            }
            else -> {
                holder.itemView.action_btn.text = walletModel.status
            }


        }

        holder.itemView.action_btn.setOnClickListener {
            if (walletStatus == "pending" && (orderDeliveryStatus == "pending" || userPaymnetStatus == "pending")) {

            } else {
                requestForPayment(position)
            }
        }

        holder.itemView.setOnClickListener {
            getOrderDeatils(walletModel.order_id)


        }
    }

    private fun getOrderDeatils(orderId : String){

        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                val jsonObject = JSONObject(`object`.toString())

                if (jsonObject.has("description")){
                    val orderModel = OrderParser().parse(jsonObject.getJSONArray("description").getJSONObject(0))
                    val intent = Intent(context, OrderDetailsActivity::class.java)
                    intent.putExtra(Constant.ORDER_KEY, orderModel)
                    context.startActivity(intent)
                }
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                Toast.makeText(context, "${context.getString(R.string.failed)} $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).getOrderDetails(orderId)


    }

    private fun requestForPayment(position: Int) {
        val param = JSONObject()
        param.put("version", "1.1")
        param.put("order_id", list[position].orderModel!!.orderId)


        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                Toast.makeText(context, context.getString(R.string.hint_request_submitted), Toast.LENGTH_SHORT).show()
                list[position].status = "Requested"
                notifyItemChanged(position)
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                Toast.makeText(context, "${context.getString(R.string.failed)} $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).requestPaymentToAdmin(param)
    }

}