package com.mazadlive.adapters

import android.animation.Animator
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import com.mazadlive.activities.PlayerActivity
import com.mazadlive.helper.Constant
import com.mazadlive.models.LiveStoryModel
import kotlinx.android.synthetic.main.view_story.view.*

class LiveStoryAdapter(var context : Context,var liveStoryList : ArrayList<LiveStoryModel>) : RecyclerView.Adapter<LiveStoryAdapter.ViewHolder>() {

    val TAG = "LiveStoryAdapter"
    inner class ViewHolder(var view:View) : RecyclerView.ViewHolder(view)

    override fun getItemViewType(position: Int): Int {
        return position
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.view_story,parent,false)
        Log.e("StoryAdapter","Notified")
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        Log.e("StoryAdapter","Notified  ${liveStoryList.size}")
        return liveStoryList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val story = liveStoryList[position]

        if(!holder.itemView.image_lay.isRippleAnimationRunning){
            holder.itemView.image_lay.startRippleAnimation()
        }
        val anim2 = AnimationUtils.loadAnimation(context, R.anim.zoom_in_out)
        holder.itemView.image.startAnimation(anim2)
      //  holder.itemView.image_lay.()
        if(story.userData != null){
            holder.itemView.username.setText(story.userData!!.username)
        }



        if(TextUtils.isEmpty(story.thumbUrl)){
            if(story.userData != null){
                Glide.with(context).load(story.userData!!.profile).apply(RequestOptions().override(80).error(R.drawable.dummy)).into(holder.itemView.image)
            }

        }else{
            Glide.with(context).load(story.thumbUrl).apply(RequestOptions().override(80).error(R.drawable.dummy)).into(holder.itemView.image)
        }

        holder.itemView.setOnClickListener {
            Log.e(TAG,"STREAM_ID ** ${liveStoryList[position].id}")
            Constant.CURRENT_STREAM_ID = liveStoryList[position].id
            val intent = Intent(context,PlayerActivity::class.java)
            intent.putExtra("TYPE","STORY")
            intent.putExtra("stream",liveStoryList[position])
            context.startActivity(intent)
        }
       // Glide.with(context).load(imageList.get(position).url).apply(RequestOptions().override(80)).into(holder.itemView.image)
    }
}