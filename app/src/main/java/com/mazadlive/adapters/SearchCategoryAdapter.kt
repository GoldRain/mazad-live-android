package com.mazadlive.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.models.PostModel
import kotlinx.android.synthetic.main.view_search_cat.view.*
import org.json.JSONObject
import java.util.*

class SearchCategoryAdapter(var context: Context, var feedlist: ArrayList<PostModel>, val listener: OnClikPost) : RecyclerView.Adapter<SearchCategoryAdapter.ViewHolder>() {

    inner class ViewHolder(var view: View) : RecyclerView.ViewHolder(view)

    var progress: CustomProgress? = null

    init {
        progress = CustomProgress(context)
    }

    override fun getItemCount(): Int {
        return feedlist.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.view_search_cat, parent, false)
        return ViewHolder(view);
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val post  = feedlist[position]


        if(post.postImages.size >0){

            if(post.postImages[0].isVideo){
                holder.itemView.im_video.visibility = View.VISIBLE
            }else{
                holder.itemView.im_video.visibility = View.GONE
            }
            Glide.with(context).load(post.postImages[0].url).apply(RequestOptions().override(500).error(R.drawable.dummy)).into(holder.itemView.image)

        }
       holder.itemView.setOnClickListener {
            listener.onClickPost(position)
        }
    }

    interface OnClikPost {
        fun onClickPost(position: Int)
        fun onLikePost(position: Int, param: JSONObject)
        fun onDisLikePost(position: Int, param: JSONObject)
        fun onVoteOnPost(position: Int, param: JSONObject)
        fun onFavoritePost(position: Int, param: JSONObject)
        fun addBidOnPost(position: Int, param: JSONObject)
        fun onOptionClick(view: View, position: Int)
    }
}