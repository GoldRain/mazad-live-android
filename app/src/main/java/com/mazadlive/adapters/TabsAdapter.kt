package com.mazadlive.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.PagerAdapter

/**
 * Created by bodacious on 15/10/18.
 */
class TabsAdapter(fm : FragmentManager) : FragmentPagerAdapter(fm) {

    var mFragmentList = ArrayList<Fragment>()
    var mFragmentName = ArrayList<String>()

    override fun getItem(position: Int): Fragment {
        return mFragmentList[position]
    }

    override fun getCount(): Int {
        return mFragmentList.size
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mFragmentName[position]
    }
}