package com.mazadlive.adapters

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mazadlive.R
import com.mazadlive.controllers.PostBuyerController
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.models.PostImages
import com.mazadlive.models.PostModel
import java.util.*

class FeedBuyerAdapter(var context: Context, var feedlist: ArrayList<PostModel>, val listener: OnClikPost) : RecyclerView.Adapter<FeedBuyerAdapter.ViewHolder>() {

    inner class ViewHolder(var view: View) : RecyclerView.ViewHolder(view)

    var progress: CustomProgress? = null
    var reportListener: ReportListener?= null
    var amount : String = "1"
    init {
        progress = CustomProgress(context)
    }

    override fun getItemCount(): Int {
        return feedlist.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.view_feed_buyer, parent, false)
        return ViewHolder(view);
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val post = feedlist[position]
        PostBuyerController(context,holder,post,this,position,listener).control()
    }



    inner class PostImageAdapter(val context: Context, private val list: List<PostImages>) : PagerAdapter() {
        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view == `object` as View
        }

        override fun getCount(): Int {
            return list.size
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            (container as ViewPager).removeView(`object` as View)
        }

        override fun getItemPosition(`object`: Any): Int {
            return POSITION_NONE
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val view = LayoutInflater.from(context).inflate(R.layout.view_profile_post, null)

            (container as ViewPager).addView(view)
            return view
        }

    }


    interface OnClikPost {
        fun onClickPost(position: Int)

    }

    open class ReportListener{
        open fun onReport(position: Int){}
        open fun onUnFav(position: Int){}
    }
}