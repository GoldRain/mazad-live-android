package com.mazadlive.adapters

import android.content.Context
import android.content.DialogInterface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.models.UserModel
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.view_search_user.view.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class SearchUserAdapter(var context: Context, val list :ArrayList<UserModel>, val type:String, val listener:OnClick) : RecyclerView.Adapter<SearchUserAdapter.ViewHolder>() {

   // var list = ArrayList<UserModel>()no
    val map = HashMap<String,UserModel>()
    val mapFollower = HashMap<String,String>()

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.view_search_user, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder,position: Int) {

        val model  = list[position]

        holder.itemView.username.setText(model.username)
        Glide.with(context).load(model.profile).apply(RequestOptions().override(500).error(R.drawable.dummy)).into(holder.itemView.image)
        holder.itemView.setOnClickListener {
            listener.onClick(position)
        }
    }


    interface OnClick{
        fun onClick(position: Int)

    }
}