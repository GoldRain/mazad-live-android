package com.mazadlive.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mazadlive.R
import com.mazadlive.controllers.PostSellerController
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.models.PostModel
import com.mazadlive.utils.MyApplication

class FeedSellerAdapter(var context: Context, var feedlist: ArrayList<PostModel>, val listener: OnClikPost) : RecyclerView.Adapter<FeedSellerAdapter.ViewHolder>() {

    inner class ViewHolder(var view: View) : RecyclerView.ViewHolder(view)
    var progress : CustomProgress?= null
    val userRef = MyApplication.instance.getUserPreferences()
    var amount = 0
    var reportListener:ReportListener?= null
    init {
        progress = CustomProgress(context)
    }

    override fun getItemCount(): Int {
        return feedlist.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        var view = LayoutInflater.from(context).inflate(R.layout.view_feed_seller, parent, false)
        return ViewHolder(view);
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val post = feedlist[position]
        PostSellerController(context,holder,post,this,position,listener).control()

    }

    interface OnClikPost {
        fun onClickPost(position: Int)

    }

    open  class ReportListener{
        open  fun onReport(position: Int){}
    }
}