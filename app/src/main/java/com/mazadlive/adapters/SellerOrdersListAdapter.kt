package com.mazadlive.adapters

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.helper.Constant
import com.mazadlive.models.OrderModel
import kotlinx.android.synthetic.main.orders_list_item_layout.view.*
import org.json.JSONObject

class SellerOrdersListAdapter (private val context: Context, private val list: ArrayList<OrderModel>) :
        RecyclerView.Adapter<SellerOrdersListAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    var listener:OnOrderClick?= null
    var progress: CustomProgress = CustomProgress(context)

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.orders_list_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val order = list[position]
        holder.itemView.status_btn.visibility = View.GONE
        holder.itemView.status_txt.visibility = View.GONE

        holder.itemView.order_id.setText(order.displayId)



        if(order.userBuyer != null){
            holder.itemView.ordered_from.text = order.userBuyer!!.username
        }

        if(order.userBuyer != null){
            holder.itemView.amount.text = order.amount
        }



        val status  = order.orderStatus
        Log.e("buyerCountry","${order.orderStatus}** ")
      //  holder.itemView.status_btn.text = status
        holder.itemView.status_btn.text = status
        holder.itemView.status_txt.text = status

        when((order.orderStatus)!!.toLowerCase().replace(" ","")){
            "accepted" ->{
                holder.itemView.status_txt.text = context.getString(R.string.waiting_delivery)
                holder.itemView.status_txt.setTextColor(ContextCompat.getColor(context, R.color.status_blue_color))
                holder.itemView.status_txt.visibility = View.VISIBLE
            }

            "pending" ->{
                holder.itemView.status_btn.text = context.getString(R.string.hint_pending)
                holder.itemView.status_btn.visibility = View.VISIBLE
            }

            "deliveryconfirmed" ->{
                holder.itemView.status_txt.text = context.getString(R.string.delivery_conform)
                holder.itemView.status_txt.setTextColor(ContextCompat.getColor(context, R.color.status_green_color))
                holder.itemView.status_txt.visibility = View.VISIBLE
            }

            "autoconfirmed" ->{
                holder.itemView.status_txt.text = context.getString(R.string.delivery_conform)
                holder.itemView.status_txt.setTextColor(ContextCompat.getColor(context, R.color.status_green_color))
                holder.itemView.status_txt.visibility = View.VISIBLE
            }
            else -> {
                holder.itemView.status_txt.setTextColor(ContextCompat.getColor(context, R.color.status_blue_color))
                holder.itemView.status_txt.visibility = View.VISIBLE
            }


        }
        holder.itemView.status_btn.setOnClickListener {

          if(status.equals("pending",true)){
              CustomDialog(context).showBidConfirmDialog(object : DialogListener() {
                  override fun okClick(any: Any) {
                      if(any.toString().equals("Accept",true)){
                          callapi(position)
                      }

                  }
              },true)
          }

        }

        holder.itemView.setOnClickListener {
            if(listener != null){
                listener!!.onClick(position)
            }
        }
    }

    private fun callapi(position: Int) {

        Log.e("SASDSDASADSADSD","API CAll")
        val param = JSONObject()
        param.put("version","1.1")
        param.put("order_id",list[position].orderId)
        param.put("status","accepted")

        progress.show()
        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                Toast.makeText(context,context.getString(R.string.hint_order_accepted),Toast.LENGTH_SHORT).show()
                list[position].orderStatus = context.getString(R.string.hint_accepted)
                notifyItemChanged(position)
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                Toast.makeText(context,"${context.getString(R.string.failed)} $errorMessage",Toast.LENGTH_SHORT).show()
            }

        }).acceptOrderBySeller(param)
    }

    interface OnOrderClick{
        fun onClick(position: Int)
    }
}