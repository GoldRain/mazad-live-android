package com.mazadlive.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import com.mazadlive.models.OrderModel
import kotlinx.android.synthetic.main.buyer_feedback_list_item_layout.view.*

class BuyerFeedbackListAdapter(private val context: Context, private val list: ArrayList<OrderModel>) :
        RecyclerView.Adapter<BuyerFeedbackListAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    var listener:OnItemCLick?= null
    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.buyer_feedback_list_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val orderModel = list[position]

        setImage(holder,orderModel)



        when(orderModel.orderType){
            "post" ->{
                holder.itemView.title.text = orderModel.post!!.serial
            }

            "story" ->{
                holder.itemView.title.text = orderModel.story!!.serial
            }
        }

        holder.itemView.action_btn.setOnClickListener {
            if(listener != null){
                listener!!.onFeedback(position)
            }
        }

        holder.itemView.setOnClickListener {
            if(listener != null){
                listener!!.onPostClick(position)
            }
        }
    }

    private fun setImage(holder: ViewHolder, orderModel: OrderModel) {

        if(orderModel.post != null){
            holder.itemView.title.text = orderModel.post!!.serial
            if(orderModel.post!!.postImages.size >0){
                Glide.with(context).load(orderModel.post!!.postImages[0].url).apply(RequestOptions().override(100)).into(holder.itemView.image_view)
            }
        }else{
            if(orderModel.story != null){
                holder.itemView.title.text = orderModel.story!!.serial
                Glide.with(context).load(orderModel.story!!.url).apply(RequestOptions().override(100)).into(holder.itemView.image_view)
            }
        }

    }

    interface OnItemCLick{
        fun onFeedback(position: Int)
        fun onPostClick(position: Int)
    }

}