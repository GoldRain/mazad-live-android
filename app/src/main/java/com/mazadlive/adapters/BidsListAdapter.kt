package com.mazadlive.adapters

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mazadlive.R
import com.mazadlive.helper.Constant
import com.mazadlive.models.BidModel
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.bids_list_item_layout.view.*

class BidsListAdapter(private val context: Context, private val list: ArrayList<BidModel>) :
        RecyclerView.Adapter<BidsListAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.bids_list_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val bidModel = list[position]
        holder.itemView.status_btn.visibility = View.GONE
        holder.itemView.status_txt.visibility = View.GONE

        //holder.itemView.order_id.setText(bidModel.id)
        holder.itemView.ordered_amount.setText(bidModel.bidAmount)
        holder.itemView.ordered_for.text = MyApplication.instance.getUserPreferences().CountryName
        holder.itemView.ordered_date.setText(Constant.getDate(bidModel.createdAt.toLong()))

        if (bidModel.status.equals("paid",true)) {
            holder.itemView.status_txt.text = bidModel.status
            holder.itemView.status_txt.setTextColor(ContextCompat.getColor(context, R.color.status_green_color))
            holder.itemView.status_txt.visibility = View.VISIBLE
        } else if (bidModel.status.equals("Cancelled",true)) {
            holder.itemView.status_txt.text = bidModel.status
            holder.itemView.status_txt.setTextColor(ContextCompat.getColor(context, R.color.status_red_color))
            holder.itemView.status_txt.visibility = View.VISIBLE
        } else if (bidModel.status.equals("approved", true)) {
            holder.itemView.status_btn.text = context.getString(R.string.pay_now)
            holder.itemView.status_btn.visibility = View.VISIBLE
        }else{
            holder.itemView.status_txt.text = bidModel.status
            holder.itemView.status_txt.visibility = View.VISIBLE
        }
    }


    interface onClick {
        fun onClick(position: Int);
    }

}