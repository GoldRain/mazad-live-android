package com.mazadlive.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.models.CurrencyModel
import kotlinx.android.synthetic.main.view_countray_item.view.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException


class CurrencyAdapter(var context : Context, private var listener : DialogListener) : RecyclerView.Adapter<CurrencyAdapter.ViewHolder>() {

    var currencyList = ArrayList<CurrencyModel>()

    init {
        try {
            val jsonObj = JSONObject(loadJsonFromAsset())
            val iter = jsonObj.keys()
            while (iter.hasNext()) {
                val key = iter.next()
                try {
                    val value = JSONObject(jsonObj.get(key).toString())
                    val currencyModel = CurrencyModel()
                    currencyModel.symbol = value.getString("symbol")
                    currencyModel.name = value.getString("name")
                    currencyModel.symbol_native = value.getString("symbol_native")
                    currencyModel.decimal_digits = value.getString("decimal_digits")
                    currencyModel.rounding = value.getString("rounding")
                    currencyModel.code = value.getString("code")
                    currencyModel.name_plural = value.getString("name_plural")
                    currencyList.add(currencyModel)
                } catch (e: JSONException) {
                    Log.e("JSON", "EX : " + e.message)
                }

            }
        } catch (e: Exception) {
            Log.e("JSON", "EX : " + e.message)
        }
    }


    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.view_countray_item,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.name.text = currencyList[position].code+" ("+currencyList[position].symbol_native+")"
        //val pngName = currencyList[position].flag!!.trim().toLowerCase()
        //holder.itemView.image.setImageResource(context.resources.getIdentifier("drawable/flag_" + pngName, null, context.getPackageName()))

        holder.itemView.setOnClickListener {
            listener.okClick(currencyList[position].code.toString())
            listener.okClick()
        }
    }

    override fun getItemCount(): Int {
        return currencyList.size
    }

    private fun loadJsonFromAsset(): String? {
        val json: String
        try {
            val `is` = context.getAssets().open("Common-Currency.json")
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            json = String(buffer, Charsets.UTF_8)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }

}