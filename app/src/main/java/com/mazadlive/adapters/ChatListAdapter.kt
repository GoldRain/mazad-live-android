package com.mazadlive.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.laundryapp.controllers.ChatDateController
import com.laundryapp.controllers.ChatReceiveTxtController
import com.laundryapp.controllers.ChatSendTxtController
import com.mazadlive.R
import com.mazadlive.database.MessageData
import com.mazadlive.models.ChatMessage
import com.mazadlive.utils.MyApplication

/**
 * Created by bodacious on 25/10/18.
 */
class ChatListAdapter (private val context: Context, private val items: List<MessageData>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var userRef = MyApplication.instance.getUserPreferences()
    companion object {
        const val typeMessageSend = 1
        const val typeMessageReceive = 2
        const val typeDate = 9
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val m = items[viewType]
        if(m.sender_id.equals(userRef.id)) {
          return  ViewHolderMessageSend(LayoutInflater.from(parent!!.context).inflate(R.layout.chat_send_txt_controller_layout, parent, false))
        }else {
          return  ViewHolderMessageReceive(LayoutInflater.from(parent!!.context).inflate(R.layout.chat_receive_txt_controller_layout, parent, false))
        }


        /* return when(viewType) {
           typeMessageSend -> {
                ViewHolderMessageSend(LayoutInflater.from(parent!!.context).inflate(R.layout.chat_send_txt_controller_layout, parent, false))
            }
            typeMessageReceive -> {
                ViewHolderMessageReceive(LayoutInflater.from(parent!!.context).inflate(R.layout.chat_receive_txt_controller_layout, parent, false))
            }
            typeDate -> {
                ViewHolderDate(LayoutInflater.from(parent!!.context).inflate(R.layout.chat_date_controller_layout, parent, false))
            }
            else -> {
                ViewHolderMessageSend(LayoutInflater.from(parent!!.context).inflate(R.layout.chat_send_txt_controller_layout, parent, false))
            }
            }*/

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val chatMessage = items[position]

        if(chatMessage.sender_id.equals(userRef.id)) {
            ChatSendTxtController(context, holder as ViewHolderMessageSend, chatMessage, this, position).control()
        }else {
            ChatReceiveTxtController(context, holder as ViewHolderMessageReceive, chatMessage, this, position).control()
        }


        /*when(chatMessage.msgType) {
            typeMessageSend -> { ChatSendTxtController(context, holder as ViewHolderMessageSend, chatMessage, this, position).control() }
            typeMessageReceive -> { ChatReceiveTxtController(context, holder as ViewHolderMessageReceive, chatMessage, this, position).control() }
            typeDate -> { ChatDateController(context, holder as ViewHolderDate, chatMessage, this, position).control() }
        }*/
    }

    inner class ViewHolderMessageSend(itemView: View) : RecyclerView.ViewHolder(itemView)
    inner class ViewHolderMessageReceive(itemView: View) : RecyclerView.ViewHolder(itemView)
    inner class ViewHolderDate(itemView: View) : RecyclerView.ViewHolder(itemView)

}