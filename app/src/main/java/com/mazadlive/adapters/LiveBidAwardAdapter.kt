package com.mazadlive.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import com.mazadlive.models.CommentModel
import kotlinx.android.synthetic.main.view_bid_awrd_list.view.*

class LiveBidAwardAdapter(private var context: Context, private var commentList: ArrayList<CommentModel>) : RecyclerView.Adapter<LiveBidAwardAdapter.ViewHolder>() {

    var listener: OnClick? = null
    var isAdmin = false

    companion object {
        var firstPosition = 0
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.view_bid_awrd_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val commentModel = commentList[position]

        if(commentModel.isCheck){
            holder.itemView.im_check.visibility = View.VISIBLE
        }else{
            holder.itemView.im_check.visibility = View.GONE
        }

        holder.itemView.comment.text = commentModel.message
        if (commentModel.userData != null) {
            holder.itemView.mUsername.text = commentModel.userData!!.username
            Glide.with(context).load(commentModel.userData!!.profile).apply(RequestOptions().override(80).error(R.drawable.dummy)).into(holder.itemView.mProfileImage)
        }


        holder.itemView.setOnClickListener {
            commentModel.isCheck = !commentModel.isCheck
            notifyItemChanged(position)
            listener!!.onSelect(position)
        }
    }

    override fun getItemCount(): Int {
        return commentList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface OnClick {

        fun onSelect(position: Int)

    }
}
