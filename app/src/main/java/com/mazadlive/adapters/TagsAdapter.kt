package com.mazadlive.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.models.AdsModel
import com.mazadlive.models.TagModel
import kotlinx.android.synthetic.main.list_item_customdialog_tag.view.*
import kotlinx.android.synthetic.main.view_ads.view.*

class TagsAdapter(private var context:Context, private  var tagList : ArrayList<TagModel>, val listener: DialogListener) : RecyclerView.Adapter<TagsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.list_item_customdialog_tag,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val tag = tagList[position]
        holder.itemView.tag_name.setText(tagList[position].name)

        if(tag.isCheck){
            holder.itemView.im_check.setImageResource(R.drawable.ic_radio_button_checked)

        }else{
            holder.itemView.im_check.setImageResource(R.drawable.ic_radio_button_unchecked)
        }

       holder.itemView.setOnClickListener {
           if(tag.isCheck){
               holder.itemView.im_check.setImageResource(R.drawable.ic_radio_button_unchecked)
           }else{
               holder.itemView.im_check.setImageResource(R.drawable.ic_radio_button_checked)
           }
           tag.isCheck = !tag.isCheck
       }
    }

    override fun getItemCount(): Int {
        return tagList.size
    }

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
    }
}