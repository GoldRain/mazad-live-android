package com.mazadlive.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import com.mazadlive.activities.PaymentActivity
import com.mazadlive.helper.Constant
import com.mazadlive.models.FeedSellerModel
import com.mazadlive.models.PostModel
import com.mazadlive.models.StoryModel
import kotlinx.android.synthetic.main.view_bid_post_list_buyer.view.*

class PostBidsListBuyerAdapter(private val context: Context, private val list: ArrayList<FeedSellerModel>) : RecyclerView.Adapter<PostBidsListBuyerAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    var listener :OnItemClick?= null

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_bid_post_list_buyer, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val feedSellerModel = list[position]

        if(feedSellerModel.type.equals("post",true)){
            setData(holder,position,feedSellerModel.post!!)
        }else{
            if(feedSellerModel.type.equals("story",true)){
                setData(holder,position,feedSellerModel.story!!)
            }
        }

    }

    @SuppressLint("SetTextI18n")
    private fun setData(holder: ViewHolder, position: Int, postModel: PostModel) {


        val bidModel = postModel.userBid

        if(postModel.postImages.size >0){
            Glide.with(context).load(postModel.postImages[0].url).apply(RequestOptions().override(80)).into(holder.itemView.image_view)
        }

        holder.itemView.mDescription.text = postModel.serial
        holder.itemView.status_btn.visibility = View.GONE
        holder.itemView.status_txt.visibility = View.GONE

        if(bidModel != null){
            holder.itemView.mTime.visibility = View.VISIBLE
            holder.itemView.mAmount.visibility = View.VISIBLE

            if(TextUtils.isEmpty(postModel.userBid!!.createdAt)){
                holder.itemView.mTime.text = "NA"
            }else{
                holder.itemView.mTime.text = Constant.getDate(postModel.userBid!!.createdAt.toLong())
            }

            holder.itemView.mAmount.text = String.format("${context.getString(R.string.currency)} ${postModel.userBid!!.bidAmount}")

            when(bidModel.status.toLowerCase()){
                "paid" ->{
                    holder.itemView.status_txt.text = context.getString(R.string.paid)
                    holder.itemView.status_txt.setTextColor(ContextCompat.getColor(context, R.color.status_green_color))
                    holder.itemView.status_txt.visibility = View.VISIBLE
                }

                "cancelled" ->{
                    holder.itemView.status_txt.text = context.getString(R.string.status_cancelled)
                    holder.itemView.status_txt.setTextColor(ContextCompat.getColor(context, R.color.status_red_color))
                    holder.itemView.status_txt.visibility = View.VISIBLE
                }

                "approved" ->{
                    holder.itemView.status_btn.text = context.getString(R.string.pay_now)
                    holder.itemView.status_btn.visibility = View.VISIBLE
                    holder.itemView.status_btn.setBackgroundResource(R.drawable.btn_background)
                    holder.itemView.frame_btn.isEnabled = true
                    if(postModel.is_story){
                        if(postModel.userBid!!.isPaid){
                            holder.itemView.frame_btn.isEnabled = false
                            holder.itemView.status_btn.setBackgroundResource(R.drawable.btn_background_gray)
                        }else{
                            holder.itemView.frame_btn.isEnabled = true
                        }
                    }else{
                        if(postModel.is_sold){
                            holder.itemView.frame_btn.isEnabled = false
                            holder.itemView.status_btn.setBackgroundResource(R.drawable.btn_background_gray)
                        }else{
                            holder.itemView.frame_btn.isEnabled = true
                        }
                    }
                }

                "pending" ->{
                    holder.itemView.status_txt.text = context.getString(R.string.pending)
                    holder.itemView.status_txt.visibility = View.VISIBLE
                }

                else ->{
                    holder.itemView.status_txt.text = bidModel.status
                    holder.itemView.status_txt.visibility = View.VISIBLE
                }

            }

            holder.itemView.frame_btn.setOnClickListener {
                if (!postModel.is_sold){
                    postModel.amount = bidModel.bidAmount
                    val intent  = Intent(context, PaymentActivity::class.java)
                    postModel.amount = bidModel.bidAmount   // Send Payment Activity to Bid Price
                    intent.putExtra("post",postModel)
                    intent.putExtra("type","post")
                    context.startActivity(intent)
                }else{
                    if(listener!= null){
                        listener!!.onItemClick(position)
                    }
                }
            }

        }else{
            holder.itemView.mTime.visibility = View.GONE
            holder.itemView.mAmount.visibility = View.GONE
            holder.itemView.status_btn.visibility = View.GONE
            holder.itemView.status_txt.visibility = View.GONE
        }

        holder.itemView.setOnClickListener {
            if(listener!= null){
                  listener!!.onItemClick(position)
            }
        }
    }

    private fun setData(holder: ViewHolder, position: Int,storyModel : StoryModel) {

        val bidModel = storyModel.userBid
        Glide.with(context).load(storyModel.url).apply(RequestOptions().override(80)).into(holder.itemView.image_view)

        holder.itemView.mDescription.text = storyModel.serial
        holder.itemView.status_btn.visibility = View.GONE
        holder.itemView.status_txt.visibility = View.GONE

        if(bidModel != null){
            holder.itemView.mTime.visibility = View.VISIBLE
            holder.itemView.mAmount.visibility = View.VISIBLE
            holder.itemView.mAmount.text = String.format("${context.getString(R.string.currency)} ${bidModel.bidAmount}")
            when(bidModel.status.toLowerCase()){
                "paid" ->{
                    holder.itemView.status_txt.text = context.getString(R.string.paid)
                    holder.itemView.status_txt.setTextColor(ContextCompat.getColor(context, R.color.status_green_color))
                    holder.itemView.status_txt.visibility = View.VISIBLE
                }

                "cancelled" ->{
                    holder.itemView.status_txt.text = context.getString(R.string.status_cancelled)
                    holder.itemView.status_txt.setTextColor(ContextCompat.getColor(context, R.color.status_red_color))
                    holder.itemView.status_txt.visibility = View.VISIBLE
                }

                "approved" ->{
                    holder.itemView.status_btn.text = context.getString(R.string.pay_now)
                    holder.itemView.status_btn.visibility = View.VISIBLE
                }

                "pending" ->{
                    holder.itemView.status_txt.text = context.getString(R.string.pending)
                    holder.itemView.status_txt.visibility = View.VISIBLE
                }

                else ->{
                    holder.itemView.status_txt.text = bidModel.status
                    holder.itemView.status_txt.visibility = View.VISIBLE
                }

            }

            holder.itemView.status_btn.setOnClickListener {
                if (!storyModel.is_sold){
                    storyModel.buyNowPrice = bidModel.bidAmount
                    val intent  = Intent(context, PaymentActivity::class.java)
                    storyModel.buyNowPrice = bidModel.bidAmount   // Send Payment Activity to Bid Price
                    intent.putExtra("post",storyModel)
                    intent.putExtra("type","story")
                    context.startActivity(intent)
                }else{
                    if(listener!= null){
                        listener!!.onItemClick(position)
                    }
                }

            }

        }else{
            holder.itemView.mTime.visibility = View.GONE
            holder.itemView.mAmount.visibility = View.GONE
            holder.itemView.status_btn.visibility = View.GONE
            holder.itemView.status_txt.visibility = View.GONE
        }

        holder.itemView.setOnClickListener {
            if(listener!= null){
                  listener!!.onItemClick(position)
            }
        }
    }


    interface OnItemClick{
        fun onItemClick(position: Int);
    }

}
