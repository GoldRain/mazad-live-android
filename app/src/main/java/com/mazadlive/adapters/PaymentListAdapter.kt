package com.mazadlive.adapters

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mazadlive.R
import com.mazadlive.models.PaymentModel
import kotlinx.android.synthetic.main.payment_list_item_layout.view.*

class PaymentListAdapter(private val context: Context, private val list: ArrayList<PaymentModel>) :
        RecyclerView.Adapter<PaymentListAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    var listener:OnItemClick?= null
    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.payment_list_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val paymentModel = list[position]
        holder.itemView.status_btn.visibility = View.GONE
        holder.itemView.status_txt.visibility = View.GONE

        holder.itemView.order_id.text = paymentModel.displayId
        holder.itemView.ordered_for.text = paymentModel.sellerName


        if(paymentModel.orderModel != null){
            holder.itemView.ordered_amount.text = paymentModel.orderModel!!.amount
        }

        holder.itemView.ordered_method.text = paymentModel.paymenMethod

        if (paymentModel.status.equals("pending",true)) {
            holder.itemView.status_txt.text = paymentModel.status
            holder.itemView.status_txt.setTextColor(ContextCompat.getColor(context, R.color.status_blue_color))
            holder.itemView.status_txt.visibility = View.VISIBLE
        } else if (paymentModel.status.equals("requested",true)) {
            holder.itemView.status_txt.text = context.getString(R.string.requested)
            holder.itemView.status_txt.setTextColor(ContextCompat.getColor(context, R.color.status_blue_color))
            holder.itemView.status_txt.visibility = View.VISIBLE
        } else if (paymentModel.status.equals("completed",true)) {
            holder.itemView.status_txt.text = context.getString(R.string.completed)
            holder.itemView.status_txt.setTextColor(ContextCompat.getColor(context, R.color.status_green_color))
            holder.itemView.status_txt.visibility = View.VISIBLE
        } else {
            holder.itemView.status_btn.text = paymentModel.status
            holder.itemView.status_btn.visibility = View.VISIBLE
        }


        holder.itemView.setOnClickListener {
            if(listener != null){
                listener!!.onItemClick(position)
            }
        }
    }


    interface OnItemClick{
        fun onItemClick(position: Int)
    }
}