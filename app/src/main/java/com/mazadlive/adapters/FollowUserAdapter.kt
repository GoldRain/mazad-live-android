package com.mazadlive.adapters

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.activities.UserProfileActivity
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.models.UserModel
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.view_user_list.view.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class FollowUserAdapter(var context: Context, val list :ArrayList<UserModel>,val type:String,val listener:OnClick) : RecyclerView.Adapter<FollowUserAdapter.ViewHolder>() {

   // var list = ArrayList<UserModel>()no
    val map = HashMap<String,UserModel>()
    val mapFollower = HashMap<String,String>()

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.view_user_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder,position: Int) {

        val model  = list[position]
        holder.itemView.mUsername.text = model.username

        if (map.containsKey(model.id)){
            holder.itemView.mButton.text = context.getString(R.string.following)
            holder.itemView.mButton.setBackgroundResource(R.drawable.btn_border)
            holder.itemView.mButton.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary))
        }else{
            holder.itemView.mButton.text = context.getString(R.string.follow)

            holder.itemView.mButton.setBackgroundResource(R.drawable.btn_background)
            holder.itemView.mButton.setTextColor(ContextCompat.getColor(context,R.color.colorWhite))
        }

        if(MyApplication.instance.getUserPreferences().id.equals(model.id)){
            holder.itemView.mButton_layout.visibility = View.GONE
        }else{
            holder.itemView.mButton_layout.visibility = View.VISIBLE
        }

        holder.itemView.setOnClickListener {

            val intent = Intent(context, UserProfileActivity::class.java)
            intent.putExtra("user",model)
            intent.putExtra("isMe",false)
            context.startActivity(intent)

        }

        Glide.with(context).load(model.profile).apply(RequestOptions().override(1500).error(R.drawable.dummy)).into(holder.itemView.mProfileImage)

        holder.itemView.mButton_layout.setOnClickListener {
            if (map.containsKey(model.id)){
                CustomDialog(context).showDialogForFollow(model,object :DialogListener(){
                    override fun okClick() {
                        listener.onFollow(position)
                    }
                })
            }else{
                listener.onFollow(position)
            }

        }
    }


    interface OnClick{
        fun onFollow(position: Int)

    }
}