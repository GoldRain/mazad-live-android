package com.mazadlive.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import com.mazadlive.activities.StoryActivity
import kotlinx.android.synthetic.main.view_profile_post.view.*

class ProfilePostAdapter(private var context: Context) : RecyclerView.Adapter<ProfilePostAdapter.ViewHolder>() {

    var list = ArrayList<String>()

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.view_profile_post,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        holder.itemView.setOnClickListener {
           /* val intent = Intent(context, StoryActivity::class.java)
            intent.putExtra("TYPE","PROFILE")
            context.startActivity(intent)*/
        }

        if(list[position] != ""){
            Glide.with(context).load(list[position]).apply(RequestOptions().override(250)).into(holder.itemView.image)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

}