package com.mazadlive.adapters

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import com.mazadlive.helper.ClickSpan
import com.mazadlive.models.PostFeedbackModel
import kotlinx.android.synthetic.main.seller_feedback_list_item_layout.view.*

class SellerFeedbackListAdapter (private val context: Context, private val list: ArrayList<PostFeedbackModel>) :
        RecyclerView.Adapter<SellerFeedbackListAdapter.ViewHolder>() {

    val TAG = "SellerFeedbackList"
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.seller_feedback_list_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val feedbackModel = list[position]
        Glide.with(context).load(feedbackModel.user_profile).apply(RequestOptions().override(100).error(R.drawable.dummy)).into(holder.itemView.image_view)
        holder.itemView.title.text = context.getString(R.string.feedback)

        holder.itemView.user_name.text = feedbackModel.user_name

        Log.e(TAG,"RATING ${feedbackModel.rating.toFloat().toInt()}")
        holder.itemView.rating_bar.ratingStar = (feedbackModel.rating.toFloat().toInt())

        if (holder.itemView.title.text.contains("You")) {
            ClickSpan.clickify(holder.itemView.title, ContextCompat.getColor(context, R.color.colorPrimary), false, "You", {})
        }
        if (holder.itemView.title.text.contains("Product")) {
            ClickSpan.clickify(holder.itemView.title, ContextCompat.getColor(context, R.color.colorPrimary), false, "Product", {})
        }
        holder.itemView.country_name.text = feedbackModel.country!!.name

        val commentData = feedbackModel.comment
        var commentText: String
        if (commentData.length > 100) {
            commentText = commentData.substring(0, 180) + context.getString(R.string.hint_read_more)
            holder.itemView.comment_txt.text = commentText
            ClickSpan.clickify(holder.itemView.comment_txt, ContextCompat.getColor(context, R.color.color_sellerfragment_readmore), false, context.getString(R.string.hint_read_more2), {})
        } else {
            commentText = commentData
            holder.itemView.comment_txt.text = commentText
        }
    }

}

//"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."