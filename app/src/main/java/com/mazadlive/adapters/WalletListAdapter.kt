package com.mazadlive.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mazadlive.R
import com.mazadlive.models.WalletModel
import kotlinx.android.synthetic.main.wallet_list_item_layout.view.*

class WalletListAdapter(private val context: Context, private val list: ArrayList<WalletModel>) :
        RecyclerView.Adapter<WalletListAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.wallet_list_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val walletModel = list[position]
        holder.itemView.message_txt.text = walletModel.message
        holder.itemView.action_btn.text = context.getString(R.string.deposite)
    }

}