package com.mazadlive.adapters

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.helper.Constant
import com.mazadlive.models.BidModel
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.bids_list_item_layout.view.*

class BidsListSellerAdapter(private val context: Context, private val list: ArrayList<BidModel>) :
        RecyclerView.Adapter<BidsListSellerAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    var listener:OnItemClick?= null

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.bids_list_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val bidModel = list[position]
        holder.itemView.status_btn.visibility = View.GONE
        holder.itemView.status_txt.visibility = View.GONE

        holder.itemView.order_id.setText(bidModel.id)
        holder.itemView.ordered_amount.setText(bidModel.bidAmount)
        Log.e("PARAM_USER_NAME","${list[position].username} ${bidModel.username}")

        holder.itemView.ordered_for.text = bidModel.username

        if(!TextUtils.isEmpty(bidModel.createdAt)){
            holder.itemView.ordered_date.setText(Constant.getDate(bidModel.createdAt.toLong()))
        }else{
            holder.itemView.ordered_date.setText("NA")
        }


       // Log.e("CREATED", "BID ${bidModel.createdAt}")

        when(bidModel.status.toLowerCase()){
            "approved" ->{
                holder.itemView.status_txt.text = bidModel.status
                holder.itemView.status_txt.setTextColor(ContextCompat.getColor(context, R.color.status_blue_color))
                holder.itemView.status_txt.visibility = View.VISIBLE
            }

            "cancelled" ->{
                holder.itemView.status_txt.text = bidModel.status
                holder.itemView.status_txt.setTextColor(ContextCompat.getColor(context, R.color.status_red_color))
                holder.itemView.status_txt.visibility = View.VISIBLE
            }

            "pending" ->{
                holder.itemView.status_btn.text = context.getString(R.string.pending)
                holder.itemView.status_btn.visibility = View.VISIBLE
            }

            else ->{

                holder.itemView.status_txt.text = bidModel.status
                holder.itemView.status_txt.setTextColor(ContextCompat.getColor(context, R.color.status_blue_color))
                holder.itemView.status_txt.visibility = View.VISIBLE
                if(TextUtils.isEmpty(bidModel.status)){
                    holder.itemView.status_txt.text = "NA"
                }
            }

        }



        holder.itemView.status_btn.setOnClickListener {
           if(bidModel.status.equals("pending",true)){
               CustomDialog(context).showBidConfirmDialog(object : DialogListener() {
                   override fun okClick(any: Any) {
                       if(listener != null){
                           listener!!.changeBidStatus(any.toString(),position)
                       }
                   }
               })
           }
        }
    }


    interface OnItemClick {
        fun changeBidStatus(status :String, position: Int);
    }

}