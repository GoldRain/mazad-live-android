package com.mazadlive.adapters

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.mazadlive.R
import kotlinx.android.synthetic.main.view_id_list.view.*
import java.util.*

/**
 * Created by bodacious on 24/5/18.
 */
class SpinnerListAdapter(val context: Context, private val items:ArrayList<String>) : BaseAdapter() {

    override fun getItem(p0: Int): Any {
     return  items[p0]
    }

    override fun getItemId(p0: Int): Long {
       return p0.toLong()
    }

    override fun getCount(): Int {
       return items.size
    }

    override fun getView(position: Int, p1: View?, p2: ViewGroup?): View {

        val view= (context as Activity).layoutInflater.inflate(R.layout.view_id_list,null)
        view.id_name.text = items[position]

        return  view
    }


}