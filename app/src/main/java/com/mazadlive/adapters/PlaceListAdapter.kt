package com.mazadlive.adapters

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mazadlive.R
import com.mazadlive.activities.SearchPostActivity
import com.mazadlive.models.CategoryModel
import kotlinx.android.synthetic.main.category_list_item_layout.view.*
import java.util.*


class PlaceListAdapter(private val context: Context, val list: ArrayList<CategoryModel>) :
        RecyclerView.Adapter<PlaceListAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.category_list_item_layout, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = list[position]

        val rnd = Random()
        val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))

        holder.itemView.image_view.setColorFilter(color, android.graphics.PorterDuff.Mode.SRC_IN);
        holder.itemView.title.text = model.name
        holder.itemView.oneText.text = (model.name)!!.substring(0, 1).toUpperCase()
        holder.itemView.mCount.text = model.number

        holder.itemView.setOnClickListener {
            val intent = Intent(context, SearchPostActivity::class.java)
            intent.putExtra("model", model)
            context.startActivity(intent)
        }
    }

}