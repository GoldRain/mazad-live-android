package com.mazadlive.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import kotlinx.android.synthetic.main.image_list_odd_item_layout.view.*
import kotlinx.android.synthetic.main.image_list_even_item_layout.view.*

class ImagesListAdapter (private val context: Context, private val list: ArrayList<ArrayList<String>>) : RecyclerView.Adapter<ImagesListAdapter.ViewHolder>() {

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view)

    override fun getItemViewType(position: Int): Int {
        return position % 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = when(viewType) {
            0 -> LayoutInflater.from(parent.context).inflate(R.layout.image_list_even_item_layout, parent, false)
            1 -> LayoutInflater.from(parent.context).inflate(R.layout.image_list_odd_item_layout, parent, false)
            else -> LayoutInflater.from(parent.context).inflate(R.layout.image_list_even_item_layout, parent, false)
        }
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val images = list[position]

        Log.e("TAG_DATA",position.toString())

        if (position % 2 == 0) {
            Glide.with(context).load(images[0]).apply(RequestOptions().override(250)).into(holder.itemView.even_image_1)
            Glide.with(context).load(images[1]).apply(RequestOptions().override(250)).into(holder.itemView.even_image_2)
            Glide.with(context).load(images[2]).apply(RequestOptions().override(250)).into(holder.itemView.even_image_3)
            Glide.with(context).load(images[3]).apply(RequestOptions().override(250)).into(holder.itemView.even_image_4)
            Glide.with(context).load(images[4]).apply(RequestOptions().override(250)).into(holder.itemView.even_image_5)
        } else {
            Glide.with(context).load(images[0]).apply(RequestOptions().override(250)).into(holder.itemView.odd_image_1)
            Glide.with(context).load(images[1]).apply(RequestOptions().override(250)).into(holder.itemView.odd_image_2)
            Glide.with(context).load(images[2]).apply(RequestOptions().override(250)).into(holder.itemView.odd_image_3)
            Glide.with(context).load(images[3]).apply(RequestOptions().override(250)).into(holder.itemView.odd_image_4)
            Glide.with(context).load(images[4]).apply(RequestOptions().override(250)).into(holder.itemView.odd_image_5)
        }
    }

}