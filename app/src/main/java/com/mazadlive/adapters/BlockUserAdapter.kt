package com.mazadlive.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import com.mazadlive.models.UserModel
import kotlinx.android.synthetic.main.view_user_list.view.*
import java.util.*

class BlockUserAdapter(var context: Context,val list : ArrayList<UserModel>, val listener:OnClick) : RecyclerView.Adapter<BlockUserAdapter.ViewHolder>() {



    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.view_user_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val userModel = list[position]
        holder.itemView.mUsername.text = userModel.username

        Glide.with(context).load(userModel.profile).apply(RequestOptions().override(80)).into(holder.itemView.mProfileImage)
        holder.itemView.mButton.text = context.getString(R.string.unblock)

        holder.itemView.mButton.setOnClickListener {
            listener.unBlock(position)
        }
    }

    interface OnClick{
        fun unBlock(position: Int)
    }

}