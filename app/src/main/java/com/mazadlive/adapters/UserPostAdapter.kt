package com.mazadlive.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import com.mazadlive.activities.StoryActivity
import com.mazadlive.models.PostModel
import kotlinx.android.synthetic.main.view_profile_post.view.*

class UserPostAdapter(private var context: Context,val list:ArrayList<PostModel>) : RecyclerView.Adapter<UserPostAdapter.ViewHolder>() {

    var listener:OnItemClick?= null

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.view_profile_post,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val post = list[position]
        if (post.postImages.size > 0 && post.postImages[0].isVideo) {
            holder.itemView.im_video.visibility = View.VISIBLE
        } else {
            holder.itemView.im_video.visibility = View.GONE
        }

        holder.itemView.setOnClickListener {
           if(listener != null){
               listener!!.onItemClick(position)
           }
        }

        Log.e("postUSERS","** ${post.userData.username}")
        if(post.postImages.size >0){
            Log.e("postImages","** ${post.postImages[0].url}")
            Glide.with(context).load(post.postImages[0].url).apply(RequestOptions().override(250)).into(holder.itemView.image)
        }

       /* if(list[position] != ""){
           // Glide.with(context).load(list[position]).apply(RequestOptions().override(250)).into(holder.itemView.image)
        }*/
    }



    override fun getItemCount(): Int {
        return list.size
    }

    interface OnItemClick{
        fun onItemClick(position: Int)
    }
}