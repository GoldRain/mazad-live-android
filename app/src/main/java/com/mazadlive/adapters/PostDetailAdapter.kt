package com.mazadlive.adapters

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.telephony.PhoneNumberUtils
import android.telephony.SmsManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.mazadlive.R
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.models.PostImages
import com.mazadlive.models.PostModel
import kotlinx.android.synthetic.main.view_feed_buyer.view.*
import org.json.JSONObject
import java.util.*

class PostDetailAdapter(var context: Context, var feedlist: ArrayList<PostModel>, val listener: OnClikPost) : RecyclerView.Adapter<PostDetailAdapter.ViewHolder>() {

    inner class ViewHolder(var view: View) : RecyclerView.ViewHolder(view)

    var progress: CustomProgress? = null
    var amount = 0
    init {
        progress = CustomProgress(context)
    }

    override fun getItemCount(): Int {
        return feedlist.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.customdialog_post_view, parent, false)
        return ViewHolder(view);
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val post = feedlist[position]

    }

    private fun setLikePost(holder: ViewHolder) {
        if (holder.itemView.heart.tag as Boolean) {
            holder.itemView.heart.setImageResource(R.drawable.ic_heart_on)
            holder.itemView.heart.tag = true
        } else {
            holder.itemView.heart.setImageResource(R.drawable.ic_heart)
            holder.itemView.heart.tag = false
        }
    }

    private fun setStar(holder: ViewHolder) {
        if (holder.itemView.start.tag as Boolean) {
            holder.itemView.start.setImageResource(R.drawable.ic_star_on)
            holder.itemView.start.tag = true
        } else {
            holder.itemView.start.setImageResource(R.drawable.ic_star)
            holder.itemView.start.tag = false
        }
    }

    private fun setDisLike(holder: ViewHolder) {
        if (holder.itemView.dislike.tag as Boolean) {
            holder.itemView.like.setImageResource(R.drawable.ic_like)
            holder.itemView.dislike.setImageResource(R.drawable.ic_unlike_on)
            holder.itemView.like.tag = false
            holder.itemView.dislike.tag = true
        } else {
            holder.itemView.dislike.setImageResource(R.drawable.ic_dislike)
            holder.itemView.dislike.tag = false
            //holder.itemView.like.setImageResource(R.drawable.ic_like)
            //  holder.itemView.like.tag = false

        }
    }

    private fun setLike(holder: ViewHolder) {

        if ((holder.itemView.like.tag as Boolean)) {
            holder.itemView.like.setImageResource(R.drawable.ic_like_on)
            holder.itemView.dislike.setImageResource(R.drawable.ic_dislike)
            holder.itemView.like.tag = true
            holder.itemView.dislike.tag = false

        } else {
            holder.itemView.like.setImageResource(R.drawable.ic_like)
            holder.itemView.like.tag = false
            // holder.itemView.dislike.setImageResource(R.drawable.ic_unlike_on)

            //  holder.itemView.dislike.tag = true
        }
    }

    fun sendSMS(phoneNo: String, msg: String) {
        try {
            val smsManager = SmsManager.getDefault()
            smsManager.sendTextMessage(phoneNo, null, msg, null, null)
            Toast.makeText(context, "Message Sent",
                    Toast.LENGTH_LONG).show()
        } catch (ex: Exception) {
            Toast.makeText(context, ex.message.toString(),
                    Toast.LENGTH_LONG).show()
            ex.printStackTrace()
        }

    }

    private fun sendWhatsAppMsg() {
        val smsNumber = "0123456789"
        val sendIntent = Intent("android.intent.action.MAIN")


        try {
            sendIntent.component = ComponentName("com.whatsapp", "com.whatsapp.Conversation")
            sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(smsNumber) + "@s.whatsapp.net")//phone number without "+" prefix
            context.startActivity(sendIntent)
        } catch (e: Exception) {
            Toast.makeText(context, context.getString(R.string.no_watsapp), Toast.LENGTH_SHORT).show()
        }

    }


    inner class PostImageAdapter(val context: Context, private val list: List<PostImages>) : PagerAdapter() {
        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view == `object` as View
        }

        override fun getCount(): Int {
            return list.size
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            (container as ViewPager).removeView(`object` as View)
        }

        override fun getItemPosition(`object`: Any): Int {
            return POSITION_NONE
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val view = LayoutInflater.from(context).inflate(R.layout.view_profile_post, null)

            (container as ViewPager).addView(view)
            return view
        }

    }


    interface OnClikPost {
        fun onClickPost(position: Int)
        fun onLikePost(position: Int, param: JSONObject)
        fun onDisLikePost(position: Int, param: JSONObject)
        fun onVoteOnPost(position: Int, param: JSONObject)
        fun onFavoritePost(position: Int, param: JSONObject)
        fun addBidOnPost(position: Int, param: JSONObject)
        fun onOptionClick(view: View, position: Int)
    }


}