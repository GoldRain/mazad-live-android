package com.mazadlive.adapters

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import com.mazadlive.helper.ClickSpan
import com.mazadlive.helper.Constant
import com.mazadlive.models.ActivityModel
import com.mazadlive.models.PostModel
import com.mazadlive.models.StoryModel
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.activities_list_item_layout.view.*
import kotlinx.android.synthetic.main.customdialog_post_view_recycle.*

class ActivitiesListAdapter(private val context: Context, private val list: ArrayList<ActivityModel>) :
        RecyclerView.Adapter<ActivitiesListAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.activities_list_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val activity = list[position]

        if(activity.type.equals("post") && activity.post != null && activity.post!!.postImages.size >0){
            Glide.with(context).load(activity.post!!.postImages[0].url).apply(RequestOptions().override(100)).into(holder.itemView.extra_image1)
        }

        if(activity.user != null){
            Glide.with(context).load(activity.user!!.profile).apply(RequestOptions().override(100).error(R.drawable.dummy)).into(holder.itemView.user_image)
            holder.itemView.user_name.text = activity.user!!.username
        }
        holder.itemView.activity_txt.text = activity.message


        holder.itemView.activity_time.text = (Constant.getDateAndTime(activity.created_at))

        holder.itemView.setOnClickListener {
            showProfileDialog(list[position])
        }

       /* holder.itemView.user_name.text = activity.userName
        holder.itemView.activity_txt.text = activity.activityTxt
        holder.itemView.activity_time.text = activity.activityTime

        if(holder.itemView.activity_txt.text.contains("product name")){
            ClickSpan.clickify(holder.itemView.activity_txt, ContextCompat.getColor(context, R.color.color_notification_activity), false, "product name", {})
        }

        if(holder.itemView.activity_txt.text.contains("favorites")){
            ClickSpan.clickify(holder.itemView.activity_txt, ContextCompat.getColor(context, R.color.color_notification_activity), false, "favorites", {})
        }


        Glide.with(context).load(activity.userImageUrl).apply(RequestOptions().override(100)).into(holder.itemView.user_image)
        Glide.with(context).load(activity.extraUrls[0]).apply(RequestOptions().override(80)).into(holder.itemView.extra_image1)
        if (activity.extraUrls.size > 1) {
            holder.itemView.extra_image_container2.visibility = View.VISIBLE
            Glide.with(context).load(activity.extraUrls[1]).apply(RequestOptions().override(80)).into(holder.itemView.extra_image2)
        }*/
    }

    private fun showProfileDialog(activityModel: ActivityModel) {

        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.customdialog_post_view_recycle)
        val wlp = dialog.window.attributes
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog.window.attributes = wlp



        dialog.recyclerview.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        val isMe = (activityModel.user!!.id) == MyApplication.instance.getUserPreferences().id

        if (activityModel.type == "post"){

            val tempPostList = ArrayList<PostModel>()
            tempPostList.add(activityModel.post!!)
            if (isMe) {



                dialog.recyclerview.adapter = FeedSellerAdapter(context, tempPostList, object : FeedSellerAdapter.OnClikPost {
                    override fun onClickPost(position: Int) {

                    }
                })

                (dialog.recyclerview.adapter as FeedSellerAdapter).reportListener = object : FeedSellerAdapter.ReportListener() {
                    override fun onReport(position: Int) {
                        dialog.dismiss()
                    }

                }

            } else {
                dialog.recyclerview.adapter = FeedBuyerAdapter(context, tempPostList, object : FeedBuyerAdapter.OnClikPost {
                    override fun onClickPost(position: Int) {

                    }
                })

                (dialog.recyclerview.adapter as FeedBuyerAdapter).reportListener = object : FeedBuyerAdapter.ReportListener() {
                    override fun onReport(position: Int) {
                        dialog.dismiss()
                    }

                }
            }
        }else{

            val tempPostList = ArrayList<StoryModel>()
            tempPostList.add(activityModel.story!!)

            if (isMe) {

                dialog.recyclerview.adapter = StoryAdapter(context,tempPostList,object : StoryAdapter.OnClikPost{
                    override fun onClickPost(position: Int) {

                    }
                })

            } else {

                dialog.recyclerview.adapter = StoryAdapter(context,tempPostList,object : StoryAdapter.OnClikPost{
                    override fun onClickPost(position: Int) {

                    }
                })

            }

        }
        dialog.recyclerview.adapter.notifyDataSetChanged()
        dialog.show()
    }


}