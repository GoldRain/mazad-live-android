package com.mazadlive.adapters

import android.app.Activity
import android.content.Context
import android.os.CountDownTimer
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mazadlive.R
import com.mazadlive.activities.StoryActivity
import com.mazadlive.models.StoryModel
import kotlinx.android.synthetic.main.fragment_full_story.view.*

/**
 * Created by bodacious on 15/10/18.
 */
/*class StoryListAdapter(val context:Context, var list : ArrayList<StoryModel>, val listFrag : ArrayList<Fragment>) : PagerAdapter() {
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object` as View
    }


    override fun getCount(): Int {
        return list.size
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return ""
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as View)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(context).inflate(R.layout.fragment_full_story, null)

       var  timer = 0
         object : CountDownTimer(5000, 45) {
            override fun onFinish() {
                (context as StoryActivity).moveNext()
            }

            override fun onTick(p0: Long) {
                timer += 1
                (context as Activity).runOnUiThread { view.mSeekbar.progress = timer }
            }
        }



        (container as ViewPager).addView(view)
        return  view
    }
}*/


class StoryListAdapter(fm : FragmentManager, var list : ArrayList<StoryModel>, val listFrag : ArrayList<Fragment>) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {

        return listFrag[position]
    }

    override fun getCount(): Int {
        return list.size
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return ""
    }
}