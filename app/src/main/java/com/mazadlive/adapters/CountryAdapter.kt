package com.mazadlive.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.helper.Constant
import com.mazadlive.models.CountryModel
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.view_countray_item.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException

class CountryAdapter(var context: Context, private var listener: DialogListener) : RecyclerView.Adapter<CountryAdapter.ViewHolder>() {

    var countryList = ArrayList<CountryModel>()
    var isNumberSelection = false

    var selectedCountry: CountryModel? = null

    init {
        try {
            /*val jsonArray = JSONArray(loadJsonFromAsset())
            for (i in 0 until jsonArray.length()) {
                val jsonObject = JSONObject(jsonArray.get(i).toString())
                val countryPozo = CountryModel()
                countryPozo.code = jsonObject.getString("e164_cc")
                countryPozo.flag = jsonObject.getString("iso2_cc")
                countryPozo.name = jsonObject.getString("name")

                if (countryPozo.name == "Bahrain" || countryPozo.name == "Kuwait" || countryPozo.name == "Saudi Arabia" || countryPozo.name == "United Arab Emirates" || countryPozo.name == "Qatar" ||countryPozo.name == "India")
                    countryList.add(countryPozo)
            }*/

            countryList.clear()
            countryList.addAll(Constant.countryList)

        } catch (e: Exception) {
            Log.e("JSON", "EX : " + e.message)
        }
    }

    fun addCountry(country:CountryModel){
        countryList.add(0,country)

    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.view_countray_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var name = countryList[position].name

        val countryModel = countryList[position]
        if (countryList[position].name!!.equals(MyApplication.instance.getUserPreferences().CountryName, true)) {
            //holder.itemView.rl.setBackgroundResource(R.drawable.select_country_back)
            selectedCountry = countryList[position]
        } else {
            // holder.itemView.rl.setBackgroundResource(0)
        }


        // val pngName = countryList[position].flag!!.trim().toLowerCase()

        // holder.itemView.image.setImageResource(context.resources.getIdentifier("drawable/flag_" + pngName, null, context.packageName))

        if (countryModel.id.equals("NA")) {
            if (isNumberSelection) {
                name = String.format("%s (%s)", countryList[position].name)
            }
            holder.itemView.image.setImageResource(R.drawable.ic_internet)
        } else {

            if (isNumberSelection) {
                name = String.format("%s (%s)", countryList[position].name, countryList[position].code)
            }
            Glide.with(context).load(countryModel.flag).apply(RequestOptions().override(80)).into(holder.itemView.image)
        }
        holder.itemView.name.text = name

        holder.itemView.setOnClickListener {
            listener.okClick(countryList[position])
            listener.okClick()
        }
    }

    override fun getItemCount(): Int {
        return countryList.size
    }

    private fun loadJsonFromAsset(): String? {
        val json: String
        try {
            val `is` = context.getAssets().open("country_codes.json")
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            json = String(buffer, Charsets.UTF_8)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }

}