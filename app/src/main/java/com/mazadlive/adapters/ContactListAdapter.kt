package com.mazadlive.adapters

import android.content.Context
import android.net.Uri
import android.provider.MediaStore
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mazadlive.R
import com.mazadlive.database.ContactData
import com.mazadlive.models.UserModel
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.view_user_contact_list.view.*
import java.util.*
import kotlin.collections.ArrayList

class ContactListAdapter(var context: Context,val contactList :ArrayList<ContactData>,val listener: OnClick) : RecyclerView.Adapter<ContactListAdapter.ViewHolder>() {

   // var list = ArrayList<UserModel>()


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun getItemCount(): Int {
        return contactList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.view_user_contact_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val con = contactList[position]

        holder.itemView.mProfileImage.visibility = View.VISIBLE

        Log.e("phoneNumbers","phone numbers in adpater: ${con.phone}")

        if(MyApplication.instance.usersHasMap.containsKey(con.phone)){

            holder.itemView.mButton_layout.visibility = View.GONE

            holder.itemView.mProfileImage.setImageResource(R.drawable.logo)

        }else{

            holder.itemView.mButton_layout.visibility = View.VISIBLE

            if(! TextUtils.isEmpty(con.photo)){

                holder.itemView.mProfileImage.setImageURI(Uri.parse(con.photo))

            }else{

                holder.itemView.mProfileImage.setImageResource(R.drawable.ic_user_default)
            }
        }

        holder.itemView.mUsername.text = con.username
        holder.itemView.mButton.setText(context.getString(R.string.invite))

        holder.itemView.mButton_layout.setOnClickListener {

            listener.onInvite(position)

        }
    }

    interface OnClick{
       fun onInvite(position: Int)
    }

}