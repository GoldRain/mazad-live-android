package com.mazadlive.adapters

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import com.mazadlive.helper.Constant
import com.mazadlive.models.FeedSellerModel
import com.mazadlive.models.PostModel
import com.mazadlive.models.StoryModel
import kotlinx.android.synthetic.main.view_bid_post_list.view.*

class PostBidListSellerAdapter(private val context: Context, private val list: ArrayList<FeedSellerModel>) : RecyclerView.Adapter<PostBidListSellerAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    var listener :OnItemClick?= null
    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_bid_post_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val feedModel = list[position]

        if(feedModel.type.equals("post",true)){
            setData(holder,feedModel.post!!,position)
        }else{
            if(feedModel.type.equals("story",true)){
                setData(holder,feedModel.story!!,position)
            }
        }

    }

    private fun setData(holder: ViewHolder, postModel: PostModel,position: Int) {

        if(postModel.postImages.size >0){
            Glide.with(context).load(postModel.postImages[0].url).apply(RequestOptions().override(80)).into(holder.itemView.image_view)
        }

      //  holder.itemView.mDescription.text = postModel.description
        holder.itemView.mTime.text = Constant.getDate(postModel.post_created_at.toLong())
        holder.itemView.mDescription.text = postModel.serial
        if(postModel.bidsList.size >100){
            holder.itemView.mBidCount.text = "100+"
        }else{
            holder.itemView.mBidCount.text = postModel.bidsList.size.toString()
        }
        holder.itemView.setOnClickListener {
            if(listener!= null){
                listener!!.onItemClick(position)
            }
        }

    }

    private fun setData(holder: ViewHolder, storyModel: StoryModel,position: Int) {

        Glide.with(context).load(storyModel.url).apply(RequestOptions().override(80)).into(holder.itemView.image_view)

        holder.itemView.mTime.text = Constant.getDate(storyModel.createdAt.toLong())
        holder.itemView.mDescription.text = storyModel.serial
        if(storyModel.bidList.size >100){
            holder.itemView.mBidCount.text = "100+"
        }else{
            holder.itemView.mBidCount.text = storyModel.bidList.size.toString()
        }


        holder.itemView.setOnClickListener {
            if(listener!= null){
                listener!!.onItemClick(position)
            }
        }

    }

    interface OnItemClick{
        fun onItemClick(position: Int);
    }

}