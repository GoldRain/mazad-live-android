package com.mazadlive.adapters

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ApiUrl
import com.mazadlive.api.ServiceRequest
import com.mazadlive.models.PostModel
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.customdialog_post_view_recycle.*
import org.json.JSONObject

class SearchTopAdapter(var context: Context, var postList: ArrayList<PostModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class viewHolderImage1 internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView)
    class viewHolderImage2 internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView)
    class viewHolderImage3 internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView)
    class viewHolderImage4 internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        Log.e("ASDASDASDASD", viewType.toString())
        val inflatedView: View
        val layoutInflater = LayoutInflater.from(context)
        when (viewType) {
            1001 -> {
                inflatedView = layoutInflater.inflate(R.layout.post_image1, parent, false)
                return viewHolderImage1(inflatedView)
            }
            1002 -> {
                inflatedView = layoutInflater.inflate(R.layout.post_image2, parent, false)
                return viewHolderImage1(inflatedView)
            }
            1003 -> {
                inflatedView = layoutInflater.inflate(R.layout.post_image3, parent, false)
                return viewHolderImage2(inflatedView)
            }
            1004 -> {
                inflatedView = layoutInflater.inflate(R.layout.post_image4, parent, false)
                return viewHolderImage3(inflatedView)
            }
            else -> {
                inflatedView = layoutInflater.inflate(R.layout.post_image1, parent, false)
                return viewHolderImage4(inflatedView)
            }
        }
    }

    override fun getItemCount(): Int {
        return postList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val postModel = postList[position]

        if (postModel.isPopular){
            holder.itemView.findViewById<TextView>(R.id.mPopular).visibility = View.VISIBLE
        }else{
            holder.itemView.findViewById<TextView>(R.id.mPopular).visibility = View.GONE
        }

        holder.itemView.setOnClickListener { showProfileDialog(holder.adapterPosition) }

        when (postModel.postType) {
            1001 -> {
                Glide.with(context).load(postModel.postImages[0].url).apply(RequestOptions().override(500)).into(holder.itemView.findViewById(R.id.mImage1))
            }
            1002 -> {
                Glide.with(context).load(postModel.postImages[0].url).apply(RequestOptions().override(500)).into(holder.itemView.findViewById(R.id.mImage1))
                Glide.with(context).load(postModel.postImages[1].url).apply(RequestOptions().override(500)).into(holder.itemView.findViewById(R.id.mImage2))
            }
            1003 -> {
                Glide.with(context).load(postModel.postImages[0].url).apply(RequestOptions().override(250)).into(holder.itemView.findViewById(R.id.mImage1))
                Glide.with(context).load(postModel.postImages[1].url).apply(RequestOptions().override(250)).into(holder.itemView.findViewById(R.id.mImage2))
                Glide.with(context).load(postModel.postImages[2].url).apply(RequestOptions().override(500)).into(holder.itemView.findViewById(R.id.mImage3))
            }
            1004 -> {
                if (postList.size > 3) {
                    Glide.with(context).load(postModel.postImages[0].url).apply(RequestOptions().override(250)).into(holder.itemView.findViewById(R.id.mImage1))
                    Glide.with(context).load(postModel.postImages[1].url).apply(RequestOptions().override(250)).into(holder.itemView.findViewById(R.id.mImage2))
                    Glide.with(context).load(postModel.postImages[2].url).apply(RequestOptions().override(250)).into(holder.itemView.findViewById(R.id.mImage3))
                    Glide.with(context).load(postModel.postImages[3].url).apply(RequestOptions().override(250)).into(holder.itemView.findViewById(R.id.mImage4))
                    when (postList.size) {
                        5 -> holder.itemView.findViewById<ImageView>(R.id.mMore).setImageResource(R.drawable.more1)
                        6 -> holder.itemView.findViewById<ImageView>(R.id.mMore).setImageResource(R.drawable.more2)
                        7 -> holder.itemView.findViewById<ImageView>(R.id.mMore).setImageResource(R.drawable.more3)
                        8 -> holder.itemView.findViewById<ImageView>(R.id.mMore).setImageResource(R.drawable.more4)
                        9 -> holder.itemView.findViewById<ImageView>(R.id.mMore).setImageResource(R.drawable.more5)
                    }
                }
            }
        }

    }

    override fun getItemViewType(position: Int): Int {
        val postModel = postList[position]
        when (postModel.postImages.size) {
            1 -> {
                postModel.postType = 1001
                return 1001
            }
            2 -> {
                postModel.postType = 1002
                return 1002
            }
            3 -> {
                postModel.postType = 1003
                return 1003
            }
            else -> {
                postModel.postType = 1004
                return 1004
            }
        }
    }

    fun showProfileDialog(position: Int) {

        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.customdialog_post_view_recycle)
        val wlp = dialog.window.attributes
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog.window.attributes = wlp

        val isMe = (postList[position].userData.id).equals(MyApplication.instance.getUserPreferences().id)

        Log.e("GET_LIST_POST", "** ${postList.size} $isMe")

        val tempPostList = ArrayList<PostModel>()
        tempPostList.add(postList.get(position))
        dialog.recyclerview.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        if (isMe) {
            dialog.recyclerview.adapter = FeedSellerAdapter(context, tempPostList, object : FeedSellerAdapter.OnClikPost {
                override fun onClickPost(position: Int) {

                }
            })

            (dialog.recyclerview.adapter as FeedSellerAdapter).reportListener = object : FeedSellerAdapter.ReportListener() {
                override fun onReport(position: Int) {
                    dialog.dismiss()
                }

            }

        } else {
            dialog.recyclerview.adapter = FeedBuyerAdapter(context, tempPostList, object : FeedBuyerAdapter.OnClikPost {
                override fun onClickPost(position: Int) {

                }
            })

            (dialog.recyclerview.adapter as FeedBuyerAdapter).reportListener = object : FeedBuyerAdapter.ReportListener() {
                override fun onReport(position: Int) {
                    dialog.dismiss()
                }

            }
        }
        dialog.recyclerview.adapter.notifyDataSetChanged()
        dialog.recyclerview.scrollToPosition(position)
        dialog.show()
    }

}