package com.mazadlive.adapters

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.R
import com.mazadlive.activities.StreamingActivity
import com.mazadlive.models.CommentModel
import kotlinx.android.synthetic.main.view_livecomment.view.*
import java.util.*


class liveCommentAdapter(private var context: Context, private  var commentList : ArrayList<CommentModel>) : RecyclerView.Adapter<liveCommentAdapter.ViewHolder>() {


    var listener:OnClick?= null
    var isAdmin = false
    companion object {
        var firstPosition = 0
    }
    override fun getItemViewType(position: Int): Int {

        return  position
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.view_livecomment,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val commentModel = commentList[position]

        holder.itemView.imBid.tag = commentModel.isBookmark


        if(commentModel.userData != null){
            holder.itemView.user_name.setText(commentModel.userData!!.username)
            Glide.with(context).load(commentModel.userData!!.profile).apply(RequestOptions().override(80).error(R.drawable.dummy)).into(holder.itemView.user_image)
        }

        val rnd = Random()
        val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))

        holder.itemView.user_name.setTextColor(color)
        holder.itemView.comment.setTextColor(color)

        Log.e("ASDASDASDASDASDASD","Here message - "+commentModel.message+"      "+isAdmin)

        if(isAdmin){
            if(commentModel.type.equals("comment")){
                holder.itemView.comment.text = commentModel.message
                holder.itemView.im_pin.visibility = View.GONE
                holder.itemView.imBid.visibility = View.GONE
            }else{
                holder.itemView.comment.text = String.format("${context.getString(R.string.currency)} ${commentModel.message}")
                holder.itemView.im_pin.visibility = View.VISIBLE
                holder.itemView.imBid.visibility = View.VISIBLE

                holder.itemView.im_pin.setBackgroundResource(0)
                if(commentModel.isBookmark){
                    if((context as StreamingActivity).bookMarkComment.containsKey(commentModel.id))
                         holder.itemView.im_pin.setBackgroundResource(R.drawable.round_blue)
                }else{
                    holder.itemView.im_pin.setBackgroundResource(0)
                }

            }
        }else{
            if(commentModel.type.equals("comment")){
                holder.itemView.comment.text = commentModel.message
            }else{
                holder.itemView.comment.text = String.format("${context.getString(R.string.currency)} ${commentModel.message}")
            }
            holder.itemView.im_pin.visibility = View.GONE
            holder.itemView.imBid.visibility = View.GONE
        }

        holder.itemView.im_pin.setOnClickListener {
            if(commentModel.status.equals("approved")){
                Toast.makeText(context,context.getString(R.string.already_awarded),Toast.LENGTH_SHORT).show()
            }else{
                if(listener != null){
                    listener!!.addBookmark(position)
                }
            }
        }

        holder.itemView.imBid.setOnClickListener {
            if(listener != null){
                listener!!.pin(position)
            }
        }
    }

    override fun getItemCount(): Int {
        return commentList.size
    }

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView)

    interface OnClick{

        fun addBookmark(position:Int)
        fun pin(position:Int)
    }
}