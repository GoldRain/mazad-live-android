package com.mazadlive.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.models.CountryModel
import com.mazadlive.utils.MyApplication
import kotlinx.android.synthetic.main.view_countray_item_multiple.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException

class CountryAdapterMultiple(var context: Context,val countryList: ArrayList<CountryModel>, private var listener: DialogListener) : RecyclerView.Adapter<CountryAdapterMultiple.ViewHolder>() {

   // var countryList = ArrayList<CountryModel>()
    var isNumberSelection = false
    var selectedCountry:CountryModel?= null
  //  var mapSelectedCountry = HashMap<String,CountryModel>()
    init {

    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.view_countray_item_multiple, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var name = countryList[position].name
        var countryModel = countryList[position]

        if(countryModel.isCheck){
            holder.itemView.im_check.setImageResource(R.drawable.ic_radio_button_checked)
        }else{
            holder.itemView.im_check.setImageResource(R.drawable.ic_radio_button_unchecked)
        }



        if(countryList[position].name!!.equals(MyApplication.instance.getUserPreferences().CountryName,true)){
            //holder.itemView.rl.setBackgroundResource(R.drawable.select_country_back)
            selectedCountry = countryList[position]
        }else{
           // holder.itemView.rl.setBackgroundResource(0)
        }

        if (isNumberSelection) {
            name = String.format("%s (+%s)", countryList[position].name, countryList[position].code)
        }

        holder.itemView.name.text = name
        //val pngName = countryList[position].flag!!.trim().toLowerCase()

       // holder.itemView.image.setImageResource(context.resources.getIdentifier("drawable/flag_" + pngName, null, context.packageName))
        Glide.with(context).load(countryModel.flag).apply(RequestOptions().override(80)).into(holder.itemView.image)

        holder.itemView.setOnClickListener {
            if(countryModel.isCheck){
                holder.itemView.im_check.setImageResource(R.drawable.ic_radio_button_unchecked)
               // mapSelectedCountry.remove(countryModel.name)
            }else{
                holder.itemView.im_check.setImageResource(R.drawable.ic_radio_button_checked)
              //  mapSelectedCountry.put(countryModel.name!!,countryModel)
            }
            countryModel.isCheck = !countryModel.isCheck
        }
    }

    override fun getItemCount(): Int {
        return countryList.size
    }

    private fun loadJsonFromAsset(): String? {
        val json: String
        try {
            val `is` = context.getAssets().open("country_codes.json")
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            json = String(buffer, Charsets.UTF_8)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }

}