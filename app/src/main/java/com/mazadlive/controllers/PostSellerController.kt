package com.mazadlive.controllers

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.telephony.PhoneNumberUtils
import android.telephony.SmsManager
import android.util.Log
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.facebook.drawee.backends.pipeline.Fresco
import com.google.firebase.FirebaseApp
import com.google.firebase.dynamiclinks.DynamicLink
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.activities.*
import com.mazadlive.adapters.FeedSellerAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ApiUrl
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.customviews.ImageOverLayView
import com.mazadlive.database.AppDatabase
import com.mazadlive.database.FavPost
import com.mazadlive.helper.Constant
import com.mazadlive.models.BidModel
import com.mazadlive.models.PostImages
import com.mazadlive.models.PostModel
import com.mazadlive.models.TapPaymentModel
import com.mazadlive.utils.MyApplication
import com.stfalcon.frescoimageviewer.ImageViewer
import kotlinx.android.synthetic.main.view_feed_seller.view.*
import kotlinx.android.synthetic.main.view_image_overlay.view.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.text.DecimalFormat
import java.util.ArrayList

/**
 * Created by bodacious on 22/12/18.
 */
class PostSellerController(val context: Context, val holder: FeedSellerAdapter.ViewHolder, val post: PostModel, val adapter: FeedSellerAdapter, val position: Int, val listener: FeedSellerAdapter.OnClikPost) {

    lateinit var progress: CustomProgress
    val userRef = MyApplication.instance.getUserPreferences()

    init {
        progress = CustomProgress(context)
    }

    @SuppressLint("SetTextI18n")
    fun control() {

        // Set Feedback
        if(post.feedbackList.size > 0){
            holder.itemView.mFeedbackLayout.visibility = View.VISIBLE
        }else{
            holder.itemView.mFeedbackLayout.visibility = View.GONE
        }


        if (post.userData.id.equals(MyApplication.instance.getUserPreferences().id)) {

            holder.itemView.whtsapp.visibility = View.GONE
            holder.itemView.mMessage.visibility = View.GONE
            holder.itemView.call.visibility = View.GONE
            //    holder.itemView.buynow.visibility = View.GONE
            // holder.itemView.mBuyNow.visibility = View.GONE
        } else {

            holder.itemView.whtsapp.visibility = View.VISIBLE
            holder.itemView.mMessage.visibility = View.VISIBLE
            holder.itemView.call.visibility = View.VISIBLE
            //  holder.itemView.buynow.visibility = View.GONE
        }


        if (post.postImages.size > 0 && post.postImages[0].isVideo) {
            holder.itemView.im_video.visibility = View.VISIBLE
        } else {
            holder.itemView.im_video.visibility = View.GONE
        }

        holder.itemView.location.text = post.place_name
        holder.itemView.buynow.text = String.format("${context.getString(R.string.currency)} ${post.amount}")
        Log.e("PARAM_VOTE", " ${post.isLike}  ${post.isVote}")
        holder.itemView.start.tag = post.isFav
        holder.itemView.heart.tag = post.isLike

        if (post.isPromoted) {
            holder.itemView.promote.text = context.getString(R.string.promoted)
            holder.itemView.promote.setBackgroundResource(R.drawable.feedview_btn_background_green)

        } else {
            holder.itemView.promote.text = "Promote Now"
            holder.itemView.promote.setBackgroundResource(R.drawable.feedview_btn_background)
        }

        when (post.isVote) {
            -1 -> {
                holder.itemView.like.tag = false
                holder.itemView.dislike.tag = false
            }

            0 -> {
                holder.itemView.like.tag = false
                holder.itemView.dislike.tag = true
            }

            1 -> {
                holder.itemView.like.tag = true
                holder.itemView.dislike.tag = false
            }
        }

        setStar(holder)
        setLikePost(holder)
        setLike(holder)
        setDisLike(holder)

        // Set Click on Feedback
        holder.itemView.mFeedbackLayout.setOnClickListener {
            val intent = Intent(context, FeedsActivity::class.java)
            intent.putParcelableArrayListExtra("feeds",post.feedbackList)
            context.startActivity(intent)
        }


        holder.itemView.username.text = userRef.username
        if (userRef.verified) {
            holder.itemView.im_verify.setImageResource(R.drawable.ic_verify)
        } else {
            holder.itemView.im_verify.setImageResource(R.drawable.ic_unverified)
        }

        val per = DecimalFormat("##.##").format((post.percent.replace("%", "")).toDouble())
        holder.itemView.tx_percent.text = "${per}%"

        if (post.postImages.size > 0) {
            val img = post.postImages[0].url
            //Log.e("IMAGE_POST","*****   $img")
            Glide.with(context).load(img).apply(RequestOptions().override(1000)).into(holder.itemView.imageView10)

        }
        post.userData.let {
            //Log.e("IMAGE_URL","size IM ${it.profile}")
            Glide.with(context).load(it.profile).apply(RequestOptions().override(80).error(R.drawable.dummy)).into(holder.itemView.user_image)
        }

        if (post.is_sold) {
            holder.itemView.mSold.visibility = View.VISIBLE
        } else {
            holder.itemView.mSold.visibility = View.GONE
        }


        holder.itemView.start.setOnClickListener {

            val param = JSONObject()
            param.put("version", "1.1")
            param.put("post_id", post.id)
            param.put("user_id", MyApplication.instance.getUserPreferences().id)
            param.put("is_fav", !(holder.itemView.start.tag as Boolean))

            // listener.onFavoritePost(position, param)
            favPost(param, position)

        }

        holder.itemView.heart.setOnClickListener {
            val param = JSONObject()
            param.put("version", "1.1")
            param.put("post_id", post.id)
            param.put("user_id", MyApplication.instance.getUserPreferences().id)

            if (holder.itemView.heart.tag as Boolean) {
                param.put("is_like", 0)
            } else {
                param.put("is_like", 1)
            }

            likePost(param, position)
            // listener.onLikePost(position, param)

        }


        holder.itemView.like.setOnClickListener {
            val param = JSONObject()
            param.put("version", "1.1")
            param.put("post_id", post.id)
            param.put("user_id", MyApplication.instance.getUserPreferences().id)

            param.put("is_vote", 1)

            addVote(position, param)
            //listener.onVoteOnPost(position, param)
        }

        holder.itemView.dislike.setOnClickListener {
            val param = JSONObject()
            param.put("version", "1.1")
            param.put("post_id", post.id)
            param.put("user_id", MyApplication.instance.getUserPreferences().id)

            param.put("is_vote", 0)

            addVote(position, param)
        }
        holder.view.mMessage.setOnClickListener {
            sendSMS("123456789", "Mazad Live")
        }
        holder.itemView.whtsapp.setOnClickListener {
            sendWhatsAppMsg()
        }

        holder.itemView.promote.setOnClickListener {
            if (!post.isPromoted && (holder.itemView.promote.text == "Promote Now")) {

                val tapPaymentModel = TapPaymentModel()
                tapPaymentModel.amount = 5.toString()
                tapPaymentModel.orderNumber = post.id

                val intent = Intent(context,WebViewActivity::class.java)
                intent.putExtra("url", ApiUrl.getPaymetLoader)
                intent.putExtra("tapModel",tapPaymentModel)
                (context as Activity).startActivityForResult(intent,5000)
                //updatePromote()
            }
        }

        holder.itemView.view_click.setOnClickListener {

            if (post.postImages[0].isVideo) {
                var path = post.postImages[0].url
                val ind = path.lastIndexOf(".")
                if (ind > 0) {
                    path = path.substring(0, ind) + ".mp4"
                }
                Log.e("PATH__", "$path  ***")
                val intent = Intent(context, StoryCommentActivity::class.java)
                intent.putExtra("isVideo", true)
                intent.putExtra("path", path)
                context.startActivity(intent)
            } else {
                showAllImage(position)
            }
        }

        holder.view.profile.setOnClickListener {
            val bundle = Bundle()
            bundle.putBoolean("isMe", true)
            (context as HomeActivity).setProfileFragment(bundle)
        }

        holder.itemView.im_menu.setOnClickListener {

            Log.e("MENU_CLICK", "MENUCLICK")
            showPopupMenu(it, position)
            // listener.onOptionClick(it, position)
        }


        holder.itemView.promote.isEnabled = false
        holder.itemView.promote.setBackgroundResource(R.drawable.feedview_btn_background_gray)

    }


    private fun setLikePost(holder: FeedSellerAdapter.ViewHolder) {
        if (holder.itemView.heart.tag as Boolean) {
            holder.itemView.heart.setImageResource(R.drawable.ic_heart_on)
            holder.itemView.heart.tag = true
        } else {
            holder.itemView.heart.setImageResource(R.drawable.ic_heart)
            holder.itemView.heart.tag = false
        }
    }

    private fun setStar(holder: FeedSellerAdapter.ViewHolder) {
        if (holder.itemView.start.tag as Boolean) {
            holder.itemView.start.setImageResource(R.drawable.ic_star_on)
            holder.itemView.start.tag = true
        } else {
            holder.itemView.start.setImageResource(R.drawable.ic_star)
            holder.itemView.start.tag = false
        }
    }


    private fun setDisLike(holder: FeedSellerAdapter.ViewHolder) {
        if (holder.itemView.dislike.tag as Boolean) {
            holder.itemView.like.setImageResource(R.drawable.ic_like)
            holder.itemView.dislike.setImageResource(R.drawable.ic_unlike_on)
            holder.itemView.like.tag = false
            holder.itemView.dislike.tag = true
        } else {
            holder.itemView.dislike.setImageResource(R.drawable.ic_dislike)
            holder.itemView.dislike.tag = false
            //holder.itemView.like.setImageResource(R.drawable.ic_like)
            //  holder.itemView.like.tag = false

        }
    }

    private fun setLike(holder: FeedSellerAdapter.ViewHolder) {

        if ((holder.itemView.like.tag as Boolean)) {
            holder.itemView.like.setImageResource(R.drawable.ic_like_on)
            holder.itemView.dislike.setImageResource(R.drawable.ic_dislike)
            holder.itemView.like.tag = true
            holder.itemView.dislike.tag = false

        } else {
            holder.itemView.like.setImageResource(R.drawable.ic_like)
            holder.itemView.like.tag = false

        }
    }


    fun sendSMS(phoneNo: String, msg: String) {
        try {
            val smsManager = SmsManager.getDefault()
            smsManager.sendTextMessage(phoneNo, null, msg, null, null)
            Toast.makeText(context, "Message Sent",
                    Toast.LENGTH_LONG).show()
        } catch (ex: Exception) {
            Toast.makeText(context, ex.message.toString(),
                    Toast.LENGTH_LONG).show()
            ex.printStackTrace()
        }

    }

    private fun sendWhatsAppMsg() {
        val smsNumber = "0123456789"
        val sendIntent = Intent("android.intent.action.MAIN")
        sendIntent.component = ComponentName("com.whatsapp", "com.whatsapp.Conversation")
        sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(smsNumber) + "@s.whatsapp.net")//phone number without "+" prefix
        context.startActivity(sendIntent)
    }


    private fun showAllImage(position: Int) {

        val imageList2 = post.postImages

        var currentImagePositon = position
        Fresco.initialize(context)
        val overView = ImageOverLayView(context)
        //val pathList = arrayOfNulls<String>(imageList2.size)
        //imageList2.toArray(pathList)


        // Log.e("IMAGE_SIZE","${imageList.size}  ** ${pathList.size}")
        //  val randomColor = intArrayOf(R.color.imageViewerColor1, R.color.imageViewerColor2, R.color.imageViewerColor3, R.color.imageViewerColor4)
        val mImageViewerBuilder = ImageViewer.Builder<PostImages>(context, imageList2)
        val mImageViewer = mImageViewerBuilder.setStartPosition(position)
                .setOverlayView(overView)
                .setStartPosition(0)
                .setBackgroundColor(ContextCompat.getColor(context, R.color.colorBlack))
                .setFormatter(object : ImageViewer.Formatter<PostImages> {
                    override fun format(t: PostImages?): String {
                        return t!!.url
                    }

                })

                .show()


        mImageViewerBuilder.setImageChangeListener(object : ImageViewer.OnImageChangeListener {
            override fun onImageChange(positionImage: Int) {
                currentImagePositon = positionImage
            }

        })

        overView.back.setOnClickListener {
            mImageViewer.onDismiss()
        }


    }


    private fun showPopupMenu(view: View, position: Int) {

        Log.e("MENU_CLICK", "MENUCLICK")
        val popupMenu = PopupMenu(context, view)
        popupMenu.inflate(R.menu.post_menu)

        if (post.userData.id == MyApplication.instance.getUserPreferences().id) {
            popupMenu.menu.findItem(R.id.mHide).isVisible = true
            popupMenu.menu.findItem(R.id.mRepost).isVisible = true
            popupMenu.menu.findItem(R.id.mReport).isVisible = false
        } else {
            popupMenu.menu.findItem(R.id.mHide).isVisible = false
            popupMenu.menu.findItem(R.id.mRepost).isVisible = false
            popupMenu.menu.findItem(R.id.mReport).isVisible = true
        }

        popupMenu.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.mShare -> {
                    generateDynamicLink()
                }

                R.id.mHide -> {
                    if (post.is_sold){
                        CustomDialog(context).showErrorDialog(context.getString(R.string.delete_sold_post))
                    }else{
                        hidePost(post, position)
                    }
                }

                R.id.mReport -> {
                    CustomDialog(context).showReportDialog("", object : DialogListener() {
                        override fun okClick(any: Any) {
                            reportPost(any.toString(), position)
                        }
                    })
                }

                R.id.mRepost -> {
                    val jsonObject = JSONObject()
                    jsonObject.put("post_id",post.id)
                    jsonObject.put("user_id",post.post_user_id)
                    ServiceRequest(object : ApiResponseListener{
                        override fun onCompleted(`object`: Any) {
                            Toast.makeText(context, context.getString(R.string.repost_success), Toast.LENGTH_SHORT).show()
                        }

                        override fun onError(errorMessage: String) {
                            Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show()
                        }

                    }).setRepost(jsonObject)
                }
            }

            true
        }

        popupMenu.show()
    }


    fun bidDialog(param: JSONObject) {
        CustomDialog(context).showBidialog(adapter.amount.toString(), object : DialogListener() {
            override fun okClick(any: Any) {
                param.put("bid_amount", any.toString())
                makeBid(param)

            }
        })
    }

    private fun updatePromote() {
        val jsonObject = JSONObject()
        jsonObject.put("version", "1.1")
        jsonObject.put("post_id", post.id)
        jsonObject.put("is_promote", !post.isPromoted)

        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress.dismiss()

//                post.is_deleted = true
                post.isPromoted = !post.isPromoted
                (context as Activity).runOnUiThread {
                    adapter.notifyItemChanged(position)
                }
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                Toast.makeText(context, "Failed to like", Toast.LENGTH_SHORT).show()
            }

        }).updatePromote(jsonObject)
    }

    private fun hidePost(post: PostModel, position: Int) {
        val jsonObject = JSONObject()
        jsonObject.put("version", "1.1")
        jsonObject.put("post_id", post.id)
        jsonObject.put("is_deleted", true)

        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                adapter.feedlist.removeAt(position)
//                post.is_deleted = true
                (context as Activity).runOnUiThread {
                    adapter.notifyDataSetChanged()
                }
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                Toast.makeText(context, "Failed to like", Toast.LENGTH_SHORT).show()
            }

        }).deletePost(jsonObject)
    }

    private fun likePost(jsonObject: JSONObject, position: Int) {
        Log.e("LIKE_POST", "*** $jsonObject")
        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                post.isLike = jsonObject.getInt("is_like") == 1
                (context as Activity).runOnUiThread {
                    adapter.notifyItemChanged(position)
                }
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                Toast.makeText(context, context.getString(R.string.failed_to_like), Toast.LENGTH_SHORT).show()
            }

        }).likePost(jsonObject)

    }

    private fun favPost(jsonObject: JSONObject, position: Int) {
        Log.e("LIKE_POST", "*** $jsonObject")
        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                post.isFav = (jsonObject.getBoolean("is_fav"))
                (context as Activity).runOnUiThread {
                    adapter.notifyItemChanged(position)
                }
                doAsync {
                    if (jsonObject.getBoolean("is_fav")) {
                        val favPost = FavPost()
                        favPost.id = post.id
                        favPost.post_id = post.id
                        AppDatabase.getAppDatabase().favPostDao().insert(favPost)
                    } else {
                        AppDatabase.getAppDatabase().favPostDao().deleteFavPost(post.id)
                    }
                }
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                Toast.makeText(context, context.getString(R.string.failed_to_like), Toast.LENGTH_SHORT).show()
            }

        }).favPost(jsonObject)

    }

    private fun makeBid(param: JSONObject) {
        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                val json = JSONObject(`object`.toString())
                if (json.has("description")) {
                    val array = json.getJSONArray("description")
                    val list = ArrayList<BidModel>()
                    for (i in 0 until array.length()) {
                        val obj = array.getJSONObject(i)
                        val bid = BidModel("")

                        if (obj.has("_id")) {
                            bid.id = obj.getString("_id")
                        }

                        if (obj.has("post_id")) {
                            bid.postId = obj.getString("post_id")
                        }

                        if (obj.has("bid_user_id")) {
                            bid.userId = obj.getString("bid_user_id")
                        }

                        if (obj.has("bid_comment")) {
                            bid.comment = obj.getString("bid_comment")
                        }

                        if (obj.has("bid_amount")) {
                            bid.bidAmount = obj.getString("bid_amount")
                        }

                        if (obj.has("bid_created_at")) {
                            bid.id = obj.getString("bid_created_at")
                        }

                        if (obj.has("is_pinned")) {
                            bid.isPinned = obj.getBoolean("is_pinned")
                        }

                        list.add(bid)
                    }
                }

                Toast.makeText(context, "Bid added successfully", Toast.LENGTH_SHORT).show()
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                CustomDialog(context).showErrorDialog("Failed to make bid \n$errorMessage")
            }

        }).addBidOnPost(param)
    }

    private fun addVote(position: Int, param: JSONObject) {

        progress.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                val json = JSONObject(`object`.toString())
                if (post.isVote == param.getInt("is_vote")) {
                    post.isVote = -1
                } else {
                    post.isVote = param.getInt("is_vote")
                }

                if (json.has("percent")) {
                    post.percent = json.getString("percent")
                }

                adapter.notifyItemChanged(position)

            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                CustomDialog(context).showErrorDialog("Failed to make bid \n$errorMessage")
            }

        }).vote(param)
    }

    // makeBid(postList[position], param)
    private fun getHeighestBid(post: PostModel, param1: JSONObject) { ////type: stream / post / story, stream_id: streamId, story_id: storyId, post_id: postId .
        val param = JSONObject() // : bid_user_id, bid_amount: bid_amount.
        param.put("version", "1.1")

        param.put("post_id", post.id)
        param.put("type", "post")


        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                val js = JSONObject(`object`.toString())//amount
                if (js.has("description")) {
                    val json = js.getJSONObject("description")
                    if (json.has("bid_amount")) {
                        try {
                            adapter.amount = json.get("bid_amount").toString().toInt()

                        } catch (e: Exception) {

                        } finally {
                            bidDialog(param1)
                        }
                    } else {
                        if (json.has("amount")) {
                            try {
                                adapter.amount = json.get("amount").toString().toInt()

                            } catch (e: Exception) {

                            } finally {
                                bidDialog(param1)
                            }
                        } else {
                            bidDialog(param1)
                        }
                    }


                } else {
                    bidDialog(param1)
                }
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                bidDialog(param1)

            }

        }).getHighestBid(param)

    }

    private fun reportPost(msg: String, position: Int) {

        val param = JSONObject()
        param.put("version", "1.1")
        param.put("self_id", userRef.id)
        param.put("post_id", post.id)
        param.put("reason", msg)
        param.put("message", msg)

        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress.dismiss()

                if (adapter.reportListener != null) {
                    adapter.reportListener!!.onReport(position)
                }
                adapter.feedlist.removeAt(position)
                adapter.notifyDataSetChanged()
                Toast.makeText(context, context.getString(R.string.report_success), Toast.LENGTH_SHORT).show()
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                Toast.makeText(context, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).reportPost(param)
    }

    val TAG  = "PostSellerController"
    private fun generateDynamicLink() {
        var linkText = ""
        FirebaseApp.initializeApp(context)
        progress.show()
        val url = "https://mazadlive.page.link?id=${post.id}&type=post"
        val shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse(url))
                .setDomainUriPrefix(Constant.PREFIX)
                .setAndroidParameters(
                        DynamicLink.AndroidParameters.Builder("com.mazadlive")
                                .build())

                .setIosParameters(
                        DynamicLink.IosParameters.Builder("com.itwhiz4u.q8mercato.mazadlive")
                                .setAppStoreId("1450514377")
                                .build())

                .setSocialMetaTagParameters(DynamicLink.SocialMetaTagParameters.Builder()
                        .setTitle("")
                        .setDescription(post.description)
                        .setImageUrl(Uri.parse(post.postImages[0].url))
                        .build())

                .setGoogleAnalyticsParameters(
                        DynamicLink.GoogleAnalyticsParameters.Builder()
                                .setSource("orkut")
                                .setMedium("social")
                                .setCampaign("example-promo")
                                .build())

                .buildShortDynamicLink()

                .addOnSuccessListener { result ->

                    val shortLink = result.shortLink
                    val flowchartLink = result.previewLink
                    linkText = shortLink.toString()

                    //  ConstantFunction.saveDynamicLink(hotelModel!!.id, linkText)
                    Log.e(TAG, "LINK :: $shortLink  *** $flowchartLink")
                    shareLink(linkText)

                }.addOnFailureListener {

                    progress.dismiss()
                    Log.e(TAG, "LINK :: addOnFailureListener  *** ")
                    progress?.dismiss()
                }
    }

    private fun shareLink(linkText: String) {
        progress.dismiss()
        val shareIntent = Intent()
        shareIntent.setAction(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, linkText)
        context.startActivity(shareIntent)
    }
}