package com.laundryapp.controllers

import android.content.Context
import com.mazadlive.adapters.ChatListAdapter
import com.mazadlive.models.ChatMessage
import kotlinx.android.synthetic.main.chat_date_controller_layout.view.*

class ChatDateController(val context: Context, val viewHolder: ChatListAdapter.ViewHolderDate,
                         val chatMessage: ChatMessage, val messageListAdapter: ChatListAdapter, val position: Int) {

    fun control() {
        viewHolder.itemView.tv_date.text = chatMessage.formattedTime
    }

}