package com.mazadlive.controllers

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.ActivityCompat.startActivityForResult
import android.support.v4.content.ContextCompat
import android.telephony.PhoneNumberUtils
import android.telephony.SmsManager
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.facebook.drawee.backends.pipeline.Fresco
import com.google.firebase.FirebaseApp
import com.google.firebase.dynamiclinks.DynamicLink
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.activities.*
import com.mazadlive.adapters.FeedBuyerAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ApiUrl
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.customviews.ImageOverLayView
import com.mazadlive.database.AppDatabase
import com.mazadlive.database.FavPost
import com.mazadlive.helper.Constant
import com.mazadlive.models.BidModel
import com.mazadlive.models.PostImages
import com.mazadlive.models.PostModel
import com.mazadlive.models.TapPaymentModel
import com.mazadlive.utils.MyApplication
import com.stfalcon.frescoimageviewer.ImageViewer
import kotlinx.android.synthetic.main.view_feed_buyer.view.*
import kotlinx.android.synthetic.main.view_image_overlay.view.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.text.DecimalFormat
import java.util.ArrayList

/**
 * Created by bodacious on 22/12/18.
 */
class PostBuyerController(val context: Context,val holder: FeedBuyerAdapter.ViewHolder,val post:PostModel,val adapter: FeedBuyerAdapter,val position:Int,val listener:FeedBuyerAdapter.OnClikPost){

    val TAG = "PostBuyerController"
    lateinit var progress:CustomProgress
    val userRef = MyApplication.instance.getUserPreferences()
    init {
        progress = CustomProgress(context)
    }
    fun control( ) {
        if (post.userData.id.equals(MyApplication.instance.getUserPreferences().id)) {
            holder.itemView.mBuyNow.visibility = View.GONE
            holder.itemView.mMakeABid.visibility = View.GONE
            holder.itemView.whtsapp.visibility = View.GONE
            holder.itemView.message.visibility = View.GONE
            holder.itemView.call.visibility = View.GONE
            //  holder.itemView.buynow.visibility = View.GONE
            // holder.itemView.mBuyNow.visibility = View.GONE
        } else {
            holder.itemView.mBuyNow.visibility = View.VISIBLE
            holder.itemView.mMakeABid.visibility = View.VISIBLE
            holder.itemView.whtsapp.visibility = View.VISIBLE
            holder.itemView.message.visibility = View.VISIBLE
            holder.itemView.call.visibility = View.VISIBLE

            if (post.whatsapp_only) {
                holder.itemView.whtsapp.visibility = View.VISIBLE
                holder.itemView.call.visibility = View.GONE
            } else {
                if (post.whatsapp_and_call) {
                    holder.itemView.whtsapp.visibility = View.GONE
                    holder.itemView.call.visibility = View.VISIBLE
                } else {
                    holder.itemView.whtsapp.visibility = View.GONE
                    holder.itemView.call.visibility = View.GONE
                }
            }
        }

        Log.e("FeedBuyerAdapter", "${post.postImages[0].isVideo} ***")

        if (post.postImages.size > 0 && post.postImages[0].isVideo) {
            holder.itemView.im_video.visibility = View.VISIBLE
        } else {
            holder.itemView.im_video.visibility = View.GONE
        }

        // Set Feedback
        if(post.feedbackList.size > 0){
            holder.itemView.mFeedbackLayout.visibility = View.VISIBLE
        }else{
            holder.itemView.mFeedbackLayout.visibility = View.GONE
        }

        // set Suponserd
        if (post.userData.id == userRef.id){
            if (post.isPromoted) {
                holder.itemView.promote.text = context.getString(R.string.promoted)
                holder.itemView.promote.setBackgroundResource(R.drawable.feedview_btn_background_green)

            } else {
                holder.itemView.promote.text = "Promote Now"
                holder.itemView.promote.setBackgroundResource(R.drawable.feedview_btn_background)
            }

            holder.itemView.promote.isEnabled = false
            holder.itemView.promote.setBackgroundResource(R.drawable.feedview_btn_background_gray)
        }else{
            if (post.isPromoted){
                holder.itemView.promote.visibility = View.VISIBLE
            }else{
                holder.itemView.promote.visibility = View.GONE
            }

            holder.itemView.promote.isEnabled = false
            holder.itemView.promote.setBackgroundResource(R.drawable.feedview_btn_background_gray)
        }

        holder.itemView.promote.setOnClickListener {
            if (!post.isPromoted && (holder.itemView.promote.text == "Promote Now") ) {

                val tapPaymentModel = TapPaymentModel()
                tapPaymentModel.amount = 5.toString()

                val intent = Intent(context,WebViewActivity::class.java)
                intent.putExtra("url", ApiUrl.getPaymetLoader)
                intent.putExtra("tapModel",tapPaymentModel)
                (context as Activity).startActivityForResult(intent,5000)
                //updatePromote()
            }
        }


        holder.itemView.buynow.text = String.format("%s %s",context.getString(R.string.currency),post.amount)
        holder.itemView.location.text = post.place_name
        holder.itemView.start.tag = post.isFav
        holder.itemView.heart.tag = post.isLike

        when (post.isVote) {
            -1 -> {
                holder.itemView.like.tag = false
                holder.itemView.dislike.tag = false
            }

            0 -> {
                holder.itemView.like.tag = false
                holder.itemView.dislike.tag = true
            }

            1 -> {
                holder.itemView.like.tag = true
                holder.itemView.dislike.tag = false
            }
        }

        setStar(holder)
        setLikePost(holder)
        setLike(holder)
        setDisLike(holder)



        holder.itemView.username.setText(post.userData.username)
        if(post.userData.verified){
            holder.itemView.im_verify.setImageResource(R.drawable.ic_verify)
        }else{
            holder.itemView.im_verify.setImageResource(R.drawable.ic_unverified)
        }

        val per = DecimalFormat("##.##").format((post.percent.replace("%","")).toDouble());
        holder.itemView.tx_percent.text = "$per%"

        if (post.is_sold) {
            holder.itemView.mSold.visibility = View.VISIBLE
            holder.itemView.ll_buy.visibility = View.GONE
        } else {
            holder.itemView.mSold.visibility = View.GONE
            holder.itemView.ll_buy.visibility = View.VISIBLE
        }

        if (TextUtils.isEmpty(post.description)) {
            holder.itemView.description.visibility = View.GONE

        } else {
            holder.itemView.description.text = post.description
            holder.itemView.description.visibility = View.VISIBLE
        }

        if (post.postImages.size > 0) {
            val img = post.postImages[0].url

            Glide.with(context).load(img).apply(RequestOptions().override(1500)).into(holder.itemView.imageView10)

        }

        post.userData?.let {
            Glide.with(context).load(it.profile).apply(RequestOptions().override(80).error(R.drawable.dummy)).into(holder.itemView.user_image)
        }

        if(post.is_auction){

            holder.itemView.mMakeABid.setBackgroundResource(R.drawable.feedview_btn_background)
            holder.itemView.mMakeABid.isEnabled = true

        }else{
            holder.itemView.mMakeABid.setBackgroundResource(R.drawable.feedview_btn_background_gray)
            holder.itemView.mMakeABid.isEnabled = false
        }

        //Log.e("POST_BUY","${post.description} ${post.only_user_from_selected_country} ${post.buy_now} ${post.isBuyAvailable}")
        if((post.buy_now && post.isBuyAvailable)){

            holder.itemView.mBuyNow.setBackgroundResource(R.drawable.feedview_btn_background)
            holder.itemView.mBuyNow.isEnabled = true


        }else{
            holder.itemView.mBuyNow.setBackgroundResource(R.drawable.feedview_btn_background_gray)
            holder.itemView.mBuyNow.isEnabled = false
        }

        // Set Click on Feedback
        holder.itemView.mFeedbackLayout.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                val intent = Intent(context, FeedsActivity::class.java)
                intent.putParcelableArrayListExtra("feeds", post.feedbackList)
                context.startActivity(intent)
            }else{

                CustomDialog(context).createDialogBox()
            }
        }

        holder.itemView.start.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                val param = JSONObject()
                param.put("version", "1.1")
                param.put("post_id", post.id)
                param.put("user_id", MyApplication.instance.getUserPreferences().id)
                param.put("is_fav", !(holder.itemView.start.tag as Boolean))

                favPost(param, position)
            }else{

                CustomDialog(context).createDialogBox()
            }
           /// listener.onFavoritePost(position, param)
        }

        holder.itemView.like.setOnClickListener {
            //  setLike(holder)

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                val param = JSONObject()
                param.put("version", "1.1")
                param.put("post_id", post.id)
                param.put("user_id", MyApplication.instance.getUserPreferences().id)

                param.put("is_vote", 1)

                addVote(position, param)
                // listener.onVoteOnPost(position, param)
            }else{

                CustomDialog(context).createDialogBox()
            }
        }

        holder.itemView.dislike.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                val param = JSONObject()
                param.put("version", "1.1")
                param.put("post_id", post.id)
                param.put("user_id", MyApplication.instance.getUserPreferences().id)

                param.put("is_vote", 0)
                addVote(position, param)
                //listener.onVoteOnPost(position, param)
            }else{

                CustomDialog(context).createDialogBox()
            }

        }

        holder.itemView.heart.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                val param = JSONObject()
                param.put("version", "1.1")
                param.put("post_id", post.id)
                param.put("user_id", "${MyApplication.instance.getUserPreferences().id}")

                if (holder.itemView.heart.tag as Boolean) {
                    param.put("is_like", 0)
                } else {
                    param.put("is_like", 1)
                }

                // listener.onLikePost(position, param)
                likePost(param, position)
            }else{

                CustomDialog(context).createDialogBox()
            }

        }

        holder.itemView.message.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                startChat()
            }else{

                CustomDialog(context).createDialogBox()
            }
        }

        holder.itemView.whtsapp.setOnClickListener {
            if(MyApplication.instance.getUserPreferences().loginStatus) {
                sendWhatsAppMsg()
            }else{

                CustomDialog(context).createDialogBox()
            }
        }

        holder.itemView.call.setOnClickListener {
            Toast.makeText(context,"Currently this feature will not work",Toast.LENGTH_SHORT).show()
        }

        holder.itemView.mMakeABid.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                if (post.isBuyAvailable) {
                    val param = JSONObject()
                    param.put("version", "1.1")
                    param.put("post_id", post.id)
                    param.put("user_id", MyApplication.instance.getUserPreferences().id)
                    param.put("comment", "hey")

                    param.put("is_pinned", false)

                    adapter.amount = "1"
                    getHeighestBid(post, param)
                } else {
                    CustomDialog(context).showAlertDialog(null, null, object : DialogListener() {
                        override fun okClick() {

                        }
                    })
                }
            }else{

                CustomDialog(context).createDialogBox()
            }

           // listener.addBidOnPost(position, param)
        }

        holder.itemView.mBuyNow.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                //  Toast.makeText(context,"Currently this feature will not work",Toast.LENGTH_SHORT).show()
                val intent = Intent(context, PaymentActivity::class.java)
                intent.putExtra("post", post)
                intent.putExtra("type", "post")
                context.startActivity(intent)
            }else{

                CustomDialog(context).createDialogBox()
            }
        }

        holder.itemView.view_click.setOnClickListener {
            if (post.postImages[0].isVideo) {
                var path = post.postImages[0].url
                val ind = path.lastIndexOf(".")
                if (ind > 0) {
                    path = path.substring(0, ind) + ".mp4"
                }
                Log.e("PATH__", "$path  ***")
                val intent = Intent(context, StoryCommentActivity::class.java)
                intent.putExtra("isVideo", true)
                intent.putExtra("path", path)
                context.startActivity(intent)

                progress
            }else{
                showAllImage(position)
            }

            //listener.onClickPost(position)
        }

        holder.view.profile.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                val bundle = Bundle()

                if (post.userData.id.equals(MyApplication.instance.getUserPreferences().id)) {
                    bundle.putBoolean("isMe", true)
                }
                bundle.putParcelable("user", post.userData)
                (context as HomeActivity).setProfileFragment(bundle)

            }else{

                CustomDialog(context).createDialogBox()
            }
        }

        holder.itemView.im_menu.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                showPopupMenu(it, position)
            }else{

                CustomDialog(context).createDialogBox()
            }
           // listener.onOptionClick(it, position)
        }

    }

    private fun setLikePost(holder: FeedBuyerAdapter.ViewHolder) {
        if (holder.itemView.heart.tag as Boolean) {
            holder.itemView.heart.setImageResource(R.drawable.ic_heart_on)
            holder.itemView.heart.tag = true
        } else {
            holder.itemView.heart.setImageResource(R.drawable.ic_heart)
            holder.itemView.heart.tag = false
        }
    }

    private fun setStar(holder: FeedBuyerAdapter.ViewHolder) {
        if (holder.itemView.start.tag as Boolean) {
            holder.itemView.start.setImageResource(R.drawable.ic_star_on)
            holder.itemView.start.tag = true
        } else {
            holder.itemView.start.setImageResource(R.drawable.ic_star)
            holder.itemView.start.tag = false
        }
    }

    private fun setDisLike(holder: FeedBuyerAdapter.ViewHolder) {
        if (holder.itemView.dislike.tag as Boolean) {
            holder.itemView.like.setImageResource(R.drawable.ic_like)
            holder.itemView.dislike.setImageResource(R.drawable.ic_unlike_on)
            holder.itemView.like.tag = false
            holder.itemView.dislike.tag = true
        } else {
            holder.itemView.dislike.setImageResource(R.drawable.ic_dislike)
            holder.itemView.dislike.tag = false
            //holder.itemView.like.setImageResource(R.drawable.ic_like)
            //  holder.itemView.like.tag = false

        }
    }

    private fun setLike(holder: FeedBuyerAdapter.ViewHolder) {

        if ((holder.itemView.like.tag as Boolean)) {
            holder.itemView.like.setImageResource(R.drawable.ic_like_on)
            holder.itemView.dislike.setImageResource(R.drawable.ic_dislike)
            holder.itemView.like.tag = true
            holder.itemView.dislike.tag = false

        } else {
            holder.itemView.like.setImageResource(R.drawable.ic_like)
            holder.itemView.like.tag = false
            // holder.itemView.dislike.setImageResource(R.drawable.ic_unlike_on)

            //  holder.itemView.dislike.tag = true
        }
    }

    private fun sendSMS(phoneNo: String, msg: String) {
        try {
            val smsManager = SmsManager.getDefault()
            smsManager.sendTextMessage(phoneNo, null, msg, null, null)
            Toast.makeText(context, "Message Sent",
                    Toast.LENGTH_LONG).show()
        } catch (ex: Exception) {
            Toast.makeText(context, ex.message.toString(),
                    Toast.LENGTH_LONG).show()
            ex.printStackTrace()
        }

    }

    private fun sendWhatsAppMsg() {
        if(post.userData != null){
            val smsNumber = post.userData.phone
            val sendIntent = Intent("android.intent.action.MAIN")
            try {
                sendIntent.component = ComponentName("com.whatsapp", "com.whatsapp.Conversation")
                sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(smsNumber) + "@s.whatsapp.net")//phone number without "+" prefix
                context.startActivity(sendIntent)
            } catch (e: Exception) {
                Toast.makeText(context, context.getString(R.string.no_watsapp), Toast.LENGTH_SHORT).show()
            }
        }else{
            Toast.makeText(context, context.getString(R.string.no_phone_no), Toast.LENGTH_SHORT).show()
        }
    }

    private fun showAllImage(position: Int) {

        val imageList2 = post.postImages

        var currentImagePositon = position
        Fresco.initialize(context);
        val overView = ImageOverLayView(context)
        //val pathList = arrayOfNulls<String>(imageList2.size)
        //imageList2.toArray(pathList)


        // Log.e("IMAGE_SIZE","${imageList.size}  ** ${pathList.size}")
        //  val randomColor = intArrayOf(R.color.imageViewerColor1, R.color.imageViewerColor2, R.color.imageViewerColor3, R.color.imageViewerColor4)
        val mImageViewerBuilder = ImageViewer.Builder<PostImages>(context, imageList2)
        val mImageViewer = mImageViewerBuilder.setStartPosition(position)
                .setOverlayView(overView)
                .setStartPosition(0)
                .setBackgroundColor(ContextCompat.getColor(context, R.color.colorBlack))
                .setFormatter(object : ImageViewer.Formatter<PostImages>{
                    override fun format(t: PostImages?): String {
                        return t!!.url
                    }

                })

                .show()


        mImageViewerBuilder.setImageChangeListener(object : ImageViewer.OnImageChangeListener{
            override fun onImageChange(positionImage: Int) {
                currentImagePositon = positionImage
            }

        })

        overView.back.setOnClickListener {
            mImageViewer.onDismiss()
        }


    }


    private fun showPopupMenu(view: View, position: Int) {

        val popupMenu = PopupMenu(context!!, view)
        popupMenu.inflate(R.menu.post_menu)
        if(post.userData.id.equals(MyApplication.instance.getUserPreferences().id)){
            popupMenu.menu.findItem(R.id.mHide).isVisible = true
            popupMenu.menu.findItem(R.id.mRepost).isVisible = true
        }else{
            popupMenu.menu.findItem(R.id.mHide).isVisible = false
            popupMenu.menu.findItem(R.id.mRepost).isVisible = false
        }

        popupMenu.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.mShare -> {
                    generateDynamicLink()
                }

                R.id.mReport -> {
                    CustomDialog(context).showReportDialog("",object :DialogListener(){
                        override fun okClick(any: Any) {
                            reportPost(any.toString(),position)
                        }
                    })
                }
            }

            true
        }

        popupMenu.show()
    }



    private fun bidDialog(param: JSONObject){
        CustomDialog(context).showBidialog(adapter.amount,object : DialogListener() {
            override fun okClick(any: Any) {
                param.put("bid_amount", any.toString())
                makeBid(param)

            }
        })
    }

    private fun startChat() {
        Log.e(TAG,"** startChat")

      /*  doAsync {
            try {
                val roomUserData = AppDatabase.getAppDatabase().roomUserDao().getRoomUser(post.userData.id)
                Log.e(TAG,"** roomUserData $roomUserData")

                if(roomUserData != null && post.userData.id.equals(roomUserData.user_id)){
                    (context as Activity).runOnUiThread {
                        val intent = Intent(context,ChatActivity::class.java)
                        intent.putExtra("userId",roomUserData.user_id)

                        context.startActivity(intent)
                    }
                }else{

                    Log.e("CHECK_ROOM","** @@@@@@@@@@@@@@")
                    (context as Activity).runOnUiThread{

                    }

                }
            }catch (e:Exception){
                e.printStackTrace()

                  (context as Activity).runOnUiThread {
                      Toast.makeText(context,"Failed to create room",Toast.LENGTH_SHORT).show()
                  }
            }
        }*/

        createRoomWithUser()
    }

    private fun createRoomWithUser() { //sender_id: senderId, receiver_id: receiverId
        val param = JSONObject()
        param.put("version","1.1")
        param.put("user_id1", userRef.id)
        param.put("user_id2", post.userData.id)

        Log.e(TAG,"Room_PARAM ${param.toString()}")
        progress.show()

        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {

                progress.dismiss()

                val intent = Intent(context,ChatActivity::class.java)
                intent.putExtra("userId",post.userData.id)
                intent.putExtra("userData",post.userData)
                context.startActivity(intent)

            }

            override fun onError(errorMessage: String) {
                progress.dismiss()

                Toast.makeText(context,"Failed to create room $errorMessage",Toast.LENGTH_SHORT).show()
            }

        }).createRoom(param)

    }

    private fun likePost(jsonObject: JSONObject, position: Int) {
        Log.e("LIKE_POST", "*** $jsonObject")
        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                post.isLike = jsonObject.getInt("is_like") ==1
                (context as Activity).runOnUiThread {
                    adapter.notifyItemChanged(position)
                }
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                Toast.makeText(context, "Failed to like", Toast.LENGTH_SHORT).show()
            }

        }).likePost(jsonObject)

    }

    private fun favPost(jsonObject: JSONObject, position: Int) {
        Log.e("LIKE_POST", "*** $jsonObject")
        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                post.isFav = (jsonObject.getBoolean("is_fav"))
                (context as Activity).runOnUiThread {
                    adapter.notifyItemChanged(position)
                }
                doAsync {
                    if(jsonObject.getBoolean("is_fav")){
                        val favPost = FavPost()
                        favPost.id= post.id
                        favPost.post_id= post.id
                        AppDatabase.getAppDatabase().favPostDao().insert(favPost)
                    }else{
                        AppDatabase.getAppDatabase().favPostDao().deleteFavPost(post.id)
                        adapter.reportListener?.onUnFav(position)
                    }
                }
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                Toast.makeText(context, "Failed to add in favorites", Toast.LENGTH_SHORT).show()
            }

        }).favPost(jsonObject)

    }

    private fun makeBid(param: JSONObject) {
        progress!!.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                val json = JSONObject(`object`.toString())
                if (json.has("description")) {
                    val array = json.getJSONArray("description")
                    val list = ArrayList<BidModel>()
                    for (i in 0 until array.length()) {
                        val obj = array.getJSONObject(i)
                        val bid = BidModel("")

                        if (obj.has("_id")) {
                            bid.id = obj.getString("_id")
                        }

                        if (obj.has("post_id")) {
                            bid.postId = obj.getString("post_id")
                        }

                        if (obj.has("bid_user_id")) {
                            bid.userId = obj.getString("bid_user_id")
                        }

                        if (obj.has("bid_comment")) {
                            bid.comment = obj.getString("bid_comment")
                        }

                        if (obj.has("bid_amount")) {
                            bid.bidAmount = obj.getString("bid_amount")
                        }

                        if (obj.has("bid_created_at")) {
                            bid.id = obj.getString("bid_created_at")
                        }

                        if (obj.has("is_pinned")) {
                            bid.isPinned = obj.getBoolean("is_pinned")
                        }

                        list.add(bid)
                    }
                }

                Toast.makeText(context!!,"Bid added successfully",Toast.LENGTH_SHORT).show()
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                CustomDialog(context!!).showErrorDialog("Failed to make bid \n$errorMessage")
            }

        }).addBidOnPost(param)
    }

    private fun addVote(position: Int, param: JSONObject) {

        progress!!.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                val json = JSONObject(`object`.toString())
                if(post.isVote == param.getInt("is_vote")){
                    post.isVote = -1
                }else{
                    post.isVote = param.getInt("is_vote")
                }

                if(json.has("percent")){
                    post.percent = json.getString("percent")
                }

                adapter.notifyItemChanged(position)

            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                CustomDialog(context!!).showErrorDialog("Failed to make bid \n$errorMessage")
            }

        }).vote(param)
    }

    private fun getHeighestBid(post: PostModel,param1: JSONObject) { ////type: stream / post / story, stream_id: streamId, story_id: storyId, post_id: postId .
        val param = JSONObject() // : bid_user_id, bid_amount: bid_amount.
        param.put("version", "1.1")

        param.put("post_id", post!!.id)
        param.put("type", "post")


        progress!!.show()
        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                progress!!.dismiss()
                val js = JSONObject(`object`.toString())//amount
                if(js.has("description")){
                    val json = js.getJSONObject("description")
                    if(json.has("bid_amount")){
                        try {
                            adapter.amount = json.get("bid_amount").toString()

                        }catch (e:Exception){

                        }finally {
                            bidDialog(param1)
                        }
                    }else{
                        if(json.has("amount")){
                            try {
                                adapter.amount = json.get("amount").toString()

                            }catch (e:Exception){

                            }finally {
                                bidDialog(param1)
                            }
                        }else{
                            bidDialog(param1)
                        }
                    }


                }else{
                    bidDialog(param1)
                }
            }

            override fun onError(errorMessage: String) {
                progress!!.dismiss()
                bidDialog(param1)

            }

        }).getHighestBid(param)

    }

    private fun reportPost(msg: String, position: Int) {

        val param = JSONObject()
        param.put("version","1.1")
        param.put("self_id",userRef.id)
        param.put("post_id",post.id)
        param.put("reason",msg)
        param.put("message",msg)

        progress.show()
        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                if(adapter.reportListener != null){
                    adapter.reportListener!!.onReport(position)
                }
                Toast.makeText(context,"Send your report successfully",Toast.LENGTH_SHORT).show()
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                Toast.makeText(context,"Failed $errorMessage",Toast.LENGTH_SHORT).show()
            }

        }).reportPost(param)
    }

    private fun generateDynamicLink() {
        var linkText = ""
        FirebaseApp.initializeApp(context)
        progress.show()
        val url = "https://mazadlive.page.link?id=${post.id}&type=post"
        val shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse(url))
                .setDomainUriPrefix(Constant.PREFIX)
                .setAndroidParameters(
                        DynamicLink.AndroidParameters.Builder("com.mazadlive")
                                .build())

                .setIosParameters(
                        DynamicLink.IosParameters.Builder("com.itwhiz4u.q8mercato.mazadlive")
                                .setAppStoreId("1450514377")
                                .build())

                .setSocialMetaTagParameters(DynamicLink.SocialMetaTagParameters.Builder()
                        .setTitle("")
                        .setDescription(post.description)
                        .setImageUrl(Uri.parse(post.postImages[0].url))
                        .build())

                .setGoogleAnalyticsParameters(
                        DynamicLink.GoogleAnalyticsParameters.Builder()
                                .setSource("orkut")
                                .setMedium("social")
                                .setCampaign("example-promo")
                                .build())

                .buildShortDynamicLink()

                .addOnSuccessListener { result ->

                    val shortLink = result.shortLink
                    val flowchartLink = result.previewLink
                    linkText = shortLink.toString()

                  //  ConstantFunction.saveDynamicLink(hotelModel!!.id, linkText)
                    Log.e(TAG, "LINK :: $shortLink  *** $flowchartLink")
                    shareLink(linkText)

                }.addOnFailureListener {

                    progress.dismiss()
                    Log.e(TAG, "LINK :: addOnFailureListener  *** ")
                    progress?.dismiss()
                }
    }

    private fun shareLink(linkText: String) {
        progress.dismiss()
        val shareIntent = Intent()
        shareIntent.setAction(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, linkText)
        context.startActivity(shareIntent)
    }

}