package com.laundryapp.controllers

import android.content.Context
import android.util.Log
import com.mazadlive.adapters.ChatListAdapter
import com.mazadlive.database.MessageData
import com.mazadlive.helper.Constant
import com.mazadlive.models.ChatMessage
import kotlinx.android.synthetic.main.chat_receive_txt_controller_layout.view.*

class ChatReceiveTxtController(val context: Context, val viewHolder: ChatListAdapter.ViewHolderMessageReceive,
                               val chatMessage: MessageData, val messageListAdapter: ChatListAdapter, val position: Int) {
    val TAG = "ChatSendTxtController"
    fun control() {
        Log.e(TAG," ${chatMessage.id}  ${chatMessage.content}")
      //  viewHolder.itemView.tv_time.text = chatMessage.formattedTime
        viewHolder.itemView.tv_msg_txt.text = chatMessage.content
        viewHolder.itemView.tv_time.text = (Constant.getTime(chatMessage.created_at))
    }

}