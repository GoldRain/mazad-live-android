package com.mazadlive.controllers

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.FirebaseApp
import com.google.firebase.dynamiclinks.DynamicLink
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.mazadlive.Interface.DialogListener
import com.mazadlive.R
import com.mazadlive.activities.FeedsActivity
import com.mazadlive.activities.StoryActivity
import com.mazadlive.adapters.StoryAdapter
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.customdialogs.CustomProgress
import com.mazadlive.helper.Constant
import com.mazadlive.models.StoryModel
import com.mazadlive.utils.MyApplication
import io.fotoapparat.log.logcat
import kotlinx.android.synthetic.main.list_item_story.view.*
import org.json.JSONObject

class StoryController(val context: Context, val holder: StoryAdapter.ViewHolder, val story: StoryModel, val adapter: StoryAdapter, val position: Int, val listener: StoryAdapter.OnClikPost) {

    var progress: CustomProgress = CustomProgress(context)

    fun control() {
        Log.d("MYLOG ", "STory is ${story.product_description}");

        if (story.is_video) {
            holder.itemView.im_video.visibility = View.VISIBLE
        } else {
            holder.itemView.im_video.visibility = View.GONE
        }

        if (story.userData!!.verified) {
            holder.itemView.im_verify.setImageResource(R.drawable.ic_verify)
        } else {
            holder.itemView.im_verify.setImageResource(R.drawable.ic_unverified)
        }

        if (story.is_sold){
            holder.itemView.mSold.visibility = View.VISIBLE
        }else{
            holder.itemView.mSold.visibility = View.GONE

        }

        if (story.userData != null) {
            holder.itemView.username.setText(story.userData!!.username)
            Glide.with(context).load(story.userData!!.profile).apply(RequestOptions().override(80).error(R.drawable.dummy)).into(holder.itemView.user_image)

            if (story.userData!!.id.equals(MyApplication.instance.getUserPreferences().id)) {
                holder.itemView.im_menu.visibility == View.VISIBLE
            } else {
                holder.itemView.im_menu.visibility == View.GONE
            }
        }

        holder.itemView.time.text = Constant.getDateAndTime(story.createdAt.toLong())
        Glide.with(context).load(story.url).apply(RequestOptions().override(500).error(R.drawable.dummy)).into(holder.itemView.story_image)
        holder.itemView.product_description.text = story.product_description

        // Set Feedback
        if (story.feedbackList.size > 0) {
            holder.itemView.mFeedbackLayout.visibility = View.VISIBLE
        } else {
            holder.itemView.mFeedbackLayout.visibility = View.GONE
        }

        // Set Click on Feedback
        holder.itemView.mFeedbackLayout.setOnClickListener {
            val intent = Intent(context, FeedsActivity::class.java)
            intent.putParcelableArrayListExtra("feeds", story.feedbackList)
            context.startActivity(intent)
        }

        holder.itemView.setOnClickListener {
            adapter.openAllStory(position)
        }

        holder.itemView.im_menu.setOnClickListener {

            if(MyApplication.instance.getUserPreferences().loginStatus) {
                showPopupMenu(it)
            }else{

                CustomDialog(context).createDialogBox()
            }
        }
    }

    private fun showPopupMenu(view: View) {

        val popupMenu = PopupMenu(context, view)
        popupMenu.inflate(R.menu.story_menu)

        if (story.userData!!.id.equals(MyApplication.instance.getUserPreferences().id)) {
            popupMenu.menu.findItem(R.id.mHide).isVisible = true
            popupMenu.menu.findItem(R.id.mReport).isVisible = false
        } else {
            popupMenu.menu.findItem(R.id.mReport).isVisible = true
            popupMenu.menu.findItem(R.id.mHide).isVisible = false
        }

        popupMenu.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.mShare -> {
                    generateDynamicLink()
                }

                R.id.mHide -> {
                    deleteStory()
                }
                R.id.mReport -> {
                    CustomDialog(context).showReportDialog("", object : DialogListener() {
                        override fun okClick(any: Any) {
                            reportStory(any.toString(), position)
                        }
                    })
                }
            }

            true
        }

        popupMenu.show()
    }

    private fun reportStory(msg: String, position: Int) {

        val param = JSONObject()
        param.put("version", "1.1")
        param.put("self_id", MyApplication.instance.getUserPreferences().id)
        param.put("story_id", story.id)
        param.put("reason", msg)
        param.put("message", msg)

        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress.dismiss()
                adapter.liveStoryList.removeAt(position)
                adapter.notifyDataSetChanged()
                Toast.makeText(context, "Send your report successfully", Toast.LENGTH_SHORT).show()
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                Toast.makeText(context, "Failed $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).reportStory(param)
    }

    private fun deleteStory() {
        val param = JSONObject()
        param.put("version", "1.1")
        param.put("user_id", MyApplication.instance.getUserPreferences().id)
        param.put("story_id", story.id)

        progress.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress.dismiss()

                adapter.liveStoryList.removeAt(position)
                adapter.notifyDataSetChanged()
            }

            override fun onError(errorMessage: String) {
                progress.dismiss()
                /*adapter.liveStoryList.removeAt(position)
                adapter.notifyDataSetChanged()*/
                Toast.makeText(context, "${context.getString(R.string.failed)} $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).deleteStory(param)
    }

    val TAG = ""

    private fun generateDynamicLink() {
        var linkText = ""
        FirebaseApp.initializeApp(context)
        progress.show()
        val url = "https://mazadlive.page.link?id=${story.id}&type=story"
        val shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse(url))
                .setDomainUriPrefix(Constant.PREFIX)
                .setAndroidParameters(
                        DynamicLink.AndroidParameters.Builder("com.mazadlive")
                                .build())

                .setIosParameters(
                        DynamicLink.IosParameters.Builder("com.itwhiz4u.q8mercato.mazadlive")
                                .setAppStoreId("1450514377")
                                .build())

                .setSocialMetaTagParameters(DynamicLink.SocialMetaTagParameters.Builder()
                        .setTitle("")
                        .setDescription(story.product_description)
                        .setImageUrl(Uri.parse(story.url))
                        .build())

                .setGoogleAnalyticsParameters(
                        DynamicLink.GoogleAnalyticsParameters.Builder()
                                .setSource("orkut")
                                .setMedium("social")
                                .setCampaign("example-promo")
                                .build())

                .buildShortDynamicLink()

                .addOnSuccessListener { result ->

                    val shortLink = result.shortLink
                    val flowchartLink = result.previewLink
                    linkText = shortLink.toString()

                    //  ConstantFunction.saveDynamicLink(hotelModel!!.id, linkText)
                    Log.e(TAG, "LINK :: $shortLink  *** $flowchartLink")
                    shareLink(linkText)

                }.addOnFailureListener {

                    progress.dismiss()
                    Log.e(TAG, "LINK :: addOnFailureListener  *** ")
                    progress?.dismiss()
                }
    }

    private fun shareLink(linkText: String) {
        progress.dismiss()
        val shareIntent = Intent()
        shareIntent.setAction(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, linkText)
        context.startActivity(shareIntent)
    }

}