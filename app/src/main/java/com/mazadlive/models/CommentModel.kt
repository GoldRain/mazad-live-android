package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

class CommentModel():Parcelable {


    var id = ""
    var user_id = ""
    var commentType = ""
    var message = ""
    var status = ""
    var date = 0L
    var type = "comment"
    var userData :UserModel?= null
    var createdAt = ""
    var isBookmark = false
    var isCheck = false

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        user_id = parcel.readString()
        commentType = parcel.readString()
        status = parcel.readString()
        message = parcel.readString()
        date = parcel.readLong()
        type = parcel.readString()
        userData = parcel.readParcelable(UserModel::class.java.classLoader)
        createdAt = parcel.readString()
        isBookmark = parcel.readByte() != 0.toByte()
        isCheck = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(user_id)
        parcel.writeString(commentType)
        parcel.writeString(message)
        parcel.writeString(status)
        parcel.writeLong(date)
        parcel.writeString(type)
        parcel.writeParcelable(userData, flags)
        parcel.writeString(createdAt)
        parcel.writeByte(if (isBookmark) 1 else 0)
        parcel.writeByte(if (isCheck) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CommentModel> {
        override fun createFromParcel(parcel: Parcel): CommentModel {
            return CommentModel(parcel)
        }

        override fun newArray(size: Int): Array<CommentModel?> {
            return arrayOfNulls(size)
        }
    }


}