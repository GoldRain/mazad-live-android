package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

class LiveStoryModel() : Parcelable {

    var id = ""
    var url = ""
    var price = ""
    var user_id = ""
    var streamName = ""
    var streamId = ""
    var status = ""
    var viewerCounts = 0
    var thumbUrl = ""
    var countries = ""
    var userData: UserModel? = null
    var currentProduct: LiveProductModel? = null
    var commentsList = ArrayList<CommentModel>()
    var countryList = ArrayList<CountryModel>()

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        url = parcel.readString()
        price = parcel.readString()
        user_id = parcel.readString()
        streamName = parcel.readString()
        streamId = parcel.readString()
        status = parcel.readString()
        viewerCounts = parcel.readInt()
        thumbUrl = parcel.readString()
        countries = parcel.readString()
        userData = parcel.readParcelable(UserModel::class.java.classLoader)
        currentProduct = parcel.readParcelable(LiveProductModel::class.java.classLoader)
        countryList = parcel.createTypedArrayList(CountryModel.CREATOR)
        commentsList = parcel.createTypedArrayList(CommentModel.CREATOR)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(url)
        parcel.writeString(price)
        parcel.writeString(user_id)
        parcel.writeString(streamName)
        parcel.writeString(streamId)
        parcel.writeString(status)
        parcel.writeInt(viewerCounts)
        parcel.writeString(thumbUrl)
        parcel.writeString(countries)
        parcel.writeParcelable(userData, flags)
        parcel.writeParcelable(currentProduct, flags)
        parcel.writeTypedList(countryList)
        parcel.writeTypedList(commentsList)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LiveStoryModel> {
        override fun createFromParcel(parcel: Parcel): LiveStoryModel {
            return LiveStoryModel(parcel)
        }

        override fun newArray(size: Int): Array<LiveStoryModel?> {
            return arrayOfNulls(size)
        }
    }


}