package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

class LiveProductModel():Parcelable {

    var itemNo = 0
    var serial = "NA"
    var description = "NA"
    var url = ""
    var path = ""

    var localId = ""
    var streamId = ""
    var userId = ""
    var price = "0"
    var paymentMode = "NA"
    var userName = "NA"
    var isSold = false
    var createdAt = ""

    constructor(parcel: Parcel) : this() {
        itemNo = parcel.readInt()
        serial = parcel.readString()
        description = parcel.readString()
        url = parcel.readString()
        path = parcel.readString()
        localId = parcel.readString()
        streamId = parcel.readString()
        userId = parcel.readString()
        price = parcel.readString()
        paymentMode = parcel.readString()
        userName = parcel.readString()
        isSold = parcel.readByte() != 0.toByte()
        createdAt = parcel.readString()

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(itemNo)
        parcel.writeString(serial)
        parcel.writeString(description)
        parcel.writeString(url)
        parcel.writeString(path)
        parcel.writeString(localId)
        parcel.writeString(streamId)
        parcel.writeString(userId)
        parcel.writeString(price)
        parcel.writeString(paymentMode)
        parcel.writeString(userName)
        parcel.writeByte(if (isSold) 1 else 0)
        parcel.writeString(createdAt)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LiveProductModel> {
        override fun createFromParcel(parcel: Parcel): LiveProductModel {
            return LiveProductModel(parcel)
        }

        override fun newArray(size: Int): Array<LiveProductModel?> {
            return arrayOfNulls(size)
        }
    }


}