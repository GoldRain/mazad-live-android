package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

class WalletModel():Parcelable{

    var id = ""
    var paymenMethod = ""
    var message = ""
    var displayId = "NA"
    var feedback = "pending"
    var status = ""
    var seller_id = ""
    var buyer_id = ""
    var order_id = "NA"
    var created_at =0.toLong()
    var type = ""
    var typeId = ""
    var orderModel:OrderModel?= null
    var postModel:PostModel?= null
    var storyModel:StoryModel?= null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        paymenMethod = parcel.readString()
        message = parcel.readString()
        displayId = parcel.readString()
        feedback = parcel.readString()
        status = parcel.readString()
        seller_id = parcel.readString()
        buyer_id = parcel.readString()
        order_id = parcel.readString()
        created_at = parcel.readLong()
        type = parcel.readString()
        typeId = parcel.readString()
        orderModel = parcel.readParcelable(OrderModel::class.java.classLoader)
        postModel = parcel.readParcelable(PostModel::class.java.classLoader)
        storyModel = parcel.readParcelable(StoryModel::class.java.classLoader)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(paymenMethod)
        parcel.writeString(message)
        parcel.writeString(displayId)
        parcel.writeString(feedback)
        parcel.writeString(status)
        parcel.writeString(seller_id)
        parcel.writeString(buyer_id)
        parcel.writeString(order_id)
        parcel.writeLong(created_at)
        parcel.writeString(type)
        parcel.writeString(typeId)
        parcel.writeParcelable(orderModel, flags)
        parcel.writeParcelable(postModel, flags)
        parcel.writeParcelable(storyModel, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<WalletModel> {
        override fun createFromParcel(parcel: Parcel): WalletModel {
            return WalletModel(parcel)
        }

        override fun newArray(size: Int): Array<WalletModel?> {
            return arrayOfNulls(size)
        }
    }


}

/*{
"message":"",
"status":"Pending",
"payment_method":"cod",
"created_at":"1546520409883",
"_id":"5c2e075937ce88443c1b1e24",
"seller_id":"5c0a5b0707be2938de836e25",
"buyer_id":"5c06183c71bbd11bde6d6fc1",
"order_id":"5c2e075937ce88443c1b1e23",
"__v":0
},*/