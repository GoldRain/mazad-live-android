package com.mazadlive.models

data class CardModel(val cardName: String, val cardIcon: Int)