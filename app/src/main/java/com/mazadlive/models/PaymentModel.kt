package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

class PaymentModel(var status1: String):Parcelable {

    var id = ""
    var paymenMethod = ""
    var message = ""
    var displayId = "NA"
    var feedback = "pending"
    var status = ""
    var seller_id = ""
    var buyer_id = ""
    var order_id = "NA"
    var type = ""
    var typeId = ""
    var sellerName = ""
    var orderModel:OrderModel?= null
    var postModel:PostModel?= null
    var storyModel:StoryModel?= null
    var created_at =0.toLong()

    constructor(parcel: Parcel) : this(parcel.readString()) {
        id = parcel.readString()
        paymenMethod = parcel.readString()
        message = parcel.readString()
        displayId = parcel.readString()
        feedback = parcel.readString()
        status = parcel.readString()
        seller_id = parcel.readString()
        buyer_id = parcel.readString()
        order_id = parcel.readString()
        type = parcel.readString()
        typeId = parcel.readString()
        sellerName = parcel.readString()
        orderModel = parcel.readParcelable(OrderModel::class.java.classLoader)
        postModel = parcel.readParcelable(PostModel::class.java.classLoader)
        storyModel = parcel.readParcelable(StoryModel::class.java.classLoader)
        created_at = parcel.readLong()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(status1)
        parcel.writeString(id)
        parcel.writeString(paymenMethod)
        parcel.writeString(message)
        parcel.writeString(displayId)
        parcel.writeString(feedback)
        parcel.writeString(status)
        parcel.writeString(seller_id)
        parcel.writeString(buyer_id)
        parcel.writeString(order_id)
        parcel.writeString(type)
        parcel.writeString(typeId)
        parcel.writeString(sellerName)
        parcel.writeParcelable(orderModel, flags)
        parcel.writeParcelable(postModel, flags)
        parcel.writeParcelable(storyModel, flags)
        parcel.writeLong(created_at)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PaymentModel> {
        override fun createFromParcel(parcel: Parcel): PaymentModel {
            return PaymentModel(parcel)
        }

        override fun newArray(size: Int): Array<PaymentModel?> {
            return arrayOfNulls(size)
        }
    }


}