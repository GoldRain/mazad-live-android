package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by bodacious on 3/1/19.
 */
class FeedbackModel() :Parcelable {

    var id = ""
    var type = ""
    var status = ""
    var user_payment_status = ""
    var transaction_id = ""
    var amount = ""
    var feedback = ""
    var user_id = ""
    var type_id = ""
    var address_id = ""
    var payment_method = ""
    var created_at = 0.toLong()
    var postModel:PostModel?= null
    var storyModel:StoryModel?= null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        type = parcel.readString()
        status = parcel.readString()
        user_payment_status = parcel.readString()
        transaction_id = parcel.readString()
        amount = parcel.readString()
        feedback = parcel.readString()
        user_id = parcel.readString()
        type_id = parcel.readString()
        address_id = parcel.readString()
        payment_method = parcel.readString()
        created_at = parcel.readLong()
        postModel = parcel.readParcelable(PostModel::class.java.classLoader)
        storyModel = parcel.readParcelable(StoryModel::class.java.classLoader)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(type)
        parcel.writeString(status)
        parcel.writeString(user_payment_status)
        parcel.writeString(transaction_id)
        parcel.writeString(amount)
        parcel.writeString(feedback)
        parcel.writeString(user_id)
        parcel.writeString(type_id)
        parcel.writeString(address_id)
        parcel.writeString(payment_method)
        parcel.writeLong(created_at)
        parcel.writeParcelable(postModel, flags)
        parcel.writeParcelable(storyModel, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FeedbackModel> {
        override fun createFromParcel(parcel: Parcel): FeedbackModel {
            return FeedbackModel(parcel)
        }

        override fun newArray(size: Int): Array<FeedbackModel?> {
            return arrayOfNulls(size)
        }
    }


}