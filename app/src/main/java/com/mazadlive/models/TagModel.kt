package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable


class TagModel() :Parcelable{

    var id =""
    var name =""
    var isCheck = false
    var createdAt = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        name = parcel.readString()
        isCheck = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeByte(if (isCheck) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TagModel> {
        override fun createFromParcel(parcel: Parcel): TagModel {
            return TagModel(parcel)
        }

        override fun newArray(size: Int): Array<TagModel?> {
            return arrayOfNulls(size)
        }
    }


}