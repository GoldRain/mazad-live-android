package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

class SubscribeModel() :Parcelable{

    var id = ""
    var activation= ""
    var expiry = ""
    var previousCount = 0
    var status = false

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        activation = parcel.readString()
        expiry = parcel.readString()
        previousCount = parcel.readInt()
        status = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(activation)
        parcel.writeString(expiry)
        parcel.writeInt(previousCount)
        parcel.writeByte(if (status) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SubscribeModel> {
        override fun createFromParcel(parcel: Parcel): SubscribeModel {
            return SubscribeModel(parcel)
        }

        override fun newArray(size: Int): Array<SubscribeModel?> {
            return arrayOfNulls(size)
        }
    }


}