package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

class ActivityModel() :Parcelable{

    var userImageUrl: String? = null
    var userName: String? = null
    var activityTxt: String? = null
    var activityTime: String? = null
    var extraUrls = ArrayList<String>()

    var id = ""
    var message = ""
    var type = ""
    var user_id = ""
    var type_id = ""
    var created_at = 0.toLong()
    var user:UserModel?= null
    var post:PostModel?= null
    var story:StoryModel?= null

    constructor(parcel: Parcel) : this() {
        userImageUrl = parcel.readString()
        userName = parcel.readString()
        activityTxt = parcel.readString()
        activityTime = parcel.readString()
        id = parcel.readString()
        message = parcel.readString()
        type = parcel.readString()
        user_id = parcel.readString()
        type_id = parcel.readString()
        created_at = parcel.readLong()
        user = parcel.readParcelable(UserModel::class.java.classLoader)
        post = parcel.readParcelable(PostModel::class.java.classLoader)
        story = parcel.readParcelable(StoryModel::class.java.classLoader)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(userImageUrl)
        parcel.writeString(userName)
        parcel.writeString(activityTxt)
        parcel.writeString(activityTime)
        parcel.writeString(id)
        parcel.writeString(message)
        parcel.writeString(type)
        parcel.writeString(user_id)
        parcel.writeString(type_id)
        parcel.writeLong(created_at)
        parcel.writeParcelable(user, flags)
        parcel.writeParcelable(post, flags)
        parcel.writeParcelable(story, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ActivityModel> {
        override fun createFromParcel(parcel: Parcel): ActivityModel {
            return ActivityModel(parcel)
        }

        override fun newArray(size: Int): Array<ActivityModel?> {
            return arrayOfNulls(size)
        }
    }


}