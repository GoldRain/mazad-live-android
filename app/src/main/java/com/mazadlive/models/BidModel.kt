package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

class BidModel(var name : String):Parcelable {

    var status = name

    var id = ""
    var postId = ""
    var userId = ""
    var username = ""
    var bidAmount = ""
    var updatedAt = ""
    var comment = ""
    var isPinned = false
    var isPaid = false

    var createdAt = ""

    constructor(parcel: Parcel) : this(parcel.readString()) {
        status = parcel.readString()
        id = parcel.readString()
        postId = parcel.readString()
        userId = parcel.readString()
        username = parcel.readString()
        bidAmount = parcel.readString()
        updatedAt = parcel.readString()
        comment = parcel.readString()
        isPinned = parcel.readByte() != 0.toByte()
        isPaid = parcel.readByte() != 0.toByte()
        createdAt = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(status)
        parcel.writeString(id)
        parcel.writeString(postId)
        parcel.writeString(userId)
        parcel.writeString(username)
        parcel.writeString(bidAmount)
        parcel.writeString(updatedAt)
        parcel.writeString(comment)
        parcel.writeByte(if (isPinned) 1 else 0)
        parcel.writeByte(if (isPaid) 1 else 0)
        parcel.writeString(createdAt)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BidModel> {
        override fun createFromParcel(parcel: Parcel): BidModel {
            return BidModel(parcel)
        }

        override fun newArray(size: Int): Array<BidModel?> {
            return arrayOfNulls(size)
        }
    }

}