package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by bodacious on 4/1/19.
 */
class FeedSellerModel() :Parcelable{
    var typeId = ""
    var type = ""
    var created_at = ""
    var post:PostModel?= null
    var story:StoryModel?= null

    constructor(parcel: Parcel) : this() {
        typeId = parcel.readString()
        type = parcel.readString()
        created_at = parcel.readString()
        post = parcel.readParcelable(PostModel::class.java.classLoader)
        story = parcel.readParcelable(StoryModel::class.java.classLoader)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(typeId)
        parcel.writeString(type)
        parcel.writeString(created_at)
        parcel.writeParcelable(post, flags)
        parcel.writeParcelable(story, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FeedSellerModel> {
        override fun createFromParcel(parcel: Parcel): FeedSellerModel {
            return FeedSellerModel(parcel)
        }

        override fun newArray(size: Int): Array<FeedSellerModel?> {
            return arrayOfNulls(size)
        }
    }
}