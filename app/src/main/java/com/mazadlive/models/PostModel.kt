package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

class PostModel() :Parcelable {

    var id = ""
    var amount = ""
    var post_user_id = ""
    var latitude = ""
    var longitude = ""
    var place_name = ""
    var description = ""
    var serial = "NA"
    var percent = "0%"
    var is_instagram = false
    var is_twitter = false
    var is_auction = false
    var buy_now = false
    var whatsapp_only = false
    var whatsapp_and_call = false
    var only_user_from_selected_country = false
    var is_sold = false
    var is_story = false
    var isLike = false
    var isFav = false
    var isPromoted = false
    var is_deleted = false
    var isBuyAvailable = false

    var isVote = -1
    var payment_type = ""
    var language = ""
    var status = ""
    var post_created_at = ""
    var userBid:BidModel?= null
    var postImages = ArrayList<PostImages>()
    var selectedCountry = ArrayList<CountryModel>()
    var selectedTags = ArrayList<TagModel>()
    var bidsList = ArrayList<BidModel>()
    var feedbackList = ArrayList<PostFeedbackModel>()
    var likeUserList = ArrayList<UserModel>()

    var userData = UserModel()

    // variable for search-top{
    var isPopular = false
    var postType = 1000

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        amount = parcel.readString()
        post_user_id = parcel.readString()
        latitude = parcel.readString()
        longitude = parcel.readString()
        serial = parcel.readString()
        place_name = parcel.readString()
        description = parcel.readString()
        percent = parcel.readString()
        is_instagram = parcel.readByte() != 0.toByte()
        is_twitter = parcel.readByte() != 0.toByte()
        isPromoted = parcel.readByte() != 0.toByte()
        is_auction = parcel.readByte() != 0.toByte()
        buy_now = parcel.readByte() != 0.toByte()
        whatsapp_only = parcel.readByte() != 0.toByte()
        whatsapp_and_call = parcel.readByte() != 0.toByte()
        only_user_from_selected_country = parcel.readByte() != 0.toByte()
        is_sold = parcel.readByte() != 0.toByte()
        is_story = parcel.readByte() != 0.toByte()
        isLike = parcel.readByte() != 0.toByte()
        isFav = parcel.readByte() != 0.toByte()
        is_deleted = parcel.readByte() != 0.toByte()
        isBuyAvailable = parcel.readByte() != 0.toByte()
        isPopular = parcel.readByte() != 0.toByte()
        isVote = parcel.readInt()
        postType = parcel.readInt()
        payment_type = parcel.readString()
        language = parcel.readString()
        status = parcel.readString()
        post_created_at = parcel.readString()
        userBid = parcel.readParcelable(BidModel::class.java.classLoader)
        userData = parcel.readParcelable(UserModel::class.java.classLoader)
        bidsList = parcel.createTypedArrayList(BidModel.CREATOR)
        selectedCountry = parcel.createTypedArrayList(CountryModel.CREATOR)
        selectedTags = parcel.createTypedArrayList(TagModel.CREATOR)
        postImages = parcel.createTypedArrayList(PostImages.CREATOR)
        feedbackList = parcel.createTypedArrayList(PostFeedbackModel.CREATOR)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(amount)
        parcel.writeString(post_user_id)
        parcel.writeString(latitude)
        parcel.writeString(longitude)
        parcel.writeString(serial)
        parcel.writeString(place_name)
        parcel.writeString(description)
        parcel.writeString(percent)
        parcel.writeByte(if (is_instagram) 1 else 0)
        parcel.writeByte(if (is_twitter) 1 else 0)
        parcel.writeByte(if (isPromoted) 1 else 0)
        parcel.writeByte(if (is_auction) 1 else 0)
        parcel.writeByte(if (buy_now) 1 else 0)
        parcel.writeByte(if (whatsapp_only) 1 else 0)
        parcel.writeByte(if (whatsapp_and_call) 1 else 0)
        parcel.writeByte(if (only_user_from_selected_country) 1 else 0)
        parcel.writeByte(if (is_sold) 1 else 0)
        parcel.writeByte(if (is_story) 1 else 0)
        parcel.writeByte(if (isLike) 1 else 0)
        parcel.writeByte(if (isFav) 1 else 0)
        parcel.writeByte(if (is_deleted) 1 else 0)
        parcel.writeByte(if (isBuyAvailable) 1 else 0)
        parcel.writeByte(if (isPopular) 1 else 0)
        parcel.writeInt(isVote)
        parcel.writeInt(postType)
        parcel.writeString(payment_type)
        parcel.writeString(language)
        parcel.writeString(status)
        parcel.writeString(post_created_at)
        parcel.writeParcelable(userBid, flags)
        parcel.writeParcelable(userData, flags)
        parcel.writeTypedList(bidsList)
        parcel.writeTypedList(selectedCountry)
        parcel.writeTypedList(selectedTags)
        parcel.writeTypedList(postImages)
        parcel.writeTypedList(feedbackList)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PostModel> {
        override fun createFromParcel(parcel: Parcel): PostModel {
            return PostModel(parcel)
        }

        override fun newArray(size: Int): Array<PostModel?> {
            return arrayOfNulls(size)
        }
    }


}