package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by bodacious on 3/1/19.

 */

class PostFeedbackModel() :Parcelable {

    var id = ""
    var user_id = ""
    var user_name = ""
    var user_profile = ""
    var comment = ""
    var order_id = ""
    var rating = ""
    var created_at = 0.toLong()

    var country:CountryModel?= null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        user_id = parcel.readString()
        user_name = parcel.readString()
        user_profile = parcel.readString()
        comment = parcel.readString()
        order_id = parcel.readString()
        rating = parcel.readString()
        created_at = parcel.readLong()
        country = parcel.readParcelable(CountryModel::class.java.classLoader)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(user_id)
        parcel.writeString(user_name)
        parcel.writeString(user_profile)
        parcel.writeString(comment)
        parcel.writeString(order_id)
        parcel.writeString(rating)
        parcel.writeLong(created_at)
        parcel.writeParcelable(country, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PostFeedbackModel> {
        override fun createFromParcel(parcel: Parcel): PostFeedbackModel {
            return PostFeedbackModel(parcel)
        }

        override fun newArray(size: Int): Array<PostFeedbackModel?> {
            return arrayOfNulls(size)
        }
    }


}