package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable


class VoteModel() :Parcelable{

    var id =""
    var postId =""
    var vote = -1
    var percent = "0"

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        postId = parcel.readString()
        vote = parcel.readInt()
        percent = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(postId)
        parcel.writeInt(vote)
        parcel.writeString(percent)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<VoteModel> {
        override fun createFromParcel(parcel: Parcel): VoteModel {
            return VoteModel(parcel)
        }

        override fun newArray(size: Int): Array<VoteModel?> {
            return arrayOfNulls(size)
        }
    }


}