package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by bodacious on 3/12/18.
 */
class PostImages() :Parcelable{

    var id = ""
    var url = ""
    var isVideo = false

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        url = parcel.readString()
        isVideo = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(url)
        parcel.writeByte(if (isVideo) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PostImages> {
        override fun createFromParcel(parcel: Parcel): PostImages {
            return PostImages(parcel)
        }

        override fun newArray(size: Int): Array<PostImages?> {
            return arrayOfNulls(size)
        }
    }


}