package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

class StoryModel() : Parcelable {

    var id = ""
    var url = ""
    var path = ""
    var user_id = ""
    var streamName = ""
    var status = ""
    var viewerCounts = 0
    var downloadId = -1
    var feedback = "Pending"
    var serial = "NA"
    var is_sold = false
    var is_buyNow = false
    var is_video = false
    var buyNowPrice = ""
    var createdAt = ""
    var whatsapp_only = false
    var whatsapp_and_call = false
    var userBid:BidModel?= null
    var userData: UserModel? = null
    var feedbackList = ArrayList<PostFeedbackModel>()
    var bidList = ArrayList<BidModel>()
    var commentsList = ArrayList<CommentModel>()
    var imagesList = ArrayList<String>()
    var product_description = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        url = parcel.readString()
        product_description = parcel.readString()
        path = parcel.readString()
        serial = parcel.readString()
        user_id = parcel.readString()
        streamName = parcel.readString()
        status = parcel.readString()
        viewerCounts = parcel.readInt()
        downloadId = parcel.readInt()
        feedback = parcel.readString()
        is_sold = parcel.readByte() != 0.toByte()
        is_buyNow = parcel.readByte() != 0.toByte()
        is_video = parcel.readByte() != 0.toByte()
        whatsapp_only = parcel.readByte() != 0.toByte()
        whatsapp_and_call = parcel.readByte() != 0.toByte()
        buyNowPrice = parcel.readString()
        createdAt = parcel.readString()
        userBid = parcel.readParcelable(BidModel::class.java.classLoader)
        userData = parcel.readParcelable(UserModel::class.java.classLoader)
        bidList = parcel.createTypedArrayList(BidModel.CREATOR)
        feedbackList = parcel.createTypedArrayList(PostFeedbackModel.CREATOR)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(url)
        parcel.writeString(product_description)
        parcel.writeString(path)
        parcel.writeString(serial)
        parcel.writeString(user_id)
        parcel.writeString(streamName)
        parcel.writeString(status)
        parcel.writeInt(viewerCounts)
        parcel.writeInt(downloadId)
        parcel.writeString(feedback)
        parcel.writeByte(if (is_sold) 1 else 0)
        parcel.writeByte(if (is_buyNow) 1 else 0)
        parcel.writeByte(if (is_video) 1 else 0)
        parcel.writeByte(if (whatsapp_only) 1 else 0)
        parcel.writeByte(if (whatsapp_and_call) 1 else 0)
        parcel.writeString(buyNowPrice)
        parcel.writeString(createdAt)
        parcel.writeParcelable(userBid, flags)
        parcel.writeParcelable(userData, flags)
        parcel.writeTypedList(bidList)
        parcel.writeTypedList(feedbackList)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<StoryModel> {
        override fun createFromParcel(parcel: Parcel): StoryModel {
            return StoryModel(parcel)
        }

        override fun newArray(size: Int): Array<StoryModel?> {
            return arrayOfNulls(size)
        }
    }


}