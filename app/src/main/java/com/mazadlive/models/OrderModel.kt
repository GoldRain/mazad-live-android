package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable
import java.util.*

class   OrderModel():Parcelable{

    var orderId: String? = null
    var orderFrom: String? = null
    var orderStatus: String? = null

    var addressModel:AddressModel?= null

    var paymenMethod = ""
    var displayId = "NA"
    var feedback = "pending"
    var paymentStatus = ""
    var user_id = ""
    var txnId = "NA"
    var post:PostModel?= null
    var story:StoryModel?= null
    var userBuyer:UserModel?= null
    var userSeller:UserModel?= null
    var buyerCountry:CountryModel?= null
    var orderType = ""
    var typeId = ""
    var amount = ""
    var created_at =0.toLong()

    constructor(parcel: Parcel) : this() {
        orderId = parcel.readString()
        orderFrom = parcel.readString()
        orderStatus = parcel.readString()
        addressModel = parcel.readParcelable(AddressModel::class.java.classLoader)
        paymenMethod = parcel.readString()
        displayId = parcel.readString()
        feedback = parcel.readString()
        paymentStatus = parcel.readString()
        user_id = parcel.readString()
        txnId = parcel.readString()
        post = parcel.readParcelable(PostModel::class.java.classLoader)
        story = parcel.readParcelable(StoryModel::class.java.classLoader)
        userBuyer = parcel.readParcelable(UserModel::class.java.classLoader)
        userSeller = parcel.readParcelable(UserModel::class.java.classLoader)
        buyerCountry = parcel.readParcelable(CountryModel::class.java.classLoader)
        orderType = parcel.readString()
        typeId = parcel.readString()
        amount = parcel.readString()
        created_at = parcel.readLong()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(orderId)
        parcel.writeString(orderFrom)
        parcel.writeString(orderStatus)
        parcel.writeParcelable(addressModel, flags)
        parcel.writeString(paymenMethod)
        parcel.writeString(displayId)
        parcel.writeString(feedback)
        parcel.writeString(paymentStatus)
        parcel.writeString(user_id)
        parcel.writeString(txnId)
        parcel.writeParcelable(post, flags)
        parcel.writeParcelable(story, flags)
        parcel.writeParcelable(userBuyer, flags)
        parcel.writeParcelable(userSeller, flags)
        parcel.writeParcelable(buyerCountry, flags)
        parcel.writeString(orderType)
        parcel.writeString(typeId)
        parcel.writeString(amount)
        parcel.writeLong(created_at)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OrderModel> {
        override fun createFromParcel(parcel: Parcel): OrderModel {
            return OrderModel(parcel)
        }

        override fun newArray(size: Int): Array<OrderModel?> {
            return arrayOfNulls(size)
        }
    }


}