package com.mazadlive.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.graphics.Bitmap

class ChatMessage {

    @ColumnInfo(name = "msg_id")
    @PrimaryKey(autoGenerate = false)
    var msgId: String = ""
    @ColumnInfo(name = "friend_id")
    var friendId: String? = null
    @ColumnInfo(name = "content_type")
    var contentType: String = "text"
    @ColumnInfo(name = "msg_text")
    var msgText: String? = null
    @ColumnInfo(name = "extra_url")
    var url: String? = null
    @ColumnInfo(name = "msg_time")
    var time: Long = 0L
    @ColumnInfo(name = "msg_type")
    var msgType: Int = 1
    @ColumnInfo(name = "read")
    var read: Int = 0
    @ColumnInfo(name = "local_url")
    var localUrl: String? = null

    @Ignore
    var formattedTime: String? = null

    @Ignore
    var bitmap: Bitmap? = null

    @Ignore
    var downloading: Boolean = false
}