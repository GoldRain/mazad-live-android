package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by bodacious on 26/10/18.
 */
class UserModel() :Parcelable{

    var name : String? = null
    var follow : Boolean = false
    var block : Boolean = false
    var verified : Boolean = false

    var username = ""
    var countryCode = ""
    var countryName = ""
    var countryId = ""
    var phone = ""
    var email = ""
    var type = ""
    var profile = ""
    var id = ""
    var description = ""

    var follower = ArrayList<UserModel>()
    var following = ArrayList<UserModel>()

    constructor(parcel: Parcel) : this() {
        name = parcel.readString()
        follow = parcel.readByte() != 0.toByte()
        block = parcel.readByte() != 0.toByte()
        verified = parcel.readByte() != 0.toByte()
        username = parcel.readString()
        countryCode = parcel.readString()
        countryId = parcel.readString()
        countryName = parcel.readString()
        description = parcel.readString()
        phone = parcel.readString()
        email = parcel.readString()
        type = parcel.readString()
        profile = parcel.readString()
        id = parcel.readString()
        follower = parcel.createTypedArrayList(UserModel.CREATOR)
        following = parcel.createTypedArrayList(UserModel.CREATOR)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeByte(if (follow) 1 else 0)
        parcel.writeByte(if (block) 1 else 0)
        parcel.writeByte(if (verified) 1 else 0)
        parcel.writeString(username)
        parcel.writeString(countryId)
        parcel.writeString(countryCode)
        parcel.writeString(countryName)
        parcel.writeString(description)
        parcel.writeString(phone)
        parcel.writeString(email)
        parcel.writeString(type)
        parcel.writeString(profile)
        parcel.writeString(id)
        parcel.writeTypedList(follower)
        parcel.writeTypedList(following)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserModel> {
        override fun createFromParcel(parcel: Parcel): UserModel {
            return UserModel(parcel)
        }

        override fun newArray(size: Int): Array<UserModel?> {
            return arrayOfNulls(size)
        }
    }


}
