package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

class CategoryModel() : Parcelable {

    var id : String? = null
    var isTag : Boolean? = null
    var name : String? = null
    var number : String? = null
    var URL : String? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        isTag = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        name = parcel.readString()
        number = parcel.readString()
        URL = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeValue(isTag)
        parcel.writeString(name)
        parcel.writeString(number)
        parcel.writeString(URL)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CategoryModel> {
        override fun createFromParcel(parcel: Parcel): CategoryModel {
            return CategoryModel(parcel)
        }

        override fun newArray(size: Int): Array<CategoryModel?> {
            return arrayOfNulls(size)
        }
    }

}