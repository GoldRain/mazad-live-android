package com.mazadlive.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class PaymentCard() : Parcelable {


    var id: String = ""
    var card:String? = null
    var createdAt: Long = 0L
    var updatedAt: Long = 0L
    var primaryCard = 0

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        card = parcel.readString()
        createdAt = parcel.readLong()
        updatedAt = parcel.readLong()
        primaryCard = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(card)
        parcel.writeLong(createdAt)
        parcel.writeLong(updatedAt)
        parcel.writeInt(primaryCard)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PaymentCard> {
        override fun createFromParcel(parcel: Parcel): PaymentCard {
            return PaymentCard(parcel)
        }

        override fun newArray(size: Int): Array<PaymentCard?> {
            return arrayOfNulls(size)
        }
    }
}
