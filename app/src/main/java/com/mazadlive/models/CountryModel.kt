package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by bodacious on 4/10/18.
 */
class CountryModel() : Parcelable {

    var id = ""
    var name = ""
    var code = ""
    var flag = ""
    var isCheck =false

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        name = parcel.readString()
        code = parcel.readString()
        flag = parcel.readString()
        isCheck = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(code)
        parcel.writeString(flag)
        parcel.writeByte(if (isCheck) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CountryModel> {
        override fun createFromParcel(parcel: Parcel): CountryModel {
            return CountryModel(parcel)
        }

        override fun newArray(size: Int): Array<CountryModel?> {
            return arrayOfNulls(size)
        }
    }


}