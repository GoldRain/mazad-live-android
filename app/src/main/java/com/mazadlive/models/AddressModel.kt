package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

class AddressModel() :Parcelable{

    var address = ""
    var id = ""
    var phone = ""
    var username = ""
    var addressLine1 = ""
    var addressLine2 = ""
    var city = ""
    var state = ""
    var pincode = ""
    var countryId = ""
    var isActive = false
    var countryModel :CountryModel?= null

    var createdAt = ""

    constructor(parcel: Parcel) : this() {
        address = parcel.readString()
        id = parcel.readString()
        phone = parcel.readString()
        username = parcel.readString()
        addressLine1 = parcel.readString()
        addressLine2 = parcel.readString()
        city = parcel.readString()
        state = parcel.readString()
        pincode = parcel.readString()
        countryId = parcel.readString()
        isActive = parcel.readByte() != 0.toByte()
        countryModel = parcel.readParcelable(CountryModel::class.java.classLoader)
        createdAt = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(address)
        parcel.writeString(id)
        parcel.writeString(phone)
        parcel.writeString(username)
        parcel.writeString(addressLine1)
        parcel.writeString(addressLine2)
        parcel.writeString(city)
        parcel.writeString(state)
        parcel.writeString(pincode)
        parcel.writeString(countryId)
        parcel.writeByte(if (isActive) 1 else 0)
        parcel.writeParcelable(countryModel, flags)
        parcel.writeString(createdAt)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AddressModel> {
        override fun createFromParcel(parcel: Parcel): AddressModel {
            return AddressModel(parcel)
        }

        override fun newArray(size: Int): Array<AddressModel?> {
            return arrayOfNulls(size)
        }
    }


}