package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

class CurrencyModel() : Parcelable{

    var symbol : String? = null
    var name : String? = null
    var symbol_native : String? = null
    var decimal_digits : String? = null
    var rounding : String? = null
    var code : String? = null
    var name_plural : String? = null

    constructor(parcel: Parcel) : this() {
        symbol = parcel.readString()
        name = parcel.readString()
        symbol_native = parcel.readString()
        decimal_digits = parcel.readString()
        rounding = parcel.readString()
        code = parcel.readString()
        name_plural = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(symbol)
        parcel.writeString(name)
        parcel.writeString(symbol_native)
        parcel.writeString(decimal_digits)
        parcel.writeString(rounding)
        parcel.writeString(code)
        parcel.writeString(name_plural)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CurrencyModel> {
        override fun createFromParcel(parcel: Parcel): CurrencyModel {
            return CurrencyModel(parcel)
        }

        override fun newArray(size: Int): Array<CurrencyModel?> {
            return arrayOfNulls(size)
        }
    }
}