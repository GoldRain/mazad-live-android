package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

class AdsModel() :Parcelable{

    var id = ""
    var purchaseType = ""
    var purchaseToken = ""
    var amount : String? = null
    var type : String? = null
    var order : String? = null
    var details : String? = null
    var imageUrl: Int? = 0
    var available: String? = null
    var subModel:SubscribeModel?= null


    var activation= ""
    var expiry = ""
    var previousCount = 0
    var status = false

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        purchaseType = parcel.readString()
        purchaseToken = parcel.readString()
        amount = parcel.readString()
        type = parcel.readString()
        order = parcel.readString()
        details = parcel.readString()
        imageUrl = parcel.readValue(Int::class.java.classLoader) as? Int
        available = parcel.readString()
        subModel = parcel.readParcelable(SubscribeModel::class.java.classLoader)
        activation = parcel.readString()
        expiry = parcel.readString()
        previousCount = parcel.readInt()
        status = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(purchaseType)
        parcel.writeString(purchaseToken)
        parcel.writeString(amount)
        parcel.writeString(type)
        parcel.writeString(order)
        parcel.writeString(details)
        parcel.writeValue(imageUrl)
        parcel.writeString(available)
        parcel.writeParcelable(subModel, flags)
        parcel.writeString(activation)
        parcel.writeString(expiry)
        parcel.writeInt(previousCount)
        parcel.writeByte(if (status) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AdsModel> {
        override fun createFromParcel(parcel: Parcel): AdsModel {
            return AdsModel(parcel)
        }

        override fun newArray(size: Int): Array<AdsModel?> {
            return arrayOfNulls(size)
        }
    }


}