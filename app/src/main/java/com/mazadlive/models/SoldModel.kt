package com.mazadlive.models

import android.os.Parcel
import android.os.Parcelable

class SoldModel() : Parcelable {

    var type = ""
    var type_id = ""

    constructor(parcel: Parcel) : this() {
        type = parcel.readString()
        type_id = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(type)
        parcel.writeString(type_id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SoldModel> {
        override fun createFromParcel(parcel: Parcel): SoldModel {
            return SoldModel(parcel)
        }

        override fun newArray(size: Int): Array<SoldModel?> {
            return arrayOfNulls(size)
        }
    }
}