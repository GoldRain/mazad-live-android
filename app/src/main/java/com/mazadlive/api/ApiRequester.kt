package com.mazadlive.api

import android.util.Log
import com.mazadlive.utils.MyApplication

abstract class ApiRequester() {

    var isNetworkAvailable = false

    init {
        isNetworkAvailable = MyApplication.instance.isNetworkAvailable()
        Log.e("ApiRequester", "request initialisation with network : $isNetworkAvailable")
    }

}