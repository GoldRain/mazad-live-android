package com.mazadlive.api

object ApiUrl {
//http://localhost:8082/register
    //"http://132.148.241.135:8085"          //http://192.168.0.18:8081

     val server: String
        get() = "http://18.218.27.17:8082" // live url

//    val server: String
//       get() = "http://192.168.0.120:8082"

    val loginUrl: String
        get() = "$server/login"

    val signup: String
        get() = "$server/register"


    val verifyCode: String
        get() = "$server/verifyCode"

    val forgotPass: String
        get() = "$server/forgotPassword"

    val changePassword: String
        get() = "$server/changePassword"

    //**********
    val getTags: String
        get() = "$server/tags"

    val getCountry: String
        get() = "$server/countries"

    val changeUserType: String
        get() = "$server/userType"

    val createPost: String
        get() = "$server/userPost"

    val getPost: String
        get() = "$server/getPost"

    val getMyPosts: String
        get() = "$server/getMyPosts"

    val getMyFavPost: String
        get() = "$server/getMyFavPost"

    val addBid: String
        get() = "$server/raiseMyBid"

    val likePost: String
        get() = "$server/postLike"

    val deletePost: String
        get() = "$server/deletePost"

    val follow: String
        get() = "$server/follow"

    val block: String
        get() = "$server/block"//block

    val reportUser: String
        get() = "$server/reportUser"//block

    val blockList: String
        get() = "$server/blockList"//block

    val follower: String
        get() = "$server/followerList"//block

    val following: String
        get() = "$server/followingList"


    //streamDetails stopMyStream

    val userDetails: String
        get() = "$server/userData"//updateProfile

    val updateProfile: String
        get() = "$server/updateProfile"

    val streamDetails: String
        get() = "$server/streamDetails" //vote

    val updateStreamThumb: String
        get() = "$server/updateThumb" //vote

    val bookMarkComment: String
        get() = "$server/bookMark"

    val stopMyStream: String
        get() = "$server/stopMyStream"

    val createStream: String
        get() = "$server/createStream"

    val reportPost: String
        get() = "$server/reportPost"

    val favPost: String
        get() = "$server/favPost"

    val vote: String
        get() = "$server/vote"//updateStorySettings

    val updateStorySettings: String
        get() = "$server/updateStorySettings"//

    val activityStatus: String
        get() = "$server/activityStatus"//

    val iban: String
        get() = "$server/iban"//


    val getMyStories: String
        get() = "$server/getMyStories"//

    val storyPost: String
        get() = "$server/storyPost"//storyPost

    val bidOnStory: String
        get() = "$server/bidOnStory"//feedback

    val feedback: String
        get() = "$server/feedback"

    val activites: String
        get() = "$server/activities"

    val highestBid: String
        get() = "$server/highestBid" //

    val notification: String
        get() = "$server/notification"

    val verifyPhone: String
        get() = "$server/verifyPhone"


    val searchUser: String
        get() = "$server/searchUser"

    val searchCategories: String
        get() = "$server/searchCategories"

    val searchByPlace: String
        get() = "$server/searchByPlace"

    val notificationAndEmail: String
        get() = "$server/notificationAndEmail"

    val getPaymetLoader : String
        get() = "$server/public/"

    val getTremAndConditionAr : String
        get() = "$server/public/mazadTermsArabic.html"

    val getTremAndConditionEn : String
        get() = "$server/public/mazadTerms.html"

    val getPrivacyAr : String
        get() = "$server/public/mazadPrivacyArabic.html"

    val getPrivacyEn : String
        get() = "$server/public/mazadPrivacy.html"


    //********************* chat

    val createRoom: String
        get() = "$server/createRoom"

    val getRooms: String
        get() = "$server/getRooms"

    val sendMessage: String
        get() = "$server/sendMessage"

    //******** address
    val addAddress = "$server/addAddress"

    val orderOtp = "$server/orderOtp"
    val checkLastLogin = "$server/checkLastLogin"
    val verifyOrderOtp = "$server/verifyOrderOtp"
    val client_token = "$server/client_token"
    val checkout = "$server/checkout"
    val orderList = "$server/orderList"
    val sellerOrder = "$server/sellerOrder"
    val acceptOrderRequestBySeller = "$server/acceptOrderRequestBySeller"
    val changeBidStatus = "$server/changeBidStatus"
    val getBidForBuyer = "$server/postBidDetails"
    val pendingFeedbackOrder = "$server/pendingFeedbackOrder"
    val addFeedbackOnPost = "$server/addFeedbackOnPost"
    val addFeedbackOnStory = "$server/addFeedbackOnStory"
    val feedbackReceived = "$server/feedbackReceived"
    val walletList = "$server/walletList"
    val requestPaymentToBuyer = "$server/requestPaymentToAdmin"
    val deliveryConfirmed = "$server/deliveryConfirmed"
    val awardBid = "$server/awardBid"
    val autoConfirmed = "$server/autoConfirmed"
    val updateProduct = "$server/updateProduct"
    val getCurrentProduct = "$server/getCurrentProduct"
    val deleteStory = "$server/deleteStory"
    val showCounts = "$server/showCounts"
    val updateAllCount = "$server/updateAllCount"
    val noMoreAds = "$server/noMoreAds"
    val userVerification = "$server/userVerification"
    val updatePromote = "$server/updatePromote"
    val searchPopularPost = "$server/popularPost"
    val categoryAndCount = "$server/categoryAndCount"
    val placeAndCount = "$server/placeAndCount"
    val respost = "$server/repost"
    val reportStory = "$server/reportStory"
    val getOrderDetails = "$server/getOrderDetails"
    val getBasicInfo = "$server/basicInfo"
    val updatePurchaseData = "$server/test"
    val getPurchaseData = "$server/getPurchaseData"


    //********************************** ORDER
    val createOrder: String
        get() = "$server/createOrder"

    //Tap
    val tapCustomerId = "$server/tapCustomerId"
    val tapVerificationPayment = "$server/verificationPayment"

}