package com.mazadlive.api

import android.util.Log
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.mazadlive.R
import com.mazadlive.database.AppDatabase
import com.mazadlive.helper.Constant
import com.mazadlive.models.CountryModel
import com.mazadlive.models.PostModel
import com.mazadlive.models.StoryModel
import com.mazadlive.models.TapPaymentModel
import com.mazadlive.parser.PostDataParser
import com.mazadlive.parser.RoomDataParser
import com.mazadlive.parser.StoryDataParse
import com.mazadlive.utils.MyApplication
import company.tap.gosellapi.api.facade.APIRequestCallback
import company.tap.gosellapi.api.facade.GoSellAPI
import company.tap.gosellapi.api.facade.GoSellError
import company.tap.gosellapi.api.model.Charge
import company.tap.gosellapi.api.model.Redirect
import company.tap.gosellapi.api.requests.CreateChargeRequest
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.util.*

class ServiceRequest(private val apiResponseListener: ApiResponseListener?) : ApiRequester() {

    private var TAG = ServiceRequest::class.java.name


    fun getCredentials() {
        val param = JSONObject()
        param.put("version","1.1")

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.getBasicInfo)
        request.addHeaders("API_KEY", Constant.API_KEY)

       request.addJSONObjectBody(param)

        Log.e("GET_CRED_URl","url:: ${ApiUrl.getBasicInfo}")
        request.setTag("getCredentials").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_GetCredentials", response.toString())
                        if (!response.getBoolean("error")) {

                            try {
                                if(response.has("description")){
                                    val json = response.getJSONObject("description")
                                    val userRef = MyApplication.instance.getUserPreferences()
                                    if(json.has("POOL_ID")){
                                        userRef.POOL_ID = json.getString("POOL_ID")
                                    }

                                    if(json.has("AUTH_TOKEN")){
                                        userRef.TAP_TOKEN = json.getString("AUTH_TOKEN")
                                    }

                                    if(json.has("ENCRYPTION_KEY")){
                                        userRef.TAP_API_KEY = json.getString("ENCRYPTION_KEY")
                                    }

                                }
                            }catch (e:Exception){
                                e.printStackTrace()
                            }

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorDetail}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    fun loginRequest(map: HashMap<String, String>) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.loginUrl)
        request.addHeaders("API_KEY", Constant.API_KEY)
        for (item in map) {
            request = request.addUrlEncodeFormBodyParameter(item.key, item.value)
        }

        request.setTag("login").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun checkLastLogin(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.checkLastLogin)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addJSONObjectBody(param)

        request.setTag("checkLastLogin").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_CheckLastLogin", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorDetail}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    fun signup(map: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.signup)
        request.addHeaders("API_KEY", Constant.API_KEY)


        request.addJSONObjectBody(map)

        request.setTag("signup").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE_SignUp", response.toString())
                        if (!response.getBoolean("error")) {
                            //saveUserInfo(response.getJSONObject("data").toString())
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    fun verifyOtp(map: HashMap<String, String>) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.verifyCode)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(JSONObject(map))
        request.setTag("verifyOtp").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE_Otp", response.toString())
                        if (!response.getBoolean("error")) {
                            //saveUserInfo(response.getJSONObject("data").toString())
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun verifyPhone(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.verifyPhone)
        request.addHeaders("API_KEY", Constant.API_KEY)

        /* for (item in map){
             request = request.addUrlEncodeFormBodyParameter(item.key, item.value)
         }
 */
        request.addJSONObjectBody(param)
        request.setTag("verfiyPhone").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_VerfiyPhone", response.toString())
                        if (!response.getBoolean("error")) {
                            //saveUserInfo(response.getJSONObject("data").toString())
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun forgotPassword(map: HashMap<String, String>) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.forgotPass)
        request.addHeaders("API_KEY", Constant.API_KEY)

        for (item in map) {
            request = request.addUrlEncodeFormBodyParameter(item.key, item.value)
        }

        request.setTag("forgotPassword").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE_FOR_PASS", response.toString())
                        if (!response.getBoolean("error")) {
                            //saveUserInfo(response.getJSONObject("data").toString())
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun changePassword(map: HashMap<String, String>) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.changePassword)
        request.addHeaders("API_KEY", Constant.API_KEY)

        for (item in map) {
            request = request.addUrlEncodeFormBodyParameter(item.key, item.value)
        }

        request.setTag("changePassword").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_Change_Password", response.toString())
                        if (!response.getBoolean("error")) {
                            //saveUserInfo(response.getJSONObject("data").toString())
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getTags(map: HashMap<String, String>) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.getTags)
        request.addHeaders("API_KEY", Constant.API_KEY)

        for (item in map) {
            request = request.addUrlEncodeFormBodyParameter(item.key, item.value)
        }

        request.setTag("getTags").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_Get_Tags", response.toString())
                        if (!response.getBoolean("error")) {
                            //saveUserInfo(response.getJSONObject("data").toString())
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getAllCountry(list: ArrayList<CountryModel>) {
        val map = HashMap<String, String>()
        map.put("type", "read")
        map.put("version", "1.1")

        var request = AndroidNetworking.post(ApiUrl.getCountry)
        request.addHeaders("API_KEY", Constant.API_KEY)

        for (item in map) {
            request = request.addUrlEncodeFormBodyParameter(item.key, item.value)
        }

        request.setTag("getCountry").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_GET_COUNTRY", response.toString())
                        if (!response.getBoolean("error")) {
                            if (response.has("description")) {
                                val array = response.getJSONArray("description")
                                list.clear()
                                for (i in 0 until array.length()) {
                                    val countryModel = CountryModel()
                                    val obj = array.getJSONObject(i)
                                    if (obj.has("_id")) {
                                        countryModel.id = obj.getString("_id")
                                    }

                                    if (obj.has("countryName")) {
                                        countryModel.name = obj.getString("countryName")
                                    }

                                    if (obj.has("countryCode")) {
                                        countryModel.code = obj.getString("countryCode")
                                    }

                                    if (obj.has("flagImageUrl")) {
                                        countryModel.flag = "${ApiUrl.server}${obj.getString("flagImageUrl")}"
                                    }

                                    Log.e("COUNTYR_IMAGE", "${countryModel.name} ${countryModel.flag}")
                                    if (obj.has("createdAt")) {
                                        //  countryModel.cr = obj.getString("createdAt")
                                    }
                                    list.add(countryModel)
                                }
                            }

                        } else {

                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")

                    }
                })
    }

    fun changeUserType(map: HashMap<String, String>) {

        var request = AndroidNetworking.post(ApiUrl.changeUserType)
        request.addHeaders("API_KEY", Constant.API_KEY)

        for (item in map) {
            request = request.addUrlEncodeFormBodyParameter(item.key, item.value)
        }

        request.setTag("changeUserType").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_changeUserType", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener!!.onCompleted(response.toString())
                        } else {
                            apiResponseListener!!.onCompleted(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun followPeople(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.follow)
        request.addHeaders("API_KEY", Constant.API_KEY)
        /* for (item in map){
             request = request.addUrlEncodeFormBodyParameter(item.key, item.value)
         }*/

        request.addJSONObjectBody(jsonObject)

        request.setTag("follow").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_Follow", response.toString())
                        if (!response.getBoolean("error")) {
                            //saveUserInfo(response.getJSONObject("data").toString())
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getFollowers(jsonObject: JSONObject, url: String) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(url)
        request.addHeaders("API_KEY", Constant.API_KEY)
        /* for (item in map){
             request = request.addUrlEncodeFormBodyParameter(item.key, item.value)
         }*/

        request.addJSONObjectBody(jsonObject)

        request.setTag("follow").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_Follower", response.toString())
                        if (!response.getBoolean("error")) {
                            //saveUserInfo(response.getJSONObject("data").toString())
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun userDetails(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.userDetails)
        request.addHeaders("API_KEY", Constant.API_KEY)
        /* for (item in map){
             request = request.addUrlEncodeFormBodyParameter(item.key, item.value)
         }*/

        request.addJSONObjectBody(jsonObject)

        request.setTag("user_details").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_UserDetails", response.toString())
                        if (!response.getBoolean("error")) {
                            //saveUserInfo(response.getJSONObject("data").toString())
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun updateUserDetails(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.updateProfile)
        request.addHeaders("API_KEY", Constant.API_KEY)
        /* for (item in map){
             request = request.addUrlEncodeFormBodyParameter(item.key, item.value)
         }*/

        request.addJSONObjectBody(jsonObject)

        request.setTag("user_details").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_UpdateDetails", response.toString())
                        if (!response.getBoolean("error")) {
                            //saveUserInfo(response.getJSONObject("data").toString())
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun blockUser(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.block)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("block").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_Block", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun reportUser(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.reportUser)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("reportUser").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_ReportUser", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun reportPost(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.reportPost)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("reportPost").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_ReportPost", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun reportStory(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.reportStory)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("reportStory").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_ReportStory", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getOrderDetails(orderId: String) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }

        val param = JSONObject()
        param.put("order_id",orderId)
        param.put("version", "1.1")
        param.put("self_id", MyApplication.instance.getUserPreferences().id)
        val request = AndroidNetworking.post(ApiUrl.getOrderDetails)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(param)

        request.setTag("reportStory").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_ReportStory", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    fun blockUserList(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.blockList)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("blockList").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_BlockList", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "* ${anError.errorDetail}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    /* **************************************************************** SETTINGS **********************************************************************************/
    fun updateStorySettings(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.updateStorySettings)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addJSONObjectBody(jsonObject)

        request.setTag("updateStorySettings").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_updateSettings", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorDetail}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun updateActivityStatus(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.activityStatus)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addJSONObjectBody(jsonObject)

        request.setTag("updateActivityStatus").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_updateSettings", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorDetail}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun updateIbanSettings(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.iban)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addJSONObjectBody(jsonObject)

        request.setTag("updateIbanSettings").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_IbanSettings", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorDetail}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    /* *******************   STREAM  *********************************************************************************************************************************/
    fun createStream(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.createStream)
        request.addHeaders("API_KEY", Constant.API_KEY)
        /* for (item in map){
             request = request.addUrlEncodeFormBodyParameter(item.key, item.value)
         }*/
        Log.e("API_CreateStream", "CALL BEF")
        // Log.e("API_StreamDetails", "BEFORE_CALL")
        request.addJSONObjectBody(jsonObject)

        request.setTag("createStream").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_CreateStream", response.toString())
                        if (!response.getBoolean("error")) {
                            //saveUserInfo(response.getJSONObject("data").toString())
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorDetail}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun stopMyStrem() {

        val jsonObject = JSONObject()
        jsonObject.put("user_id", MyApplication.instance.getUserPreferences().id)
        jsonObject.put("version", "1.1")

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.stopMyStream)
        request.addHeaders("API_KEY", Constant.API_KEY)
        /* for (item in map){
             request = request.addUrlEncodeFormBodyParameter(item.key, item.value)
         }*/

        request.addJSONObjectBody(jsonObject)

        request.setTag("stopMyStrem").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_StopMyStream", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun streamDetails(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.streamDetails)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("streamDetails").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_StreamDetails", response.toString())
                        if (!response.getBoolean("error")) {
                            //saveUserInfo(response.getJSONObject("data").toString())
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorDetail}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }//stream_id = req.body.stream_id, comment_id = req.body.comment_id, is_bookMarked = req.body.is_bookMarked

    fun updateStreamThumb(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.updateStreamThumb)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("updateThumb").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_StreamThumb", response.toString())
                        if (!response.getBoolean("error")) {
                            //saveUserInfo(response.getJSONObject("data").toString())
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorDetail}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun bookmarkComment(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.bookMarkComment)
        request.addHeaders("API_KEY", Constant.API_KEY)
        /* for (item in map){
             request = request.addUrlEncodeFormBodyParameter(item.key, item.value)
         }*/

        // Log.e("API_StreamDetails", "BEFORE_CALL")
        request.addJSONObjectBody(jsonObject)

        request.setTag("bookMarkComment").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_BookMarkComment", response.toString())
                        if (!response.getBoolean("error")) {
                            //saveUserInfo(response.getJSONObject("data").toString())
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorDetail}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun awardBid(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.awardBid)
        request.addHeaders("API_KEY", Constant.API_KEY)
        /* for (item in map){
             request = request.addUrlEncodeFormBodyParameter(item.key, item.value)
         }*/

        // Log.e("API_StreamDetails", "BEFORE_CALL")
        request.addJSONObjectBody(jsonObject)

        request.setTag("awardBid").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_AwardBid", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorDetail}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun updateProduct(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.updateProduct)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("updateProduct").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_UpdateProduct", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "** ${anError.errorDetail}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getCurrentProduct(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.getCurrentProduct)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("getCurrentProduct").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_GetCurrentProduct", response.toString())
                        if (!response.getBoolean("error")) {


                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "** ${anError.errorDetail}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    /* *******************   POST  ******************************/

    fun addBidOnPost(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.addBid)
        request.addHeaders("API_KEY", Constant.API_KEY)
        /* for (item in map){
             request = request.addUrlEncodeFormBodyParameter(item.key, item.value)
         }*/

        request.addJSONObjectBody(jsonObject)

        request.setTag("addBid").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_Add_Bid", response.toString())
                        if (!response.getBoolean("error")) {
                            //saveUserInfo(response.getJSONObject("data").toString())
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun likePost(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.likePost)
        request.addHeaders("API_KEY", Constant.API_KEY)
        /* for (item in map){
             request = request.addUrlEncodeFormBodyParameter(item.key, item.value)
         }*/

        request.addJSONObjectBody(jsonObject)

        request.setTag("like").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_LikePost", response.toString())
                        if (!response.getBoolean("error")) {
                            //saveUserInfo(response.getJSONObject("data").toString())
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun deletePost(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        var request = AndroidNetworking.post(ApiUrl.deletePost)
        request.addHeaders("API_KEY", Constant.API_KEY)
        /* for (item in map){
             request = request.addUrlEncodeFormBodyParameter(item.key, item.value)
         }*/

        request.addJSONObjectBody(jsonObject)

        request.setTag("deletePost").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_DeletePost", response.toString())
                        if (!response.getBoolean("error")) {
                            //saveUserInfo(response.getJSONObject("data").toString())
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun favPost(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.favPost)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("fav_post").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_FAV_POST", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun vote(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.vote)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("vote").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_VOTE_POST", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun postCreate(jsonObject: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.createPost)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("myPost").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_Post_Create", response.toString())
                        if (!response.getBoolean("error")) {
                            //saveUserInfo(response.getJSONObject("data").toString())
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getPost(postList: ArrayList<PostModel>, jsonObject: JSONObject, url: String, flag: Boolean) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(url)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("get post").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_ALl_Post", response.toString())
                        if (!response.getBoolean("error")) {
                            if (response.has("description")) {
                                val postObjArray = response.getJSONArray("description")
                                postList.clear()
                                val parser = PostDataParser()
                                for (i in 0 until postObjArray.length()) {

                                    val postObj = postObjArray.getJSONObject(i)
                                    val postModel = parser.parse(postObj)

                                    doAsync {
                                        val favPost = AppDatabase.getAppDatabase().favPostDao().getFavPost(postModel.id)
                                        if (favPost != null) {
                                            postModel.isFav = favPost.post_id.equals(postModel.id, true)
                                        }
                                    }

                                    if (url.equals(ApiUrl.getPost)) {
                                        if (!postModel!!.userData!!.id.equals(MyApplication.instance.getUserPreferences().id)) {
                                            postList.add(postModel)
                                        }
                                    } else {
                                        Log.e("tag", "${postModel.is_deleted}")
                                        if (!postModel.is_deleted)
                                            postList.add(postModel)
                                    }
                                }
                            }
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getAuctionPost(jsonObject: JSONObject, url: String, flag: Boolean) {
        jsonObject.put("user_id", MyApplication.instance.getUserPreferences().id)
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(url)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("get post").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_ALl_Post", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getFavPost(postList: ArrayList<PostModel>, jsonObject: JSONObject, url: String, flag: Boolean) {
        jsonObject.put("user_id", MyApplication.instance.getUserPreferences().id)
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(url)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("get post").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_ALl_FAV_Post", response.toString())
                        if (!response.getBoolean("error")) {
                            if (response.has("description")) {
                                val postObjArray = response.getJSONArray("description")
                                val parser = PostDataParser()
                                for (i in 0 until postObjArray.length()) {
                                    val postObj = postObjArray.getJSONObject(i)
                                    val postModel = parser.parse(JSONObject(postObj.toString()))

                                    doAsync {
                                        val favPost = AppDatabase.getAppDatabase().favPostDao().getFavPost(postModel.id)
                                        if (favPost != null) {
                                            postModel.isFav = favPost.post_id.equals(postModel.id, true)
                                        }
                                    }

                                    postList.add(postModel)
                                }

                                PostDataParser().parsePostList(postList, postObjArray, flag)
                            }
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    /**************************************************** STORY **********************/
    fun getMyStoryList(list: ArrayList<StoryModel>, jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.getMyStories)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("getMyStoryList").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_MyStoryList", response.toString())
                        if (!response.getBoolean("error")) {
                            if (response.has("description")) {
                                val storyObjArray = response.getJSONArray("description")
                                StoryDataParse().parseStoryList(list, storyObjArray)
                            }
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun deleteStory(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.deleteStory)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("deleteStory").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_DeleteStory", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun postStories(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.storyPost)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("storyPost").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_StoryPost", response.toString())
                        if (!response.getBoolean("error")) {
                            /*if(response.has("description")){
                               // val storyObjArray = response.getJSONArray("description")
                                //StoryDataParse().parseStoryList(list,storyObjArray)
                            }*/
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun bidStories(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.bidOnStory)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("bidOnStory").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_StoryBid", response.toString())
                        if (!response.getBoolean("error")) {
                            /*if(response.has("description")){
                               // val storyObjArray = response.getJSONArray("description")
                                //StoryDataParse().parseStoryList(list,storyObjArray)
                            }*/
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun notification(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.notification)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("notification").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_Notification", response.toString())
                        if (!response.getBoolean("error")) {
                            /*if(response.has("description")){
                               // val storyObjArray = response.getJSONArray("description")
                                //StoryDataParse().parseStoryList(list,storyObjArray)
                            }*/
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun notificationSetting(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.notificationAndEmail)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addJSONObjectBody(jsonObject)
        request.setTag("notificationSetting").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_Notification", response.toString())
                        if (!response.getBoolean("error")) {
                            if (response.has("description")) {
                                val description = response.getJSONObject("description")
                                if (description.has("push_notification")) {
                                    Log.e("ASDASDASDSADSAD", "From Server")
                                    Log.e("ASDASDASDSADSAD", (description.getString("push_notification") == "true").toString())
                                    MyApplication.instance.getUserPreferences().pushNotification = description.getString("push_notification") == "true"
                                }
                                if (description.has("email_and_notification")) {
                                    Log.e("ASDASDASDSADSAD", (description.getString("email_and_notification") == "true").toString())
                                    MyApplication.instance.getUserPreferences().emailAndNotification = description.getString("email_and_notification") == "true"
                                }
                                apiResponseListener?.onCompleted(response.toString())
                            }
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getHighestBid(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.highestBid)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("highestBid").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_GetHighestBid", response.toString())
                        if (!response.getBoolean("error")) {
                            /*if(response.has("description")){
                               // val storyObjArray = response.getJSONArray("description")
                                //StoryDataParse().parseStoryList(list,storyObjArray)
                            }*/
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun changeBidStatus(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.changeBidStatus)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("changeBidStatus").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_ChangeBid", response.toString())
                        if (!response.getBoolean("error")) {
                            if (response.has("description")) {
                                // val storyObjArray = response.getJSONArray("description")
                                //StoryDataParse().parseStoryList(list,storyObjArray)
                            }
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getBidsForBuyer(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.getBidForBuyer)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("getBidForBuyer").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_GetBidForBuyer", response.toString())
                        if (!response.getBoolean("error")) {
                            if (response.has("description")) {
                                // val storyObjArray = response.getJSONArray("description")
                                //StoryDataParse().parseStoryList(list,storyObjArray)
                            }
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun activities(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.activites)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("activites").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_Activites", response.toString())
                        if (!response.getBoolean("error")) {
                            /*if(response.has("description")){
                               // val storyObjArray = response.getJSONArray("description")
                                //StoryDataParse().parseStoryList(list,storyObjArray)
                            }*/
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getFeedbackForBuyer(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.pendingFeedbackOrder)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("pendingFeedbackOrder").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_FeedbackForBuyer", response.toString())
                        if (!response.getBoolean("error")) {
                            if (response.has("description")) {
                                // val storyObjArray = response.getJSONArray("description")
                                //StoryDataParse().parseStoryList(list,storyObjArray)
                            }
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getFeedbackForSeller(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.feedbackReceived)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("feedbackReceived").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_FeedbackForSeller", response.toString())
                        if (!response.getBoolean("error")) {
                            if (response.has("description")) {
                                // val storyObjArray = response.getJSONArray("description")
                                //StoryDataParse().parseStoryList(list,storyObjArray)
                            }
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun addFeedback(jsonObject: JSONObject, url: String) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(url)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("Add_Feedback").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_Add_Feedback", response.toString())
                        if (!response.getBoolean("error")) {
                            if (response.has("description")) {
                                // val storyObjArray = response.getJSONArray("description")
                                //StoryDataParse().parseStoryList(list,storyObjArray)
                            }
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    //  *********************************************************************** Search ***********************************

    fun searchUser(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.searchUser)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("searchUser").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_SearchUser", response.toString())
                        if (!response.getBoolean("error")) {
                            /*if(response.has("description")){
                               // val storyObjArray = response.getJSONArray("description")
                                //StoryDataParse().parseStoryList(list,storyObjArray)
                            }*/
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun searchCategories(jsonObject: JSONObject, URL: String) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(URL)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("searchCategories").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_Categories_DATA", response.toString())
                        if (!response.getBoolean("error")) {
                            /*if(response.has("description")){
                               // val storyObjArray = response.getJSONArray("description")
                                //StoryDataParse().parseStoryList(list,storyObjArray)
                            }*/
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun searchByPlace(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.searchByPlace)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("searchByPlace").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_SearchByPlace", response.toString())
                        if (!response.getBoolean("error")) {
                            /*if(response.has("description")){
                               // val storyObjArray = response.getJSONArray("description")
                                //StoryDataParse().parseStoryList(list,storyObjArray)
                            }*/
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    //********************************************************************** ORDER ***********************************************************

    fun verifyOrderOtp(map: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.verifyOrderOtp)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(map)
        request.setTag("verifyOtp").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE_Otp", response.toString())
                        if (!response.getBoolean("error")) {
                            //saveUserInfo(response.getJSONObject("data").toString())
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun orderOtp(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.orderOtp)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("orderOtp").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_OrderOtp", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorDetail}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun createOrder(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.createOrder)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("createOrder").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_CreateOrder", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun orderList(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.orderList)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("orderList").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_OrderList", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun sellerOrder(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.sellerOrder)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("sellerOrder").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_SellerOrder", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun acceptOrderBySeller(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.acceptOrderRequestBySeller)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("sellerOrder").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_SellerOrder", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun deliveryConfirmed(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.deliveryConfirmed)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("deliveryConfirmed").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_DeliveryConfirmed", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun autoConfirmed(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.autoConfirmed)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("autoConfirmed").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_AutoConfirmed", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    //******************************************************************* Payment ****************************************************************

    fun getChargeRequest(model: TapPaymentModel) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val chargeMetadata = HashMap<String, String>()
        chargeMetadata["Order Number"] = model.orderNumber

        Log.e("Order_Amount", model.amount + "Heree come")

        GoSellAPI.getInstance(MyApplication.instance.getUserPreferences().TAP_TOKEN).createCharge(
                CreateChargeRequest.Builder(model.amount.toDouble(), model.currency, Redirect(model.redirect, ""))
                        .statement_descriptor(model.statementDescriptor)
                        .capture(model.capture)
                        .receipt_email(model.receiptEmail)
                        .receipt_sms(model.receiptSms)
                        .first_name(model.firstName)
                        .description(model.description)
                        .source(null)
                        .metadata(chargeMetadata)
                        .build(),
                object : APIRequestCallback<Charge> {
                    override fun onSuccess(responseCode: Int, serializedResponse: Charge) {
                        Log.d("API", "onSuccess createCharge: serializedResponse:$serializedResponse")
                        apiResponseListener?.onCompleted(serializedResponse.redirect.url)
                    }

                    override fun onFailure(errorDetails: GoSellError) {
                        Log.d("API_Error", "onFailure createCharge, errorCode: " + errorDetails.errorCode + ", errorBody: " + errorDetails.errorBody + ", throwable: " + errorDetails.throwable)
                        apiResponseListener?.onError(errorDetails.errorBody)
                    }
                }
        )
    }


    //*******************************************************************Address ****************************************************************
    fun addAddress(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.addAddress)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("addAddress").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_AddAddress", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorDetail}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    // ************************************************************************** CHAT *************************************************************

    fun createRoom(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.createRoom)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("createRoom").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_CREATE_ROOM", response.toString())
                        if (!response.getBoolean("error")) {
                            RoomDataParser().parse2(response)

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {

                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getRooms(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.getRooms)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("getRooms").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_GET_ROOM", response.toString())
                        if (!response.getBoolean("error")) {
                            RoomDataParser().parse(response)

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {

                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun sendMessage(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.sendMessage)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("sendMessage").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_SendMessage", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    //**************************************** PAYMENTS ********************************
    fun getToken(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.client_token)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("client_token").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_ClientToken", response.toString())
                        apiResponseListener?.onCompleted(response.toString())
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun checkout(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.checkout)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("checkout").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_checkout", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun walletList(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.walletList)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("walletList").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_WalletList", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun requestPaymentToAdmin(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.requestPaymentToBuyer)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("requestPaymentToAdmin").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RequestPayment", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    //************************* TAP PAYMENT *************************//user_id: userId, tap_customer_id: tapCustomId
    fun updateTapCustomerId(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.tapCustomerId)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("tapCustomerId").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_UpdateTapCustomerId", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun verificationBudgePayment(tapPaymentModel: TapPaymentModel) {

        val jsonObject = JSONObject()
        jsonObject.put("chargeid", tapPaymentModel.chargeid)
        jsonObject.put("crd", tapPaymentModel.crd)
        jsonObject.put("crdtype", tapPaymentModel.crdtype)
        jsonObject.put("hash", tapPaymentModel.hash)
        jsonObject.put("payid", tapPaymentModel.payid)
        jsonObject.put("ref", tapPaymentModel.ref)
        jsonObject.put("result", tapPaymentModel.result)
        jsonObject.put("trackid", tapPaymentModel.trackid)
        jsonObject.put("version", "1.1")
        jsonObject.put("user_id", MyApplication.instance.getUserPreferences().id)

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.tapVerificationPayment)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("tapCustomerId").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e(TAG, response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e(TAG, "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    // *******************************************************************************************************************************************************************

    fun showCounts(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.showCounts)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("showCounts").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_ShowCounts", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun updateAllCount(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.updateAllCount)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("showCounts").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_ShowCounts", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun noMoreAds(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.noMoreAds)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("showCounts").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_ShowCounts", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun userVerification(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.userVerification)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("userVerification").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e(TAG, response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e(TAG, anError.errorBody)
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    fun updatePromote(jsonObject: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.updatePromote)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("updatePrompt").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_UpdatePrompt", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e("API_ERROR", "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun updateCount(type: String) {

        val jsonObject = JSONObject()
        jsonObject.put("version", "1.1")
        jsonObject.put("user_id", MyApplication.instance.getUserPreferences().id)
        jsonObject.put("type", type)
        jsonObject.put("count", "10")

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.tapVerificationPayment)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("tapCustomerId").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e(TAG, response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e(TAG, "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    //************************* Search *************************//

    fun getSearchPopularPost() {

        val jsonObject = JSONObject()
        jsonObject.put("version", "1.1")
        jsonObject.put("user_id", MyApplication.instance.getUserPreferences().id)

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.searchPopularPost)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("tapCustomerId").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e(TAG, response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response)
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e(TAG, "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getCategoryAndCount() {

        val jsonObject = JSONObject()
        jsonObject.put("version", "1.1")


        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.categoryAndCount)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("tapCustomerId").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e(TAG, response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response)
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e(TAG, "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getplaceAndCount() {

        val jsonObject = JSONObject()
        jsonObject.put("version", "1.1")


        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.placeAndCount)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("tapCustomerId").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e(TAG, response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response)
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e(TAG, "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun setRepost(jsonObject: JSONObject) {

        jsonObject.put("version", "1.1")

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.respost)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("tapCustomerId").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e(TAG, response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response)
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e(TAG, "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }



    fun updatePurchaseData(jsonObject: JSONObject) {

        jsonObject.put("version", "1.1")

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.updatePurchaseData)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("updatePurchaseData").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e(TAG, "updatePurchaseData  "+response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response)
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e(TAG, "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getPurchaseData(jsonObject: JSONObject) {

        jsonObject.put("version", "1.1")

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val request = AndroidNetworking.post(ApiUrl.getPurchaseData)
        request.addHeaders("API_KEY", Constant.API_KEY)

        request.addJSONObjectBody(jsonObject)

        request.setTag("getPurchaseData").setPriority(Priority.HIGH).build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e(TAG, "getPurchaseData  "+response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response)
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        Log.e(TAG, "${anError.errorBody}")
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }
}