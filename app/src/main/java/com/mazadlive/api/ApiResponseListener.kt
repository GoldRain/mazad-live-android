package com.mazadlive.api

interface ApiResponseListener {

    fun onCompleted(`object`: Any)
    fun onError(errorMessage: String)

}