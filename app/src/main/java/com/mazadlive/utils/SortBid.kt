package com.mazadlive.utils

import com.mazadlive.models.FeedSellerModel
import com.mazadlive.models.OrderModel

/**
 * Created by bodacious on 3/1/19.
 */
class SortBid:Comparator<FeedSellerModel> {
    override fun compare(o1: FeedSellerModel?, o2: FeedSellerModel?): Int {
        return if((o1!!.created_at).toLong() < (o2!!.created_at).toLong()) 1 else -1
    }
}