package com.mazadlive.utils

import com.mazadlive.models.OrderModel

/**
 * Created by bodacious on 3/1/19.
 */
class SortOrder:Comparator<OrderModel> {
    override fun compare(o1: OrderModel?, o2: OrderModel?): Int {

        return if(o1!!.created_at < o2!!.created_at) 1 else -1
    }
}