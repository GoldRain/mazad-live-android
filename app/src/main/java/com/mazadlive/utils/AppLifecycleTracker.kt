package com.mazadlive.utils

import android.app.Activity
import android.app.AlertDialog
import android.app.Application
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.mazadlive.Interface.DialogListener
import com.mazadlive.activities.SignInActivity
import com.mazadlive.api.ApiResponseListener
import com.mazadlive.api.ServiceRequest
import com.mazadlive.customdialogs.CustomDialog
import com.mazadlive.helper.Constant
import com.mazadlive.helper.MessageEvent
import org.greenrobot.eventbus.EventBus
import org.json.JSONException
import org.json.JSONObject
import android.view.WindowManager



/**
 * Created by bodacious on 9/1/19.
 */
class AppLifecycleTracker : Application.ActivityLifecycleCallbacks {

    var isApiCall = false
    override fun onActivityPaused(activity: Activity?) {

    }

    override fun onActivityResumed(activity: Activity?) {

    }

    override fun onActivityDestroyed(activity: Activity?) {

    }

    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {

    }

    override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {

    }

    private var numStarted = 0

    private val TAG = "AppLifecycleTracker"
    override fun onActivityStarted(activity: Activity?) {
        if (numStarted == 0) {
            if (MyApplication.instance.getUserPreferences().loginStatus) {
                checkForLastLogin()
            }
            Log.e(TAG, "app went to foreground")
        }
        numStarted++
    }

    override fun onActivityStopped(activity: Activity?) {
        numStarted--
        if (numStarted == 0) {
            Log.e(TAG, " app went to background")
            // app went to background
        }
    }


    private fun checkForLastLogin() {
        isApiCall = true
        val param = JSONObject()
        try {
            param.put("version", "1.1")
            param.put("user_id", MyApplication.instance.getUserPreferences().id)
            param.put("time", MyApplication.instance.getUserPreferences().lastLoginTime)

          //  Log.e(TAG, "PARAM ${param.toString()}")
            ServiceRequest(object : ApiResponseListener {
                override fun onCompleted(`object`: Any) {
                    isApiCall = false
                    Constant.setOnLine(true)
                }

                override fun onError(errorMessage: String) {
                    isApiCall = false

                    if (errorMessage.contains("You have been logged in")) {
                        Constant.setOnLine(false)

                        MyApplication.instance.getUserPreferences().loginStatus = false
                        val intent = Intent(MyApplication.instance, SignInActivity::class.java)
                        intent.putExtra("showDialog",true)
                        (MyApplication.instance).startActivity(intent)

                    }
                }

            }).checkLastLogin(param)

        } catch (e: JSONException) {
            isApiCall = false
            Constant.setOnLine(true)
            e.printStackTrace()
        }


    }
}