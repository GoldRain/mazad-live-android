package com.mazadlive.utils

import android.os.Environment

object AppConstants {

    fun getChatImageLocation(): String {
        return Environment.getExternalStorageDirectory().toString() + "/LaundryApp/Chat/Images"
    }

    fun getTempImageLocation(): String {
        return Environment.getExternalStorageDirectory().toString() + "/LaundryApp/TempFolder"
    }

}