package com.mazadlive.utils

import com.mazadlive.models.OrderModel
import com.mazadlive.models.PaymentModel
import com.mazadlive.models.WalletModel

/**
 * Created by bodacious on 3/1/19.
 */
class SortPayment:Comparator<PaymentModel> {
    override fun compare(o1: PaymentModel?, o2: PaymentModel?): Int {

        return if(o1!!.created_at < o2!!.created_at) 1 else -1
    }
}