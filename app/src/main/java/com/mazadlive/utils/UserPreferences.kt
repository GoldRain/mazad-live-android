package com.mazadlive.utils

import android.content.SharedPreferences
import android.util.Base64
import com.google.gson.Gson
import com.mazadlive.models.UserInfo

class UserPreferences(private val preferences: SharedPreferences, private val editor: SharedPreferences.Editor) {

    companion object{
       val  KEY_DYNEMIC_LINK = "KEY_DYNEMIC_LINK"
    }
    var loginStatus: Boolean
        get() = preferences.getBoolean("USER_LOGGED_IN", false)
        set(value) { editor.putBoolean("USER_LOGGED_IN", value).commit() }

    var isSkip: Boolean
        get() = preferences.getBoolean("isSkip", false)
        set(value) { editor.putBoolean("isSkip", value).commit() }

    var POOL_ID: String
        get() = preferences.getString("POOL_ID", "")
        set(value) { editor.putString("POOL_ID", value).commit() }

    var TAP_TOKEN: String
        get() = preferences.getString("TAP_TOKEN", "")
        set(value) { editor.putString("TAP_TOKEN", value).commit() }


    var TAP_API_KEY: String
        get() = preferences.getString("TAP_API_KEY", "")
        set(value) { editor.putString("TAP_API_KEY", value).commit() }


    var appLanguage: String
        get() = preferences.getString("APP_LANGUAGE", "en")
        set(value) { editor.putString("APP_LANGUAGE", value).commit() }

    var CountryFlag: String
        get() = preferences.getString("CURRENT_COUNTRY","us")
        set(value) { editor.putString("CURRENT_COUNTRY",value).commit()}

    var CountryName: String
        get() = preferences.getString("COUNTRY_NAME","United States")
        set(value) { editor.putString("COUNTRY_NAME",value).commit()}

    var countryCode: String
        get() = preferences.getString("COUNTRY_CODE","91")
        set(value) { editor.putString("COUNTRY_CODE",value).commit()}

    var countryId: String
        get() = preferences.getString("COUNTRY_ID","")
        set(value) { editor.putString("COUNTRY_ID",value).commit()}

    var appModeBuyer: Boolean
        get() = preferences.getBoolean("APP_MODE",true)
        set(value) { editor.putBoolean("APP_MODE",value).commit() }

    /* Story settings*/

    var activityStatus: Boolean
        get() = preferences.getBoolean("activityStatus",false)
        set(value) { editor.putBoolean("activityStatus",value).commit() }

    var isLoginInfoSave: Boolean
        get() = preferences.getBoolean("LoginInfoSave",false)
        set(value) { editor.putBoolean("LoginInfoSave",value).commit() }

    var isAuction: Boolean
        get() = preferences.getBoolean("isAuction",false)
        set(value) { editor.putBoolean("isAuction",value).commit() }

    var isBuyNow: Boolean
        get() = preferences.getBoolean("isBuyNow",false)
        set(value) { editor.putBoolean("isBuyNow",value).commit() }

    var isWhatsAppOnly: Boolean
        get() = preferences.getBoolean("isWhatsAppOnly",false)
        set(value) { editor.putBoolean("isWhatsAppOnly",value).commit() }

    var isWhatsAndCall: Boolean
        get() = preferences.getBoolean("isWhatsAndCall",false)
        set(value) { editor.putBoolean("isWhatsAndCall",value).commit() }

    var pushNotification: Boolean
        get() = preferences.getBoolean("pushNotification",true)
        set(value) { editor.putBoolean("pushNotification",value).commit()}

    var emailAndNotification: Boolean
        get() = preferences.getBoolean("emailAndNotification",false)
        set(value) { editor.putBoolean("emailAndNotification",value).commit()}

    var userInfo: UserInfo
        get() = Gson().fromJson<UserInfo>(preferences.getString("USER_INFO", ""), UserInfo::class.java)
        set(value) { editor.putString("USER_INFO", Gson().toJson(value)).commit() }

    var apiKey: String
        get() {
            val encoded = preferences.getString("key_token", "")
            return String(Base64.decode(encoded, Base64.DEFAULT), charset("UTF-16"))
        }
        set(value) {
            val byteArray = value.toByteArray(charset("UTF-16"))
            val encoded = Base64.encodeToString(byteArray, Base64.DEFAULT)
            editor.putString("key_token", encoded).commit()
        }

    fun clearAll() {
        editor.remove("USER_LOGGED_IN").remove("USER_INFO").commit()
        editor.clear().commit()
    }


    var playerId: String
        get() = preferences.getString("PLAYER_ID","")
        set(value) { editor.putString("PLAYER_ID",value).commit()}

    /* ********************** **************************************************************************************************************** */

    var id: String
        get() = preferences.getString("APP_ID","")
        set(value) { editor.putString("APP_ID",value).commit()}

    var username: String
        get() = preferences.getString("USERNAME","")
        set(value) { editor.putString("USERNAME",value).commit()}

    var tapCustomerId: String
        get() = preferences.getString("tapCustomerId","")
        set(value) { editor.putString("tapCustomerId",value).commit() }

    var lastLoginTime: String
        get() = preferences.getString("lastLoginTime","")
        set(value) { editor.putString("lastLoginTime",value).commit() }

    var phone: String
        get() = preferences.getString("PHONE","")
        set(value) { editor.putString("PHONE",value).commit()}

    var email: String
        get() = preferences.getString("EMAIL","")
        set(value) { editor.putString("EMAIL",value).commit()}

    var password: String
        get() = preferences.getString("PASSWORD","")
        set(value) { editor.putString("PASSWORD",value).commit()}

    var profile: String
        get() = preferences.getString("PROFILE","")
        set(value) { editor.putString("PROFILE",value).commit()}

    var type: String
        get() = preferences.getString("TYPE","")
        set(value) { editor.putString("TYPE",value).commit()}

    var description: String
        get() = preferences.getString("DESC","")
        set(value) { editor.putString("DESC",value).commit()}

    var verificationStatus: String
        get() = preferences.getString("verificationStatus","")
        set(value) { editor.putString("verificationStatus",value).commit() }

    var badgeDailogShow : Boolean
        get() = preferences.getBoolean("badgeDailogShow",false)
        set(value) { editor.putBoolean("badgeDailogShow",value).commit() }

    var verified: Boolean
        get() = preferences.getBoolean("verified",false)
        set(value) { editor.putBoolean("verified",value).commit() }

    var showAds: Boolean
        get() = preferences.getBoolean("showAds",false)
        set(value) { editor.putBoolean("showAds",value).commit() }

    var promotted: Boolean
        get() = preferences.getBoolean("promotted",false)
        set(value) { editor.putBoolean("promotted",value).commit() }

    var showVerifyDialog: Boolean
        get() = preferences.getBoolean("verifiedDiaog",false)
        set(value) { editor.putBoolean("verifiedDiaog",value).commit() }


    var ibanCountryCode: String
        get() = preferences.getString("ibanCountryCode","")
        set(value) { editor.putString("ibanCountryCode",value).commit()}

    var ibanBankName: String
        get() = preferences.getString("ibanBankName","")
        set(value) { editor.putString("ibanBankName",value).commit()}

    var ibanBanNumber: String
        get() = preferences.getString("ibanBanNumber","")
        set(value) { editor.putString("ibanBanNumber",value).commit()}

    var ibanPhoneNumber: String
        get() = preferences.getString("ibanPhoneNumber","")
        set(value) { editor.putString("ibanPhoneNumber",value).commit()}


    fun setString(key:String, value:String){
        editor.putString(key,value).commit()
    }

    fun getString(key:String):String{
        return preferences.getString(key,"")
    }
}