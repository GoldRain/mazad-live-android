package com.mazadlive.utils

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.StrictMode
import android.util.Log
import com.androidnetworking.AndroidNetworking
import com.crashlytics.android.Crashlytics
import com.mazadlive.Socket.SocketManager
import com.mazadlive.api.ApiUrl
import com.mazadlive.helper.Constant
import com.mazadlive.models.UserModel
import com.mazadlive.service.ExampleNotificationOpenedHandler
import com.mazadlive.service.ExampleNotificationReceivedHandler
import com.onesignal.OneSignal
import com.red5pro.streaming.R5StreamProtocol
import com.red5pro.streaming.config.R5Configuration
import io.fabric.sdk.android.Fabric
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.engineio.client.transports.WebSocket
import okhttp3.OkHttpClient
import java.net.URISyntaxException
import java.util.concurrent.TimeUnit


class MyApplication : AbstractAppPauseApplication() {

    private val TAG = "MyApplication"

    private var socketManager: SocketManager? = null
    lateinit var sharedPreferences: SharedPreferences
        private set
    lateinit var editor: SharedPreferences.Editor
        private set
    private var userPreferences: UserPreferences? = null

    var socket: Socket? = null
        private set

    var isPause = true

    lateinit var usersHasMap: HashMap<String, UserModel>

    companion object {
        lateinit var instance: MyApplication
            private set
    }

    init {
        try {
            val option = IO.Options()
            option.reconnection = true
            option.upgrade = false
            option.transports = arrayOf(WebSocket.NAME)
            socket = IO.socket(ApiUrl.server)
        } catch (e: URISyntaxException) {
            throw RuntimeException(e)
        }

    }

    override fun onCreate() {
        super.onCreate()

        registerActivityLifecycleCallbacks(AppLifecycleTracker())
        instance = this

        usersHasMap = HashMap()

        val okHttpClient = OkHttpClient().newBuilder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .retryOnConnectionFailure(false)
                .build()
        AndroidNetworking.initialize(applicationContext, okHttpClient)

        sharedPreferences = getSharedPreferences("laundry-app-prefs", Context.MODE_PRIVATE)
        editor = sharedPreferences!!.edit()

        Fabric.with(this, Crashlytics())

        OneSignal.startInit(this)
                .autoPromptLocation(false) // default call promptLocation later
                .setNotificationReceivedHandler(ExampleNotificationReceivedHandler())
                .setNotificationOpenedHandler(ExampleNotificationOpenedHandler())
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init()

        com.onesignal.OneSignal.idsAvailable { userId, registrationId ->
            getUserPreferences()!!.playerId = userId
            Log.e("OneSignal", "userId=$userId registrationId=$registrationId")
        }

        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build());
    }

    override fun onAppPause() {
        isPause = true
        Log.e(TAG, "ON_APP_PAUSE")

        if (userPreferences!!.loginStatus) {
            if (socketManager != null) {
                Constant.setOnLine(false)
                //  socketManager!!.disConnect()
                //  socketManager!!.socket!!.close()
            }
        }

    }

    override fun onAppResume() {
        Log.e(TAG, "ON_APP_RESUME")
        isPause = false


        if (userPreferences!!.loginStatus) {
            if (socketManager == null) {

                socketManager = SocketManager(applicationContext, socket)

            }
            Constant.setOnLine(true)

        } else {

            Log.e("Socket", "Not logged in")
        }

    }


    fun getSocketManager(): SocketManager {
        if (socketManager == null)
            socketManager = SocketManager(applicationContext, socket)
        return socketManager!!
    }

    fun getUserPreferences(): UserPreferences {
        if (userPreferences == null)
            userPreferences = UserPreferences(sharedPreferences, editor)
        return userPreferences as UserPreferences
    }

    fun isNetworkAvailable(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }


    fun showLogoutDialog() {
        Alerts.displayError(getApplicationContext(), "test Error");
    }


    private var config:R5Configuration?= null
    fun getRedConfig():R5Configuration{
        if(config == null){
            config = R5Configuration(R5StreamProtocol.RTSP, Constant.HOST,  8554, "live", 0.5f);


            config!!.licenseKey = Constant.LICENSE_KEY
            config!!.bundleID = packageName
        }

        return config!!
    }

}