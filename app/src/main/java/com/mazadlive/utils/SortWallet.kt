package com.mazadlive.utils

import com.mazadlive.models.OrderModel
import com.mazadlive.models.WalletModel

/**
 * Created by bodacious on 3/1/19.
 */
class SortWallet:Comparator<WalletModel> {
    override fun compare(o1: WalletModel?, o2: WalletModel?): Int {

        return if(o1!!.created_at < o2!!.created_at) 1 else -1
    }
}