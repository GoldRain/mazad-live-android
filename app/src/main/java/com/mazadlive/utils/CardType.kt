package com.mazadlive.utils

import android.support.annotation.DrawableRes
import com.mazadlive.R
import java.io.Serializable

internal object CardRegex {
    // See: http://www.regular-expressions.info/creditcard.html
    val REGX_VISA = "^4[0-9]{15}?" // VISA 16
    val REGX_MC = "^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$" // MC 16
    val REGX_AMEX = "^3[47][0-9]{13}$" // AMEX 15
    val REGX_DISCOVER = "^6(?:011|5[0-9]{2})[0-9]{12}$" // Discover 16
    val REGX_DINERS_CLUB = "^3(?:0[0-5]|[68][0-9])[0-9]{11}$" // DinersClub 14
    val REGX_JCB = "^35[0-9]{14}$" // JCB 16
    val REGX_VERVE = "^(506099|5061[0-8][0-9]|50619[0-8])[0-9]{13}$" // Interswitch Verve [Nigeria]

    val REGX_VISA_TYPE = "^4[0-9]{3}?" // VISA 16
    val REGX_MC_TYPE = "^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)$" // MC 16
    val REGX_AMEX_TYPE = "^3[47][0-9]{2}$" // AMEX 15
    val REGX_DISCOVER_TYPE = "^6(?:011|5[0-9]{2})$" // Discover 16
    val REGX_DINERS_CLUB_TYPE = "^3(?:0[0-5]|[68][0-9])[0-9]$" // DinersClub 14
    val REGX_JCB_TYPE = "^35[0-9]{2}$" // JCB 15
    val REGX_VERVE_TYPE = "^506[0,1]$" // Interswitch Verve [Nigeria]
}

/**
 * represents the Category of card the user used
 */
enum class CardType (
        /** name for humans  */
        val cardName: String,
        /** drawable for the front of the card  */
        @param:DrawableRes val frontResource: Int,
        /** regex that matches the entire card number  */
        val fullRegex: String?,
        /** regex that will match when there is enough of the card to determine Category  */
        val typeRegex: String?) : Serializable {
    VISA("VISA", R.mipmap.visa, CardRegex.REGX_VISA, CardRegex.REGX_VISA_TYPE),
    MASTERCARD("MasterCard", R.mipmap.master_card, CardRegex.REGX_MC, CardRegex.REGX_MC_TYPE),
    AMEX("American Express", R.mipmap.amex, CardRegex.REGX_AMEX, CardRegex.REGX_AMEX_TYPE),
    DISCOVER("Discover", R.mipmap.discover, CardRegex.REGX_DISCOVER, CardRegex.REGX_DISCOVER_TYPE),
    DINERS("DinersClub", R.mipmap.diners_club, CardRegex.REGX_DINERS_CLUB, CardRegex.REGX_DINERS_CLUB_TYPE),
    JCB("JCB", R.mipmap.jcb_payment_ico, CardRegex.REGX_JCB, CardRegex.REGX_JCB_TYPE),
    VERVE("Verve", R.mipmap.payment_ic_verve, CardRegex.REGX_VERVE, CardRegex.REGX_VERVE_TYPE),
    INVALID("Unknown", R.mipmap.unknown_cc, null, null);

    override fun toString(): String {
        return name
    }
}
